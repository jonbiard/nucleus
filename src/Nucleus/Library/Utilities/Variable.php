<?php
/**
 * Class Variable
 *
 * @package Nucleus\Library
 * @subpackage Utilities
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Utils;

/**
 * Class Variable
 *
 * @package Nucleus\Library
 * @subpackage Utilities
 */
class Variable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Unset a variable
     *
     * @param mixed &$var The variable reference
     */
    public function destroy(&$var)
    {
        unset($var);
    }


    /**
     * Check if a variable is set
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if the variable is set, else false
     */
    public function exists(&$var)
    {
        return isset($var);
    }


    /**
     * Get the type of a variable
     *
     * @param mixed &$var The variable reference
     *
     * @return string The type of the variable
     */
    public function getType(&$var)
    {
        return gettype($var);
    }


    /**
     * Initializes a variable
     *
     * @param mixed &$var The variable reference
     * @param mixed $default The default value for the variable being initialized
     */
    public function initialize(&$var, $default = null)
    {
        $var = isset($var) ? $var : $default;
    }


    /**
     * Check if a variable is an array
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is an array, else false
     */
    public function isArray(&$var)
    {
        return isset($var) && is_array($var);
    }


    /**
     * Check if a variable is a boolean
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is a boolean, else false
     */
    public function isBoolean(&$var)
    {
        return isset($var) && is_bool($var);
    }


    /**
     * Check if a variable is empty
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is empty, else false
     */
    public function isEmpty(&$var)
    {
        if ($var === '0') {
            return false;
        }
        return empty($var);
    }


    /**
     * Check if a variable is a float
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is a float, else false
     */
    public function isFloat(&$var)
    {
        return isset($var) && is_float($var);
    }


    /**
     * Check if a variable is an integer
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is an integer, else false
     */
    public function isInteger(&$var)
    {
        return isset($var) && is_int($var);
    }


    /**
     * Check if a variable is null
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is null, else false
     */
    public function isNull(&$var)
    {
        return isset($var) && $var === null;
    }


    /**
     * Check if a variable is numeric
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is numeric, else false
     */
    public function isNumeric(&$var)
    {
        return isset($var) && is_numeric($var);
    }


    /**
     * Check if a variable is an object
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is an object, else false
     */
    public function isObject(&$var)
    {
        return isset($var) && is_object($var);
    }


    /**
     * Check if a variable is a resource
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is a resource, else false
     */
    public function isResource(&$var)
    {
        return isset($var) && is_resource($var);
    }


    /**
     * Check if a variable is a scalar
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is a scalar, else false
     */
    public function isScalar(&$var)
    {
        return isset($var) && is_scalar($var);
    }


    /**
     * Check if a variable is a string
     *
     * @param mixed &$var The variable reference
     *
     * @return bool True if variable is a string, else false
     */
    public function isString(&$var)
    {
        return isset($var) && is_string($var);
    }
}

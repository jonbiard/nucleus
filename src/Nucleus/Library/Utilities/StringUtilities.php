<?php
/**
 * Class StringUtilities
 *
 * @package Nucleus\Library
 * @subpackage Utilities
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Utilities;

/**
 * Class StringUtilities
 *
 * @package Nucleus\Library
 * @subpackage Utilities
 */
class StringUtilities
{
    /**
     * The character set to use for multibyte functions
     * @var string
     */
    private $charset;
    /**
     * Map of characters to remove accents
     * @var string[]
     */
    private $map = [
        '£' => '',
        '©' => '(c)',
        'ª' => 'a',
        'º' => 'o',
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'AE',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ð' => 'D',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ý' => 'Y',
        'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'ae',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'd',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ü' => 'u',
        'ý' => 'y',
        'þ' => 'th',
        'ÿ' => 'y',
        'Ā' => 'A',
        'ā' => 'a',
        'Ă' => 'A',
        'ă' => 'a',
        'Ą' => 'A',
        'ą' => 'a',
        'Ć' => 'C',
        'ć' => 'c',
        'Ĉ' => 'C',
        'ĉ' => 'c',
        'Ċ' => 'C',
        'ċ' => 'c',
        'Č' => 'C',
        'č' => 'c',
        'Ď' => 'D',
        'ď' => 'd',
        'Đ' => 'D',
        'đ' => 'd',
        'Ē' => 'E',
        'ē' => 'e',
        'Ĕ' => 'E',
        'ĕ' => 'e',
        'Ė' => 'E',
        'ė' => 'e',
        'Ę' => 'E',
        'ę' => 'e',
        'Ě' => 'E',
        'ě' => 'e',
        'Ĝ' => 'G',
        'ĝ' => 'g',
        'Ğ' => 'G',
        'ğ' => 'g',
        'Ġ' => 'G',
        'ġ' => 'g',
        'Ģ' => 'G',
        'ģ' => 'g',
        'Ĥ' => 'H',
        'ĥ' => 'h',
        'Ħ' => 'H',
        'ħ' => 'h',
        'Ĩ' => 'I',
        'ĩ' => 'i',
        'Ī' => 'I',
        'ī' => 'i',
        'Ĭ' => 'I',
        'ĭ' => 'i',
        'Į' => 'I',
        'į' => 'i',
        'İ' => 'I',
        'ı' => 'i',
        'Ĳ' => 'IJ',
        'ĳ' => 'ij',
        'Ĵ' => 'J',
        'ĵ' => 'j',
        'Ķ' => 'K',
        'ķ' => 'k',
        'ĸ' => 'k',
        'Ĺ' => 'L',
        'ĺ' => 'l',
        'Ļ' => 'L',
        'ļ' => 'l',
        'Ľ' => 'L',
        'ľ' => 'l',
        'Ŀ' => 'L',
        'ŀ' => 'l',
        'Ł' => 'L',
        'ł' => 'l',
        'Ń' => 'N',
        'ń' => 'n',
        'Ņ' => 'N',
        'ņ' => 'n',
        'Ň' => 'N',
        'ň' => 'n',
        'ŉ' => 'N',
        'Ŋ' => 'n',
        'ŋ' => 'N',
        'Ō' => 'O',
        'ō' => 'o',
        'Ŏ' => 'O',
        'ŏ' => 'o',
        'Ő' => 'O',
        'ő' => 'o',
        'Œ' => 'OE',
        'œ' => 'oe',
        'Ŕ' => 'R',
        'ŕ' => 'r',
        'Ŗ' => 'R',
        'ŗ' => 'r',
        'Ř' => 'R',
        'ř' => 'r',
        'Ś' => 'S',
        'ś' => 's',
        'Ŝ' => 'S',
        'ŝ' => 's',
        'Ş' => 'S',
        'ş' => 's',
        'Š' => 'S',
        'š' => 's',
        'Ţ' => 'T',
        'ţ' => 't',
        'Ť' => 'T',
        'ť' => 't',
        'Ŧ' => 'T',
        'ŧ' => 't',
        'Ũ' => 'U',
        'ũ' => 'u',
        'Ū' => 'U',
        'ū' => 'u',
        'Ŭ' => 'U',
        'ŭ' => 'u',
        'Ů' => 'U',
        'ů' => 'u',
        'Ű' => 'U',
        'ű' => 'u',
        'Ų' => 'U',
        'ų' => 'u',
        'Ŵ' => 'W',
        'ŵ' => 'w',
        'Ŷ' => 'Y',
        'ŷ' => 'y',
        'Ÿ' => 'Y',
        'Ź' => 'Z',
        'ź' => 'z',
        'Ż' => 'Z',
        'ż' => 'z',
        'Ž' => 'Z',
        'ž' => 'z',
        'ſ' => 's',
        'Ơ' => 'O',
        'ơ' => 'o',
        'Ư' => 'U',
        'ư' => 'u',
        'Ș' => 'S',
        'ș' => 's',
        'Ț' => 'T',
        'ț' => 't',
        'Ά' => 'A',
        'Έ' => 'E',
        'Ή' => 'H',
        'Ί' => 'I',
        'Ό' => 'O',
        'Ύ' => 'Y',
        'Ώ' => 'W',
        'ΐ' => 'i',
        'Α' => 'A',
        'Β' => 'B',
        'Γ' => 'G',
        'Δ' => 'D',
        'Ε' => 'E',
        'Ζ' => 'Z',
        'Η' => 'H',
        'Θ' => '8',
        'Ι' => 'I',
        'Κ' => 'K',
        'Λ' => 'L',
        'Μ' => 'M',
        'Ν' => 'N',
        'Ξ' => '3',
        'Ο' => 'O',
        'Π' => 'P',
        'Ρ' => 'R',
        'Σ' => 'S',
        'Τ' => 'T',
        'Υ' => 'Y',
        'Φ' => 'F',
        'Χ' => 'X',
        'Ψ' => 'PS',
        'Ω' => 'W',
        'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'ά' => 'a',
        'έ' => 'e',
        'ή' => 'h',
        'ί' => 'i',
        'ΰ' => 'y',
        'α' => 'a',
        'β' => 'b',
        'γ' => 'g',
        'δ' => 'd',
        'ε' => 'e',
        'ζ' => 'z',
        'η' => 'h',
        'θ' => '8',
        'ι' => 'i',
        'κ' => 'k',
        'λ' => 'l',
        'μ' => 'm',
        'ν' => 'n',
        'ξ' => '3',
        'ο' => 'o',
        'π' => 'p',
        'ρ' => 'r',
        'ς' => 's',
        'σ' => 's',
        'τ' => 't',
        'υ' => 'y',
        'φ' => 'f',
        'χ' => 'x',
        'ψ' => 'ps',
        'ω' => 'w',
        'ϊ' => 'i',
        'ϋ' => 'y',
        'ό' => 'o',
        'ύ' => 'y',
        'ώ' => 'w',
        'Ё' => 'Yo',
        'Є' => 'Ye',
        'І' => 'I',
        'Ї' => 'Yi',
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'J',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sh',
        'Ъ' => '',
        'Ы' => 'Y',
        'Ь' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sh',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        'ё' => 'yo',
        'є' => 'ye',
        'і' => 'i',
        'ї' => 'yi',
        'Ґ' => 'G',
        'ґ' => 'g',
        'Ạ' => 'A',
        'ạ' => 'a',
        'Ả' => 'A',
        'ả' => 'a',
        'Ấ' => 'A',
        'ấ' => 'a',
        'Ầ' => 'A',
        'ầ' => 'a',
        'Ẩ' => 'A',
        'ẩ' => 'a',
        'Ẫ' => 'A',
        'ẫ' => 'a',
        'Ậ' => 'A',
        'ậ' => 'a',
        'Ắ' => 'A',
        'ắ' => 'a',
        'Ằ' => 'A',
        'ằ' => 'a',
        'Ẳ' => 'A',
        'ẳ' => 'a',
        'Ẵ' => 'A',
        'ẵ' => 'a',
        'Ặ' => 'A',
        'ặ' => 'a',
        'Ẹ' => 'E',
        'ẹ' => 'e',
        'Ẻ' => 'E',
        'ẻ' => 'e',
        'Ẽ' => 'E',
        'ẽ' => 'e',
        'Ế' => 'E',
        'ế' => 'e',
        'Ề' => 'E',
        'ề' => 'e',
        'Ể' => 'E',
        'ể' => 'e',
        'Ễ' => 'E',
        'ễ' => 'e',
        'Ệ' => 'E',
        'ệ' => 'e',
        'Ỉ' => 'I',
        'ỉ' => 'i',
        'Ị' => 'I',
        'ị' => 'i',
        'Ọ' => 'O',
        'ọ' => 'o',
        'Ỏ' => 'O',
        'ỏ' => 'o',
        'Ố' => 'O',
        'ố' => 'o',
        'Ồ' => 'O',
        'ồ' => 'o',
        'Ổ' => 'O',
        'ổ' => 'o',
        'Ỗ' => 'O',
        'ỗ' => 'o',
        'Ộ' => 'O',
        'ộ' => 'o',
        'Ớ' => 'O',
        'ớ' => 'o',
        'Ờ' => 'O',
        'ờ' => 'o',
        'Ở' => 'O',
        'ở' => 'o',
        'Ỡ' => 'O',
        'ỡ' => 'o',
        'Ợ' => 'O',
        'ợ' => 'o',
        'Ụ' => 'U',
        'ụ' => 'u',
        'Ủ' => 'U',
        'ủ' => 'u',
        'Ứ' => 'U',
        'ứ' => 'u',
        'Ừ' => 'U',
        'ừ' => 'u',
        'Ử' => 'U',
        'ử' => 'u',
        'Ữ' => 'U',
        'ữ' => 'u',
        'Ự' => 'U',
        'ự' => 'u',
        'Ỳ' => 'Y',
        'ỳ' => 'y',
        'Ỵ' => 'Y',
        'ỵ' => 'y',
        'Ỷ' => 'Y',
        'ỷ' => 'y',
        'Ỹ' => 'Y',
        'ỹ' => 'y',
        '€' => 'E',
    ];
    /**
     * The count of the last replace call
     * @var string
     */
    private $replaceCount = 0;


    /**
     * Constructor
     *
     * @param string $charset The character set to use for multibyte functions
     */
    public function __construct($charset = 'utf-8')
    {
        $this->charset = $charset;
    }


    /**
     * Gets a string's best match in an array of words
     *
     * @param string $str The string to use
     * @param array $words The array of words to match against
     * @param float $tolerance The tolerance threshold to discard low-scoring matches
     *
     * @return bool|null The closest match for the string
     */
    public function bestMatch($str, $words, $tolerance = 0.25)
    {
        $closest  = null;
        $shortest = null;

        foreach ($words as $word) {
            $lev = $this->levenshtein($str, $word, $pct);

            if ($lev == 0) {
                $closest = $word;
                break;
            }

            if (($shortest === null || $lev <= $shortest) && $pct <= $tolerance) {
                $closest  = $word;
                $shortest = $lev;
            }
        }

        return $closest !== null ? $closest : false;
    }


    /**
     * Gets the size of the string in bytes
     *
     * @param string $str The string to use
     *
     * @return int The size in bytes
     */
    public function byteSize($str)
    {
        return strlen($str);
    }


    /**
     * Gets the character at a specific position
     *
     * @param string $str The string to use
     * @param int $i The position
     *
     * @return string The character
     */
    public function charAt($str, $i)
    {
        return mb_substr($str, $i, 1, $this->charset);
    }


    /**
     * Splits a string into chunks of a specific length
     *
     * @param string $str The string to use
     * @param int $length The length of each chunk
     *
     * @return array The array of chunks
     */
    public function chunk($str, $length = 1)
    {
        $chunks = [];
        $strlen = mb_strlen($str, $this->charset);

        for ($i = 0; $i < $strlen; $i += $length) {
            $chunks[] = mb_substr($str, $i, $length, $this->charset);
        }

        return $chunks;
    }


    /**
     * Compares strings using a locale for comparison
     *
     * @param string $str1 The string to use
     * @param string $str2 The string to compare against
     * @param string $locale The locale to use
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return int Returns < 0 if str1 is less than str2; > 0 if str1 is greater than str2, and 0 if they are equal.
     */
    public function collate($str1, $str2, $locale, $ci = false)
    {
        if ($ci) {
            $str1 = mb_strtolower($str1, $this->charset);
            $str2 = mb_strtolower($str2, $this->charset);
        }

        $old = setlocale(LC_ALL, 0);
        setlocale(LC_ALL, $locale . '.' . $this->charset);
        $return = strcoll($str1, $str2);
        setlocale(LC_ALL, $old);

        return $return;
    }


    /**
     * Compares strings
     *
     * @param string $str1 The string to use
     * @param string $str2 The string to compare against
     * @param bool $ci Whether to compare as case-sensitive or not
     * @param bool $nat Whether to use natural comparison or not
     *
     * @return int Returns < 0 if str1 is less than str2; > 0 if str1 is greater than str2, and 0 if they are equal.
     */
    public function compare($str1, $str2, $ci = false, $nat = false)
    {
        if ($ci) {
            $str1 = mb_strtolower($str1, $this->charset);
            $str2 = mb_strtolower($str2, $this->charset);
        }

        return $nat ? strnatcmp($str1, $str2) : strcmp($str1, $str2);
    }


    /**
     * Compresses repeating strings into one to eliminate repetitions
     *
     * @param string $str The string to use
     * @param string $needle The string to compress
     *
     * @return string The string with repetitions eliminated
     */
    public function compress($str, $needle = ' ')
    {
        $search = $needle . $needle;

        while (($tmp = str_replace($search, $needle, $str)) !== $str) {
            $str = $tmp;
        }

        return $str;
    }


    /**
     * Checks if a string contains another
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return bool True if the needle is found, otherwise false is returned
     */
    public function contains($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stripos' : 'mb_strpos';
        return $func($str, $needle, 0, $this->charset) !== false;
    }


    /**
     * Converts a string to another character set
     *
     * @param string $str The string to use
     * @param string $to The charset to convert to
     * @param string $sub The character to use for substitution when a character cannot be converted.
     * This character should be part of the charset you are trying to convert to.
     *
     * @return string The string converted to the new charset
     */
    public function convertCharset($str, $to, $sub = '?')
    {
        $old = mb_substitute_character();
        $sub = hexdec(bin2hex(mb_substr($sub, 0, 1, $to)));

        mb_substitute_character($sub);
        $str = mb_convert_encoding($str, $to, $this->charset);
        mb_substitute_character($old);

        return $str;
    }


    /**
     * Gets the number of occurences of a string within a string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return int The number of occurences found
     */
    public function count($str, $needle, $ci = false)
    {
        $indices = $this->indicesOf($str, $needle, $ci);
        return $indices === false ? 0 : count($indices);
    }


    /**
     * Gets the number of words
     *
     * @param string $str The string to use
     * @param string $charlist The characters to be considered as part of a word
     *
     * @return int The number of words
     */
    public function countWords($str, $charlist = '')
    {
        return count($this->words($str, $charlist));
    }


    /**
     * Gets the density of a string within another string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return int|float The density
     */
    public function densityOf($str, $needle, $ci = false)
    {
        $str1 = $this->compress($str, ' ');
        $str2 = $this->compress($needle, ' ');

        $x = $this->length($str1);
        $y = $this->length($str2);
        $z = $this->count($str1, $str2, $ci);

        return $z * ($y / $x);
    }


    /**
     * Detects the character encoding of a string
     *
     * @param string $str The string to use
     * @param null|string $list The comma-separated list of charsets
     *
     * @return string|bool The character encoding or false if the encoding cannot be detected
     */
    public function detectCharset($str, $list = null)
    {
        if ($list === null) {
            return mb_detect_encoding($str);
        }

        return mb_detect_encoding($str, $list);
    }


    /**
     * Encloses a string by padding left and right with another string
     *
     * @param string $str The string to use
     * @param string $enclosure The enclosure string to use
     *
     * @return string The enclosed string
     */
    public function enclose($str, $enclosure)
    {
        return $enclosure . $str . $enclosure;
    }


    /**
     * Checks if a string ends with a given string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return bool True if the string ends with the needle, otherwise false is returned
     */
    public function endsWith($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stripos' : 'mb_strpos';
        $len  = mb_strlen($str, $this->charset);
        $pos  = $len - mb_strlen($needle, $this->charset);

        return $func($str, $needle, 0, $this->charset) === $pos;
    }


    /**
     * Checks if two strings are equal
     *
     * @param string $str1 The string to use
     * @param string $str2 The string to compare against
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return bool True if the strings are equal, otherwise false is returned
     */
    public function equals($str1, $str2, $ci = false)
    {
        if ($ci) {
            $str1 = mb_strtolower($str1, $this->charset);
            $str2 = mb_strtolower($str2, $this->charset);
        }

        return $str1 === $str2;
    }


    /**
     * Checks whether a charset works with a given string
     *
     * @param string $str The string to use
     * @param string $charset The charset to check against
     *
     * @return bool True on success or false on failure
     */
    public function fitsCharset($str, $charset)
    {
        return mb_check_encoding($str, $charset);
    }


    /**
     * Formats a string as per PHP's sprintf() function
     *
     * param mixed $args,... Optional unlimited amount of arguments to use for formatting
     *
     * @param string $str The string to use (see PHP's sprintf() function for usage)
     *
     * @return string The formatted string
     */
    public function format($str)
    {
        $argv = func_get_args();
        array_shift($argv);

        return vsprintf($str, $argv);
    }


    /**
     * Hashes a string
     *
     * @param string $str The string to use
     * @param string $algo The algorithm to use for hashing
     * @param bool $rawOutput When set to true, outputs raw binary data. False outputs lowercase hexits.
     *
     * @return string The hashed string
     */
    public function hash($str, $algo = 'md5', $rawOutput = false)
    {
        return hash($algo, $str, $rawOutput);
    }


    /**
     * Gets the position of the first occurrence of a string within another string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return int|bool The position of the first occurence or false if not found
     */
    public function indexOf($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stripos' : 'mb_strpos';
        return $func($str, $needle, 0, $this->charset);
    }


    /**
     * Gets the position of the last occurrence of a string within another string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return int|bool The position of the last occurence or false if not found
     */
    public function indexOfLast($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_strripos' : 'mb_strrpos';
        return $func($str, $needle, 0, $this->charset);
    }


    /**
     * Gets the positions of the occurrences of a string within another string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return array|bool The array of positions of the occurrences or false if not found
     */
    public function indicesOf($str, $needle, $ci = false)
    {
        $indices = [];
        $pos     = -1;
        $func    = $ci ? 'mb_stripos' : 'mb_strpos';

        while (($pos = $func($str, $needle, ++$pos, $this->charset)) !== false) {
            $indices[] = $pos;
        }

        return $indices ? $indices : false;
    }


    /**
     * Gets the amount of characters in a string
     *
     * @param string $str The string to use
     *
     * @return int The amount of characters
     */
    public function length($str)
    {
        return mb_strlen($str, $this->charset);
    }


    /**
     * Calculates the levenshtein distance score of a string to another string
     *
     * @param string $str1 The string to use
     * @param string $str2 The string to compare against
     * @param int $pct The variable reference to store the matching percent
     *
     * @return int The levenshtein distance score which is the number of
     * insert, replace, and delete operations required to transform str1 into str2
     */
    public function levenshtein($str1, $str2, &$pct = 0)
    {
        $len1   = mb_strlen($str1, $this->charset);
        $len2   = mb_strlen($str2, $this->charset);
        $maxlen = $len1 > $len2 ? $len1 : $len2;

        // strip common prefix
        $i = 0;
        do {
            if (mb_substr($str1, $i, 1, $this->charset) != mb_substr($str2, $i, 1, $this->charset)) {
                break;
            }
            $i++;
            $len1--;
            $len2--;
        } while ($len1 > 0 && $len2 > 0);
        if ($i > 0) {
            $str1 = mb_substr($str1, $i, $len1, $this->charset);
            $str2 = mb_substr($str2, $i, $len2, $this->charset);
        }

        // strip common suffix
        $i = 0;
        do {
            if (mb_substr($str1, $len1 - 1, 1, $this->charset) != mb_substr($str2, $len2 - 1, 1, $this->charset)) {
                break;
            }
            $i++;
            $len1--;
            $len2--;
        } while ($len1 > 0 && $len2 > 0);
        if ($i > 0) {
            $str1 = mb_substr($str1, 0, $len1, $this->charset);
            $str2 = mb_substr($str2, 0, $len2, $this->charset);
        }

        if ($len1 == 0) {
            $pct = $len2 / $maxlen;
            return $len2;
        }
        if ($len2 == 0) {
            $pct = $len1 / $maxlen;
            return $len1;
        }

        $v0 = range(0, $len1);
        $v1 = array();

        for ($i = 1; $i <= $len2; $i++) {
            $v1[0] = $i;
            $str2j = mb_substr($str2, $i - 1, 1, $this->charset);

            for ($j = 1; $j <= $len1; $j++) {
                $cost = (mb_substr($str1, $j - 1, 1, $this->charset) == $str2j) ? 0 : 1;

                $m_min = $v0[$j] + 1;
                $b     = $v1[$j - 1] + 1;
                $c     = $v0[$j - 1] + $cost;

                if ($b < $m_min) {
                    $m_min = $b;
                }
                if ($c < $m_min) {
                    $m_min = $c;
                }

                $v1[$j] = $m_min;
            }

            $vTmp = $v0;
            $v0   = $v1;
            $v1   = $vTmp;
        }

        $lev = $v0[$len1];
        $pct = $lev / $maxlen;

        return $lev;
    }


    /**
     * Calculates the metaphone key of a string
     *
     * @param string $str The string to use
     * @param int $limit The amount of characters to return
     *
     * @return string The metaphone key
     */
    public function metaphone($str, $limit = 0)
    {
        return metaphone($str, $limit);
    }


    /**
     * Pads a string to a given size
     *
     * @param string $str The string to use
     * @param int $size The final size of the string
     * @param string $pad The character to use for padding
     *
     * @return string The padded string or original string if the string is longer than the size
     */
    public function pad($str, $size, $pad)
    {
        $len = mb_strlen($str, $this->charset);

        if (($diff = $size - $len) <= 0) {
            return $str;
        }

        $pad  = $this->repeat($pad, $diff);
        $half = $diff / 2;
        $padl = mb_substr($pad, 0, floor($half), $this->charset);
        $padr = mb_substr($pad, 0, ceil($half), $this->charset);

        return $padl . $str . $padr;
    }


    /**
     * Left-pads a string to a given size
     *
     * @param string $str The string to use
     * @param int $size The final size of the string
     * @param string $pad The character to use for padding
     *
     * @return string The left-padded string or original string if the string is longer than the size
     */
    public function padLeft($str, $size, $pad)
    {
        $len = mb_strlen($str, $this->charset);

        if (($diff = $size - $len) <= 0) {
            return $str;
        }

        $pad = $this->repeat($pad, $diff);
        $pad = mb_substr($pad, 0, $diff, $this->charset);

        return $pad . $str;
    }


    /**
     * Right-pads a string to a given size
     *
     * @param string $str The string to use
     * @param int $size The final size of the string
     * @param string $pad The character to use for padding
     *
     * @return string The right-padded string or original string if the string is longer than the size
     */
    public function padRight($str, $size, $pad)
    {
        $len = mb_strlen($str, $this->charset);

        if (($diff = $size - $len) <= 0) {
            return $str;
        }

        $pad = $this->repeat($pad, $diff);
        $pad = mb_substr($pad, 0, $diff, $this->charset);

        return $str . $pad;
    }


    /**
     * Hashes the password twice using a generated and embedded salt
     *
     * @param string $password The password
     * @param string $algo The algorithm to use for hashing
     *
     * @return mixed The password hash with an embedded salt
     */
    public function passwordCreateHash($password, $algo = 'sha256')
    {
        $salt = '';
        $hex  = '0123456789abcdef';

        for ($i = 0; $i < 4; $i++) {
            $salt .= $hex[mt_rand(0, 15)];
        }

        $hash = hash($algo, $salt . hash($algo, $password));

        return substr_replace($hash, $salt, 17, 4);
    }


    /**
     * Checks if a password hash is valid
     *
     * @param string $password The password
     * @param string $hash The hashed password to check against
     * @param string $algo The algorithm to use for hashing
     *
     * @return bool True if the hash is valid for the password
     */
    public function passwordHashIsValid($password, $hash, $algo = 'sha256')
    {
        $salt  = substr($hash, 17, 4);
        $vhash = hash($algo, $salt . hash($algo, $password));

        return (string)$hash === substr_replace($vhash, $salt, 17, 4);
    }


    /**
     * Generate a random string
     *
     * @param int $length The length of the random string to generate
     * @param string $charlist The list of characters used in the random string.
     * Default consists of all alphanumerical characters in lowercase only
     *
     * @return string The random string
     */
    public function random($length, $charlist = 'abcdefghijklmnopqrstuvwxyz0123456789')
    {
        $max = mb_strlen($charlist, $this->charset) - 1;

        for ($i = 0, $rand = ''; $i < $length; $i++) {
            $rand .= mb_substr($charlist, mt_rand(0, $max), 1, $this->charset);
        }

        return $rand;
    }


    /**
     * Set the seed for the random number generator
     *
     * @param int|null $seed The seed for the random number generator
     */
    public function randomSeed($seed)
    {
        srand($seed);
        mt_srand($seed);
    }


    /**
     * Appends a string to itself a given amount of times
     *
     * @param string $str The string to use
     * @param int $amount The amount of times to self-append
     *
     * @return string The repeated string
     */
    public function repeat($str, $amount = 1)
    {
        $repeat = $str;

        while ($amount-- > 0) {
            $str .= $repeat;
        }

        return $str;
    }


    /**
     * Replaces a string with another
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param string $replacement The replacement string
     * @param bool $ci Whether to compare as case-sensitive or not
     * @param null|int $limit The maximum amount of times to replace
     *
     * @return string The string with all replacements applied
     */
    public function replace($str, $needle, $replacement, $ci = false, $limit = null)
    {
        $this->replaceCount = 0;

        $func = $ci ? 'mb_stripos' : 'mb_strpos';

        if ($limit === null) {
            $limit = PHP_INT_MAX;
        } elseif ($limit < 0) {
            $limit = $this->count($str, $needle, $ci) + $limit;
        }

        $pos  = 0;
        $nlen = strlen($needle);

        while ($limit-- > 0 && ($pos = $func($str, $needle, $pos, $this->charset)) !== false) {
            $bytePos = strlen($this->slice($str, 0, $pos));
            $str     = substr_replace($str, $replacement, $bytePos, $nlen);
            $pos += $nlen;
            $this->replaceCount++;
        }

        return $str;
    }


    /**
     * Reverses a string
     *
     * @param string $str The string to use
     *
     * @return string The reverse string
     */
    public function reverse($str)
    {
        return implode('', array_reverse($this->toArray($str)));
    }


    /**
     * Performs the rot13 transform on a string
     *
     * @param string $str The string to use
     *
     * @return string The rot13 transformed string
     */
    public function rot13($str)
    {
        $rot13 = [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z'
        ];

        $arr = $this->toArray($str);
        $str = '';

        foreach ($arr as $char) {
            if (in_array($char, $rot13)) {
                $str .= str_rot13($char);
            } else {
                $str .= $char;
            }
        }

        return $str;
    }


    /**
     * Sanitizes a string so that it holds only the characters allowed by charlist
     *
     * @param string $str The string to use
     * @param string $allowedChars The characters allowed to remain in the string. This charlist will
     * be used inside a regular expression. You may use ranges such as "a-zA-Z0-9".
     *
     * @return string The sanitized string
     */
    public function sanitize($str, $allowedChars = '')
    {
        $chars = $this->toArray($str);
        $str   = '';

        $allowedChars = str_replace('~', '\~', $allowedChars);

        foreach ($chars as $char) {
            if (preg_match("~[$allowedChars]~", $char)) {
                $str .= $char;
            }
        }

        return $str;
    }


    /**
     * Parses a string
     *
     * @param string $str The string to use
     * @param string $format The format to parse
     *
     * @return array The array of values parsed
     */
    public function scan($str, $format)
    {
        return sscanf($str, $format);
    }


    /**
     * Shuffles a string
     *
     * @param string $str The string to use
     *
     * @return string The shuffled string
     */
    public function shuffle($str)
    {
        $arr = $this->toArray($str);
        shuffle($arr);

        return implode('', $arr);
    }


    /**
     * Slices a string using a position
     *
     * @param string $str The string to use
     * @param int $pos The position to start the slice
     * @param null|int $length The length of the slice
     *
     * @return string The sliced string
     */
    public function slice($str, $pos, $length = null)
    {
        if ($length === null) {
            $length = mb_strlen($str, $this->charset);
        }

        return mb_substr($str, $pos, $length, $this->charset);
    }


    /**
     * Slices a string using a needle
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return string The remainder of the string after, but not including, the needle
     */
    public function sliceAfter($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stristr' : 'mb_strstr';
        $nlen = mb_strlen($needle, $this->charset);
        $str  = $func($str, $needle, false, $this->charset);

        return mb_substr($str, $nlen, null, $this->charset);
    }


    /**
     * Slices a string using a needle
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return string The part of the string before, but not including, the needle
     */
    public function sliceBefore($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stristr' : 'mb_strstr';
        return $func($str, $needle, true, $this->charset);
    }


    /**
     * Slices a string using a needle
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return string The remainder of the string as of the needle
     */
    public function sliceFrom($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stristr' : 'mb_strstr';
        return $func($str, $needle, false, $this->charset);
    }


    /**
     * Calculates the soundex key of a string
     *
     * @param string $str The string to use
     *
     * @return string The soundex key
     */
    public function soundex($str)
    {
        return soundex($str);
    }


    /**
     * Removes a portion of a string and replaces the removed portion with another string
     *
     * @param string $str The string to use
     * @param int $pos The start position
     * @param null|int $length The length of the portion to remove
     * @param string $replacement The replacement string
     *
     * @return string The spliced string
     */
    public function splice($str, $pos, $length = null, $replacement = '')
    {
        if ($length === null) {
            $left = mb_substr($str, 0, $pos, $this->charset);
            return $left . $replacement;
        }

        $left  = mb_substr($str, 0, $pos, $this->charset);
        $right = mb_substr($str, $pos + $length, null, $this->charset);

        return $left . $replacement . $right;
    }


    /**
     * Splits a string into an array using a needle
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param null|int $limit The maximum amount of elements in the returned array
     *
     * @return array The array of strings split up by needle
     */
    public function split($str, $needle, $limit = null)
    {
        if ($limit === null) {
            return explode($needle, $str);
        }

        return explode($needle, $str, $limit);
    }


    /**
     * Checks if a string starts with a given string
     *
     * @param string $str The string to use
     * @param string $needle The string to search for
     * @param bool $ci Whether to compare as case-sensitive or not
     *
     * @return bool True if the string starts with the other, otherwise false is returned
     */
    public function startsWith($str, $needle, $ci = false)
    {
        $func = $ci ? 'mb_stripos' : 'mb_strpos';
        return $func($str, $needle, 0, $this->charset) === 0;
    }


    /**
     * Replaces invisible (whitespace) characters with a space
     *
     * @param string $str The string to use
     *
     * @return string The stripped string
     */
    public function stripInvisible($str)
    {
        $chars = $this->toArray($str);
        $str   = '';

        foreach ($chars as $char) {
            if (trim($char) == '') {
                $str .= ' ';
            } else {
                $str .= $char;
            }
        }

        return $str;
    }


    /**
     * Transliterates a string to ASCII
     *
     * Will attempt to match each multibyte char to an ASCII char.
     * If a multibyte char has no translation, it will be removed.
     *
     * @param string $str The string to use
     *
     * @return string The ASCII string
     */
    public function toASCII($str)
    {
        return preg_replace('~[\x80-\xff]~', '', strtr($str, $this->map));
    }


    /**
     * Generates an array of all the characters in a given string
     *
     * @param string $str The string to use
     *
     * @return array The array of characters
     */
    public function toArray($str)
    {
        $chars = preg_split('~~u', $str);
        array_shift($chars);
        array_pop($chars);

        return $chars;
    }


    /**
     * Changes a string to lowercase
     *
     * @param string $str The string to use
     *
     * @return string The lowercase string
     */
    public function toLowerCase($str)
    {
        return mb_strtolower($str, $this->charset);
    }


    /**
     * Changes a string to a slug (URL-friendly string)
     *
     * @param string $str The string to use
     * @param string $delim The delimiter
     *
     * @return string The string as a slug
     */
    public function toSlug($str, $delim = '-')
    {
        $str = $this->toASCII($str);
        $str = strtolower($str);
        $str = preg_replace('~[^0-9a-z]+~u', $delim, $str);
        $str = $this->compress($str, $delim);

        return trim($str, $delim);
    }


    /**
     * Changes a string to titlecase
     *
     * @param string $str The string to use
     *
     * @return string The titlecase string
     */
    public function toTitleCase($str)
    {
        return mb_convert_case($str, MB_CASE_TITLE, $this->charset);
    }


    /**
     * Changes a string to uppercase
     *
     * @param string $str The string to use
     *
     * @return string The uppercase string
     */
    public function toUpperCase($str)
    {
        return mb_strtoupper($str, $this->charset);
    }


    /**
     * Trims a string
     *
     * @param string $str The string to use
     * @param string $charlist The characters to trim off of the ends
     *
     * @return string The trimmed string
     */
    public function trim($str, $charlist = " \t\n\r\0\x0B")
    {
        return trim($str, $charlist);
    }


    /**
     * Trims the left side of a string
     *
     * @param string $str The string to use
     * @param string $charlist The characters to trim off of the ends
     *
     * @return string The trimmed string
     */
    public function trimLeft($str, $charlist = " \t\n\r\0\x0B")
    {
        return ltrim($str, $charlist);
    }


    /**
     * Trims the right side of a string
     *
     * @param string $str The string to use
     * @param string $charlist The characters to trim off of the ends
     *
     * @return string The trimmed string
     */
    public function trimRight($str, $charlist = " \t\n\r\0\x0B")
    {
        return rtrim($str, $charlist);
    }


    /**
     * Truncates a string to a given
     *
     * @param string $str The string to use
     * @param int $length The desired length of the string
     * @param string $end The string to append if truncation occurred
     *
     * @return string The truncated string
     */
    public function truncate($str, $length, $end = '')
    {
        $strlen = mb_strlen($str, $this->charset);

        if ($strlen > $length) {
            $length = $length - mb_strlen($end, $this->charset);
            $str    = mb_substr($str, 0, $length, $this->charset) . $end;
        }

        return $str;
    }


    /**
     * Splits a string into an array of words
     *
     * @param string $str The string to use
     * @param string $charlist The characters to be considered as part of a word
     *
     * @return array The array of words
     */
    public function words($str, $charlist = '')
    {
        $charlist = preg_quote($charlist, '~');
        preg_match_all('~[\p{L}' . $charlist . ']+~u', $str, $matches);

        return array_filter(
            $matches[0],
            function ($val) {
                return trim($val) !== '';
            }
        );
    }


    /**
     * Wraps lines in a string that go past the width
     *
     * @param string $str The string to use
     * @param int $width The maximum width of each line
     * @param string $break The string used for breaking lines
     * @param bool $cut If the cut is set to true, the string is always wrapped at or before the specified width.
     * So if you have a word that is larger than the given width, it is broken apart.
     *
     * @return string The string with wrapped lines
     */
    public function wrap($str, $width = 75, $break = "\n", $cut = false)
    {
        $length = mb_strlen($str, $this->charset);

        $real = [];
        $fake = [];

        for ($i = 0; $i < $length; ++$i) {
            $char   = mb_substr($str, $i, 1, $this->charset);
            $real[] = $char;
            $fake[] = (!preg_match('~[\x80-\xff]~', $char)) ? $char : 'a';
        }

        $fake = implode('', $fake);
        $fake = wordwrap($fake, $width, $break, $cut);
        $fake = explode($break, $fake);
        $wrap = [];

        foreach ($fake as $line) {
            $length = mb_strlen($line, $this->charset);
            $chars  = [];

            while ($length-- >= 0) {
                $chars[] = array_shift($real);
            }

            $wrap[] = trim(implode('', $chars));
        }

        return implode($break, $wrap);
    }
}

<?php
/**
 * Class Math
 *
 * @package Nucleus\Library
 * @subpackage Math
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Math;

use Nucleus\Library\Exception\InvalidArgumentException;
use Nucleus\Library\Exception\LogicException;

/**
 * Class Math
 *
 * @package Nucleus\Library
 * @subpackage Math
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class Math
{
    // Math constants
    const INF   = INF;
    const NAN   = NAN;
    const E     = M_E;
    const PI    = M_PI;
    const EULER = M_EULER;

    // Rounding options
    const ROUND_HALF_DOWN = PHP_ROUND_HALF_DOWN;
    const ROUND_HALF_EVEN = PHP_ROUND_HALF_EVEN;
    const ROUND_HALF_ODD  = PHP_ROUND_HALF_ODD;
    const ROUND_HALF_UP   = PHP_ROUND_HALF_UP;

    // Angle units
    const UNIT_RADIAN  = 100;
    const UNIT_DEGREE  = 101;
    const UNIT_GRADIAN = 102;
    const UNIT_DMS     = 103;

    // Area units
    const UNIT_SQUARE_METER      = 200;
    const UNIT_SQUARE_DECIMETER  = 201;
    const UNIT_SQUARE_CENTIMETER = 202;
    const UNIT_SQUARE_MILLIMETER = 203;
    const UNIT_SQUARE_MICROMETER = 204;
    const UNIT_SQUARE_PICOMETER  = 205;
    const UNIT_SQUARE_NANOMETER  = 206;
    const UNIT_SQUARE_DECAMETER  = 207;
    const UNIT_SQUARE_HECTOMETER = 208;
    const UNIT_SQUARE_KILOMETER  = 209;
    const UNIT_SQUARE_MEGAMETER  = 210;
    const UNIT_SQUARE_GIGAMETER  = 211;
    const UNIT_SQUARE_TERAMETER  = 212;
    const UNIT_SQUARE_INCH       = 213;
    const UNIT_SQUARE_FOOT       = 214;
    const UNIT_SQUARE_YARD       = 215;
    const UNIT_SQUARE_MILE       = 216;
    const UNIT_ACRE              = 217;

    // Binary units
    const UNIT_BIT         = 300;
    const UNIT_CRUMB       = 301;
    const UNIT_NIBBLE      = 302;
    const UNIT_KILOBIT     = 303;
    const UNIT_MEGABIT     = 304;
    const UNIT_GIGABIT     = 305;
    const UNIT_TERABIT     = 306;
    const UNIT_PETABIT     = 307;
    const UNIT_EXABIT      = 308;
    const UNIT_ZETTABIT    = 309;
    const UNIT_YOTTABIT    = 310;
    const UNIT_BYTE        = 311;
    const UNIT_OCTET       = 312;
    const UNIT_KILOBYTE    = 313;
    const UNIT_MEGABYTE    = 314;
    const UNIT_GIGABYTE    = 315;
    const UNIT_TERABYTE    = 316;
    const UNIT_PETABYTE    = 317;
    const UNIT_EXABYTE     = 318;
    const UNIT_KILOBYTE_SI = 319;
    const UNIT_MEGABYTE_SI = 320;
    const UNIT_GIGABYTE_SI = 321;
    const UNIT_TERABYTE_SI = 322;
    const UNIT_PETABYTE_SI = 323;
    const UNIT_EXABYTE_SI  = 324;

    // Distance units
    const UNIT_METER      = 400;
    const UNIT_DECIMETER  = 401;
    const UNIT_CENTIMETER = 402;
    const UNIT_MILLIMETER = 403;
    const UNIT_MICROMETER = 404;
    const UNIT_PICOMETER  = 405;
    const UNIT_NANOMETER  = 406;
    const UNIT_DECAMETER  = 407;
    const UNIT_HECTOMETER = 408;
    const UNIT_KILOMETER  = 409;
    const UNIT_MEGAMETER  = 410;
    const UNIT_GIGAMETER  = 411;
    const UNIT_TERAMETER  = 412;
    const UNIT_INCH       = 413;
    const UNIT_FOOT       = 414;
    const UNIT_YARD       = 415;
    const UNIT_MILE       = 416;

    // Mass units
    const UNIT_GRAM       = 500;
    const UNIT_DECIGRAM   = 501;
    const UNIT_CENTIGRAM  = 502;
    const UNIT_MILLIGRAM  = 503;
    const UNIT_MICROGRAM  = 504;
    const UNIT_PICOGRAM   = 505;
    const UNIT_NANOGRAM   = 506;
    const UNIT_DECAGRAM   = 507;
    const UNIT_HECTOGRAM  = 508;
    const UNIT_KILOGRAM   = 509;
    const UNIT_MEGAGRAM   = 510;
    const UNIT_GIGAGRAM   = 511;
    const UNIT_TERAGRAM   = 512;
    const UNIT_POUND      = 513;
    const UNIT_CARAT      = 514;
    const UNIT_OUNCE      = 515;
    const UNIT_TON_LONG   = 516;
    const UNIT_TON_SHORT  = 517;
    const UNIT_TON_METRIC = 518;

    // Pressure units
    const UNIT_PASCAL      = 600;
    const UNIT_DECIPASCAL  = 601;
    const UNIT_CENTIPASCAL = 602;
    const UNIT_MILLIPASCAL = 603;
    const UNIT_MICROPASCAL = 604;
    const UNIT_PICOPASCAL  = 605;
    const UNIT_NANOPASCAL  = 606;
    const UNIT_DECAPASCAL  = 607;
    const UNIT_HECTOPASCAL = 608;
    const UNIT_KILOPASCAL  = 609;
    const UNIT_MEGAPASCAL  = 610;
    const UNIT_GIGAPASCAL  = 611;
    const UNIT_TERAPASCAL  = 612;
    const UNIT_PSI         = 613;
    const UNIT_BAR         = 614;

    // Temperature units
    const UNIT_KELVIN     = 700;
    const UNIT_CELSIUS    = 701;
    const UNIT_FAHRENHEIT = 702;
    const UNIT_RANKINE    = 703;
    const UNIT_DELISLE    = 704;
    const UNIT_NEWTON     = 705;
    const UNIT_REAUMUR    = 706;
    const UNIT_ROMER      = 707;

    // Time units
    const UNIT_SECOND      = 800;
    const UNIT_DECISECOND  = 801;
    const UNIT_CENTISECOND = 802;
    const UNIT_MILLISECOND = 803;
    const UNIT_MICROSECOND = 804;
    const UNIT_PICOSECOND  = 805;
    const UNIT_NANOSECOND  = 806;
    const UNIT_DECASECOND  = 807;
    const UNIT_HECTOSECOND = 808;
    const UNIT_KILOSECOND  = 809;
    const UNIT_MEGASECOND  = 810;
    const UNIT_GIGASECOND  = 811;
    const UNIT_TERASECOND  = 812;
    const UNIT_MINUTE      = 813;
    const UNIT_HOUR        = 814;
    const UNIT_DAY         = 815;
    const UNIT_WEEK        = 816;
    const UNIT_MONTH_AVG   = 817;
    const UNIT_MONTH_28    = 818;
    const UNIT_MONTH_29    = 819;
    const UNIT_MONTH_30    = 820;
    const UNIT_MONTH_31    = 821;
    const UNIT_QUARTER     = 822;
    const UNIT_YEAR        = 823;
    const UNIT_DECADE      = 824;
    const UNIT_CENTURY     = 825;
    const UNIT_MILLENNIUM  = 826;

    // Volume units
    const UNIT_CUBIC_METER      = 900;
    const UNIT_CUBIC_DECIMETER  = 901;
    const UNIT_CUBIC_CENTIMETER = 902;
    const UNIT_CUBIC_MILLIMETER = 903;
    const UNIT_CUBIC_MICROMETER = 904;
    const UNIT_CUBIC_PICOMETER  = 905;
    const UNIT_CUBIC_NANOMETER  = 906;
    const UNIT_CUBIC_DECAMETER  = 907;
    const UNIT_CUBIC_HECTOMETER = 908;
    const UNIT_CUBIC_KILOMETER  = 909;
    const UNIT_CUBIC_MEGAMETER  = 910;
    const UNIT_CUBIC_GIGAMETER  = 911;
    const UNIT_CUBIC_TERAMETER  = 912;
    const UNIT_CUBIC_INCH       = 913;
    const UNIT_CUBIC_FOOT       = 914;
    const UNIT_CUBIC_YARD       = 915;
    const UNIT_CUBIC_MILE       = 916;
    const UNIT_LITER            = 917;
    const UNIT_DECILITER        = 918;
    const UNIT_CENTILITER       = 919;
    const UNIT_MILLILITER       = 920;
    const UNIT_MICROLITER       = 921;
    const UNIT_NANOLITER        = 922;
    const UNIT_PICOLITER        = 923;
    const UNIT_DECALITER        = 924;
    const UNIT_HECTOLITER       = 925;
    const UNIT_KILOLITER        = 926;
    const UNIT_MEGALITER        = 927;
    const UNIT_GIGALITER        = 928;
    const UNIT_TERALITER        = 929;
    const UNIT_FLUID_OUNCE      = 930;
    const UNIT_PINT             = 931;
    const UNIT_CUP              = 932;
    const UNIT_CUP_METRIC       = 933;
    const UNIT_QUART            = 934;
    const UNIT_GALLON           = 935;
    const UNIT_UK_FLUID_OUNCE   = 936;
    const UNIT_UK_PINT          = 937;
    const UNIT_UK_QUART         = 938;
    const UNIT_UK_GALLON        = 939;


    /**
     * The unit conversion rules
     * @var array
     */
    private $units;


    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Get the absolute value of a number
     *
     * @param int|float $x The number
     *
     * @return int|float The absolute value
     */
    public function abs($x)
    {
        return abs($x);
    }


    /**
     * Compute the inverse cosine (arccosine) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse cosine value
     */
    public function acos($x)
    {
        return acos($x);
    }


    /**
     * Compute the inverse hyperbolic cosine (arccosine) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic cosine value
     */
    public function acosh($x)
    {
        return acosh($x);
    }


    /**
     * Compute the inverse cotangent (arccotangent) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse cotangent value
     */
    public function acot($x)
    {
        return atan(1 / $x);
    }


    /**
     * Compute the inverse hyperbolic cotangent (arccotangent) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic cotangent value
     */
    public function acoth($x)
    {
        return atanh(1 / $x);
    }


    /**
     * Compute the inverse cosecant (arccosecant) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse cosecant value
     */
    public function acsc($x)
    {
        return asin(1 / $x);
    }


    /**
     * Compute the inverse hyperbolic cosecant (arccosecant) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic cosecant value
     */
    public function acsch($x)
    {
        return asinh(1 / $x);
    }


    /**
     * Compute the inverse secant (arcsecant) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse secant value
     */
    public function asec($x)
    {
        return acos(1 / $x);
    }


    /**
     * Compute the inverse hyperbolic secant (arcsecant) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic secant value
     */
    public function asech($x)
    {
        return acosh(1 / $x);
    }


    /**
     * Compute the inverse sine (arcsine) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse sine value
     */
    public function asin($x)
    {
        return asin($x);
    }


    /**
     * Compute the inverse hyperbolic sine (arcsine) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic sine value
     */
    public function asinh($x)
    {
        return asinh($x);
    }


    /**
     * Compute the inverse tangent (arctangent) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse tangent value
     */
    public function atan($x)
    {
        return atan($x);
    }


    /**
     * Compute the four-quadrant inverse tangent (arctangent) value of two numbers
     *
     * @param int|float $y The dividend parameter
     * @param int|float $x The divisor parameter
     *
     * @return float The inverse tangent value
     */
    public function atan2($y, $x)
    {
        return atan2($y, $x);
    }


    /**
     * Compute the inverse hyperbolic tangent (arctangent) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The inverse hyperbolic tangent value
     */
    public function atanh($x)
    {
        return atanh($x);
    }


    /**
     * Compute the cubic root value of a number
     *
     * @param int|float $x The number
     *
     * @return float The cubic root value
     */
    public function cbrt($x)
    {
        return pow($x, 1 / 3);
    }


    /**
     * Compute the ceiling value of a number
     *
     * @param int|float $x The number
     * @param int $precision The number of decimals for rounding
     *
     * @return float The ceiling value
     */
    public function ceil($x, $precision = 0)
    {
        $factor = pow(10, -1 * $precision);
        return ceil($x / $factor) * $factor;
    }


    /**
     * Convert from base 2 up to base 36
     *
     * @param mixed $x The number to convert
     * @param int $fromBase The source base
     * @param int $toBase The target base
     *
     * @return string The converted number
     */
    public function convertBase($x, $fromBase, $toBase)
    {
        return base_convert($x, $fromBase, $toBase);
    }


    /**
     * Convert a number between units
     *
     * @param mixed $x The number to convert
     * @param int $fromUnit The source unit, one of the UNIT_* constants
     * @param int $toUnit The target unit, one of the UNIT_* constants
     *
     * @return mixed The converted number
     *
     * @throws \Nucleus\Library\Exception\InvalidArgumentException
     *
     * @link http://www.asknumbers.com/
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function convertUnit($x, $fromUnit, $toUnit)
    {
        if ($this->units === null) {

            // @codingStandardsIgnoreStart
            $this->units = [
                // Angle units
                self::UNIT_RADIAN            => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_DEGREE            => [
                    function ($x) { return deg2rad($x); },
                    function ($x) { return rad2deg($x); },
                ],
                self::UNIT_GRADIAN           => [
                    function ($x) { return $x * M_PI / 200; },
                    function ($x) { return $x * 200 / M_PI; },
                ],
                self::UNIT_DMS               => [
                    function ($x) { return deg2rad($x[0] + ((($x[1] * 60) + ($x[2])) / 3600)); },
                    function ($x) {
                        $x   = rad2deg($x);
                        $deg = floor($x);
                        $tmp = ($x - $deg) * 60;
                        $min = floor($tmp);
                        $sec = ($tmp - $min) * 60;
                        return [$deg, $min, $sec];
                    },
                ],
                // Area units
                self::UNIT_SQUARE_METER      => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_SQUARE_DECIMETER  => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_SQUARE_CENTIMETER => [
                    function ($x) { return $x * 1e-4; },
                    function ($x) { return $x * 1e+4; },
                ],
                self::UNIT_SQUARE_MILLIMETER => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_SQUARE_MICROMETER => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_SQUARE_PICOMETER  => [
                    function ($x) { return $x * 1e-18; },
                    function ($x) { return $x * 1e+18; },
                ],
                self::UNIT_SQUARE_NANOMETER  => [
                    function ($x) { return $x * 1e-24; },
                    function ($x) { return $x * 1e+24; },
                ],
                self::UNIT_SQUARE_DECAMETER  => [
                    function ($x) { return $x * 1e+2; },
                    function ($x) { return $x * 1e-2; },
                ],
                self::UNIT_SQUARE_HECTOMETER => [
                    function ($x) { return $x * 1e+4; },
                    function ($x) { return $x * 1e-4; },
                ],
                self::UNIT_SQUARE_KILOMETER  => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_SQUARE_MEGAMETER  => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_SQUARE_GIGAMETER  => [
                    function ($x) { return $x * 1e+18; },
                    function ($x) { return $x * 1e-18; },
                ],
                self::UNIT_SQUARE_TERAMETER  => [
                    function ($x) { return $x * 1e+24; },
                    function ($x) { return $x * 1e-24; },
                ],
                self::UNIT_SQUARE_INCH       => [
                    function ($x) { return $x * 0.00064516; },
                    function ($x) { return $x * 1550.0031; },
                ],
                self::UNIT_SQUARE_FOOT       => [
                    function ($x) { return $x * 0.0929030401; },
                    function ($x) { return $x * 10.7639104; },
                ],
                self::UNIT_SQUARE_YARD       => [
                    function ($x) { return $x * 0.8361273574; },
                    function ($x) { return $x * 1.19599005; },
                ],
                self::UNIT_SQUARE_MILE       => [
                    function ($x) { return $x * 2589988.11035241; },
                    function ($x) { return $x * 3.8610215854e-7; },
                ],
                self::UNIT_ACRE              => [
                    function ($x) { return $x * 4046.8564300507; },
                    function ($x) { return $x * 0.0002471053; },
                ],
                // Binary units
                self::UNIT_BIT               => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_CRUMB             => [
                    function ($x) { return $x * 2; },
                    function ($x) { return $x * 0.5; },
                ],
                self::UNIT_NIBBLE            => [
                    function ($x) { return $x * 4; },
                    function ($x) { return $x * 0.25; },
                ],
                self::UNIT_KILOBIT           => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_MEGABIT           => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_GIGABIT           => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_TERABIT           => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_PETABIT           => [
                    function ($x) { return $x * 1e+15; },
                    function ($x) { return $x * 1e-15; },
                ],
                self::UNIT_EXABIT            => [
                    function ($x) { return $x * 1e+18; },
                    function ($x) { return $x * 1e-18; },
                ],
                self::UNIT_ZETTABIT          => [
                    function ($x) { return $x * 1e+21; },
                    function ($x) { return $x * 1e-21; },
                ],
                self::UNIT_YOTTABIT          => [
                    function ($x) { return $x * 1e+24; },
                    function ($x) { return $x * 1e-24; },
                ],
                self::UNIT_BYTE              => [
                    function ($x) { return $x * 8; },
                    function ($x) { return $x * 0.125; },
                ],
                self::UNIT_OCTET             => [
                    function ($x) { return $x * 8; },
                    function ($x) { return $x * 0.125; },
                ],
                self::UNIT_KILOBYTE          => [
                    function ($x) { return $x * 8192; },
                    function ($x) { return $x * 1 / 8192; },
                ],
                self::UNIT_MEGABYTE          => [
                    function ($x) { return $x * 8388608; },
                    function ($x) { return $x * 1 / 8388608; },
                ],
                self::UNIT_GIGABYTE          => [
                    function ($x) { return $x * 8589934592; },
                    function ($x) { return $x * 1 / 8589934592; },
                ],
                self::UNIT_TERABYTE          => [
                    function ($x) { return $x * 8796093025955; },
                    function ($x) { return $x * 1 / 8796093025955; },
                ],
                self::UNIT_PETABYTE          => [
                    function ($x) { return $x * 9.00719925469726e+15; },
                    function ($x) { return $x * 1.11022302463055e-16; },
                ],
                self::UNIT_EXABYTE           => [
                    function ($x) { return $x * 9.22337203688447e+18; },
                    function ($x) { return $x * 1.08420217248201e-19; },
                ],
                self::UNIT_KILOBYTE_SI       => [
                    function ($x) { return $x * 8e+3; },
                    function ($x) { return $x * 0.125e-3; },
                ],
                self::UNIT_MEGABYTE_SI       => [
                    function ($x) { return $x * 8e+6; },
                    function ($x) { return $x * 0.125e-6; },
                ],
                self::UNIT_GIGABYTE_SI       => [
                    function ($x) { return $x * 8e+9; },
                    function ($x) { return $x * 0.125e-9; },
                ],
                self::UNIT_TERABYTE_SI       => [
                    function ($x) { return $x * 8e+12; },
                    function ($x) { return $x * 0.125e-12; },
                ],
                self::UNIT_PETABYTE_SI       => [
                    function ($x) { return $x * 8e+15; },
                    function ($x) { return $x * 0.125e-15; },
                ],
                self::UNIT_EXABYTE_SI        => [
                    function ($x) { return $x * 8e+18; },
                    function ($x) { return $x * 0.125e-18; },
                ],
                // Distance units
                self::UNIT_METER             => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_DECIMETER         => [
                    function ($x) { return $x * 1e-1; },
                    function ($x) { return $x * 1e+1; },
                ],
                self::UNIT_CENTIMETER        => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_MILLIMETER        => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_MICROMETER        => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_PICOMETER         => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_NANOMETER         => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_DECAMETER         => [
                    function ($x) { return $x * 1e+1; },
                    function ($x) { return $x * 1e-1; },
                ],
                self::UNIT_HECTOMETER        => [
                    function ($x) { return $x * 1e+2; },
                    function ($x) { return $x * 1e-2; },
                ],
                self::UNIT_KILOMETER         => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_MEGAMETER         => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_GIGAMETER         => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_TERAMETER         => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_INCH              => [
                    function ($x) { return $x * 0.0254; },
                    function ($x) { return $x * 39.3700787; },
                ],
                self::UNIT_FOOT              => [
                    function ($x) { return $x * 0.3048; },
                    function ($x) { return $x * 3.280839895; },
                ],
                self::UNIT_YARD              => [
                    function ($x) { return $x * 0.9143999986; },
                    function ($x) { return $x * 1.0936133; },
                ],
                self::UNIT_MILE              => [
                    function ($x) { return $x * 1609.3440006146; },
                    function ($x) { return $x * 0.0006213711; },
                ],
                // Mass units
                self::UNIT_GRAM              => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_DECIGRAM          => [
                    function ($x) { return $x * 1e-1; },
                    function ($x) { return $x * 1e+1; },
                ],
                self::UNIT_CENTIGRAM         => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_MILLIGRAM         => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_MICROGRAM         => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_PICOGRAM          => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_NANOGRAM          => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_DECAGRAM          => [
                    function ($x) { return $x * 1e+1; },
                    function ($x) { return $x * 1e-1; },
                ],
                self::UNIT_HECTOGRAM         => [
                    function ($x) { return $x * 1e+2; },
                    function ($x) { return $x * 1e-2; },
                ],
                self::UNIT_KILOGRAM          => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_MEGAGRAM          => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_GIGAGRAM          => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_TERAGRAM          => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_POUND             => [
                    function ($x) { return $x * 453.5923703803; },
                    function ($x) { return $x * 0.0022046226; },
                ],
                self::UNIT_CARAT             => [
                    function ($x) { return $x * 0.2; },
                    function ($x) { return $x * 5; },
                ],
                self::UNIT_OUNCE             => [
                    function ($x) { return $x * 28.3495231648; },
                    function ($x) { return $x * 0.0352739619; },
                ],
                self::UNIT_TON_LONG          => [
                    function ($x) { return $x * 1016046.90839848; },
                    function ($x) { return $x * 9.84206528e-7; },
                ],
                self::UNIT_TON_SHORT         => [
                    function ($x) { return $x * 907184.740760757; },
                    function ($x) { return $x * 1.10231131e-6; },
                ],
                self::UNIT_TON_METRIC        => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                // Pressure units
                self::UNIT_PASCAL            => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_DECIPASCAL        => [
                    function ($x) { return $x * 1e-1; },
                    function ($x) { return $x * 1e+1; },
                ],
                self::UNIT_CENTIPASCAL       => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_MILLIPASCAL       => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_MICROPASCAL       => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_PICOPASCAL        => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_NANOPASCAL        => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_DECAPASCAL        => [
                    function ($x) { return $x * 1e+1; },
                    function ($x) { return $x * 1e-1; },
                ],
                self::UNIT_HECTOPASCAL       => [
                    function ($x) { return $x * 1e+2; },
                    function ($x) { return $x * 1e-2; },
                ],
                self::UNIT_KILOPASCAL        => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_MEGAPASCAL        => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_GIGAPASCAL        => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_TERAPASCAL        => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_PSI               => [
                    function ($x) { return $x * 6894.7572803431; },
                    function ($x) { return $x * 0.0001450377; },
                ],
                self::UNIT_BAR               => [
                    function ($x) { return $x * 1e+5; },
                    function ($x) { return $x * 1e-5; },
                ],
                // Temperature units
                self::UNIT_KELVIN            => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_CELSIUS           => [
                    function ($x) { return $x + 273.15; },
                    function ($x) { return $x - 273.15; },
                ],
                self::UNIT_FAHRENHEIT        => [
                    function ($x) { return ($x + 459.67) * 5 / 9; },
                    function ($x) { return $x * 1.8 - 459.67; },
                ],
                self::UNIT_RANKINE           => [
                    function ($x) { return $x * 5 / 9; },
                    function ($x) { return $x * 1.8; },
                ],
                self::UNIT_DELISLE           => [
                    function ($x) { return 373.15 - $x * 2 / 3; },
                    function ($x) { return (373.15 - $x) * 1.5; },
                ],
                self::UNIT_NEWTON            => [
                    function ($x) { return $x * 100 / 33 + 273.15; },
                    function ($x) { return ($x - 273.15) * 0.33; },
                ],
                self::UNIT_REAUMUR           => [
                    function ($x) { return $x * 1.25 + 273.15; },
                    function ($x) { return ($x - 273.15) * 0.8; },
                ],
                self::UNIT_ROMER             => [
                    function ($x) { return ($x - 7.5) * 40 / 21 + 273.15; },
                    function ($x) { return ($x - 273.15) * 0.525 + 7.5; },
                ],
                // Time units
                self::UNIT_SECOND            => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_DECISECOND        => [
                    function ($x) { return $x * 1e-1; },
                    function ($x) { return $x * 1e+1; },
                ],
                self::UNIT_CENTISECOND       => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_MILLISECOND       => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_MICROSECOND       => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_PICOSECOND        => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_NANOSECOND        => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_DECASECOND        => [
                    function ($x) { return $x * 1e+1; },
                    function ($x) { return $x * 1e-1; },
                ],
                self::UNIT_HECTOSECOND       => [
                    function ($x) { return $x * 1e+2; },
                    function ($x) { return $x * 1e-2; },
                ],
                self::UNIT_KILOSECOND        => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_MEGASECOND        => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_GIGASECOND        => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_TERASECOND        => [
                    function ($x) { return $x * 1e+12; },
                    function ($x) { return $x * 1e-12; },
                ],
                self::UNIT_MINUTE            => [
                    function ($x) { return $x * 6e+1; },
                    function ($x) { return $x / 6e+1; },
                ],
                self::UNIT_HOUR              => [
                    function ($x) { return $x * 36e+2; },
                    function ($x) { return $x / 36e+2; },
                ],
                self::UNIT_DAY               => [
                    function ($x) { return $x * 864e+2; },
                    function ($x) { return $x / 864e+2; },
                ],
                self::UNIT_WEEK              => [
                    function ($x) { return $x * 6048e+2; },
                    function ($x) { return $x / 6048e+2; },
                ],
                self::UNIT_MONTH_AVG         => [
                    function ($x) { return $x * 26298e+2; },
                    function ($x) { return $x / 26298e+2; },
                ],
                self::UNIT_MONTH_28          => [
                    function ($x) { return $x * 24192e+2; },
                    function ($x) { return $x / 24192e+2; },
                ],
                self::UNIT_MONTH_29          => [
                    function ($x) { return $x * 25056e+2; },
                    function ($x) { return $x / 25056e+2; },
                ],
                self::UNIT_MONTH_30          => [
                    function ($x) { return $x * 2592e+3; },
                    function ($x) { return $x / 2592e+3; },
                ],
                self::UNIT_MONTH_31          => [
                    function ($x) { return $x * 26784e+2; },
                    function ($x) { return $x / 26784e+2; },
                ],
                self::UNIT_QUARTER           => [
                    function ($x) { return $x * 78894e+2; },
                    function ($x) { return $x / 78894e+2; },
                ],
                self::UNIT_YEAR              => [
                    function ($x) { return $x * 315576e+2; },
                    function ($x) { return $x / 315576e+2; },
                ],
                self::UNIT_DECADE            => [
                    function ($x) { return $x * 315576e+3; },
                    function ($x) { return $x / 315576e+3; },
                ],
                self::UNIT_CENTURY           => [
                    function ($x) { return $x * 315576e+4; },
                    function ($x) { return $x / 315576e+4; },
                ],
                self::UNIT_MILLENNIUM        => [
                    function ($x) { return $x * 315576e+5; },
                    function ($x) { return $x / 315576e+5; },
                ],
                // Volume units
                self::UNIT_CUBIC_METER       => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_CUBIC_DECIMETER   => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_CUBIC_CENTIMETER  => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_CUBIC_MILLIMETER  => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_CUBIC_MICROMETER  => [
                    function ($x) { return $x * 1e-18; },
                    function ($x) { return $x * 1e+18; },
                ],
                self::UNIT_CUBIC_PICOMETER   => [
                    function ($x) { return $x * 1e-27; },
                    function ($x) { return $x * 1e+27; },
                ],
                self::UNIT_CUBIC_NANOMETER   => [
                    function ($x) { return $x * 1e-36; },
                    function ($x) { return $x * 1e+36; },
                ],
                self::UNIT_CUBIC_DECAMETER   => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_CUBIC_HECTOMETER  => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_CUBIC_KILOMETER   => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_CUBIC_MEGAMETER   => [
                    function ($x) { return $x * 1e+18; },
                    function ($x) { return $x * 1e-18; },
                ],
                self::UNIT_CUBIC_GIGAMETER   => [
                    function ($x) { return $x * 1e+27; },
                    function ($x) { return $x * 1e-27; },
                ],
                self::UNIT_CUBIC_TERAMETER   => [
                    function ($x) { return $x * 1e+36; },
                    function ($x) { return $x * 1e-36; },
                ],
                self::UNIT_CUBIC_INCH        => [
                    function ($x) { return $x * 1.63870639985854e-5; },
                    function ($x) { return $x * 61023.7441; },
                ],
                self::UNIT_CUBIC_FOOT        => [
                    function ($x) { return $x * 0.0283168466; },
                    function ($x) { return $x * 35.3146667; },
                ],
                self::UNIT_CUBIC_YARD        => [
                    function ($x) { return $x * 0.7645548575; },
                    function ($x) { return $x * 1.30795062; },
                ],
                self::UNIT_CUBIC_MILE        => [
                    function ($x) { return $x * 4168181818.12498; },
                    function ($x) { return $x * 2.39912759e-10; },
                ],
                self::UNIT_LITER             => [
                    function ($x) { return $x * 1e-3; },
                    function ($x) { return $x * 1e+3; },
                ],
                self::UNIT_DECILITER         => [
                    function ($x) { return $x * 1e-4; },
                    function ($x) { return $x * 1e+4; },
                ],
                self::UNIT_CENTILITER        => [
                    function ($x) { return $x * 1e-5; },
                    function ($x) { return $x * 1e+5; },
                ],
                self::UNIT_MILLILITER        => [
                    function ($x) { return $x * 1e-6; },
                    function ($x) { return $x * 1e+6; },
                ],
                self::UNIT_MICROLITER        => [
                    function ($x) { return $x * 1e-9; },
                    function ($x) { return $x * 1e+9; },
                ],
                self::UNIT_NANOLITER         => [
                    function ($x) { return $x * 1e-12; },
                    function ($x) { return $x * 1e+12; },
                ],
                self::UNIT_PICOLITER         => [
                    function ($x) { return $x * 1e-15; },
                    function ($x) { return $x * 1e+15; },
                ],
                self::UNIT_DECALITER         => [
                    function ($x) { return $x * 1e-2; },
                    function ($x) { return $x * 1e+2; },
                ],
                self::UNIT_HECTOLITER        => [
                    function ($x) { return $x * 10; },
                    function ($x) { return $x * 0.1; },
                ],
                self::UNIT_KILOLITER         => [
                    function ($x) { return $x; },
                    function ($x) { return $x; },
                ],
                self::UNIT_MEGALITER         => [
                    function ($x) { return $x * 1e+3; },
                    function ($x) { return $x * 1e-3; },
                ],
                self::UNIT_GIGALITER         => [
                    function ($x) { return $x * 1e+6; },
                    function ($x) { return $x * 1e-6; },
                ],
                self::UNIT_TERALITER         => [
                    function ($x) { return $x * 1e+9; },
                    function ($x) { return $x * 1e-9; },
                ],
                self::UNIT_FLUID_OUNCE       => [
                    function ($x) { return $x * 2.95735295641119e-5; },
                    function ($x) { return $x * 33814.0227; },
                ],
                self::UNIT_PINT              => [
                    function ($x) { return $x * 0.0004731764; },
                    function ($x) { return $x * 2113.37642; },
                ],
                self::UNIT_CUP               => [
                    function ($x) { return $x * 0.0002365882; },
                    function ($x) { return $x * 4226.75284; },
                ],
                self::UNIT_CUP_METRIC        => [
                    function ($x) { return $x * 0.00025; },
                    function ($x) { return $x * 4000; },
                ],
                self::UNIT_QUART             => [
                    function ($x) { return $x * 0.0009463529; },
                    function ($x) { return $x * 1056.68821; },
                ],
                self::UNIT_GALLON            => [
                    function ($x) { return $x * 0.0037854117; },
                    function ($x) { return $x * 264.172052; },
                ],
                self::UNIT_UK_FLUID_OUNCE    => [
                    function ($x) { return $x * 2.84130624998822e-5; },
                    function ($x) { return $x * 35195.079728; },
                ],
                self::UNIT_UK_PINT           => [
                    function ($x) { return $x * 0.0005682612; },
                    function ($x) { return $x * 1759.7539864; },
                ],
                self::UNIT_UK_QUART          => [
                    function ($x) { return $x * 0.0011365224; },
                    function ($x) { return $x * 879.8769932; },
                ],
                self::UNIT_UK_GALLON         => [
                    function ($x) { return $x * 0.0045460899; },
                    function ($x) { return $x * 219.9692483; },
                ],
            ];
            // @codingStandardsIgnoreEnd
        }

        if (!isset($this->units[$fromUnit])
            || !isset($this->units[$toUnit])
            || floor($fromUnit / 100) !== floor($toUnit / 100)
        ) {
            throw new InvalidArgumentException('Invalid unit conversion requested');
        }

        $fx = $this->units[$fromUnit][0];
        $gx = $this->units[$toUnit][1];

        return $gx($fx($x));
    }


    /**
     * Compute the cosine value of a number
     *
     * @param int|float $x The number
     *
     * @return float The cosine value
     */
    public function cos($x)
    {
        return cos($x);
    }


    /**
     * Compute the hyperbolic cosine value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic cosine value
     */
    public function cosh($x)
    {
        return cosh($x);
    }


    /**
     * Compute the cotangent value of a number
     *
     * @param int|float $x The number
     *
     * @return float The cotangent value
     */
    public function cot($x)
    {
        return 1 / tan($x);
    }


    /**
     * Compute the hyperbolic cotangent value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic cotangent value
     */
    public function coth($x)
    {
        return 1 / tanh($x);
    }


    /**
     * Compute the cosecant value of a number
     *
     * @param int|float $x The number
     *
     * @return float The cosecant value
     */
    public function csc($x)
    {
        return 1 / sin($x);
    }


    /**
     * Compute the hyperbolic cosecant value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic cosecant value
     */
    public function csch($x)
    {
        return 1 / sinh($x);
    }


    /**
     * Compute the exponential (euler's number) value of a number
     *
     * @param int|float $x The number
     *
     * @return float The exponential value
     */
    public function exp($x)
    {
        return exp($x);
    }


    /**
     * Compute exp(x)-1 accurately for small values of x
     *
     * @param int|float $x The number
     *
     * @return float The exponential value
     */
    public function expm1($x)
    {
        return expm1($x);
    }


    /**
     * Compute the factorial of a number
     *
     * @param int $x The number
     *
     * @return int The factorial value
     */
    public function factorial($x)
    {
        if ($x <= 1) {
            return 1;
        }
        while ($x > 1) {
            $x *= --$x;
        }
        return $x;
    }


    /**
     * Compute the floor value of a number
     *
     * @param int|float $x The number
     * @param int $precision The number of decimals for rounding
     *
     * @return float The floor value
     */
    public function floor($x, $precision = 0)
    {
        $factor = pow(10, -1 * $precision);
        return floor($x / $factor) * $factor;
    }


    /**
     * Compute the length of the hypotenuse of a right-angle triangle
     *
     * @param int|float $x One of the triangle's sides
     * @param int|float $y The other side of the triangle
     *
     * @return float The hypotenuse length
     */
    public function hypot($x, $y)
    {
        return hypot($x, $y);
    }


    /**
     * Compute the inverse of a number
     *
     * @param int|float $x The number
     *
     * @return int|float The inverse value
     */
    public function inverse($x)
    {
        return 1 / $x;
    }


    /**
     * Check if an integer is even
     *
     * @param int $x The number
     *
     * @return bool True if the number is even, else false
     */
    public function isEven($x)
    {
        return ((int)$x & 1) ? false : true;
    }


    /**
     * Check if a value is a finite number
     *
     * @param int $x The value
     *
     * @return bool True if the value is a finite number, else false
     */
    public function isFinite($x)
    {
        return is_finite($x);
    }


    /**
     * Check if a value is infinite
     *
     * @param int $x The value
     *
     * @return bool True if the value is infinite, else false
     */
    public function isInfinite($x)
    {
        return is_infinite($x);
    }


    /**
     * Check if a value is not a number
     *
     * @param int $x The value
     *
     * @return bool True if the value is not a number, else false
     */
    public function isNAN($x)
    {
        return is_nan($x);
    }


    /**
     * Check if an integer is odd
     *
     * @param int $x The number
     *
     * @return bool True if the number is odd, else false
     */
    public function isOdd($x)
    {
        return ($x & 1) ? true : false;
    }


    /**
     * Compute the logarithm of a number
     *
     * @param int|float $x
     * @param int|float $base
     *
     * @return float The logarithm value
     */
    public function log($x, $base = self::E)
    {
        return log($x, $base);
    }


    /**
     * Compute the natural logarithm log(1+x) accurately for small values of x
     *
     * @param int|float $x The number
     *
     * @return int|float The natural logarithm value
     */
    public function log1p($x)
    {
        return log1p($x);
    }


    /**
     * Identify the maximum value
     *
     * Note: Unlimited OPTIONAL number of additional arguments if not providing an array
     *
     * @param mixed $v An array to get the max value of an array, or the first value of a list of arguments
     *
     * @return mixed The max value
     */
    public function max($v)
    {
        if (is_array($v)) {
            return call_user_func_array('max', $v);
        }
        return call_user_func_array('max', func_get_args());
    }


    /**
     * Identify the minimum value
     *
     * Note: Unlimited OPTIONAL number of additional arguments if not providing an array
     *
     * @param mixed $v An array to get the min value of an array, or the first value of a list of arguments
     *
     * @return mixed The min value
     */
    public function min($v)
    {
        if (is_array($v)) {
            return call_user_func_array('min', $v);
        }
        return call_user_func_array('min', func_get_args());
    }


    /**
     * Compute the modulus of a division
     *
     * @param int|float $x The dividend parameter
     * @param int|float $y The divisor parameter
     *
     * @return float The modulus
     */
    public function mod($x, $y)
    {
        return fmod($x, $y);
    }


    /**
     * Get the number of possible permutations
     *
     * @param int|float $min The minimum value of the random number
     * @param int|float $max The maximum value of the random number
     * @param int $n The number of items
     * @param int $precision The number of decimals of the random number
     * @param bool $unique Whether all value should be unique or allowed to repeat
     *
     * @return int|null The number of permutations possible or null if not enough unique values exist
     */
    public function permutations($min, $max, $n = 1, $precision = 0, $unique = false)
    {
        $numValues = $this->valuesInRange($min, $max, $precision);

        if (!$unique) {
            return pow($numValues, $n);
        }

        if ($n > $numValues) {
            return null;
        }

        $permutations = 1;

        for ($i = $n; $i > 0; $i--) {
            $permutations *= $numValues--;
        }

        return $permutations;
    }


    /**
     * Compute a base raised to a power
     *
     * @param int|float $base The base number
     * @param int|float $power The power
     *
     * @return int|float The base raised to the given power
     */
    public function pow($base, $power)
    {
        return pow($base, $power);
    }


    /**
     * Generate random numbers
     *
     * @param int|float $min The minimum value of the random number
     * @param int|float $max The maximum value of the random number
     * @param int $n The number of items
     * @param int $precision The number of decimals of the random number
     * @param bool $unique Whether the returned numbers should be unique
     *
     * @return int|float|int[]|float[] The random number
     *
     * @throws LogicException When requesting more unique values than possibilities exist
     */
    public function random($min, $max, $n = 1, $precision = 0, $unique = false)
    {
        $rands  = [];
        $factor = pow(10, -1 * $precision);
        $fMin   = round($min / $factor);
        $fMax   = round($max / $factor);

        if ($n === 1) {
            return mt_rand($fMin, $fMax) * $factor;
        }

        if (!$unique) {
            for ($i = $n; $i > 0; $i--) {
                $rands[] = mt_rand($fMin, $fMax) * $factor;
            }

            return $rands;
        }

        $valuesInRange = $this->valuesInRange($min, $max, $precision);

        if ($n > $valuesInRange) {
            return null;
        }

        if ($n > $valuesInRange * 0.05) {
            $rands = array_rand(range($min, $max, $factor), $n);
        } else {
            for ($i = 0; $i < $n; $i++) {
                do {
                    $rand = mt_rand($fMin, $fMax) * $factor;
                } while (in_array($rand, $rands));

                $rands[] = $rand;
            }
        }

        return $rands;
    }


    /**
     * Get the maximum random number that the system can generate
     *
     * @return int The maximum possible random number
     */
    public function randomMaxNumber()
    {
        return mt_getrandmax();
    }


    /**
     * Set the seed for the random number generator
     *
     * @param int|null $seed The seed for the random number generator
     */
    public function randomSeed($seed)
    {
        mt_srand($seed);
    }


    /**
     * Compute the remainder of a division
     *
     * @param int|float $x The dividend parameter
     * @param int|float $y The divisor parameter
     *
     * @return float The remainder
     */
    public function rem($x, $y)
    {
        $z = ($x / $y);
        return ($z - (int)$z) * $y;
    }


    /**
     * Round a number
     *
     * @param int|float $x The number
     * @param int $precision The number of decimals for rounding
     * @param int $mode The mode of rounding, one of the ROUND_* constants
     *
     * @return int|float The rounded value
     */
    public function round($x, $precision = 0, $mode = self::ROUND_HALF_UP)
    {
        return round($x, $precision, $mode);
    }


    /**
     * Compute the secant value of a number
     *
     * @param int|float $x The number
     *
     * @return float The secant value
     */
    public function sec($x)
    {
        return 1 / cos($x);
    }


    /**
     * Compute the hyperbolic secant value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic secant value
     */
    public function sech($x)
    {
        return 1 / cosh($x);
    }


    /**
     * Get the sign of a number
     *
     * @param int|float $x The number
     *
     * @return int -1 if number is negative, 1 if number is positive, else 0
     */
    public function sign($x)
    {
        if ($x < 0) {
            return -1;
        } elseif ($x > 0) {
            return 1;
        } else {
            return 0;
        }
    }


    /**
     * Compute the sine value of a number
     *
     * @param int|float $x The number
     *
     * @return float The sine value
     */
    public function sin($x)
    {
        return sin($x);
    }


    /**
     * Compute the hyperbolic sine value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic sine value
     */
    public function sinh($x)
    {
        return sinh($x);
    }


    /**
     * Compute the square root value of a number
     *
     * @param int|float $x The number
     *
     * @return float The square root value
     */
    public function sqrt($x)
    {
        return sqrt($x);
    }


    /**
     * Compute the tangent value of a number
     *
     * @param int|float $x The number
     *
     * @return float The tangent value
     */
    public function tan($x)
    {
        return tan($x);
    }


    /**
     * Compute the hyperbolic tangent value of a number
     *
     * @param int|float $x The number
     *
     * @return float The hyperbolic tangent value
     */
    public function tanh($x)
    {
        return tanh($x);
    }


    /**
     * Convert a number in radians to degrees
     *
     * @param int|float $x The number measured in radians
     *
     * @return float The number in degrees
     */
    public function toDeg($x)
    {
        return rad2deg($x);
    }


    /**
     * Convert a number in degrees to radians
     *
     * @param int|float $x The number measured in degrees
     *
     * @return float The number in radians
     */
    public function toRad($x)
    {
        return deg2rad($x);
    }


    /**
     * Truncate a number (round towards zero)
     *
     * @param int|float $x The number
     * @param int $precision The number of decimals for rounding
     *
     * @return int|float The truncated value
     */
    public function truncate($x, $precision = 0)
    {
        $factor = pow(10, -1 * $precision);
        return (int)($x / $factor) * $factor;
    }


    /**
     * Get the number of unique values within a range
     *
     * @param int|float $min The minimum value
     * @param int|float $max The maximum value
     * @param int $precision The number of decimals
     *
     * @return int|null The number of unique values
     */
    public function valuesInRange($min, $max, $precision = 0)
    {
        return (($max - $min) * pow(10, $precision)) + 1;
    }
}

<?php
/**
 * Class Imagick
 *
 * @package Nucleus\Library
 * @subpackage ImageEditor
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\ImageEditor;

/**
 * Class Imagick
 *
 * @package Nucleus\Library
 * @subpackage ImageEditor
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Imagick extends \Imagick
{
    const X_ALIGN_CENTER = 2;
    const X_ALIGN_LEFT   = 1;
    const X_ALIGN_NONE   = 0;
    const X_ALIGN_RIGHT  = 3;
    const Y_ALIGN_BOTTOM = 3;
    const Y_ALIGN_MIDDLE = 2;
    const Y_ALIGN_NONE   = 0;
    const Y_ALIGN_TOP    = 1;


    /**
     * Constructor
     *
     * @param string|array $files The path to an image to load or an array of paths.
     * Paths can include wildcards for file names, or can be URLs.
     */
    public function __construct($files)
    {
        parent::__construct($files);

        $this->setImageBackgroundColor('#000000');
        $this->setImageCompression($this::COMPRESSION_JPEG);
        $this->setImageCompressionQuality(80);
        $this->setImageFormat('jpeg');
        //$this->flattenImages();
    }


    /**
     * Adds borders using this instance's options
     *
     * @param int $minWidth The minimum width of the final image
     * @param int $minHeight The minimum height of the final image
     * @param string $fillColor The color of the borders that will be added
     */
    public function autoBorderImage($minWidth, $minHeight, $fillColor)
    {
        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        if ($imageWidth < $minWidth || $imageHeight < $minHeight) {
            $borderWidth  = 0;
            $borderHeight = 0;

            // Calculate borders and add 1 so we are certain not to be short on size
            if ($imageWidth < $minWidth) {
                $borderWidth = (($minWidth - $imageWidth) / 2) + 1;
            }
            if ($imageHeight < $minHeight) {
                $borderHeight = (($minHeight - $imageHeight) / 2) + 1;
            }

            // Add the borders
            $this->borderImage($borderWidth, $borderHeight, $fillColor);

            // Get the new dimensions
            $cropWidth  = $this->getImageWidth();
            $cropHeight = $this->getImageHeight();

            // Reduce the borders back to minimum dimensions if we had to go over by 1
            if ($cropWidth > $minWidth) {
                $cropWidth = $minWidth;
            }
            if ($cropHeight > $minHeight) {
                $cropHeight = $minHeight;
            }

            // Crop, just in case
            $this->cropImage($cropWidth, $cropHeight, 0, 0);
        }
    }


    /**
     * Crops using this instance's options
     *
     * @param int $maxWidth The maximum width of the final image
     * @param int $maxHeight The maximum height of the final image
     * @param int $xCropAlign [optional] The horizontal alignment of the crop (use class constants)
     * @param int $yCropAlign [optional] The vertical alignment of the crop (use class constants)
     */
    public function autoCropImage($maxWidth, $maxHeight, $xCropAlign = 2, $yCropAlign = 2)
    {
        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        $cropX      = 0;
        $cropY      = 0;
        $cropWidth  = $imageWidth;
        $cropHeight = $imageHeight;

        if ($xCropAlign && $imageWidth > $maxWidth) {
            $cropWidth = $maxWidth;

            switch ($xCropAlign) {
                case $this::X_ALIGN_CENTER:
                    $cropX = round(($imageWidth / 2) - ($cropWidth / 2));
                    break;
                case $this::X_ALIGN_RIGHT:
                    $cropX = $imageWidth - $cropWidth;
                    break;
            }
        }

        if ($yCropAlign && $imageHeight > $maxHeight) {
            $cropHeight = $maxHeight;

            switch ($yCropAlign) {
                case $this::Y_ALIGN_MIDDLE:
                    $cropY = round(($imageHeight / 2) - ($cropHeight / 2));
                    break;
                case $this::Y_ALIGN_BOTTOM:
                    $cropY = $imageHeight - $cropHeight;
                    break;
            }
        }

        $this->cropImage($cropWidth, $cropHeight, $cropX, $cropY);
    }


    /**
     * Adds borders to an image
     *
     * @param int $borderWidth The width of the vertical borders
     * @param int $borderHeight The height of the horizontal borders
     * @param string $fillColor The color of the borders that will be added
     *
     * @return bool True on success
     */
    public function borderImage($borderWidth, $borderHeight, $fillColor)
    {
        $imageWidth  = $this->getImageWidth() + ($borderWidth * 2);
        $imageHeight = $this->getImageHeight() + ($borderHeight * 2);

        $success = parent::borderImage($fillColor, $borderWidth, $borderHeight);
        $this->setImagePage($imageWidth, $imageHeight, 0, 0);

        return $success;
    }


    /**
     * Crops an image and sets the new canvas size
     *
     * @param int $width The new width of the image
     * @param int $height The new height of the image
     * @param int $offsetX [optional] The X offset relative to the left
     * @param int $offsetY [optional] The Y offset relative to the top
     *
     * @return bool True on success
     */
    public function cropImage($width, $height, $offsetX = 0, $offsetY = 0)
    {
        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        if ($offsetX < 0) {
            $offsetX = 0;
        }
        if ($offsetY < 0) {
            $offsetY = 0;
        }
        if ($offsetX + $width > $imageWidth) {
            $offsetX = $imageWidth - $width;
        }
        if ($offsetY + $height > $imageHeight) {
            $offsetY = $imageHeight - $height;
        }

        $success = parent::cropImage($width, $height, $offsetX, $offsetY);
        $this->setImagePage($width, $height, 0, 0);

        return $success;
    }


    /**
     * Resizes an image to the most optimal size using this instance's options
     *
     * Will also crop or add borders if necessary to respect the options' minimums and maximums
     *
     * @param int $maxWidth The maximum width
     * @param int $maxHeight The maximum height
     * @param int $minWidth The minimum width
     * @param int $minHeight The minimum height
     * @param int $xCropAlign The horizontal cropping alignment (use class constants)
     * @param int $yCropAlign The vertical cropping alignment (use class constants)
     * @param string $fillColor The color to be used for borders
     */
    public function autoResizeImage(
        $maxWidth,
        $maxHeight,
        $minWidth,
        $minHeight,
        $xCropAlign,
        $yCropAlign,
        $fillColor
    ) {
        if ($optimal = $this->getOptimalDimensions($maxWidth, $maxHeight, $minWidth, $minHeight)) {
            $this->resizeImage($optimal['width'], $optimal['height']);
            $this->autoCropImage($maxWidth, $maxHeight, $xCropAlign, $yCropAlign);
            $this->autoBorderImage($minWidth, $minHeight, $fillColor);
        }
    }


    /**
     * Resizes an image and sets the new canvas size
     *
     * @param int $newWidth The new width of the image
     * @param int $newHeight The new height of the image
     * @param int $filter [optional] Refer to the list of filter constants
     * @param float $blur [optional] The blur factor where > 1 is blurry, < 1 is sharp
     * @param bool $bestFit [optional] Optional fit parameter
     *
     * @return bool True on success
     */
    public function resizeImage($newWidth, $newHeight, $filter = null, $blur = 1.0, $bestFit = false)
    {
        if ($filter === null) {
            $filter = $this::FILTER_LANCZOS;
        }

        $success = parent::resizeImage($newWidth, $newHeight, $filter, $blur, $bestFit);
        $this->setImagePage($newWidth, $newHeight, 0, 0);

        return $success;
    }


    /**
     * Rotates an image
     *
     * @param int $angle The amount of degrees to rotate clockwise
     * @param string $fillColor The background color
     *
     * @return bool True on success
     */
    public function rotateImage($angle, $fillColor)
    {
        $success = parent::rotateimage($fillColor, $angle);

        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        $this->setImagePage($imageWidth, $imageHeight, 0, 0);

        return $success;
    }


    /**
     * Alias for writeImage
     *
     * @param string $filePath The filepath to write to or null for stdout
     * @param bool $strip Whether to strip metadata from the file before saving
     * @param int $permissions The chmod() permissions
     *
     * @return bool True on success
     */
    public function save($filePath = null, $strip = true, $permissions = 0666)
    {
        return $this->writeImage($filePath, $strip, $permissions);
    }


    /**
     * Strips, writes, and chmods an image to file
     *
     * @param string $filePath The filepath to write to or null for stdout
     * @param bool $strip Whether to strip metadata from the file before saving
     * @param int $permissions The chmod() permissions
     *
     * @return bool True on success
     */
    public function writeImage($filePath = null, $strip = true, $permissions = 0666)
    {
        if ($strip) {
            $this->stripImage();
        }

        $success = parent::writeImage($filePath);

        if ($filePath !== null) {
            @chmod($filePath, $permissions);
        }

        return $success;
    }


    /**
     * Sorting function that compares possibilities
     *
     * Note that $a - $b results in ascending order, $b - $a is descending
     *
     * @param array $a The first array element
     * @param array $b The second array element
     *
     * @return int Returns -1, 0, or 1
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function comparePossibilities($a, $b)
    {
        $aPriority = (int)$a['priority'];
        $aArea     = (int)$a['area'];
        $aCrop     = (int)$a['crop'];
        $aBorder   = (int)$a['border'];

        $bPriority = (int)$b['priority'];
        $bArea     = (int)$b['area'];
        $bCrop     = (int)$b['crop'];
        $bBorder   = (int)$b['border'];

        if ($aPriority != $bPriority) {
            return $aPriority - $bPriority;
        }

        switch ($aPriority) {
            case 1:
                // High priority, sort by area descending
                if ($aArea != $bArea) {
                    return $bArea - $aArea;
                }
                break;
            case 2:
                // Medium priority, sort by crop ascending
                if ($aCrop != $bCrop) {
                    return $aCrop - $bCrop;
                }
                if ($aArea != $bArea) {
                    return $bArea - $aArea;
                }
                break;
            case 3:
                // Low priority, sort by border ascending
                if ($aBorder != $bBorder) {
                    return $aBorder - $bBorder;
                }
                if ($aArea != $bArea) {
                    return $bArea - $aArea;
                }
                break;
        }

        return 0;
    }


    /**
     * Gets the possibilities of sizes that respect minimums and maximums
     *
     * Priorities Array
     * ----------------
     * high   (1) -> simple resize falls within the bounds
     * normal (2) -> requires cropping
     * low    (3) -> requires borders
     *
     * @param int $maxWidth The maximum width
     * @param int $maxHeight The maximum height
     * @param int $minWidth The minimum width
     * @param int $minHeight The minimum height
     *
     * @return array The array of possible sizes
     */
    private function getHeightPossibilities($maxWidth, $maxHeight, $minWidth, $minHeight)
    {
        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        $possibilities = [];

        // Start as tall as possible
        $testHeight = $imageHeight > $maxHeight ? $maxHeight : $imageHeight;

        // Register possibilities while iterating towards minimum
        for (; $testHeight >= $minHeight; $testHeight--) {
            // Get computed width from aspect ratio
            $newWidth = ceil(($testHeight / $imageHeight) * $imageWidth);

            if ($newWidth >= $minWidth && $newWidth <= $maxWidth) {
                // high priority
                $possibilities[] = [
                    'priority' => 1,
                    'width'    => $newWidth,
                    'height'   => $testHeight,
                    'area'     => $newWidth * $testHeight,
                    'crop'     => 0,
                    'border'   => 0,
                ];

            } elseif ($newWidth > $maxWidth) {
                // normal priority
                $possibilities[] = [
                    'priority' => 2,
                    'width'    => $newWidth,
                    'height'   => $testHeight,
                    'area'     => $newWidth * $testHeight,
                    'crop'     => $newWidth - $maxWidth,
                    'border'   => 0,
                ];

            } elseif ($newWidth < $minWidth) {
                // low priority
                $possibilities[] = [
                    'priority' => 3,
                    'width'    => $newWidth,
                    'height'   => $testHeight,
                    'area'     => $newWidth * $testHeight,
                    'crop'     => 0,
                    'border'   => $minWidth - $newWidth,
                ];
            }
        }

        return $possibilities;
    }


    /**
     * Gets the optimal dimensions
     *
     * @param int $maxWidth The maximum width
     * @param int $maxHeight The maximum height
     * @param int $minWidth The minimum width
     * @param int $minHeight The minimum height
     *
     * @return null|array The optimal dimensions array or null if none exist
     */
    private function getOptimalDimensions($maxWidth, $maxHeight, $minWidth, $minHeight)
    {
        $possibilities = array_merge(
            $this->getWidthPossibilities($maxWidth, $maxHeight, $minWidth, $minHeight),
            $this->getHeightPossibilities($maxWidth, $maxHeight, $minWidth, $minHeight)
        );

        if (!empty($possibilities)) {
            usort(
                $possibilities,
                function ($a, $b) {
                    return $this->comparePossibilities($a, $b);
                }
            );

            return $possibilities[0];
        }

        return [
            'width'  => $this->getImageWidth(),
            'height' => $this->getImageHeight(),
        ];
    }


    /**
     * Gets the possibilities of sizes that respect minimums and maximums
     *
     * Priorities Array
     * ----------------
     * high   (1) -> simple resize falls within the bounds
     * normal (2) -> requires cropping
     * low    (3) -> requires borders
     *
     * @param int $maxWidth The maximum width
     * @param int $maxHeight The maximum height
     * @param int $minWidth The minimum width
     * @param int $minHeight The minimum height
     *
     * @return array The array of possible sizes
     */
    private function getWidthPossibilities($maxWidth, $maxHeight, $minWidth, $minHeight)
    {
        $imageWidth  = $this->getImageWidth();
        $imageHeight = $this->getImageHeight();

        $possibilities = [];

        // Start as wide as possible
        $testWidth = $imageWidth > $maxWidth ? $maxWidth : $imageWidth;

        // Register possibilities while iterating towards minimum
        for (; $testWidth >= $minWidth; $testWidth--) {
            // Get computed height from aspect ratio
            $newHeight = ceil(($testWidth / $imageWidth) * $imageHeight);

            if ($newHeight >= $minHeight && $newHeight <= $maxHeight) {
                // high priority
                $possibilities[] = [
                    'priority' => 1,
                    'width'    => $testWidth,
                    'height'   => $newHeight,
                    'area'     => $testWidth * $newHeight,
                    'crop'     => 0,
                    'border'   => 0,
                ];

            } elseif ($newHeight > $maxHeight) {
                // normal priority
                $possibilities[] = [
                    'priority' => 2,
                    'width'    => $testWidth,
                    'height'   => $newHeight,
                    'area'     => $testWidth * $newHeight,
                    'crop'     => $newHeight - $maxHeight,
                    'border'   => 0,
                ];

            } elseif ($newHeight < $minHeight) {
                // low priority
                $possibilities[] = [
                    'priority' => 3,
                    'width'    => $testWidth,
                    'height'   => $newHeight,
                    'area'     => $testWidth * $newHeight,
                    'crop'     => 0,
                    'border'   => $minHeight - $newHeight,
                ];
            }
        }

        return $possibilities;
    }
}

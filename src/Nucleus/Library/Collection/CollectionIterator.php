<?php
/**
 * Class CollectionIterator
 *
 * @package Nucleus\Library
 * @subpackage Collection
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Collection;

/**
 * Class CollectionIterator
 *
 * @package Nucleus\Library
 * @subpackage Collection
 */
class CollectionIterator extends \ArrayIterator
{
}

<?php
/**
 * Class Collection
 *
 * @package Nucleus\Library
 * @subpackage Collection
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Collection;

use IteratorAggregate;

/**
 * Class Collection
 *
 * @package Nucleus\Library
 * @subpackage Collection
 */
class Collection implements IteratorAggregate
{
    /**
     * Internal collection data
     * @var array
     */
    protected $data = [];


    /**
     * Constructor
     *
     * @param array $data The collection data as an array
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }


    /**
     * Create a collection by matching keys with values
     *
     * @param array|Collection $keys The array or collection of keys
     * @param array|Collection $vals The array or collection of values
     *
     * @return Collection The new collection
     */
    public static function combine($keys, $vals)
    {
        return new self(array_combine($keys, $vals));
    }


    /**
     * Create a collection by assigning the same value to all keys
     *
     * @param array|Collection $keys The array or collection of keys
     * @param mixed $val The value to fill the keys with
     *
     * @return Collection The new collection
     */
    public static function fillKeys($keys, $val)
    {
        return new self(array_fill_keys($keys, $val));
    }


    /**
     * Create a collection by assigning the same value to a given number of numeric indexes
     *
     * @param mixed $val The value to fill the keys with
     * @param int $n The number of times to fill
     * @param int $startIndex An optional start index instead of the default 0
     *
     * @return Collection
     */
    public static function fillValues($val, $n, $startIndex = 0)
    {
        return new self(array_fill($startIndex, $n, $val));
    }


    /**
     * Insert a new element at the end of the collection
     *
     * @param mixed $arg1 Can be the key of a key/value pair if arg2 is given, else it's the value
     * @param mixed $arg2 The value of a key/value pair
     *
     * @return Collection The mutated collection with the added element
     */
    public function append($arg1, $arg2 = null)
    {
        $data = $this->data;
        if (func_num_args() < 2) {
            $data[] = $arg1;
        } else {
            $data[$arg1] = $arg2;
        }
        return new self($data);
    }


    /**
     * Select elements of a collection at specific positions
     *
     * params int $i, ... Unlimited number of position arguments, negative positions mean starting from the end
     *
     * @return Collection The collection subset consisting of the elements at specific positions
     */
    public function at()
    {
        $data    = [];
        $count   = count($this->data);
        $indexes = func_get_args();
        foreach ($indexes as $k => $v) {
            if ($v < 0) {
                $indexes[$k] = $count + $v;
            }
        }
        $count = 0;
        foreach ($this->data as $k => $v) {
            if (in_array($count++, $indexes, true)) {
                $data[$k] = $v;
            }
        }
        return new self($data);
    }


    /**
     * Split a collection into chunks
     *
     * @param int $size The size of each chunk
     * @param bool $preserveKeys Whether keys will be preserved
     *
     * @return Collection The collection of chunks
     */
    public function chunk($size, $preserveKeys = false)
    {
        return new self(array_chunk($this->data, $size, $preserveKeys));
    }


    /**
     * Count the recurrences for all values of the collection, or for a specific value
     *
     * @param callable $callback The optional callback as function ($k, $v) {...} where $k is the key, $v is the value,
     * and the function must return a boolean representing whether the item matched or not.
     *
     * @return int|Collection If a callback is given, the function will return the number of items for which the
     * callback returns true. If omitted or null, the function will return a collection with values as keys, and number
     * of recurrences in the original collection as values.
     */
    public function count(callable $callback = null)
    {
        if ($callback === null) {
            return new self(array_count_values($this->data));
        }
        $count = 0;
        foreach ($this->data as $k => $v) {
            if ($callback($k, $v)) {
                ++$count;
            }
        }
        return $count;
    }


    /**
     * Return all items whose key/value pairs are not in any of the passed Collection arguments' key/value pairs
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the difference by key/value pairs
     */
    public function differenceByAssoc()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_diff_assoc', array_merge([$this->data], $args)));
    }


    /**
     * Return all items whose keys are not in any of the passed Collection arguments' keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the difference by keys
     */
    public function differenceByKeys()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_diff_key', array_merge([$this->data], $args)));
    }


    /**
     * Return all items whose values are not in any of the passed Collection arguments' values
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the difference by values
     */
    public function differenceByValues()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_diff', array_merge([$this->data], $args)));
    }


    /**
     * Select the duplicate elements from a collection
     *
     * @return Collection The collection of duplicate elements
     */
    public function duplicates()
    {
        return new self(array_diff_key($this->data, array_unique($this->data)));
    }


    /**
     * Select the first n elements of a collection
     *
     * @param int $n The number of items
     *
     * @return Collection The collection subset consisting of the first n elements of a collection
     */
    public function first($n = 1)
    {
        return new self(array_slice($this->data, 0, $n, true));
    }


    /**
     * Flip a collection so that keys become values and values become keys
     *
     * @return Collection The flipped collection
     */
    public function flip()
    {
        return new self(array_flip($this->data));
    }


    /**
     * Format a collection
     *
     * Format should be an associative array of key/value pairs.
     * The returned collection will have no more or no less than the keys specified in format
     * and will initiaite unset keys to the default value of the format. If the key is already specified,
     * the value will not change.
     *
     * @param array $format The collection format as an associative array
     *
     * @return Collection The formatted collection
     */
    public function format($format)
    {
        return new self(array_intersect_key(array_replace($format, $this->data), $format));
    }


    /**
     * Get a collection value by its key
     *
     * @param int|string $key The key name
     *
     * @return mixed The value
     */
    public function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }


    /**
     * Return an iterator object to loop over the collection
     *
     * @return CollectionIterator The iterator object
     */
    public function getIterator()
    {
        return new CollectionIterator($this->data);
    }


    /**
     * Check if all collection items match the given callback
     *
     * @param callable $callback The callback as function ($k, $v) {...} where $k is the key, $v is the value, and the
     * function must return a boolean representing whether the item matched or not.
     *
     * @return bool Whether all collection items match the given callback
     */
    public function hasAll(callable $callback)
    {
        foreach ($this->data as $k => $v) {
            if (!$callback($k, $v)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Check if any of the collection items match the given callback
     *
     * @param callable $callback The callback as function ($k, $v) {...} where $k is the key, $v is the value, and the
     * function must return a boolean representing whether the item matched or not.
     *
     * @return bool Whether any of the collection items match the given callback
     */
    public function hasAny(callable $callback)
    {
        foreach ($this->data as $k => $v) {
            if ($callback($k, $v)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Check if the collection has the given key
     *
     * @param int|string $k The key
     *
     * @return bool Whether the collection has the given key
     */
    public function hasKey($k)
    {
        return array_key_exists($k, $this->data);
    }


    /**
     * Check if the collection has all the given keys
     *
     * params int|string $k, ... Unlimited number of key arguments
     *
     * @return bool Whether the collection has all the given keys
     */
    public function hasKeys()
    {
        foreach (func_get_args() as $arg) {
            if (!array_key_exists($arg, $this->data)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Check if the collection has the given value
     *
     * @param int|string $v The value
     *
     * @return bool Whether the collection has the given value
     */
    public function hasValue($v)
    {
        return in_array($v, $this->data, true);
    }


    /**
     * Check if the collection has all the given values
     *
     * params int|string $v, ... Unlimited number of value arguments
     *
     * @return bool Whether the collection has all the given values
     */
    public function hasValues()
    {
        foreach (func_get_args() as $arg) {
            if (!in_array($arg, $this->data)) {
                return false;
            }
        }
        return true;
    }


    /**
     * Insert a new element after another in the collection
     *
     * @param int|string $afterKey The key after which the new key/value pair will be added
     * @param mixed $arg1 Can be the key of a key/value pair if arg2 is given, else it's the value
     * @param mixed $arg2 The value of a key/value pair
     *
     * @return Collection The mutated collection with the added element
     */
    public function insertAfter($afterKey, $arg1, $arg2 = null)
    {
        $data = [];
        foreach ($this->data as $k => $v) {
            $data[$k] = $v;
            if ($k === $afterKey) {
                if (func_num_args() < 3) {
                    $data[] = $arg1;
                } else {
                    $data[$arg1] = $arg2;
                }
            }
        }
        return new self($data);
    }


    /**
     * Insert a new element at a specific position in the collection
     *
     * @param int $i The position at which to insert the new element
     * @param mixed $arg1 Can be the key of a key/value pair if arg2 is given, else it's the value
     * @param mixed $arg2 The value of a key/value pair
     *
     * @return Collection The mutated collection with the added element
     */
    public function insertAt($i, $arg1, $arg2 = null)
    {
        $slice = array_slice($this->data, 0, $i, true);
        if (func_num_args() < 3) {
            $slice[] = $arg1;
        } else {
            $slice[$arg1] = $arg2;
        }
        return new self($slice + $this->data);
    }


    /**
     * Insert a new element before another in the collection
     *
     * @param int|string $beforeKey The key before which the new key/value pair will be added
     * @param mixed $arg1 Can be the key of a key/value pair if arg2 is given, else it's the value
     * @param mixed $arg2 The value of a key/value pair
     *
     * @return Collection The mutated collection with the added element
     */
    public function insertBefore($beforeKey, $arg1, $arg2 = null)
    {
        $data = [];
        foreach ($this->data as $k => $v) {
            if ($k === $beforeKey) {
                if (func_num_args() < 3) {
                    $data[] = $arg1;
                } else {
                    $data[$arg1] = $arg2;
                }
            }
            $data[$k] = $v;
        }
        return new self($data);
    }


    /**
     * Return all items whose key/value pairs are in all of the passed Collection arguments' key/value pairs
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the intersection by key/value pairs
     */
    public function intersectByAssoc()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_intersect_assoc', array_merge([$this->data], $args)));
    }


    /**
     * Return all items whose keys are in all of the passed Collection arguments' keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the intersection by keys
     */
    public function intersectByKeys()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_intersect_key', array_merge([$this->data], $args)));
    }


    /**
     * Return all items whose values are in all of the passed Collection arguments' values
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to compare against
     *
     * @return Collection The collection consisting of the intersection by values
     */
    public function intersectByValues()
    {
        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_intersect', array_merge([$this->data], $args)));
    }


    /**
     * Check if the collection is empty
     *
     * @return bool Whether the collection is empty
     */
    public function isEmpty()
    {
        return $this->data === [];
    }


    /**
     * Join collection elements with a string separator
     *
     * @param string $glue The string separator
     *
     * @return string The joined string
     */
    public function join($glue = '')
    {
        return implode($glue, $this->data);
    }


    /**
     * Return first key of the collection
     *
     * @return mixed The first key
     */
    public function key()
    {
        reset($this->data);
        return key($this->data);
    }


    /**
     * Return a new collection containing the keys of the current collection
     *
     * @return Collection The collection of keys
     */
    public function keys()
    {
        return new self(array_keys($this->data));
    }


    /**
     * Return the keys of a specific value
     *
     * @param mixed $val The value to search for
     *
     * @return Collection The collection of keys having the value
     */
    public function keysOf($val)
    {
        return new self(array_keys($this->data, $val, true));
    }


    /**
     * Select the last n elements of a collection
     *
     * @param int $n The number of items
     *
     * @return Collection The collection subset consisting of the last n elements of a collection
     */
    public function last($n = 1)
    {
        return new self(array_slice($this->data, -$n, null, true));
    }


    /**
     * Returns the number of objects in the collection
     *
     * @return int The number of objects in the collection
     */
    public function length()
    {
        return count($this->data);
    }


    /**
     * Applies the callback to the elements of the collection
     *
     * @param callable $callback The callback as a function ($v) {...} that returns the modified value
     *
     * @return Collection The collection containing all the elements of the original collection after applying the
     * callback function to each one
     */
    public function map(callable $callback)
    {
        return new self(array_map($callback, $this->data));
    }


    /**
     * Applies the callback to the keys of the collection
     *
     * @param callable $callback The callback as a function ($v) {...} that returns the modified key
     *
     * @return Collection The collection containing all the elements of the original collection after applying the
     * callback function to each one
     */
    public function mapKeys(callable $callback)
    {
        return new self(array_combine(array_map($callback, array_keys($this->data)), $this->data));
    }


    /**
     * Merge with other arrays or collections, does not overwrite numeric keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to merge with
     *
     * @return Collection The collection consisting of the merged collections
     */
    public function merge()
    {
        // Note: array_merge treats string keys and numeric keys differently
        // by appending and renaming numeric keys in the final array, while
        // array_replace overwrites every key whether numeric or string.
        // Also, both functions will create keys that don't exist.

        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_merge', array_merge([$this->data], $args)));
    }


    /**
     * Merge with other arrays or collections recursively, does not overwrite numeric keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to merge with
     *
     * @return Collection The collection consisting of the merged collections
     */
    public function mergeRecursive()
    {
        // Note: array_merge treats string keys and numeric keys differently
        // by appending and renaming numeric keys in the final array, while
        // array_replace overwrites every key whether numeric or string.
        // Also, both functions will create keys that don't exist.

        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_merge_recursive', array_merge([$this->data], $args)));
    }


    /**
     * Select a subset of a collection not matching a callback
     *
     * @param callable $callback The callback as function ($k, $v) {...} where $k is the key, $v is the value, and the
     * function must return a boolean representing whether the item matched or not.
     * @param null|int $lim The maximum number of items to return
     *
     * @return Collection The collection subset consisting of the elements not matching the given callback
     */
    public function not(callable $callback, $lim = null)
    {
        $data = [];
        if ($lim !== null) {
            foreach ($this->data as $k => $v) {
                if ($lim < 1) {
                    break;
                }
                if (!$callback($k, $v)) {
                    $data[$k] = $v;
                    --$lim;
                }
            }
        } else {
            foreach ($this->data as $k => $v) {
                if (!$callback($k, $v)) {
                    $data[$k] = $v;
                }
            }
        }
        return new self($data);
    }


    /**
     * Select all elements of a collection except those at specific positions
     *
     * params int $i, ... Unlimited number of position arguments, negative positions mean starting from the end
     *
     * @return Collection The collection subset consisting of all elements except those at specific positions
     */
    public function notAt()
    {
        $data    = [];
        $count   = count($this->data);
        $indexes = func_get_args();
        foreach ($indexes as $k => $v) {
            if ($v < 0) {
                $indexes[$k] = $count + $v;
            }
        }
        $count = 0;
        foreach ($this->data as $k => $v) {
            if (!in_array($count++, $indexes, true)) {
                $data[$k] = $v;
            }
        }
        return new self($data);
    }


    /**
     * Select all but the first n elements of a collection
     *
     * @param int $n The number of items
     *
     * @return Collection The collection subset consisting of all but the first n elements of a collection
     */
    public function notFirst($n = 1)
    {
        return new self(array_slice($this->data, $n, null, true));
    }


    /**
     * Select all but the last n elements of a collection
     *
     * @param int $n The number of items
     *
     * @return Collection The collection subset consisting of all but the last n elements of a collection
     */
    public function notLast($n = 1)
    {
        return new self(array_slice($this->data, 0, -$n, true));
    }


    /**
     * Pad array on the left side to the specified length with a value
     *
     * @param int $size The size to pad if the collection is shorter
     * @param mixed $val The value to use when padding
     *
     * @return Collection The padded collection
     */
    public function padLeft($size, $val = null)
    {
        return new self(array_pad($this->data, -1 * $size, $val));
    }


    /**
     * Pad array on the right side to the specified length with a value
     *
     * @param int $size The size to pad if the collection is shorter
     * @param mixed $val The value to use when padding
     *
     * @return Collection The padded collection
     */
    public function padRight($size, $val = null)
    {
        return new self(array_pad($this->data, $size, $val));
    }


    /**
     * Return the values from a single column of the multidimensional collection
     *
     * @param mixed $column The column of values to return
     *
     * @return Collection The collection consisting of the column values
     */
    public function pluck($column)
    {
        $data = [];
        foreach ($this->data as $k => $v) {
            $data[$k] = isset($v[$column]) ? $v[$column] : null;
        }
        return new self($data);
    }


    /**
     * Insert a new element at the beginning of the collection
     *
     * When key and value are specified and the key already exists, the previous value will be overwritten
     * by the new one and it will now be at the beginning of the collection.
     *
     * @param mixed $arg1 Can be the key of a key/value pair if arg2 is given, else it's the value
     * @param mixed $arg2 The value of a key/value pair
     *
     * @return Collection The mutated collection with the added element
     */
    public function prepend($arg1, $arg2 = null)
    {
        $data = [];
        if (func_num_args() < 2) {
            $data[] = $arg1;
            $data   = array_merge($data, $this->data);
        } else {
            $data[$arg1] = $arg2;
            $data += $this->data;
        }
        return new self($data);
    }


    /**
     * Select n random elements from a collection
     *
     * @param int $n The number of items
     *
     * @return Collection The collection subset consisting of the n random elements
     */
    public function random($n = 1)
    {
        $count = count($this->data) - 1;
        $range = range(0, $count);
        for ($i = $count; $i > 0; $i--) {
            $j         = mt_rand(0, $i);
            $tmp       = $range[$i];
            $range[$i] = $range[$j];
            $range[$j] = $tmp;
        }
        $rands = array_slice($range, 0, $n);
        $data  = [];
        $keys  = array_keys($this->data);
        $vals  = array_values($this->data);
        foreach ($rands as $rand) {
            $data[$keys[$rand]] = $vals[$rand];
        }
        return new self($data);
    }


    /**
     * Set the seed for the random number generator
     *
     * @param int|null $seed The seed for the random number generator
     */
    public function randomSeed($seed)
    {
        srand($seed); // for shuffle()
        mt_srand($seed);
    }


    /**
     * Iteratively reduce the array to a single value using a callback function
     *
     * @param callable $callback The callback function
     * @param null $default If the optional initial is available, it will be used at the beginning of the process, or as
     * a final result in case the array is empty
     *
     * @return mixed The resulting value. If the collection is empty and an initial value is not passed, returns null
     */
    public function reduce(callable $callback, $default = null)
    {
        return array_reduce($this->data, $callback, $default);
    }


    /**
     * Reindex the numeric keys of the collection
     *
     * @return Collection The reindexed collection
     */
    public function reindex()
    {
        return new self(array_merge($this->data));
    }


    /**
     * Rename a key of the collection
     *
     * @param int|string $oldKey The key to rename
     * @param int|string $newKey The new key name
     *
     * @return Collection The collection with the renamed key
     */
    public function renameKey($oldKey, $newKey)
    {
        $data = [];
        foreach ($this->data as $k => $v) {
            if ($k === $oldKey) {
                $k = $newKey;
            }
            $data[$k] = $v;
        }
        return new self($data);
    }


    /**
     * Repeat the collection elements n times
     *
     * @param int $n The number of times to repeat
     *
     * @return Collection The repeated collection
     */
    public function repeat($n = 1)
    {
        $length = $n * count($this->data);
        $merge  = array_values($this->data);
        $data   = $merge;
        while (count($data) < $length) {
            $data = array_merge($data, $merge);
        }
        return new self($data);
    }


    /**
     * Merge with other arrays or collections, overwrites numeric keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to use for replacement
     *
     * @return Collection The collection with replacements applied
     */
    public function replace()
    {
        // Note: array_merge treats string keys and numeric keys differently
        // by appending and renaming numeric keys in the final array, while
        // array_replace overwrites every key whether numeric or string.
        // Also, both functions will create keys that don't exist.

        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_replace', array_merge([$this->data], $args)));
    }


    /**
     * Merge with other arrays or collections recursively, overwrites numeric keys
     *
     * params array|Collection $arg, ... Unlimited number of array or Collection arguments to use for replacement
     *
     * @return Collection The collection with replacements applied
     */
    public function replaceRecursive()
    {
        // Note: array_merge treats string keys and numeric keys differently
        // by appending and renaming numeric keys in the final array, while
        // array_replace overwrites every key whether numeric or string.
        // Also, both functions will create keys that don't exist.

        $args = func_get_args();

        foreach ($args as $k => $v) {
            if ($v instanceof Collection) {
                $args[$k] = $v->toArray();
            }
        }

        return new self(call_user_func_array('array_replace_recursive', array_merge([$this->data], $args)));
    }


    /**
     * Reverse the order of the collection
     */
    public function reverse()
    {
        return new self(array_reverse($this->data));
    }


    /**
     * Rotate the collection by taking the last element and bringing it to the front
     *
     * @param int $n The number of rotations
     *
     * @return Collection The rotated collection
     */
    public function rotate($n = 1)
    {
        $data = $this->data;
        while ($n-- > 0) {
            $tmp1 = array_slice($data, 0, -1, true);
            $tmp2 = array_slice($data, -1, null, true);
            $data = array_merge($tmp2, $tmp1);
        }
        return new self($data);
    }


    /**
     * Select a subset of a collection matching a callback
     *
     * @param callable $callback The callback as function ($k, $v) {...} where $k is the key, $v is the value, and the
     * function must return a boolean representing whether the item matched or not.
     * @param null|int $lim The maximum number of items to return
     *
     * @return Collection The collection subset consisting of the elements matching the given callback
     */
    public function select(callable $callback, $lim = null)
    {
        $data = [];
        if ($lim !== null) {
            foreach ($this->data as $k => $v) {
                if ($lim < 1) {
                    break;
                }
                if ($callback($k, $v)) {
                    $data[$k] = $v;
                    --$lim;
                }
            }
        } else {
            foreach ($this->data as $k => $v) {
                if ($callback($k, $v)) {
                    $data[$k] = $v;
                }
            }
        }
        return new self($data);
    }


    /**
     * Randomize the order of elements in the collection
     *
     * @return Collection The shuffled collection
     */
    public function shuffle()
    {
        $data = [];
        $keys = array_keys($this->data);

        shuffle($keys);

        foreach ($keys as $key) {
            $data[$key] = $this->data[$key];
        }

        return new self($data);
    }


    /**
     * Select a slice from a collection
     *
     * @param int $offset If offset is non-negative, the sequence will start at that offset in the array. If offset is
     * negative, the sequence will start that far from the end of the array.
     * @param null $length If length is given and is positive, then the sequence will have that many elements in it. If
     * length is given and is negative then the sequence will stop that many elements from the end of the array. If it
     * is omitted, then the sequence will have everything from offset up until the end of the array.
     *
     * @return Collection The collection slice
     */
    public function slice($offset = 1, $length = null)
    {
        return new self(array_slice($this->data, $offset, $length, true));
    }


    /**
     * Sort the collection according to a callback
     *
     * @param callable $callback The callback as function ($k1, $v1, $k2, $v2) {...} where $k1, $v1 are the key/value of
     * the first item, and $k2, $v2 are the key/value of the second item, return -1, 0, or 1.
     *
     * @return Collection The sorted collection
     */
    public function sort(callable $callback)
    {
        $data = $this->data;
        uksort(
            $data,
            function ($k1, $k2) use ($callback) {
                $v1 = $this->data[$k1];
                $v2 = $this->data[$k2];
                return $callback($k1, $v1, $k2, $v2);
            }
        );
        return new self($data);
    }


    /**
     * Sort the collection according to its keys
     *
     * Note that the collection will be comparing keys as strings for sorting. This is necessary for mixed keys.
     * In the sorted collection, numeric keys will be before string keys (unless some string keys start with a number).
     *
     * @param bool $reverse Whether to sort in descending order
     *
     * @return Collection The sorted collection
     */
    public function sortByKeys($reverse = false)
    {
        $data = $this->data;
        if ($reverse) {
            krsort($data, SORT_STRING);
        } else {
            ksort($data, SORT_STRING);
        }
        return new self($data);
    }


    /**
     * Sort the collection according to its values
     *
     * Note that unlike sortByKeys(), this method compares values as-is.
     *
     * @param bool $reverse Whether to sort in descending order
     *
     * @return Collection The sorted collection
     */
    public function sortByValues($reverse = false)
    {
        $data = $this->data;
        if ($reverse) {
            arsort($data);
        } else {
            asort($data);
        }
        return new self($data);
    }


    /**
     * Remove a portion of the collection and replace it with something else
     *
     * @param int $offset The offset where to start removing the portion
     * @param int $length The length of the portion to remove
     * @param array|Collection $replacement The replacement array or Collection
     *
     * @return Collection The spliced Collection
     */
    public function splice($offset, $length, $replacement)
    {
        if ($replacement instanceof Collection) {
            $replacement = $replacement->toArray();
        }

        $replacement = (array)$replacement;
        $keys1       = array_keys($this->data);
        $vals1       = array_values($this->data);
        $keys2       = array_keys($replacement);
        $vals2       = array_values($replacement);
        if (count($replacement) === 1) {
            $keys2 = $keys2[0];
            $vals2 = $vals2[0];
        }
        array_splice($keys1, $offset, $length, $keys2);
        array_splice($vals1, $offset, $length, $vals2);
        return new self(array_combine($keys1, $vals1));
    }


    /**
     * Return the collection data as a primitive array
     *
     * @return array The collection as an array
     */
    public function toArray()
    {
        return $this->data;
    }


    /**
     * Select the unique elements from a collection
     *
     * Preserves the key association for the first unique value encountered.
     *
     * @return Collection The collection of unique elements
     */
    public function uniques()
    {
        return new self(array_unique($this->data));
    }


    /**
     * Return first value of the collection
     *
     * @return mixed The first value
     */
    public function value()
    {
        return current($this->data);
    }


    /**
     * Return a new collection containing the values of the current collection
     *
     * @return Collection The collection of values
     */
    public function values()
    {
        return new self(array_values($this->data));
    }
}

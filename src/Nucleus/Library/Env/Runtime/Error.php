<?php
/**
 * Class Error
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Env\Runtime;

/**
 * Class Error
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Error
{
    /**
     * The file path where the error occurred
     * @var string
     */
    private $file;
    /**
     * The line where the error occurred
     * @var int
     */
    private $line;
    /**
     * The error message
     * @var string
     */
    private $message;
    /**
     * The error type
     * @var int
     */
    private $type;


    /**
     * Constructor
     *
     * @param int $type The error type
     * @param string $message The error message
     * @param string $file The file path where the error occurred
     * @param int $line The line where the error occurred
     */
    public function __construct($type, $message, $file, $line)
    {
        $this->type    = $type;
        $this->message = $message;
        $this->file    = $file;
        $this->line    = $line;
    }


    /**
     * Get File
     *
     * @return string The File
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Get Line
     *
     * @return int The Line
     */
    public function getLine()
    {
        return $this->line;
    }


    /**
     * Get Message
     *
     * @return string The Message
     */
    public function getMessage()
    {
        return $this->message;
    }


    /**
     * Get Type
     *
     * @return int The Type
     */
    public function getType()
    {
        return $this->type;
    }
}

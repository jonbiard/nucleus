<?php
/**
 * Class Backtrace
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Env\Runtime;

/**
 * Class Backtrace
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Backtrace
{
    /**
     * If inside a function, this lists the functions arguments.
     * If inside an included file, this lists the included file name(s).
     * @var array
     */
    private $args;
    /**
     * The current class
     * @var string
     */
    private $class;
    /**
     * The current file name
     * @var string
     */
    private $file;
    /**
     * The current function name
     * @var string
     */
    private $function;
    /**
     * The current line number
     * @var int
     */
    private $line;
    /**
     * The current object
     * @var object
     */
    private $object;
    /**
     * The current call type.
     * If a method call, "->" is returned.
     * If a static method call, "::" is returned.
     * If a function call, nothing is returned.
     * @var string
     */
    private $type;


    /**
     * Constructor
     *
     * @param string $function The current function name
     * @param int $line The current line number
     * @param string $file The current file name
     * @param string $class The current class
     * @param object $object The current object
     * @param string $type The current call type
     * @param array $args The arguments if inside a function or the included file names if inside an included file
     */
    public function __construct($function, $line, $file, $class, $object, $type, $args)
    {
        $this->function = $function;
        $this->line     = $line;
        $this->file     = $file;
        $this->class    = $class;
        $this->object   = $object;
        $this->type     = $type;
        $this->args     = $args;
    }


    /**
     * Get Args
     *
     * @return array The Args
     */
    public function getArgs()
    {
        return $this->args;
    }


    /**
     * Get Class
     *
     * @return string The Class
     */
    public function getClass()
    {
        return $this->class;
    }


    /**
     * Get File
     *
     * @return string The File
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Get Function
     *
     * @return string The Function
     */
    public function getFunction()
    {
        return $this->function;
    }


    /**
     * Get Line
     *
     * @return int The Line
     */
    public function getLine()
    {
        return $this->line;
    }


    /**
     * Get Object
     *
     * @return object The Object
     */
    public function getObject()
    {
        return $this->object;
    }


    /**
     * Get Type
     *
     * @return string The Type
     */
    public function getType()
    {
        return $this->type;
    }
}

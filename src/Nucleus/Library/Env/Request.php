<?php
/**
 * Class Request
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Env;

if (!function_exists('apache_request_headers')) {
    // @codingStandardsIgnoreStart
    function apache_request_headers() {
        $out = [];

        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === "HTTP_") {
                $key = str_replace(" ", "-", ucwords(strtolower(str_replace("_", " ", substr($key, 5)))));
            }

            $out[$key] = $value;
        }

        return $out;
    }
    // @codingStandardsIgnoreEnd
}

/**
 * Class Request
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Request
{
    /**
     * The server array in the PHP $_FILES[] superglobal format
     * @var array
     */
    private $FILES;
    /**
     * The server array in the PHP $_GET[] superglobal format
     * @var array
     */
    private $GET;
    /**
     * The server array in the PHP $_POST[] superglobal format
     * @var array
     */
    private $POST;
    /**
     * The server array in the PHP $_SERVER[] superglobal format
     * @var array
     */
    private $SERVER;
    /**
     * The cached processed POST superglobal data
     * @var array
     */
    private $processedPost;


    /**
     * Constructor
     *
     * @param array $server The server array in the PHP $_SERVER[] superglobal format
     * @param array $get The get array in the PHP $_GET[] superglobal format
     * @param array $post The post array in the PHP $_POST[] superglobal format
     * @param array $files The files array in the PHP $_FILES[] superglobal format
     */
    public function __construct(array $server, array $get, array $post, array $files)
    {
        $this->SERVER = $server;
        $this->GET    = $get;
        $this->POST   = $post;
        $this->FILES  = $files;

        $this->processedPost = null;
    }


    /**
     * Get number of command line arguments
     *
     * @return int The number of command line arguments
     */
    public function getArgc()
    {
        return $this->SERVER['argc'];
    }


    /**
     * Get command line argument values
     *
     * @return array The command line arguments
     */
    public function getArgv()
    {
        return $this->SERVER['argv'];
    }


    /**
     * Get the document root
     *
     * @return string The document root
     */
    public function getDocumentRoot()
    {
        return $this->SERVER['DOCUMENT_ROOT'];
    }


    /**
     * Get GET parameters
     *
     * @return array The HTTP request GET parameters
     */
    public function getGet()
    {
        return $this->GET;
    }


    /**
     * Get a request header item
     *
     * @param string $name The name of the header item to get
     *
     * @return string|null The header data
     */
    public function getHeader($name)
    {
        $headers = apache_request_headers();
        return isset($headers[$name]) ? $headers[$name] : null;
    }


    /**
     * Get all request headers
     *
     * @return string[] The headers as an associative array where keys are the header names, and values are the header
     * values.
     */
    public function getHeaders()
    {
        return apache_request_headers();
    }


    /**
     * Get the host URL
     *
     * @return string The host URL
     */
    public function getHostURL()
    {
        $hostURL = 'http';

        if ($this->SERVER['HTTPS'] == 'on') {
            $hostURL .= 's';
        }

        $hostURL .= '://' . $this->SERVER['SERVER_NAME'];

        if ($this->SERVER['SERVER_PORT'] != '80') {
            $hostURL .= ':' . $this->SERVER['SERVER_PORT'];
        }

        return $hostURL;
    }


    /**
     * Get the request method
     *
     * @return string The request method
     */
    public function getMethod()
    {
        if (strtoupper(PHP_SAPI) === 'CLI') {
            return 'CLI';
        }

        return strtoupper($this->SERVER['REQUEST_METHOD']);
    }


    /**
     * Get POST parameters
     *
     * @return array The HTTP request POST parameters
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getPost()
    {
        if ($this->processedPost === null) {
            // map error numbers to error messages
            $errors = [
                UPLOAD_ERR_OK         => 'No error',
                UPLOAD_ERR_INI_SIZE   => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
                UPLOAD_ERR_FORM_SIZE  => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified'
                . 'in the HTML form',
                UPLOAD_ERR_PARTIAL    => 'The uploaded file was only partially uploaded',
                UPLOAD_ERR_NO_FILE    => 'No file was uploaded',
                UPLOAD_ERR_NO_TMP_DIR => 'Missing a temporary folder',
                UPLOAD_ERR_CANT_WRITE => 'Failed to write file to disk',
                UPLOAD_ERR_EXTENSION  => 'A PHP extension stopped the file upload',
            ];

            // normalize array structure
            $norm = [];
            foreach ($this->FILES as $field => $info) {
                // this field is a single upload field
                if (!is_array($info['name'])) {
                    $norm[$field]['name'][0]     = $info['name'];
                    $norm[$field]['type'][0]     = $info['type'];
                    $norm[$field]['size'][0]     = $info['size'];
                    $norm[$field]['tmp_name'][0] = $info['tmp_name'];
                    $norm[$field]['error'][0]    = $info['error'];
                    continue;
                }

                // this field is a multi upload field
                foreach ($info as $key => $arr) {
                    foreach ($arr as $i => $val) {
                        $norm[$field][$i][$key] = $val;
                    }
                }
            }

            // merge uploads into POST data
            $post = $this->POST;
            foreach ($norm as $field => $files) {
                foreach ($files as $i => $info) {
                    // security check
                    if (!is_uploaded_file($info['tmp_name'])) {
                        continue;
                    }

                    $post[$field][$i]['name']  = $info['name'];
                    $post[$field][$i]['type']  = $info['type'];
                    $post[$field][$i]['size']  = $info['size'];
                    $post[$field][$i]['path']  = $info['tmp_name'];
                    $post[$field][$i]['errno'] = $info['error'];

                    if (isset($errors[$info['error']])) {
                        $post[$field][$i]['error'] = $errors[$info['error']];
                    } else {
                        $post[$field][$i]['error'] = 'Unknown error';
                    }
                }

                // single upload field is not an array
                if (count($post[$field]) == 1) {
                    $post[$field] = $post[$field][0];
                }
            }

            $this->processedPost = $post;
        }

        return $this->processedPost;
    }


    /**
     * Get the file path of the first executed script
     *
     * In CLI mode, this function ignores parameters and will always return the path relative to the current working
     * directory of the console.
     *
     * @param bool $absolute True for the absolute path, else the path will be relative to the document root for HTTP
     * requests.
     *
     * @param bool $pathInfo Whether to include path info for HTTP requests.
     * Eg: WITHOUT pathInfo: [...]/index.php vs. WITH pathInfo: [...]/index.php/foo/bar
     *
     * @return string The path of the first executed script
     */
    public function getScriptFilePath($absolute = true, $pathInfo = false)
    {
        $path = $pathInfo ? $this->SERVER['PHP_SELF'] : $this->SERVER['SCRIPT_NAME'];

        if ($absolute) {
            $path = $this->SERVER['DOCUMENT_ROOT'] . $path;
        }

        return $path;
    }


    /**
     * Get the request timestamp
     *
     * @param bool $micro False to get time as an int, true to get microtime as a float
     *
     * @return int|float The time of the request as an int, or the microtime as a float
     */
    public function getTimeStamp($micro = false)
    {
        if ($micro) {
            return $this->SERVER['REQUEST_TIME_FLOAT'];
        }

        return $this->SERVER['REQUEST_TIME'];
    }


    /**
     * Get the requested URI
     *
     * @return string The request URI
     */
    public function getURI()
    {
        return $this->SERVER['REQUEST_URI'];
    }


    /**
     * Get the requested full URL
     *
     * @return string The request URL
     */
    public function getURL()
    {
        return $this->getHostURL() . $this->SERVER['REQUEST_URI'];
    }


    /**
     * Check if request is an AJAX request
     *
     * @return bool True if request is an AJAX request
     */
    public function isAJAX()
    {
        if (isset($this->SERVER['HTTP_X_REQUESTED_WITH'])) {
            return strtoupper($this->SERVER['HTTP_X_REQUESTED_WITH']) === 'XMLHTTPREQUEST';
        }

        return false;
    }


    /**
     * Check if request is a CLI request
     *
     * @return bool True if request is a CLI request
     */
    public function isCLI()
    {
        return $this->getMethod() === 'CLI';
    }


    /**
     * Check if request is a DELETE request
     *
     * @return bool True if request is a DELETE request
     */
    public function isDelete()
    {
        return $this->getMethod() === 'DELETE';
    }


    /**
     * Check if request is a GET request
     *
     * @return bool True if request is a GET request
     */
    public function isGet()
    {
        return $this->getMethod() === 'GET';
    }


    /**
     * Check if request is a HTTP request
     *
     * @return bool True if request is a HTTP request
     */
    public function isHTTP()
    {
        return !$this->isCLI() && !$this->isHTTPS();
    }


    /**
     * Check if request is a HTTPS request
     *
     * @return bool True if request is a HTTPS request
     */
    public function isHTTPS()
    {
        if (isset($this->SERVER['HTTPS'])) {
            return strtoupper($this->SERVER['HTTPS']) === 'ON';
        }

        return false;
    }


    /**
     * Check if request is a HEAD request
     *
     * @return bool True if request is a HEAD request
     */
    public function isHead()
    {
        return $this->getMethod() === 'HEAD';
    }


    /**
     * Check if request is a POST request
     *
     * @return bool True if request is a POST request
     */
    public function isPost()
    {
        return $this->getMethod() === 'POST';
    }


    /**
     * Check if request is a PUT request
     *
     * @return bool True if request is a PUT request
     */
    public function isPut()
    {
        return $this->getMethod() === 'PUT';
    }
}

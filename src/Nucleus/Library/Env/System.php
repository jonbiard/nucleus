<?php
/**
 * Class Env
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Env;

/**
 * Class Env
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class System
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Get the free disk space
     *
     * @param null|string $unit The bytesize unit, one of 'kb', 'mb', 'gb', 'tb', defaults to bytes
     * @param null|string $dir Directory path of any directory located on the disk to investigate
     *
     * @return float The free disk space converted to the requested bytesize unit
     */
    public function getFreeDiskSpace($unit = null, $dir = null)
    {
        if ($dir) {
            $dir = __DIR__;
        }

        switch ($unit) {
            case ('kb'):
                $divisor = 1024;
                break;
            case ('mb'):
                $divisor = 1048576;
                break;
            case ('gb'):
                $divisor = 1073741824;
                break;
            case ('tb'):
                $divisor = 1099511627776;
                break;
            default:
                $divisor = 1;
        }

        return disk_free_space($dir) / $divisor;
    }


    /**
     * Get the current memory usage
     *
     * @param null|string $unit The bytesize unit, one of 'kb', 'mb', 'gb', defaults to bytes
     * @param bool $realUsage Set to true to get the real size of memory allocated from system. If set to false, only
     * the memory used by emalloc() is reported.
     *
     * @return float The current memory usage converted to the requested bytesize unit
     */
    public function getMemoryUsage($unit = null, $realUsage = false)
    {
        switch ($unit) {
            case ('kb'):
                $divisor = 1024;
                break;
            case ('mb'):
                $divisor = 1048576;
                break;
            case ('gb'):
                $divisor = 1073741824;
                break;
            default:
                $divisor = 1;
        }

        return memory_get_usage($realUsage) / $divisor;
    }


    /**
     * Get the peak memory usage
     *
     * @param null|string $unit The bytesize unit, one of 'kb', 'mb', 'gb', defaults to bytes
     * @param bool $realUsage Set to true to get the real size of memory allocated from system. If set to false, only
     * the memory used by emalloc() is reported.
     *
     * @return float The peak memory usage converted to the requested bytesize unit
     */
    public function getPeakMemoryUsage($unit = null, $realUsage = false)
    {
        switch ($unit) {
            case ('kb'):
                $divisor = 1024;
                break;
            case ('mb'):
                $divisor = 1048576;
                break;
            case ('gb'):
                $divisor = 1073741824;
                break;
            default:
                $divisor = 1;
        }

        return memory_get_peak_usage($realUsage) / $divisor;
    }


    /**
     * Get the average system load
     *
     * @param int $period The period in minutes for the average load, valid values are 1, 5, or 15, defaults to 1
     *
     * @return float The load for the requested time period
     */
    public function getSystemLoad($period = 1)
    {
        switch ($period) {
            case (15):
                return sys_getloadavg()[2];
            case (5):
                return sys_getloadavg()[1];
            default:
                return sys_getloadavg()[0];
        }
    }


    /**
     * Get the time in seconds that the system spent processing system processes
     *
     * @param int $who If who is 1, getrusage will be called with RUSAGE_CHILDREN
     *
     * @return float The time in seconds that the system spent processing system processes
     */
    public function getSystemResourceUsage($who = 0)
    {
        $usage = getrusage($who);
        return $usage['ru_stime.tv_sec'] + $usage['ru_stime.tv_usec'] / 1000000;
    }


    /**
     * Get the total disk space
     *
     * @param null|string $unit The bytesize unit, one of 'kb', 'mb', 'gb', 'tb', defaults to bytes
     * @param null|string $dir Directory path of any directory located on the disk to investigate
     *
     * @return float The total disk space converted to the requested bytesize unit
     */
    public function getTotalDiskSpace($unit = null, $dir = null)
    {
        if ($dir) {
            $dir = __DIR__;
        }

        switch ($unit) {
            case ('kb'):
                $divisor = 1024;
                break;
            case ('mb'):
                $divisor = 1048576;
                break;
            case ('gb'):
                $divisor = 1073741824;
                break;
            case ('tb'):
                $divisor = 1099511627776;
                break;
            default:
                $divisor = 1;
        }

        return disk_total_space($dir) / $divisor;
    }


    /**
     * Get the used disk space
     *
     * @param null|string $unit The bytesize unit, one of 'kb', 'mb', 'gb', 'tb', defaults to bytes
     * @param null|string $dir Directory path of any directory located on the disk to investigate
     *
     * @return float The used disk space converted to the requested bytesize unit
     */
    public function getUsedDiskSpace($unit = null, $dir = null)
    {
        return $this->getTotalDiskSpace($unit, $dir) - $this->getFreeDiskSpace($unit, $dir);
    }


    /**
     * Get the time in seconds that the system spent processing user logic
     *
     * @param int $who If who is 1, getrusage will be called with RUSAGE_CHILDREN
     *
     * @return float The time in seconds that the system spent processing user logic
     */
    public function getUserResourceUsage($who = 0)
    {
        $usage = getrusage($who);
        return $usage['ru_utime.tv_sec'] + $usage['ru_utime.tv_usec'] / 1000000;
    }
}

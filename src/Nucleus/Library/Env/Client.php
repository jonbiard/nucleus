<?php
/**
 * Class Client
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Env;

use Nucleus\Library\HTTP\Cookie;

/**
 * Class Client
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Client
{
    /**
     * The cookies array in the PHP $_COOKIE[] superglobal format
     * @var array
     */
    private $COOKIES;
    /**
     * The server array in the PHP $_SERVER[] superglobal format
     * @var array
     */
    private $SERVER;


    /**
     * Constructor
     *
     * @param array $server The server array in the PHP $_SERVER[] superglobal format
     * @param array $cookies The cookies array in the PHP $_COOKIE[] superglobal format
     */
    public function __construct(array $server, array $cookies)
    {
        $this->COOKIES = $cookies;
        $this->SERVER  = $server;
    }


    /**
     * Get remote address
     *
     * @return string The remote address
     */
    public function getAddress()
    {
        return $this->SERVER['REMOTE_ADDR'];
    }


    /**
     * Get a specific cookie by name
     *
     * @param int|string $name The cookie name
     *
     * @return Cookie|null The cookie as a Cookie object if it exists, else null
     */
    public function getCookie($name)
    {
        if ($this->hasCookie($name)) {
            return new Cookie($name, $this->COOKIES[$name]);
        }

        return null;
    }


    /**
     * Get all cookies
     *
     * @return Cookie[] An array of cookie objects
     */
    public function getCookies()
    {
        $cookies = [];

        foreach ($this->COOKIES as $key => $val) {
            $cookies[$key] = new Cookie($key, $val);
        }

        return $cookies;
    }


    /**
     * Get remote port
     *
     * @return int The remote port
     */
    public function getPort()
    {
        return $this->SERVER['REMOTE_PORT'];
    }


    /**
     * Get the referrer URL
     *
     * @return string The referrer URL
     */
    public function getReferrer()
    {
        return isset($this->SERVER['HTTP_REFERER']) ? $this->SERVER['HTTP_REFERER'] : null;
    }


    /**
     * Get the user agent
     *
     * @return string The user agent
     */
    public function getUserAgent()
    {
        return isset($this->SERVER['HTTP_USER_AGENT']) ? $this->SERVER['HTTP_USER_AGENT'] : null;
    }


    /**
     * Check if the client has aborted the connection
     *
     * @return bool Whether the client has aborted the connection
     */
    public function hasAborted()
    {
        return (bool)connection_aborted();
    }


    /**
     * Check if a cookie exist
     *
     * @param int|string $name The cookie name
     *
     * @return bool True if the cookie name is set, else false
     */
    public function hasCookie($name)
    {
        return array_key_exists($name, $this->COOKIES);
    }


    /**
     * Check if the client connection has timed out
     *
     * @return bool Whether the client connection has timed out
     */
    public function hasTimedOut()
    {
        return (bool)connection_timeout();
    }


    /**
     * Check if the client is connected
     *
     * @return bool Whether the client is connected
     */
    public function isConnected()
    {
        return (bool)connection_timeout();
    }
}

<?php
/**
 * Class Server
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Env;

/**
 * Class Server
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Server
{
    /**
     * The server array in the PHP $_SERVER[] superglobal format
     * @var array
     */
    private $SERVER;


    /**
     * Constructor
     *
     * @param array $server The server array in the PHP $_SERVER[] superglobal format
     */
    public function __construct(array $server)
    {
        $this->SERVER = $server;
    }


    /**
     * Get server address
     *
     * @return string The server address
     */
    public function getAddress()
    {
        return $this->SERVER['SERVER_ADDR'];
    }


    /**
     * Get server admin
     *
     * @return string The server admin
     */
    public function getAdmin()
    {
        return $this->SERVER['SERVER_ADMIN'];
    }


    /**
     * Get server CGI version
     *
     * @return string The server CGI version
     */
    public function getCGIVersion()
    {
        return $this->SERVER['GATEWAY_INTERFACE'];
    }


    /**
     * Get server name
     *
     * @return string The server name
     */
    public function getName()
    {
        return $this->SERVER['SERVER_NAME'];
    }


    /**
     * Get server port
     *
     * @return int The server port
     */
    public function getPort()
    {
        return (int)$this->SERVER['SERVER_PORT'];
    }


    /**
     * Get server protocol
     *
     * @return string The server protocol
     */
    public function getProtocol()
    {
        return $this->SERVER['SERVER_PROTOCOL'];
    }


    /**
     * Get server signature
     *
     * @return string The server signature
     */
    public function getSignature()
    {
        return $this->SERVER['SERVER_SIGNATURE'];
    }


    /**
     * Get server software
     *
     * @return string The server software
     */
    public function getSoftware()
    {
        return $this->SERVER['SERVER_SOFTWARE'];
    }
}

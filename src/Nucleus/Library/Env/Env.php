<?php
/**
 * Class Env
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Env;

/**
 * Class Env
 *
 * @package Nucleus\Library
 * @subpackage Env
 */
class Env
{
    /**
     * The client object
     * @var Client
     */
    public $client;
    /**
     * The request object
     * @var Request
     */
    public $request;
    /**
     * The runtime object
     * @var Runtime
     */
    public $runtime;
    /**
     * The server object
     * @var Server
     */
    public $server;
    /**
     * The system object
     * @var System
     */
    public $system;


    /**
     * Constructor
     *
     * @param array $server The server array in the PHP $_SERVER[] superglobal format
     * @param array $cookies The server array in the PHP $_COOKIE[] superglobal format
     * @param array $get The get array in the PHP $_GET[] superglobal format
     * @param array $post The post array in the PHP $_POST[] superglobal format
     * @param array $files The files array in the PHP $_FILES[] superglobal format
     */
    public function __construct(array $server, array $cookies, array $get, array $post, array $files)
    {
        $this->client  = new Client($server, $cookies);
        $this->request = new Request($server, $get, $post, $files);
        $this->runtime = new Runtime();
        $this->server  = new Server($server);
        $this->system  = new System();
    }
}

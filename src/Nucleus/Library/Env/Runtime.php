<?php
/**
 * Class Runtime
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Env;

use Nucleus\Library\Env\Runtime\Backtrace;
use Nucleus\Library\Env\Runtime\Error;

/**
 * Class Runtime
 *
 * @package Nucleus\Library
 * @subpackage Env
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Runtime
{
    const E_ALL               = E_ALL;
    const E_COMPILE_ERROR     = E_COMPILE_ERROR;
    const E_COMPILE_WARNING   = E_COMPILE_WARNING;
    const E_CORE_ERROR        = E_CORE_ERROR;
    const E_CORE_WARNING      = E_CORE_WARNING;
    const E_DEPRECATED        = E_DEPRECATED;
    const E_ERROR             = E_ERROR;
    const E_NOTICE            = E_NOTICE;
    const E_PARSE             = E_PARSE;
    const E_RECOVERABLE_ERROR = E_RECOVERABLE_ERROR;
    const E_STRICT            = E_STRICT;
    const E_USER_DEPRECATED   = E_USER_DEPRECATED;
    const E_USER_ERROR        = E_USER_ERROR;
    const E_USER_NOTICE       = E_USER_NOTICE;
    const E_USER_WARNING      = E_USER_WARNING;
    const E_WARNING           = E_WARNING;


    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Generate a backtrace
     *
     * @param bool $provideObject Whether or not to populate the object index
     * @param bool $provideArgs Whether or not to populate the args index
     * @param int $limit Limit the number of stack frames returned, returns all stack frames by default
     *
     * @return Backtrace[] The array of backtrace objects
     */
    public function getBackTrace($provideObject = true, $provideArgs = true, $limit = 0)
    {
        $flags = 0;

        if ($provideObject) {
            $flags |= DEBUG_BACKTRACE_PROVIDE_OBJECT;
        }

        if (!$provideArgs) {
            $flags |= DEBUG_BACKTRACE_IGNORE_ARGS;
        }

        if ($limit > 0) {
            $traces = array_shift(debug_backtrace($flags, $limit + 1));
        } else {
            $traces = array_shift(debug_backtrace($flags));
        }

        $traceObjects = [];
        $defaults     = [
            'function' => null,
            'line'     => null,
            'file'     => null,
            'class'    => null,
            'object'   => null,
            'type'     => null,
            'args'     => null,
        ];

        foreach ($traces as $trace) {
            $trace = array_merge($defaults, $trace);

            $traceObjects[] = new Backtrace(
                $trace['function'],
                $trace['line'],
                $trace['file'],
                $trace['class'],
                $trace['object'],
                $trace['type'],
                $trace['args']
            );
        }

        return $traceObjects;
    }


    /**
     * Return all defined classes in the current execution
     *
     * @return string[] The names of the defined classes in the current execution
     */
    public function getDefinedClasses()
    {
        return get_declared_classes();
    }


    /**
     * Return all defined constants in the current execution
     *
     * @return array The names of the defined constants in the current execution
     */
    public function getDefinedConstants()
    {
        return get_defined_constants();
    }


    /**
     * Return all defined functions in the current execution
     *
     * @return string[] The names of the defined functions in the current execution
     */
    public function getDefinedFunctions()
    {
        $funcs = get_defined_functions();
        return array_merge($funcs['internal'], $funcs['user']);
    }


    /**
     * Return all defined interfaces in the current execution
     *
     * @return string[] The names of the defined interfaces in the current execution
     */
    public function getDefinedInterfaces()
    {
        return get_declared_interfaces();
    }


    /**
     * Return all defined traits in the current execution
     *
     * @return string[] The names of the defined traits in the current execution
     */
    public function getDefinedTraits()
    {
        return get_declared_traits();
    }


    /**
     * Get the end of line marker for this platform
     *
     * @return string The end of line marker
     */
    public function getEOL()
    {
        return PHP_EOL;
    }


    /**
     * Return all global variables
     *
     * @return array The global variables
     */
    public function getGlobalVariables()
    {
        return $GLOBALS;
    }


    /**
     * Return all included or required files
     *
     * @return string[] The names of included or required files
     */
    public function getIncludedFiles()
    {
        return get_included_files();
    }


    /**
     * Return the last error
     *
     * @return array|null The last error with keys: type, message, file and line, or null if no error has occured yet
     */
    public function getLastError()
    {
        if (($lastError = error_get_last()) === null) {
            return null;
        }

        return new Error(
            $lastError['type'],
            $lastError['message'],
            $lastError['file'],
            $lastError['line']
        );
    }


    /**
     * Return the names of normal extensions compiled and loaded
     *
     * @return string[] The names of normal extensions compiled and loaded
     */
    public function getLoadedExtensions()
    {
        return get_loaded_extensions();
    }


    /**
     * Return the absolute filepath to the loaded php.ini file
     *
     * @return string|null The absolute filepath of the loaded php.ini file
     */
    public function getLoadedIniFile()
    {
        $result = php_ini_loaded_file();

        if ($result === false) {
            return null;
        }

        return $result;
    }


    /**
     * Return the names of Zend extensions compiled and loaded
     *
     * @return string[] The names of Zend extensions compiled and loaded
     */
    public function getLoadedZendExtensions()
    {
        return get_loaded_extensions(true);
    }


    /**
     * Return the major PHP version
     *
     * @return string The major PHP version
     */
    public function getMajorPHPVersion()
    {
        return PHP_MAJOR_VERSION;
    }


    /**
     * Return the minor PHP version
     *
     * @return string The minor PHP version
     */
    public function getMinorPHPVersion()
    {
        return PHP_MINOR_VERSION;
    }


    /**
     * Get the operating system name
     *
     * @return string The operating system name
     */
    public function getOS()
    {
        return PHP_OS;
    }


    /**
     * Return the value of a configuration option
     *
     * @param string $name The configuration option name
     *
     * @return string
     * The value of the configuration option as a string on success, or an empty string on failure or for null values
     */
    public function getOption($name)
    {
        return ini_get($name);
    }


    /**
     * Return all configuration options
     *
     * @param string|null $extension The optional extension for which to get configuration options
     * @param bool $details Whether to include global_value, local_value, and access details
     *
     * @return array The array of configuration options
     */
    public function getOptions($extension = null, $details = false)
    {
        return ini_get_all($extension, $details);
    }


    /**
     * Return the full PHP version
     *
     * @return string The full PHP version
     */
    public function getPHPVersion()
    {
        return PHP_VERSION;
    }


    /**
     * Return the PHP process ID
     *
     * @return int The PHP process ID
     */
    public function getProcessID()
    {
        return getmypid();
    }


    /**
     * Return the release PHP version
     *
     * @return string The release PHP version
     */
    public function getReleasePHPVersion()
    {
        return PHP_RELEASE_VERSION;
    }


    /**
     * Return the type of interface between the system and PHP
     *
     * @return string The type of interface between the system and PHP in lowercase
     */
    public function getSAPI()
    {
        return php_sapi_name();
    }


    /**
     * Return a list of .ini files parsed from the additional ini dir
     *
     * @return string[] The additional scanned .ini files
     */
    public function getScannedIniFiles()
    {
        if ($filelist = php_ini_scanned_files()) {
            if (strlen($filelist) > 0) {
                $files = explode(',', $filelist);

                foreach ($files as $i => $file) {
                    $files[$i] = trim($file);
                }

                return $files;
            }
        }

        return [];
    }


    /**
     * Get the build-platform's shared library suffix
     *
     * @return string The shared library suffix, such as "so" (most Unixes) or "dll" (Windows)
     */
    public function getSharedLibrarySuffix()
    {
        return PHP_SHLIB_SUFFIX;
    }


    /**
     * Return the current unix timestamp
     *
     * @return int The current unix timestamp
     */
    public function getTimeStamp()
    {
        return time();
    }


    /**
     * Check if a class has been defined
     *
     * @param string $class The class name including namespace
     *
     * @return bool Whether the class has been defined or not
     */
    public function hasDeclaredClass($class)
    {
        return class_exists($class, false);
    }


    /**
     * Check if a constant has been defined
     *
     * @param string $constant The constant name including namespace and class name if a class constant
     *
     * @return bool Whether the constant given by name has been defined
     */
    public function hasDeclaredConstant($constant)
    {
        return defined($constant);
    }


    /**
     * Check if a function has been defined
     *
     * @param string $func The function name including namespace
     *
     * @return bool Whether the function has been defined or not
     */
    public function hasDeclaredFunction($func)
    {
        return function_exists($func);
    }


    /**
     * Check if an interface has been defined
     *
     * @param string $interface The interface name including namespace
     *
     * @return bool Whether the interface has been defined or not
     */
    public function hasDeclaredInterface($interface)
    {
        return interface_exists($interface, false);
    }


    /**
     * Check if a trait has been defined
     *
     * @param string $trait The trait name including namespace
     *
     * @return bool Whether the trait has been defined or not
     */
    public function hasDeclaredTrait($trait)
    {
        return trait_exists($trait, false);
    }


    /**
     * Check if an extension is loaded
     *
     * @param string $name The extension name
     *
     * @return bool Whether the extension has been loaded or not
     */
    public function hasExtension($name)
    {
        return extension_loaded($name);
    }


    /**
     * Check if a file has been included
     *
     * @param string $filepath The file as an absolute filepath
     *
     * @return bool Whether the file has been included or not
     */
    public function hasIncludedFile($filepath)
    {
        return in_array($filepath, get_included_files());
    }


    /**
     * Check if system is running a certain PHP version
     *
     * @param string $operator The optional operator, one of <, lt, <=, le, >, gt, >=, ge, ==, =, eq, !=, <>, ne
     * @param string $version The version to check for
     *
     * @return bool True if the relationship is the one specified by the operator, false otherwise
     */
    public function hasPHPVersion($operator, $version)
    {
        return version_compare(PHP_VERSION, strtolower($version), $operator);
    }


    /**
     * Print the PHP info
     */
    public function printPHPInfo()
    {
        phpinfo();
    }


    /**
     * Register a function that will execute on system shutdown
     *
     * param callable $func The function to execute
     * param mixed $arg,... Unlimited OPTIONAL number of arguments to pass to the shutdown function
     */
    public function registerShutdownFunction()
    {
        call_user_func_array('register_shutdown_function', func_get_args());
    }


    /**
     * Restore the previous error handler function
     */
    public function restoreErrorHandler()
    {
        restore_error_handler();
    }


    /**
     * Restore the previous exception handler function
     */
    public function restoreExceptionHandler()
    {
        restore_exception_handler();
    }


    /**
     * Restore the value of a configuration option
     *
     * @param string $name The option name
     */
    public function restoreOption($name)
    {
        ini_restore($name);
    }


    /**
     * Set a user-defined error handler function
     *
     * @param callable $callback The error handler function
     * @param int $types The error types to handle with this error handler, as bitwise flags
     *
     * @return mixed A string containing the previously defined error handler (if any). If the built-in error handler
     * is used null is returned. null is also returned in case of an error such as an invalid callback. If the previous
     * error handler was a class method, this function will return an indexed array with the class and the method name.
     */
    public function setErrorHandler($callback, $types = null)
    {
        if ($types === null) {
            $types = self::E_ALL | self::E_STRICT;
        }

        return set_error_handler($callback, $types);
    }


    /**
     * Set a user-defined exception handler function
     *
     * @param callable $callback The exception handler function
     *
     * @return callable
     * The name of the previously defined exception handler, or null on error or if no previous handler was defined
     */
    public function setExceptionHandler($callback)
    {
        return set_exception_handler($callback);
    }


    /**
     * Set a configuration option at runtime
     *
     * @param string $name The option name
     * @param string $value The option value
     *
     * @return string|bool The old option value, or false on error
     */
    public function setOption($name, $value)
    {
        return ini_set($name, $value);
    }


    /**
     * End the script execution
     *
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function shutdown()
    {
        exit;
    }


    /**
     * Delay the script execution for a period of time
     *
     * @param int|float $seconds The number of seconds to sleep, with up to 6 decimal precision
     */
    public function sleep($seconds)
    {
        usleep($seconds * 1e+6);
    }


    /**
     * Delay the script execution until a given unix timestamp
     *
     * @param int|float $timestamp The timestamp when the script should resume execution
     *
     * @return bool True on success or false on failure
     */
    public function sleepUntil($timestamp)
    {
        return time_sleep_until($timestamp);
    }


    /**
     * Trigger a user error
     *
     * @param string $message The designated error message for this error limited to 1024 characters in length
     * @param int $type The designated error type for this error. One of the E_USER family of constants.
     *
     * @return bool
     */
    public function triggerError($message, $type = self::E_USER_NOTICE)
    {
        return trigger_error($message, $type);
    }
}

<?php
/**
 * Class View
 *
 * @package Nucleus\Library
 * @subpackage View
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\View;

/**
 * Class View
 *
 * @package Nucleus\Library
 * @subpackage View
 */
class View
{
    // @codingStandardsIgnoreStart
    /**
     * The templates directory
     * @var string
     */
    private $__templatesDir__;
    /**
     * Whether to capture from stdout or as a return value
     * @var bool
     */
    private $__fromStdOut__;
    // @codingStandardsIgnoreEnd


    /**
     * Constructor
     *
     * @param string $templatesDir The subpath to the template file
     * @param bool $fromStdOut Set to true when the include file outputs rather than return a value
     */
    public function __construct($templatesDir, $fromStdOut = false)
    {
        $this->__templatesDir__ = $templatesDir;
        $this->__fromStdOut__   = $fromStdOut;
    }


    /**
     * Magic getter
     *
     * Automatically escapes the data. In order to get the raw data, prefix the variable name with "raw_"
     *
     * @param string $key The key
     *
     * @return null|mixed The value of that key
     */
    public function __get($key)
    {
        $raw = false;

        if (substr($key, 0, 4) === 'raw_' && strlen($key) > 4) {
            $key = substr($key, 4);
            $raw = true;
        }

        return isset($this->$key) ? ($raw ? $this->$key : htmlspecialchars($this->$key, ENT_QUOTES)) : null;
    }


    /**
     * Magic setter
     *
     * @param string $key The key to use
     * @param mixed $val The value to set
     */
    public function __set($key, $val)
    {
        $this->$key = $val;
    }


    /**
     * Magic isset
     *
     * @param string $key The key to use
     *
     * @return bool True if key is set, false otherwise
     */
    public function __isset($key)
    {
        return isset($this->$key);
    }


    /**
     * Magic unset
     *
     * @param string $key The key to use
     */
    public function __unset($key)
    {
        unset($this->$key);
    }


    /**
     * Extract array
     *
     * Similar to PHP's extract() function
     *
     * @param array $array The array to expand
     */
    public function extract($array)
    {
        foreach ($array as $key => $val) {
            $key = (string)$key;
            $this->$key = $val;
        }
    }


    /**
     * Captures output from a template and returns it
     *
     * @param string $__path__ The path to the view
     *
     * @return string The rendered view
     */
    public function capture($__path__)
    {
        if ($this->__fromStdOut__) {
            ob_start();

            /** @noinspection PhpIncludeInspection */
            require $this->__templatesDir__ . $__path__;

            $include = ob_get_contents();
            ob_end_clean();

        } else {
            /** @noinspection PhpIncludeInspection */
            $include = require $this->__templatesDir__ . $__path__;
        }

        return $include;
    }
}

<?php
/**
 * Class LogicException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class LogicException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class LogicException extends Exception
{
}

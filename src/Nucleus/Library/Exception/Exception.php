<?php
/**
 * Class Exception
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class Exception
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class Exception extends \Exception
{
}

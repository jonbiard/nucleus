<?php
/**
 * Class RuntimeException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class RuntimeException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class RuntimeException extends Exception
{
}

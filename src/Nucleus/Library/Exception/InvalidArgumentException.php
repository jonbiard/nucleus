<?php
/**
 * Class InvalidArgumentException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class InvalidArgumentException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class InvalidArgumentException extends Exception
{
}

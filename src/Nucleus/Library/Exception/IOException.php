<?php
/**
 * Class IOException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class IOException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class IOException extends RuntimeException
{
}

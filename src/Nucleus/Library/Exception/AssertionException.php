<?php
/**
 * Class AssertionException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\Exception;

/**
 * Class AssertionException
 *
 * @package Nucleus\Library
 * @subpackage Exception
 */
class AssertionException extends Exception
{
}

<?php
/**
 * Class Pyramid
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Pyramid
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Pyramid
{
    /**
     * Edge height
     * @var float
     */
    private $edgeHeight;
    /**
     * Height
     * @var float
     */
    private $height;
    /**
     * Lateral surface area
     * @var float
     */
    private $lateralArea;
    /**
     * Number of base sides
     * @var int
     */
    private $numSides;
    /**
     * Length of one side of the base
     * @var float
     */
    private $sideLength;
    /**
     * Slant height
     * @var float
     */
    private $slantHeight;
    /**
     * Total surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * The base is always a regular polygon
     *
     * @param int|float $l The length of each side of the base
     * @param int|float $h The height
     * @param int $n The number of sides for the base
     *
     * @throws LogicException When a pyramid cannot be constructed with the values passed
     */
    public function __construct($l = 1, $h = 1, $n = 4)
    {
        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a pyramid with the specified length.");
        } elseif (!is_scalar($h) || $h <= 0) {
            throw new LogicException("Cannot create a pyramid with the specified height.");
        } elseif (!is_scalar($n) || (int)$n < 3) {
            throw new LogicException("Cannot create a pyramid with less than 3 sides.");
        }

        $n = (int)$n;

        $basePolygon = new RegularPolygon($l, $n);

        $x = deg2rad(180 / $n);

        $h2 = pow($h, 2);
        $l2 = pow($l, 2);
        $a2 = pow($basePolygon->getApothem(), 2);
        $c2 = pow($basePolygon->getCircumradius(), 2);

        $s = sqrt($h2 + $a2);
        $e = sqrt($h2 + $c2);
        $S = ($l * $n * $s) / 2;
        $V = (1 / 12) * $n * (1 / tan($x)) * $l2 * $h;

        $this->sideLength  = $l;
        $this->height      = $h;
        $this->numSides    = $n;
        $this->slantHeight = $s;
        $this->edgeHeight  = $e;
        $this->lateralArea = $S;
        $this->surfaceArea = $basePolygon->getArea() + $S;
        $this->volume      = $V;
    }


    /**
     * Get the base regular polygon shape
     *
     * @return RegularPolygon The base regular polygon shape
     */
    public function getBase()
    {
        return new RegularPolygon($this->sideLength, $this->numSides);
    }


    /**
     * Get the edge height
     *
     * @return int|float The edge height
     */
    public function getEdgeHeight()
    {
        return $this->edgeHeight;
    }


    /**
     * Get the height
     *
     * @return int|float The height
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Get the lateral area
     *
     * @return int|float The lateral area
     */
    public function getLateralArea()
    {
        return $this->lateralArea;
    }


    /**
     * Get the number of sides
     *
     * @return int The number of sides
     */
    public function getNumberOfSides()
    {
        return $this->numSides;
    }


    /**
     * Get the length of the sides
     *
     * @return int|float The length of the sides
     */
    public function getSideLength()
    {
        return $this->sideLength;
    }


    /**
     * Get the slant height
     *
     * @return int|float The slant height
     */
    public function getSlantHeight()
    {
        return $this->slantHeight;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the edge height
     *
     * The length of the base sides will become what they need to be
     * to result in the specified edge height
     *
     * @param int|float $s The edge height
     *
     * @return Pyramid New pyramid object
     */
    public function setEdgeHeight($s)
    {
        $SF = $s / $this->edgeHeight;

        $l = $this->sideLength * $SF;
        $h = $this->height * $SF;

        return new self($l, $h, $this->numSides);
    }


    /**
     * Set the height
     *
     * @param int|float $h The height
     *
     * @return Pyramid New pyramid object
     */
    public function setHeight($h)
    {
        return new self($this->sideLength, $h, $this->numSides);
    }


    /**
     * Set the lateral area
     *
     * The length of the base sides will become what they need to be
     * to result in the specified lateral area
     *
     * @param int|float $s The lateral area
     *
     * @return Pyramid New pyramid object
     */
    public function setLateralArea($s)
    {
        $SF = sqrt($s / $this->lateralArea);

        $l = $this->sideLength * $SF;
        $h = $this->height * $SF;

        return new self($l, $h, $this->numSides);
    }


    /**
     * Set the length
     *
     * @param int|float $l The length
     *
     * @return Pyramid New pyramid object
     */
    public function setLength($l)
    {
        return new self($l, $this->height, $this->numSides);
    }


    /**
     * Set the number of sides
     *
     * @param int $n The number of sides
     *
     * @return Pyramid New pyramid object
     */
    public function setNumberOfSides($n)
    {
        return new self($this->sideLength, $this->height, $n);
    }


    /**
     * Set the slant height
     *
     * The length of the base sides will become what they need to be
     * to result in the specified slant height
     *
     * @param int|float $s The slant height
     *
     * @return Pyramid New pyramid object
     */
    public function setSlantHeight($s)
    {
        $SF = $s / $this->slantHeight;

        $l = $this->sideLength * $SF;
        $h = $this->height * $SF;

        return new self($l, $h, $this->numSides);
    }


    /**
     * Set the surface area
     *
     * The length of the base sides will become what they need to be
     * to result in the specified surface area
     *
     * @param int|float $s The surface area
     *
     * @return Pyramid New pyramid object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $l = $this->sideLength * $SF;
        $h = $this->height * $SF;

        return new self($l, $h, $this->numSides);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The radius and height will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return Pyramid New pyramid object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $l = $this->sideLength * $SF;
        $h = $this->height * $SF;

        return new self($l, $h, $this->numSides);
    }
}

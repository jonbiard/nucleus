<?php
/**
 * Class Cube
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Cube
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Cube
{
    /**
     * Face diagonal
     * @var float
     */
    private $faceDiagonal;
    /**
     * Length
     * @var float
     */
    private $sideLength;
    /**
     * Solid diagonal
     * @var float
     */
    private $solidDiagonal;
    /**
     * Surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $l The length of the sides
     *
     * @throws LogicException When a cube cannot be constructed with the values passed
     */
    public function __construct($l = 1)
    {
        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a cube with the specified length.");
        }

        $this->faceDiagonal  = $l * sqrt(2);
        $this->solidDiagonal = $l * sqrt(3);
        $this->volume        = $l * $l * $l;
        $this->surfaceArea   = 6 * $l * $l;
        $this->sideLength    = $l;
    }


    /**
     * Get a face of the cube as a square
     *
     * @return Square The face of the cube
     */
    public function getFace()
    {
        return new Square($this->sideLength);
    }


    /**
     * Get the face diagonal of the cube
     *
     * @return Square The face diagonal
     */
    public function getFaceDiagonal()
    {
        return $this->faceDiagonal;
    }


    /**
     * Get the length
     *
     * @return int|float The length
     */
    public function getLength()
    {
        return $this->sideLength;
    }


    /**
     * Get the solid diagonal of the cube
     *
     * @return Square The solid diagonal
     */
    public function getSolidDiagonal()
    {
        return $this->solidDiagonal;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Get the face diagonal of the cube
     *
     * @param int|float $d The face diagonal
     *
     * @return Cube new cube object
     */
    public function setFaceDiagonal($d)
    {
        return new self($d / sqrt(2));
    }


    /**
     * Set the length
     *
     * @param int|float $l The length
     *
     * @return Cube New cube object
     */
    public function setLength($l)
    {
        return new self($l);
    }


    /**
     * Get the solid diagonal of the cube
     *
     * @param int|float $d The solid diagonal
     *
     * @return Cube new cube object
     */
    public function setSolidDiagonal($d)
    {
        return new self($d / sqrt(3));
    }


    /**
     * Set the surface area
     *
     * The length of the sides will become what it needs to be to result in the specified surface area
     *
     * @param int|float $s The surface area
     *
     * @return Cube New cube object
     */
    public function setSurfaceArea($s)
    {
        return new self(sqrt($s / 6));
    }


    /**
     * Set the volume
     *
     * The length of the sides will become what it needs to be to result in the specified volume
     *
     * @param int|float $v The volume
     *
     * @return Cube New cube object
     */
    public function setVolume($v)
    {
        return new self(pow($v, 1 / 3));
    }
}

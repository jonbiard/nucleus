<?php
/**
 * Class Rectangle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Rectangle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Rectangle
{
    /**
     * Area
     * @var float
     */
    private $area;
    /**
     * Height
     * @var float
     */
    private $height;
    /**
     * Length
     * @var float
     */
    private $length;
    /**
     * Perimeter
     * @var float
     */
    private $perimeter;


    /**
     * Constructor
     *
     * @param int|float $l The length
     * @param int|float $h The height
     *
     * @throws LogicException When a rectangle cannot be constructed with the values passed
     */
    public function __construct($l = 1, $h = 1)
    {
        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a rectangle with the specified length.");
        } elseif (!is_scalar($h) || $h <= 0) {
            throw new LogicException("Cannot create a rectangle with the specified height.");
        }

        $this->area      = $l * $h;
        $this->perimeter = ($l * 2) + ($h * 2);
        $this->length    = $l;
        $this->height    = $h;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the height
     *
     * @return int|float The height
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Get the length
     *
     * @return int|float The length
     */
    public function getLength()
    {
        return $this->length;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->perimeter;
    }


    /**
     * Set the area, maintaining the aspect ratio
     *
     * The length and height will become what they need to be to
     * result in the specified area with the same aspect ratio
     *
     * @param int|float $a The area
     *
     * @return Rectangle New rectangle object
     */
    public function setArea($a)
    {
        $SF = sqrt($a / $this->area);

        $h = $this->height * $SF;
        $l = $this->length * $SF;

        return new self($l, $h);
    }


    /**
     * Set the height
     *
     * @param int|float $h The height
     *
     * @return Rectangle New rectangle object
     */
    public function setHeight($h)
    {
        return new self($this->length, $h);
    }


    /**
     * Set the length
     *
     * @param int|float $l The length
     *
     * @return Rectangle New rectangle object
     */
    public function setLength($l)
    {
        return new self($l, $this->height);
    }


    /**
     * Set the perimeter, maintaining the aspect ratio
     *
     * The length and height will become what they need to be to
     * result in the specified perimeter with the same aspect ratio
     *
     * @param int|float $p The perimeter
     *
     * @return Rectangle New rectangle object
     */
    public function setPerimeter($p)
    {
        $SF = $p / $this->perimeter;

        $h = $this->height * $SF;
        $l = $this->length * $SF;

        return new self($l, $h);
    }
}

<?php
/**
 * Class TriangularPrism
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class TriangularPrism
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class TriangularPrism
{
    /**
     * Angle A (top angle)
     * @var float
     */
    private $angleA;
    /**
     * Angle B (right angle)
     * @var float
     */
    private $angleB;
    /**
     * Angle C (left angle)
     * @var float
     */
    private $angleC;
    /**
     * Depth
     * @var float
     */
    private $depth;
    /**
     * Side A length (bottom side)
     * @var float
     */
    private $sideA;
    /**
     * Side B length (left side)
     * @var float
     */
    private $sideB;
    /**
     * Side C length (right side)
     * @var float
     */
    private $sideC;
    /**
     * Surface Area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * Must specify 1 side and 1 angle, as well your choice of another side or angle
     *
     * <code>
     * // To specify 2 sides and 1 angle
     * new TriangularPrism(1, 60, 1);
     *
     * // To specify 1 side and 2 angles
     * new TriangularPrism(1, 60, null, 60);
     *
     * Illustration:
     *
     *    A
     *    |\
     *    | \
     *    |  \
     *  b |   \ c
     *    |    \
     *    |     \
     *    |      \
     *    |_______\
     *   C    a    B
     * </code>
     *
     * @param int|float $a The A-side length (bottom side)
     * @param int|float $C The C-angle degrees (left angle)
     * @param int|float $b The B-side length (left side)
     * @param int|float $B The B-angle degrees (right angle)
     * @param int|float $d The depth
     *
     * @throws LogicException When a triangular prism cannot be constructed with the values passed
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function __construct($a = 1, $C = 60, $b = 1, $B = 60, $d = 1)
    {
        if (!is_scalar($a) || $a <= 0) {
            throw new LogicException("Cannot create a triangular prism with the specified A side length.");
        } elseif (!is_scalar($C) || $C <= 0 || $C >= 180) {
            throw new LogicException("Cannot create a triangular prism with the specified C angle.");
        } elseif (!is_scalar($d) || $d <= 0) {
            throw new LogicException("Cannot create a triangular prism with the specified depth.");
        }

        if ($b === null) {
            // use 1 side and 2 angles
            if (!is_scalar($B) || $B <= 0 || $B + $C >= 180) {
                throw new LogicException("Cannot create a triangular prism with the specified B angle.");
            }

            $A = 180 - $B - $C;
            $c = ($a * sin(deg2rad($C))) / sin(deg2rad($A));
            $b = ($a * sin(deg2rad($B))) / sin(deg2rad($A));
        } else {
            // use 2 sides and 1 angle
            if (!is_scalar($b) || $b <= 0) {
                throw new LogicException("Cannot create a triangular prism with the specified B side length.");
            }

            $c = sqrt(pow($a, 2) + pow($b, 2) - (2 * $a * $b * cos(deg2rad($C))));
            $A = rad2deg(acos((pow($b, 2) + pow($c, 2) - pow($a, 2)) / (2 * $b * $c)));
            $B = rad2deg(acos((pow($a, 2) + pow($c, 2) - pow($b, 2)) / (2 * $a * $c)));
        }

        $baseArea = (1 / 2) * $a * $b * sin(deg2rad($C));

        $this->sideA = $a;
        $this->sideB = $b;
        $this->sideC = $c;

        $this->angleA = $A;
        $this->angleB = $B;
        $this->angleC = $C;

        $this->surfaceArea = ($baseArea * 2) + (($a + $b + $c) * $d);
        $this->volume      = $baseArea * $d;
        $this->depth       = $d;
    }


    /**
     * Get the angle A
     *
     * @return int|float The angle A
     */
    public function getAngleA()
    {
        return $this->angleA;
    }


    /**
     * Get the angle B
     *
     * @return int|float The angle B
     */
    public function getAngleB()
    {
        return $this->angleB;
    }


    /**
     * Get the angle C
     *
     * @return int|float The angle C
     */
    public function getAngleC()
    {
        return $this->angleC;
    }


    /**
     * Get the depth
     *
     * @return int|float The depth
     */
    public function getDepth()
    {
        return $this->depth;
    }


    /**
     * Get the face as a triangle
     *
     * @return Triangle New triangle object
     */
    public function getFace()
    {
        return new Triangle($this->sideA, $this->angleC, null, $this->angleB);
    }


    /**
     * Get the A side face as a rectangle
     *
     * @return Rectangle New rectangle object
     */
    public function getFaceA()
    {
        return new Rectangle($this->sideA, $this->depth);
    }


    /**
     * Get the B side face as a rectangle
     *
     * @return Rectangle New rectangle object
     */
    public function getFaceB()
    {
        return new Rectangle($this->sideB, $this->depth);
    }


    /**
     * Get the C side face as a rectangle
     *
     * @return Rectangle New rectangle object
     */
    public function getFaceC()
    {
        return new Rectangle($this->sideC, $this->depth);
    }


    /**
     * Get the length of side A
     *
     * @return int|float The length of side A
     */
    public function getSideALength()
    {
        return $this->sideA;
    }


    /**
     * Get the length of side B
     *
     * @return int|float The length of side B
     */
    public function getSideBLength()
    {
        return $this->sideB;
    }


    /**
     * Get the length of side C
     *
     * @return int|float The length of side C
     */
    public function getSideCLength()
    {
        return $this->sideC;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Rotates the triangular prism clockwise, maintaining A association at the bottom
     */
    public function rotateClockwise()
    {
        $aTmp  = $this->sideA;
        $aaTmp = $this->angleA;

        $this->sideA = $this->sideC;
        $this->sideC = $this->sideB;
        $this->sideB = $aTmp;

        $this->angleA = $this->angleC;
        $this->angleC = $this->angleB;
        $this->angleB = $aaTmp;
    }


    /**
     * Rotates the triangular prism counter clockwise, maintaining A association at the bottom
     */
    public function rotateCounterClockwise()
    {
        $aTmp  = $this->sideA;
        $aaTmp = $this->angleA;

        $this->sideA = $this->sideB;
        $this->sideB = $this->sideC;
        $this->sideC = $aTmp;

        $this->angleA = $this->angleB;
        $this->angleB = $this->angleC;
        $this->angleC = $aaTmp;
    }


    /**
     * Set the length of side A
     *
     * Side B and angle C will be used with the supplied A side length to create a new triangular prism
     *
     * @param int|float $a The length of side A
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setALength($a)
    {
        return new self($a, $this->angleC, $this->sideB, null, $this->depth);
    }


    /**
     * Set the A angle
     *
     * Sides B and C will be used with the supplied A angle to create a new triangular prism
     *
     * @param int|float $a The A angle
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setAngleA($a)
    {
        $this->rotateClockwise();

        $newTriangularPrism = $this->setAngleB($a);

        $this->rotateCounterClockwise();
        $newTriangularPrism->rotateCounterClockwise();

        return $newTriangularPrism;
    }


    /**
     * Set the B angle
     *
     * Sides A and C will be used with the supplied B angle to create a new triangular prism
     *
     * @param int|float $b The B angle
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setAngleB($b)
    {
        return new self($this->sideA, $this->angleC, null, $b, $this->depth);
    }


    /**
     * Set the C angle
     *
     * Sides A and B will be used with the supplied C angle to create a new triangular prism
     *
     * @param int|float $c The C angle
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setAngleC($c)
    {
        return new self($this->sideA, $c, null, $this->angleB, $this->depth);
    }


    /**
     * Set the length of side B
     *
     * Side A and angle C will be used with the supplied B side length to create a new triangular prism
     *
     * @param int|float $b The length of side B
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setBLength($b)
    {
        return new self($this->sideA, $this->angleC, $b, null, $this->depth);
    }


    /**
     * Set the length of side C
     *
     * Side A and angle B will be used with the supplied C side length to create a new triangular prism
     *
     * @param int|float $c The length of side C
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setCLength($c)
    {
        $newTriangularPrism = new self($c, $this->angleB, $this->sideA, null, $this->depth);
        $newTriangularPrism->rotateCounterClockwise();

        return $newTriangularPrism;
    }


    /**
     * Set the depth
     *
     * @param int|float $d The depth
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setDepth($d)
    {
        return new self($this->sideA, $this->angleC, null, $this->angleB, $d);
    }


    /**
     * Set the surface area, maintaining aspect ratio
     *
     * All sides will become what they need to be to
     * result in the specified area with the same aspect ratio
     *
     * @param int|float $a The area
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setSurfaceArea($a)
    {
        $SF = sqrt($a / $this->surfaceArea);

        $a = $this->sideA * $SF;

        return new self($a, $this->angleC, null, $this->angleB, $this->depth);
    }


    /**
     * Set the volume, maintaining aspect ratio
     *
     * All sides will become what they need to be to
     * result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return TriangularPrism New triangular prism object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $a = $this->sideA * $SF;

        return new self($a, $this->angleC, null, $this->angleB, $this->depth);
    }
}

<?php
/**
 * Class Cone
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Cone
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Cone
{
    /**
     * Height
     * @var float
     */
    private $height;
    /**
     * Lateral surface area
     * @var float
     */
    private $lateralArea;
    /**
     * Radius of the base
     * @var float
     */
    private $radius;
    /**
     * Slant height
     * @var float
     */
    private $slantHeight;
    /**
     * Total surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $r The radius of the base
     * @param int|float $h The height
     *
     * @throws LogicException When a cone cannot be constructed with the values passed
     */
    public function __construct($r = 1, $h = 1)
    {
        if (!is_scalar($r) || $r <= 0) {
            throw new LogicException("Cannot create a cone with the specified radius.");
        } elseif (!is_scalar($h) || $h <= 0) {
            throw new LogicException("Cannot create a cone with the specified height.");
        }

        $r2 = pow($r, 2);
        $h2 = pow($h, 2);
        $l  = sqrt($r2 + $h2);

        $this->volume      = 1 / 3 * M_PI * $r2 * $h;
        $this->lateralArea = (M_PI * $r * $l);
        $this->surfaceArea = (M_PI * $r2) + (M_PI * $r * $l);
        $this->slantHeight = $l;
        $this->radius      = $r;
        $this->height      = $h;
    }


    /**
     * Get the base of the cone as a circle
     *
     * @return Circle New circle object
     */
    public function getBase()
    {
        return new Circle($this->radius);
    }


    /**
     * Get the height
     *
     * @return int|float The height
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Get the lateral area
     *
     * @return int|float The lateral area
     */
    public function getLateralArea()
    {
        return $this->lateralArea;
    }


    /**
     * Get the radius
     *
     * @return int|float The radius
     */
    public function getRadius()
    {
        return $this->radius;
    }


    /**
     * Get the slant height
     *
     * @return int|float The slant height
     */
    public function getSlantHeight()
    {
        return $this->slantHeight;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the height
     *
     * @param int|float $h The height
     *
     * @return Cone New cone object
     */
    public function setHeight($h)
    {
        return new self($this->radius, $h);
    }


    /**
     * Set the lateral area, maintaining the aspect ratio
     *
     * The radius of the base and height will become what they need to be
     * to result in the specified lateral area with the same aspect ratio
     *
     * @param int|float $s The lateral area
     *
     * @return Cone New cone object
     */
    public function setLateralArea($s)
    {
        $SF = sqrt($s / $this->lateralArea);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }


    /**
     * Set the radius of the base
     *
     * @param int|float $r The radius of the base
     *
     * @return Cone New cone object
     */
    public function setRadius($r)
    {
        return new self($r, $this->height);
    }


    /**
     * Set the slant height, maintaining the aspect ratio
     *
     * The radius of the base and height will become what they need to be
     * to result in the specified slant height with the same aspect ratio
     *
     * @param int|float $s The lateral area
     *
     * @return Cone New cone object
     */
    public function setSlantHeight($s)
    {
        $SF = $s / $this->slantHeight;

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }


    /**
     * Set the surface area, maintaining the aspect ratio
     *
     * The radius of the base and height will become what they need to be
     * to result in the specified surface area with the same aspect ratio
     *
     * @param int|float $s The surface area
     *
     * @return Cone New cone object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The radius of the base and height will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return Cone New cone object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }
}

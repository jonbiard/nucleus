<?php
/**
 * Class Square
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Square
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Square
{
    /**
     * Area
     * @var float
     */
    private $area;
    /**
     * Length
     * @var float
     */
    private $length;
    /**
     * Perimeter
     * @var float
     */
    private $perimeter;


    /**
     * Constructor
     *
     * @param int|float $l The length
     *
     * @throws LogicException When a square cannot be constructed with the values passed
     */
    public function __construct($l = 1)
    {
        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a square with the specified length.");
        }

        $this->area      = $l * $l;
        $this->perimeter = $l * 4;
        $this->length    = $l;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the length
     *
     * @return int|float The length
     */
    public function getLength()
    {
        return $this->length;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->perimeter;
    }


    /**
     * Set the area
     *
     * The length of the sides will become what it needs to be to result in the specified area
     *
     * @param int|float $a The area
     *
     * @return Square New square object
     */
    public function setArea($a)
    {
        return new self(sqrt($a));
    }


    /**
     * Set the length
     *
     * @param int|float $l The length
     *
     * @return Square New square object
     */
    public function setLength($l)
    {
        return new self($l);
    }


    /**
     * Set the perimeter
     *
     * The length of the sides will become what it needs to be to result in the specified perimeter
     *
     * @param int|float $p The perimeter
     *
     * @return Square New square object
     */
    public function setPerimeter($p)
    {
        return new self($p / 4);
    }
}

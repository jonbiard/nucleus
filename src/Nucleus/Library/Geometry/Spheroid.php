<?php
/**
 * Class Spheroid
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Spheroid
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Spheroid
{
    /**
     * Eccentricity
     * @var float
     */
    private $eccentricity;
    /**
     * Semi-axis A length
     * @var float
     */
    private $semiAxisA;
    /**
     * Semi-axis C length
     * @var float
     */
    private $semiAxisC;
    /**
     * Surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $a The semi-axis A length
     * @param int|float $c The semi-axis C length
     *
     * @throws LogicException When a spheroid cannot be constructed with the values passed
     */
    public function __construct($a = 1, $c = 1)
    {
        if (!is_scalar($a) || $a <= 0) {
            throw new LogicException("Cannot create a spheroid with the specified semi-axis A.");
        } elseif (!is_scalar($c) || $c <= 0) {
            throw new LogicException("Cannot create a spheroid with the specified semi-axis C.");
        }

        $a2 = pow($a, 2);
        $c2 = pow($c, 2);

        if ($a < $c) {
            // Prolate Spheroid
            $e = sqrt(1 - ($a2 / $c2));

            $this->surfaceArea  = 2 * M_PI * $a2 * (1 + ($c / ($a * $e)) * asin($e));
            $this->eccentricity = rad2deg(acos($a / $c));
        } elseif ($a > $c) {
            // Oblate Spheroid
            $e  = sqrt(1 - ($c2 / $a2));
            $e2 = pow($e, 2);

            $this->surfaceArea  = 2 * M_PI * $a2 * (1 + ((1 - $e2) / $e) * atanh($e));
            $this->eccentricity = rad2deg(acos($c / $a));
        } else {
            // Sphere
            $this->surfaceArea  = 4 * M_PI * $a2;
            $this->eccentricity = 0;
        }

        $this->volume    = ((4 * M_PI) / 3) * $a2 * $c;
        $this->semiAxisA = $a;
        $this->semiAxisC = $c;
    }


    /**
     * Get the A-axis length
     *
     * @return int|float The A-axis length
     */
    public function getAxisALength()
    {
        return $this->semiAxisA * 2;
    }


    /**
     * Get the C-axis length
     *
     * @return int|float The C-axis length
     */
    public function getAxisCLength()
    {
        return $this->semiAxisC * 2;
    }


    /**
     * Get the eccentricity
     *
     * @return int|float The eccentricity
     */
    public function getEccentricity()
    {
        return $this->eccentricity;
    }


    /**
     * Get the semi-axis A length
     *
     * @return int|float The semi-axis A length
     */
    public function getSemiAxisALength()
    {
        return $this->semiAxisA;
    }


    /**
     * Get the semi-axis C length
     *
     * @return int|float The semi-axis C length
     */
    public function getSemiAxisCLength()
    {
        return $this->semiAxisC;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the A-axis length
     *
     * @param int|float $a The A-axis length
     *
     * @return Spheroid New spheroid object
     */
    public function setAxisALength($a)
    {
        return new self($a / 2, $this->semiAxisC);
    }


    /**
     * Set the C-axis length
     *
     * @param int|float $c The C-axis length
     *
     * @return Spheroid New spheroid object
     */
    public function setAxisCLength($c)
    {
        return new self($this->semiAxisA, $c / 2);
    }


    /**
     * Set the semi-axis A length
     *
     * @param int|float $a The semi-axis A length
     *
     * @return Spheroid New spheroid object
     */
    public function setSemiAxisALength($a)
    {
        return new self($a, $this->semiAxisC);
    }


    /**
     * Set the semi-axis C length
     *
     * @param int|float $c The semi-axis C length
     *
     * @return Spheroid New spheroid object
     */
    public function setSemiAxisCLength($c)
    {
        return new self($this->semiAxisA, $c);
    }


    /**
     * Set the surface area, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified surface area with the same aspect ratio
     *
     * @param int|float $s The surface area
     *
     * @return Spheroid New spheroid object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $a = $this->semiAxisA * $SF;
        $c = $this->semiAxisC * $SF;

        return new self($a, $c);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return Spheroid New spheroid object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $a = $this->semiAxisA * $SF;
        $c = $this->semiAxisC * $SF;

        return new self($a, $c);
    }
}

<?php
/**
 * Class Cylinder
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Cylinder
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Cylinder
{
    /**
     * Height
     * @var float
     */
    private $height;
    /**
     * Lateral surface area
     * @var float
     */
    private $lateralArea;
    /**
     * Radius
     * @var float
     */
    private $radius;
    /**
     * Total surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $r The radius
     * @param int|float $h The height
     *
     * @throws LogicException When a cylinder cannot be constructed with the values passed
     */
    public function __construct($r = 1, $h = 1)
    {
        if (!is_scalar($r) || $r <= 0) {
            throw new LogicException("Cannot create a cylinder with the specified radius.");
        } elseif (!is_scalar($h) || $h <= 0) {
            throw new LogicException("Cannot create a cylinder with the specified height.");
        }

        $r2 = pow($r, 2);

        $this->volume      = M_PI * $r2 * $h;
        $this->lateralArea = (2 * M_PI * $r * $h);
        $this->surfaceArea = (2 * (M_PI * $r2)) + (2 * M_PI * $r * $h);
        $this->radius      = $r;
        $this->height      = $h;
    }


    /**
     * Get a base of the cylinder as a circle
     *
     * @return Circle New circle object
     */
    public function getBase()
    {
        return new Circle($this->radius);
    }


    /**
     * Get the height
     *
     * @return int|float The height
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Get the lateral area
     *
     * @return int|float The lateral area
     */
    public function getLateralArea()
    {
        return $this->lateralArea;
    }


    /**
     * Get the radius
     *
     * @return int|float The radius
     */
    public function getRadius()
    {
        return $this->radius;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the height
     *
     * @param int|float $h The height
     *
     * @return Cylinder New cylinder object
     */
    public function setHeight($h)
    {
        return new self($this->radius, $h);
    }


    /**
     * Set the lateral area, maintaining the aspect ratio
     *
     * The radius and height will become what they need to be
     * to result in the specified lateral area with the same aspect ratio
     *
     * @param int|float $s The lateral area
     *
     * @return Cylinder New cylinder object
     */
    public function setLateralArea($s)
    {
        $SF = sqrt($s / $this->lateralArea);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }


    /**
     * Set the radius
     *
     * @param int|float $r The radius
     *
     * @return Cylinder New cylinder object
     */
    public function setRadius($r)
    {
        return new self($r, $this->height);
    }


    /**
     * Set the surface area, maintaining the aspect ratio
     *
     * The radius and height will become what they need to be
     * to result in the specified surface area with the same aspect ratio
     *
     * @param int|float $s The surface area
     *
     * @return Cylinder New cylinder object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The radius and height will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return Cylinder New cylinder object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $r = $this->radius * $SF;
        $h = $this->height * $SF;

        return new self($r, $h);
    }
}

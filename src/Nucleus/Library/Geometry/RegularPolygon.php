<?php
/**
 * Class RegularPolygon
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class RegularPolygon
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class RegularPolygon
{
    /**
     * Apothem
     * @var int
     */
    private $apothem;
    /**
     * Area
     * @var int
     */
    private $area;
    /**
     * Circumradius
     * @var int
     */
    private $circumradius;
    /**
     * Exterior angle
     * @var float
     */
    private $exteriorAngle;
    /**
     * Interior angle
     * @var float
     */
    private $interiorAngle;
    /**
     * Number of sides
     * @var int
     */
    private $numSides;
    /**
     * Perimeter
     * @var int
     */
    private $perimeter;
    /**
     * Side length
     * @var float
     */
    private $sideLength;


    /**
     * Constructor
     *
     * @param int|float $l The length of each side
     * @param int $n The number of sides
     *
     * @throws LogicException When a regular polygon cannot be constructed with the values passed
     */
    public function __construct($l = 1, $n = 3)
    {
        $n = (int)$n;

        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a regular polygon with the specified length.");
        } elseif (!is_scalar($n) || (int)$n < 3) {
            throw new LogicException("Cannot create a regular polygon with less than 3 sides.");
        }

        $a = $l / (2 * tan(deg2rad(180 / $n)));
        $r = $l / (2 * sin(deg2rad(180 / $n)));
        $x = (($n - 2) / $n) * 180;
        $y = 360 / $n;
        $A = ($n * $l * $a) / 2;
        $P = $n * $l;

        $this->sideLength    = $l;
        $this->numSides      = $n;
        $this->apothem       = $a;
        $this->circumradius  = $r;
        $this->area          = $A;
        $this->perimeter     = $P;
        $this->interiorAngle = $y;
        $this->exteriorAngle = $x;
    }


    /**
     * Get the apothem
     *
     * @return int|float The apothem
     */
    public function getApothem()
    {
        return $this->apothem;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the circumradius
     *
     * @return int|float The circumradius
     */
    public function getCircumradius()
    {
        return $this->circumradius;
    }


    /**
     * Get the exterior angle
     *
     * @return int|float The exterior angle
     */
    public function getExteriorAngle()
    {
        return $this->exteriorAngle;
    }


    /**
     * Get the interior angle
     *
     * @return int|float The interior angle
     */
    public function getInteriorAngle()
    {
        return $this->interiorAngle;
    }


    /**
     * Get the length of the sides
     *
     * @return int|float The length of the sides
     */
    public function getLength()
    {
        return $this->sideLength;
    }


    /**
     * Get the number of sides
     *
     * @return int The number of sides
     */
    public function getNumberOfSides()
    {
        return $this->numSides;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->perimeter;
    }


    /**
     * Get the radius (alias for circumradius)
     *
     * @return int|float The radius (alias for circumradius)
     */
    public function getRadius()
    {
        return $this->circumradius;
    }


    /**
     * Set the apothem
     *
     * @param int|float $a The apothem
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setApothem($a)
    {
        $SF = $a / $this->apothem;

        $l = $this->sideLength * $SF;

        return new self($l, $this->numSides);
    }


    /**
     * Set the area
     *
     * The length of the sides will become what it needs to be to result in the specified area
     *
     * @param int|float $a The area
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setArea($a)
    {
        $SF = sqrt($a / $this->area);

        $l = $this->sideLength * $SF;

        return new self($l, $this->numSides);
    }


    /**
     * Set the circumradius
     *
     * The length of the sides will become what it needs to be to result in the specified circumradius
     *
     * @param int|float $r The circumradius
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setCircumradius($r)
    {
        $SF = $r / $this->circumradius;

        $l = $this->sideLength * $SF;

        return new self($l, $this->numSides);
    }


    /**
     * Set the length of the sides
     *
     * @param int|float $l The length of the sides
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setLength($l)
    {
        return new self($l, $this->numSides);
    }


    /**
     * Set the number of sides
     *
     * @param int $n The number of sides
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setNumberOfSides($n)
    {
        return new self($this->sideLength, $n);
    }


    /**
     * Set the perimeter
     *
     * The length of the sides will become what it needs to be to result in the specified perimeter
     *
     * @param int|float $p The perimeter
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setPerimeter($p)
    {
        $n = $this->numSides;

        $l = $p / $n;

        return new self($l, $n);
    }


    /**
     * Set the radius (alias for circumradius)
     *
     * The length of the sides will become what it needs to be to result in the specified radius (circumradius)
     *
     * @param int|float $r The radius (alias for circumradius)
     *
     * @return RegularPolygon New regular polygon object
     */
    public function setRadius($r)
    {
        return $this->setCircumradius($r);
    }
}

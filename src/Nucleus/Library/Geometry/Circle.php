<?php
/**
 * Class Circle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Circle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Circle
{
    /**
     * Area
     * @var float
     */
    private $area;
    /**
     * Circumference
     * @var float
     */
    private $circumference;
    /**
     * Diameter
     * @var float
     */
    private $diameter;
    /**
     * Radius
     * @var float
     */
    private $radius;


    /**
     * Constructor
     *
     * @param int|float $r The radius
     *
     * @throws LogicException When a circle cannot be constructed with the values passed
     */
    public function __construct($r = 1)
    {
        if (!is_scalar($r) || $r <= 0) {
            throw new LogicException("Cannot create a circle with the specified radius.");
        }

        $r2 = pow($r, 2);

        $this->area          = M_PI * $r2;
        $this->circumference = 2 * M_PI * $r;
        $this->diameter      = 2 * $r;
        $this->radius        = $r;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the circumference
     *
     * @return int|float The circumference
     */
    public function getCircumference()
    {
        return $this->circumference;
    }


    /**
     * Get the diameter
     *
     * @return int|float The diameter
     */
    public function getDiameter()
    {
        return $this->diameter;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->circumference;
    }


    /**
     * Get the radius
     *
     * @return int|float The radius
     */
    public function getRadius()
    {
        return $this->radius;
    }


    /**
     * Set the area
     *
     * The radius will become what it needs to be to result in the specified area
     *
     * @param int|float $a The area
     *
     * @return Circle New circle object
     */
    public function setArea($a)
    {
        return new self(sqrt($a / M_PI));
    }


    /**
     * Set the circumference
     *
     * The radius will become what it needs to be to result in the specified circumference
     *
     * @param int|float $c The circumference
     *
     * @return Circle New circle object
     */
    public function setCircumference($c)
    {
        return new self($c / (2 * M_PI));
    }


    /**
     * Set the diameter
     *
     * @param int|float $d The diameter
     *
     * @return Circle New circle object
     */
    public function setDiameter($d)
    {
        return new self($d / 2);
    }


    /**
     * Set the perimeter
     *
     * The radius will become what it needs to be to result in the specified perimeter
     *
     * @param int|float $p The perimeter
     *
     * @return Circle New circle object
     */
    public function setPerimeter($p)
    {
        return new self($p / (2 * M_PI));
    }


    /**
     * Set the radius
     *
     * @param int|float $r The radius
     *
     * @return Circle New circle object
     */
    public function setRadius($r)
    {
        return new self($r);
    }
}

<?php
/**
 * Class Triangle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Triangle
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Triangle
{
    /**
     * Angle A (top angle)
     * @var float
     */
    private $angleA;
    /**
     * Angle B (right angle)
     * @var float
     */
    private $angleB;
    /**
     * Angle C (left angle)
     * @var float
     */
    private $angleC;
    /**
     * Area
     * @var float
     */
    private $area;
    /**
     * Perimeter
     * @var float
     */
    private $perimeter;
    /**
     * Side A length (bottom side)
     * @var float
     */
    private $sideA;
    /**
     * Side B length (left side)
     * @var float
     */
    private $sideB;
    /**
     * Side C length (right side)
     * @var float
     */
    private $sideC;


    /**
     * Constructor
     *
     * Must specify 1 side and 1 angle, as well your choice of another side or angle
     *
     * <code>
     * // To specify 2 sides and 1 angle
     * new Triangle(1, 60, 1);
     *
     * // To specify 1 side and 2 angles
     * new Triangle(1, 60, null, 60);
     *
     * Illustration:
     *
     *    A
     *    |\
     *    | \
     *    |  \
     *  b |   \ c
     *    |    \
     *    |     \
     *    |      \
     *    |_______\
     *   C    a    B
     * </code>
     *
     * @param int|float $a The A-side length (bottom side)
     * @param int|float $C The C-angle degrees (left angle)
     * @param int|float $b The B-side length (left side)
     * @param int|float $B The B-angle degrees (right angle)
     *
     * @throws LogicException When a triangle cannot be constructed with the values passed
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function __construct($a = 1, $C = 60, $b = 1, $B = 60)
    {
        if (!is_scalar($a) || $a <= 0) {
            throw new LogicException("Cannot create a triangle with the specified A side length.");
        } elseif (!is_scalar($C) || $C <= 0 || $C >= 180) {
            throw new LogicException("Cannot create a triangle with the specified C angle.");
        }

        if ($b === null) {
            // use 1 side and 2 angles
            if (!is_scalar($B) || $B <= 0 || $B + $C >= 180) {
                throw new LogicException("Cannot create a triangle with the specified B angle.");
            }

            $A = 180 - $B - $C;
            $c = ($a * sin(deg2rad($C))) / sin(deg2rad($A));
            $b = ($a * sin(deg2rad($B))) / sin(deg2rad($A));
        } else {
            // use 2 sides and 1 angle
            if (!is_scalar($b) || $b <= 0) {
                throw new LogicException("Cannot create a triangle with the specified B side length.");
            }

            $c = sqrt(pow($a, 2) + pow($b, 2) - (2 * $a * $b * cos(deg2rad($C))));
            $A = rad2deg(acos((pow($b, 2) + pow($c, 2) - pow($a, 2)) / (2 * $b * $c)));
            $B = rad2deg(acos((pow($a, 2) + pow($c, 2) - pow($b, 2)) / (2 * $a * $c)));
        }

        $this->sideA = $a;
        $this->sideB = $b;
        $this->sideC = $c;

        $this->angleA = $A;
        $this->angleB = $B;
        $this->angleC = $C;

        $this->area      = (1 / 2) * $a * $b * sin(deg2rad($C));
        $this->perimeter = $a + $b + $c;
    }


    /**
     * Get the angle A
     *
     * @return int|float The angle A
     */
    public function getAngleA()
    {
        return $this->angleA;
    }


    /**
     * Get the angle B
     *
     * @return int|float The angle B
     */
    public function getAngleB()
    {
        return $this->angleB;
    }


    /**
     * Get the angle C
     *
     * @return int|float The angle C
     */
    public function getAngleC()
    {
        return $this->angleC;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->perimeter;
    }


    /**
     * Get the length of side A
     *
     * @return int|float The length of side A
     */
    public function getSideALength()
    {
        return $this->sideA;
    }


    /**
     * Get the length of side B
     *
     * @return int|float The length of side B
     */
    public function getSideBLength()
    {
        return $this->sideB;
    }


    /**
     * Get the length of side C
     *
     * @return int|float The length of side C
     */
    public function getSideCLength()
    {
        return $this->sideC;
    }


    /**
     * Rotates the triangle clockwise, maintaining A association at the bottom
     */
    public function rotateClockwise()
    {
        $aTmp  = $this->sideA;
        $aaTmp = $this->angleA;

        $this->sideA = $this->sideC;
        $this->sideC = $this->sideB;
        $this->sideB = $aTmp;

        $this->angleA = $this->angleC;
        $this->angleC = $this->angleB;
        $this->angleB = $aaTmp;
    }


    /**
     * Rotates the triangle counter clockwise, maintaining A association at the bottom
     */
    public function rotateCounterClockwise()
    {
        $aTmp  = $this->sideA;
        $aaTmp = $this->angleA;

        $this->sideA = $this->sideB;
        $this->sideB = $this->sideC;
        $this->sideC = $aTmp;

        $this->angleA = $this->angleB;
        $this->angleB = $this->angleC;
        $this->angleC = $aaTmp;
    }


    /**
     * Set the length of side A
     *
     * Side B and angle C will be used with the supplied A side length to create a new triangle
     *
     * @param int|float $a The length of side A
     *
     * @return Triangle New triangle object
     */
    public function setALength($a)
    {
        return new self($a, $this->angleC, $this->sideB);
    }


    /**
     * Set the A angle
     *
     * Sides B and C will be used with the supplied A angle to create a new triangle
     *
     * @param int|float $a The A angle
     *
     * @return Triangle New triangle object
     */
    public function setAngleA($a)
    {
        $this->rotateClockwise();

        $newTriangle = $this->setAngleB($a);

        $this->rotateCounterClockwise();
        $newTriangle->rotateCounterClockwise();

        return $newTriangle;
    }


    /**
     * Set the B angle
     *
     * Sides A and C will be used with the supplied B angle to create a new triangle
     *
     * @param int|float $b The B angle
     *
     * @return Triangle New triangle object
     */
    public function setAngleB($b)
    {
        return new self($this->sideA, $this->angleC, null, $b);
    }


    /**
     * Set the C angle
     *
     * Sides A and B will be used with the supplied C angle to create a new triangle
     *
     * @param int|float $c The C angle
     *
     * @return Triangle New triangle object
     */
    public function setAngleC($c)
    {
        return new self($this->sideA, $c, null, $this->angleB);
    }


    /**
     * Set the area, maintaining aspect ratio
     *
     * All sides will become what they need to be to
     * result in the specified area with the same aspect ratio
     *
     * @param int|float $a The area
     *
     * @return Triangle New triangle object
     */
    public function setArea($a)
    {
        $SF = sqrt($a / $this->area);

        $a = $this->sideA * $SF;

        return new self($a, $this->angleC, null, $this->angleB);
    }


    /**
     * Set the length of side B
     *
     * Side A and angle C will be used with the supplied B side length to create a new triangle
     *
     * @param int|float $b The length of side B
     *
     * @return Triangle New triangle object
     */
    public function setBLength($b)
    {
        return new self($this->sideA, $this->angleC, $b);
    }


    /**
     * Set the length of side C
     *
     * Side A and angle B will be used with the supplied C side length to create a new triangle
     *
     * @param int|float $c The length of side C
     *
     * @return Triangle New triangle object
     */
    public function setCLength($c)
    {
        $newTriangle = new self($c, $this->angleB, $this->sideA);
        $newTriangle->rotateCounterClockwise();
        return $newTriangle;
    }


    /**
     * Set the perimeter, maintaining aspect ratio
     *
     * All sides will become what they need to be to
     * result in the specified perimeter with the same aspect ratio
     *
     * @param int|float $p The perimeter
     *
     * @return Triangle New triangle object
     */
    public function setPerimeter($p)
    {
        $SF = $p / $this->perimeter;

        $a = $this->sideA * $SF;

        return new self($a, $this->angleC, null, $this->angleB);
    }
}

<?php
/**
 * Class Ellipse
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Ellipse
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Ellipse
{
    /**
     * Area
     * @var float
     */
    private $area;
    /**
     * Circumference
     * @var float
     */
    private $circumference;
    /**
     * Eccentricity
     * @var float
     */
    private $eccentricity;
    /**
     * Foci distance
     * @var float
     */
    private $fociDistance;
    /**
     * Semi-axis A
     * @var float
     */
    private $semiAxisA;
    /**
     * Semi-axis B
     * @var float
     */
    private $semiAxisB;


    /**
     * Constructor
     *
     * @param int|float $a The semi-axis A length
     * @param int|float $b The semi-axis B length
     *
     * @throws LogicException When an ellipse cannot be constructed with the values passed
     *
     * @link http://www.mathsisfun.com/geometry/ellipse-perimeter.html Reference
     */
    public function __construct($a = 1, $b = 1)
    {
        if (!is_scalar($a) || $a <= 0) {
            throw new LogicException("Cannot create an ellipse with the specified semi-axis A.");
        } elseif (!is_scalar($b) || $b <= 0) {
            throw new LogicException("Cannot create an ellipse with the specified semi-axis B.");
        }

        $h  = pow($a - $b, 2) / pow($a + $b, 2);
        $h2 = pow($h, 2);
        $h3 = pow($h, 3);
        $h4 = pow($h, 4);
        $h5 = pow($h, 5);
        $h6 = pow($h, 6);
        $A2 = pow($a * 2, 2);
        $B2 = pow($b * 2, 2);

        $this->area          = M_PI * $a * $b;
        $this->circumference = M_PI * ($a + $b) *
            (
                1 +
                ($h / 4) +
                ($h2 / 64) +
                ($h3 / 256) +
                (25 * $h4 / 16384) +
                (49 * $h5 / 65536) +
                (441 * $h6 / 1048576)
            );
        $this->fociDistance  = $a >= $b ? sqrt($A2 - $B2) : sqrt($B2 - $A2);
        $this->eccentricity  = $a >= $b ? sqrt(($A2 - $B2) / $A2) : sqrt(($B2 - $A2) / $B2);
        $this->semiAxisA     = $a;
        $this->semiAxisB     = $b;
    }


    /**
     * Get the area
     *
     * @return int|float The area
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Get the axis A length
     *
     * @return int|float The axis A length
     */
    public function getAxisALength()
    {
        return $this->semiAxisA * 2;
    }


    /**
     * Get the axis B length
     *
     * @return int|float The axis B length
     */
    public function getAxisBLength()
    {
        return $this->semiAxisB * 2;
    }


    /**
     * Get the circumference
     *
     * @return int|float The circumference
     */
    public function getCircumference()
    {
        return $this->circumference;
    }


    /**
     * Get the eccentricity
     *
     * @return int|float The eccentricity
     */
    public function getEccentricity()
    {
        return $this->eccentricity;
    }


    /**
     * Get the foci distance
     *
     * @return int|float The foci distance
     */
    public function getFociDistance()
    {
        return $this->fociDistance;
    }


    /**
     * Get the perimeter
     *
     * @return int|float The perimeter
     */
    public function getPerimeter()
    {
        return $this->circumference;
    }


    /**
     * Get the semi-axis A length
     *
     * @return int|float The semi-axis A length
     */
    public function getSemiAxisALength()
    {
        return $this->semiAxisA;
    }


    /**
     * Get the semi-axis B length
     *
     * @return int|float The semi-axis B length
     */
    public function getSemiAxisBLength()
    {
        return $this->semiAxisB;
    }


    /**
     * Set the area, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified area with the same aspect ratio
     *
     * @param int|float $a The area
     *
     * @return ellipse New ellipse object
     */
    public function setArea($a)
    {
        $SF = sqrt($a / $this->area);

        $a = $this->semiAxisA * $SF;
        $b = $this->semiAxisB * $SF;

        return new self($a, $b);
    }


    /**
     * Set the axis A length
     *
     * @param int|float $a The axis A length
     *
     * @return Ellipse New ellipse object
     */
    public function setAxisALength($a)
    {
        return new self($a / 2, $this->semiAxisB);
    }


    /**
     * Set the axis B length
     *
     * @param int|float $b The axis B length
     *
     * @return Ellipse New ellipse object
     */
    public function setAxisBLength($b)
    {
        return new self($this->semiAxisA, $b / 2);
    }


    /**
     * Set the circumference, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified circumference with the same aspect ratio
     *
     * @param int|float $c The circumference
     *
     * @return ellipse New ellipse object
     */
    public function setCircumference($c)
    {
        $SF = $c / $this->circumference;

        $a = $this->semiAxisA * $SF;
        $b = $this->semiAxisB * $SF;

        return new self($a, $b);
    }


    /**
     * Set the foci distance, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified foci distance with the same aspect ratio
     *
     * @param int|float $d The foci distance
     *
     * @return ellipse New ellipse object
     */
    public function setFociDistance($d)
    {
        $SF = $d / $this->fociDistance;

        $a = $this->semiAxisA * $SF;
        $b = $this->semiAxisB * $SF;

        return new self($a, $b);
    }


    /**
     * Set the perimeter, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified perimeter with the same aspect ratio
     *
     * @param int|float $p The perimeter
     *
     * @return ellipse New ellipse object
     */
    public function setPerimeter($p)
    {
        return $this->setCircumference($p);
    }


    /**
     * Set the semi-axis A length
     *
     * @param int|float $a The semi-axis A length
     *
     * @return Ellipse New ellipse object
     */
    public function setSemiAxisALength($a)
    {
        return new self($a, $this->semiAxisB);
    }


    /**
     * Set the semi-axis B length
     *
     * @param int|float $b The semi-axis B length
     *
     * @return Ellipse New ellipse object
     */
    public function setSemiAxisBLength($b)
    {
        return new self($this->semiAxisA, $b);
    }
}

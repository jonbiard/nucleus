<?php
/**
 * Class RectangularPrism
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class RectangularPrism
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class RectangularPrism
{
    /**
     * Depth
     * @var float
     */
    private $depth;
    /**
     * Diagonal
     * @var float
     */
    private $diagonal;
    /**
     * Height
     * @var float
     */
    private $height;
    /**
     * Length
     * @var float
     */
    private $length;
    /**
     * Surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $l The length
     * @param int|float $h The height
     * @param int|float $d The depth
     *
     * @throws LogicException When a rectangle cannot be constructed with the values passed
     */
    public function __construct($l = 1, $h = 1, $d = 1)
    {
        if (!is_scalar($l) || $l <= 0) {
            throw new LogicException("Cannot create a rectangular prism with the specified length.");
        } elseif (!is_scalar($h) || $h <= 0) {
            throw new LogicException("Cannot create a rectangular prism with the specified height.");
        } elseif (!is_scalar($d) || $d <= 0) {
            throw new LogicException("Cannot create a rectangular prism with the specified depth.");
        }

        $l2 = pow($l, 2);
        $h2 = pow($h, 2);
        $d2 = pow($d, 2);

        $this->diagonal    = sqrt($l2 + $h2 + $d2);
        $this->volume      = $l * $h * $d;
        $this->surfaceArea = (($l * $h) + ($l * $d) + ($d * $h)) * 2;
        $this->length      = $l;
        $this->height      = $h;
        $this->depth       = $d;
    }


    /**
     * Get the depth
     *
     * @return int|float The depth
     */
    public function getDepth()
    {
        return $this->depth;
    }


    /**
     * Get the diagonal
     *
     * @return int|float The diagonal
     */
    public function getDiagonal()
    {
        return $this->diagonal;
    }


    /**
     * Get the front face
     *
     * @return Square The front face
     */
    public function getFrontFace()
    {
        return new Rectangle($this->length, $this->height);
    }


    /**
     * Get the height
     *
     * @return int|float The height
     */
    public function getHeight()
    {
        return $this->height;
    }


    /**
     * Get the length
     *
     * @return int|float The length
     */
    public function getLength()
    {
        return $this->length;
    }


    /**
     * Get the side face
     *
     * @return Square The side face
     */
    public function getSideFace()
    {
        return new Rectangle($this->depth, $this->height);
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the top face
     *
     * @return Square The top face
     */
    public function getTopFace()
    {
        return new Rectangle($this->length, $this->depth);
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the depth
     *
     * @param int|float $d The depth
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setDepth($d)
    {
        return new self($this->length, $this->height, $d);
    }


    /**
     * Set the diagonal
     *
     * @param int|float $d The diagonal
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setDiagonal($d)
    {
        $SF = sqrt($d / $this->diagonal);

        $l = $this->length * $SF;
        $h = $this->height * $SF;
        $d = $this->depth * $SF;

        return new self($l, $h, $d);
    }


    /**
     * Set the height
     *
     * @param int|float $h The height
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setHeight($h)
    {
        return new self($this->length, $h, $this->depth);
    }


    /**
     * Set the length
     *
     * @param int|float $l The length
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setLength($l)
    {
        return new self($l, $this->height, $this->depth);
    }


    /**
     * Set the surface area, maintaining the aspect ratio
     *
     * The length, height, and depth will become what they need to be
     * to result in the specified surface area with the same aspect ratio
     *
     * @param int|float $s The surface area
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $l = $this->length * $SF;
        $h = $this->height * $SF;
        $d = $this->depth * $SF;

        return new self($l, $h, $d);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The length, height, and depth will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return RectangularPrism New rectangular prism object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $l = $this->length * $SF;
        $h = $this->height * $SF;
        $d = $this->depth * $SF;

        return new self($l, $h, $d);
    }
}

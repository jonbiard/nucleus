<?php
/**
 * Class Sphere
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Geometry;

use Nucleus\Library\Exception\LogicException;

/**
 * Class Sphere
 *
 * @package Nucleus\Library
 * @subpackage Geometry
 */
class Sphere
{
    /**
     * Radius
     * @var float
     */
    private $radius;
    /**
     * Surface area
     * @var float
     */
    private $surfaceArea;
    /**
     * Volume
     * @var float
     */
    private $volume;


    /**
     * Constructor
     *
     * @param int|float $r The radius
     *
     * @throws LogicException When a sphere cannot be constructed with the values passed
     */
    public function __construct($r = 1)
    {
        if (!is_scalar($r) || $r <= 0) {
            throw new LogicException("Cannot create a sphere with the specified radius.");
        }

        $r2 = pow($r, 2);
        $r3 = pow($r, 3);

        $this->surfaceArea = 4 * M_PI * $r2;
        $this->volume      = (4 / 3) * M_PI * $r3;
        $this->radius      = $r;
    }


    /**
     * Get the diameter
     *
     * @return int|float The diameter
     */
    public function getDiameter()
    {
        return $this->radius * 2;
    }


    /**
     * Get the radius
     *
     * @return int|float The radius
     */
    public function getRadius()
    {
        return $this->radius;
    }


    /**
     * Get the surface area
     *
     * @return int|float The surface area
     */
    public function getSurfaceArea()
    {
        return $this->surfaceArea;
    }


    /**
     * Get the volume
     *
     * @return int|float The volume
     */
    public function getVolume()
    {
        return $this->volume;
    }


    /**
     * Set the diameter
     *
     * @param int|float $d The diameter
     *
     * @return Sphere New sphere object
     */
    public function setDiameter($d)
    {
        return new self($d / 2);
    }


    /**
     * Set the radius
     *
     * @param int|float $c The radius
     *
     * @return Sphere New sphere object
     */
    public function setRadius($c)
    {
        return new self($c);
    }


    /**
     * Set the surface area, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified surface area with the same aspect ratio
     *
     * @param int|float $s The surface area
     *
     * @return Sphere New sphere object
     */
    public function setSurfaceArea($s)
    {
        $SF = sqrt($s / $this->surfaceArea);

        $r = $this->radius * $SF;

        return new self($r);
    }


    /**
     * Set the volume, maintaining the aspect ratio
     *
     * The length of the axes will become what they need to be
     * to result in the specified volume with the same aspect ratio
     *
     * @param int|float $v The volume
     *
     * @return Sphere New sphere object
     */
    public function setVolume($v)
    {
        $SF = pow($v / $this->volume, 1 / 3);

        $r = $this->radius * $SF;

        return new self($r);
    }
}

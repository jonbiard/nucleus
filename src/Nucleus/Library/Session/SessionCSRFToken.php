<?php
/**
 * Class SessionCSRFToken
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Session;

use Nucleus\Library\Exception\Exception;
use Nucleus\Library\Session\SessionSegment;

/**
 * Class SessionCSRFToken
 *
 * @package Nucleus\Library
 * @subpackage Session
 */
class SessionCSRFToken
{
    /**
     * Number of bytes for the cryptographically secure value
     * @var int
     */
    private $cryptoBytes;
    /**
     * The session segment object
     * @var SessionSegment
     */
    private $segment;


    /**
     * Constructor
     *
     * @param SessionSegment $segment The session segment object
     */
    public function __construct(SessionSegment $segment)
    {
        $this->cryptoBytes = 32;
        $this->segment     = $segment;
    }


    /**
     * Get the token
     *
     * @return string The token
     */
    public function get()
    {
        if (!isset($this->segment->token)) {
            $this->regenerate();
        }

        return $this->segment->token;
    }


    /**
     * Checks if the token is valid
     *
     * @param string $value The token value to check against
     *
     * @return bool True if valid, otherwise false is returned
     */
    public function isValid($value)
    {
        return $value === $this->segment->token;
    }


    /**
     * Regenerates the token
     *
     * @throws Exception If a cryptographically secure token cannot be generated
     */
    public function regenerate()
    {
        $newToken = null;

        if (extension_loaded('openssl')) {
            $newToken = hash('sha512', openssl_random_pseudo_bytes($this->cryptoBytes));
        } elseif (extension_loaded('mcrypt')) {
            $newToken = hash('sha512', mcrypt_create_iv($this->cryptoBytes, MCRYPT_DEV_URANDOM));
        }

        if ($newToken === null) {
            $message = "Cannot generate cryptographically secure random values. "
                . "Please install extension 'openssl' or 'mcrypt', or use "
                . "another cryptographically secure implementation.";

            throw new Exception($message);
        }

        $this->segment->token = $newToken;
    }
}

<?php
/**
 * Interface SessionStorage
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Session\SessionStorage;

/**
 * Interface SessionStorage
 *
 * @package Nucleus\Library
 * @subpackage Session
 */
interface SessionStorage
{
    /**
     * Commits the session
     *
     * Also releases the lock on the session data
     */
    public function commit();


    /**
     * Deletes a key
     *
     * @param string $segment [optional] The segment
     * @param string $key [optional] The key
     */
    public function delete($segment = null, $key = null);


    /**
     * Destroys a session
     *
     * @return bool True on success or false on failure
     */
    public function destroy();


    /**
     * Checks if key exists
     *
     * @param string $segment The segment
     * @param string $key The key
     *
     * @return bool True if key is set, false otherwise
     */
    public function exists($segment, $key);


    /**
     * Getter
     *
     * @param string $segment The segment
     * @param string $key The key
     *
     * @return null|mixed The value
     */
    public function get($segment, $key);


    /**
     * Gets the current session id
     *
     * @return string The ID
     */
    public function getId();


    /**
     * Returns the current session name
     *
     * @return string The name
     */
    public function getName();


    /**
     * Starts the session
     *
     * @return bool True if session is started, false otherwise
     */
    public function isLoaded();


    /**
     * Regenerates and replaces the current session id
     *
     * Also regenerates the CSRF token value if one exists
     *
     * @param bool $deleteOld Whether to delete the old associated session file or not
     *
     * @return bool True is regeneration worked, otherwise false is returned
     */
    public function regenerateId($deleteOld = false);


    /**
     * Setter
     *
     * @param string $segment The segment
     * @param string $key The key
     * @param mixed $val The value
     */
    public function set($segment, $key, $val);


    /**
     * Sets the current session name
     *
     * @param string $name The session name
     */
    public function setName($name);


    /**
     * Starts the session
     *
     * Also creates an exclusive lock on the session data for this execution
     */
    public function start();
}

<?php
/**
 * Class NativeSessionStorage
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Session\SessionStorage;

/**
 * Class NativeSessionStorage
 *
 * @package Nucleus\Library
 * @subpackage Session
 */
class NativeSessionStorage implements SessionStorage
{
    /**
     * Whether the session is loaded or not
     * @var bool
     */
    private $loaded = false;


    /**
     * Commits the session
     *
     * {@inheritdoc}
     */
    public function commit()
    {
        $this->start();
        session_commit();
    }


    /**
     * Deletes a key
     *
     * @param string $segment The segment
     * @param string $key The key
     */
    public function delete($segment = null, $key = null)
    {
        $this->start();

        if ($segment === null && $key === null) {
            unset($_SESSION);
        } elseif ($segment === null) {
            unset($_SESSION[$segment]);
        } else {
            unset($_SESSION[$segment][$key]);
        }
    }


    /**
     * Destroys a session
     *
     * @return bool True on success or false on failure
     */
    public function destroy()
    {
        $this->start();
        $this->delete();

        return session_destroy();
    }


    /**
     * Checks if key exists
     *
     * @param string $segment The segment
     * @param string $key The key
     *
     * @return bool True if key is set, false otherwise
     */
    public function exists($segment, $key)
    {
        $this->start();
        return isset($_SESSION[$segment][$key]);
    }


    /**
     * Getter
     *
     * @param string $segment The segment
     * @param string $key The key
     *
     * @return null|mixed The value
     */
    public function get($segment, $key)
    {
        $this->start();
        return isset($_SESSION[$segment][$key]) ? $_SESSION[$segment][$key] : null;
    }


    /**
     * Gets the current session id
     *
     * @return string The ID
     */
    public function getId()
    {
        $this->start();
        return session_id();
    }


    /**
     * Returns the current session name
     *
     * @return string The name
     */
    public function getName()
    {
        $this->start();
        return session_name();
    }


    /**
     * Gets the session save path
     *
     * @return string The save path
     */
    public function getSavePath()
    {
        return session_save_path();
    }


    /**
     * Starts the session
     *
     * @return bool True if session is started, false otherwise
     */
    public function isLoaded()
    {
        return $this->loaded;
    }


    /**
     * Regenerates and replaces the current session id
     *
     * Also regenerates the CSRF token value if one exists
     *
     * @param bool $deleteOld [optional] Whether to delete the old associated session file or not
     *
     * @return bool True is regeneration worked, otherwise false is returned
     */
    public function regenerateId($deleteOld = false)
    {
        $this->start();
        return session_regenerate_id($deleteOld);
    }


    /**
     * Setter
     *
     * @param string $segment The segment
     * @param string $key The key
     * @param mixed $val The value
     */
    public function set($segment, $key, $val)
    {
        $this->start();

        if (!isset($_SESSION[$segment])) {
            $_SESSION[$segment] = [];
        }

        $_SESSION[$segment][$key] = $val;
    }


    /**
     * Sets the current session name
     *
     * @param string $name The session name
     */
    public function setName($name)
    {
        session_name($name);
    }


    /**
     * Sets the session save path
     *
     * @param string $path The new save path
     *
     * @return string The path of the current directory used for data storage
     */
    public function setSavePath($path)
    {
        if ($path === null) {
            $path = $this->getSavePath();
        }

        return session_save_path($path);
    }


    /**
     * Starts the session
     *
     * {@inheritdoc}
     */
    public function start()
    {
        if (!$this->loaded) {
            $this->loaded = session_start();
        }

        return $this->loaded;
    }
}

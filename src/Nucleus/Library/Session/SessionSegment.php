<?php
/**
 * Class SessionSegment
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Session;

use Nucleus\Library\Session\SessionStorage\SessionStorage;

/**
 * Class SessionSegment
 *
 * @package Nucleus\Library
 * @subpackage Session
 */
class SessionSegment
{
    /**
     * The name of this segment
     * @var string
     */
    private $name;
    /**
     * The session storage object
     * @var SessionStorage
     */
    private $storage;


    /**
     * Constructor
     *
     * @param SessionStorage $storage The session storage object
     * @param string $name The segment's name
     */
    public function __construct(SessionStorage $storage, $name)
    {
        $this->name    = $name;
        $this->storage = $storage;
    }


    /**
     * Magic getter
     *
     * @param string $key The key
     *
     * @return null|mixed The value
     */
    public function __get($key)
    {
        return $this->storage->get($this->name, $key);
    }


    /**
     * Magic setter
     *
     * @param string $key The key
     * @param mixed $val The value
     */
    public function __set($key, $val)
    {
        $this->storage->set($this->name, $key, $val);
    }


    /**
     * Magic isset
     *
     * @param string $key The key
     *
     * @return bool True if key is set, false otherwise
     */
    public function __isset($key)
    {
        return $this->storage->exists($this->name, $key);
    }


    /**
     * Magic unset
     *
     * @param string $key The key
     */
    public function __unset($key)
    {
        $this->storage->delete($this->name, $key);
    }


    /**
     * Name Getter
     *
     * @return string The value
     */
    public function getName()
    {
        return $this->name;
    }
}

<?php
/**
 * Class Session
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Session;

use Nucleus\Library\Env\Client;
use Nucleus\Library\HTTP\Cookie;
use Nucleus\Library\HTTP\Response;
use Nucleus\Library\Session\SessionCSRFToken;
use Nucleus\Library\Session\SessionSegment;
use Nucleus\Library\Session\SessionStorage\NativeSessionStorage;
use Nucleus\Library\Session\SessionStorage\SessionStorage;

/**
 * Class Session
 *
 * @package Nucleus\Library
 * @subpackage Session
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Session
{
    /**
     * The CSRF token object
     * @var SessionCSRFToken
     */
    public $CSRFToken;
    /**
     * The cookie object for the session cookie
     * @var Cookie
     */
    private $cookie;
    /**
     * The client environment object to check if session cookie already exists
     * @var Client
     */
    private $client;
    /**
     * The response object to set the session cookie
     * @var Response
     */
    private $response;
    /**
     * The session segments that have been instantiated
     * @var SessionSegment[]
     */
    private $segments;
    /**
     * The session storage object
     * @var SessionStorage
     */
    private $storage;


    /**
     * Constructor
     *
     * @param Cookie $cookie The cookie object for the session cookie
     * @param Client $client The client environment object to check if session cookie already exists
     * @param Response $response The response object to set the session cookie
     * @param SessionStorage $storage The session storage object
     */
    public function __construct(Cookie $cookie, Client $client, Response $response, SessionStorage $storage)
    {
        $this->segments = [];

        $this->cookie   = $cookie;
        $this->client   = $client;
        $this->response = $response;
        $this->storage  = $storage;

        if ($this->storage instanceof NativeSessionStorage) {
            session_name($this->cookie->getName());

            session_set_cookie_params(
                $this->cookie->getExpire() - time(),
                $this->cookie->getPath(),
                $this->cookie->getDomain(),
                $this->cookie->isSecure(),
                $this->cookie->isHTTPOnly()
            );
        }

        if ($this->isAvailable()) {
            $this->cookie = $this->persist();
        } else {
            $this->storage->setName($this->cookie->getName());
            $this->cookie->setValue($this->getId());
        }

        $this->response->cookies->set($this->cookie);

        $this->CSRFToken = new SessionCSRFToken($this->getSegment('Nucleus\Library\Session\CSRF'));
    }


    /**
     * Commits the session
     */
    public function commit()
    {
        $this->storage->commit();
    }


    /**
     * Deletes all session variables across all segments
     */
    public function delete()
    {
        $this->storage->delete();
    }


    /**
     * Destroys the session entirely
     *
     * @return bool True on success or false on failure
     */
    public function destroy()
    {
        return $this->storage->destroy();
    }


    /**
     * Cookie Getter
     *
     * @return Cookie The value
     */
    public function getCookie()
    {
        return $this->cookie;
    }


    /**
     * Gets the current session id
     *
     * @return string The ID
     */
    public function getId()
    {
        return $this->storage->getId();
    }


    /**
     * Gets the current session name
     *
     * @return string The name
     */
    public function getName()
    {
        return $this->storage->getName();
    }


    /**
     * Gets a new session segment instance by name
     *
     * @param string $name The name of the session segment, typically a fully-qualified class name
     *
     * @return SessionSegment
     */
    public function getSegment($name)
    {
        if (!isset($this->segments[$name])) {
            $this->segments[$name] = new SessionSegment($this->storage, $name);
        }

        return $this->segments[$name];
    }


    /**
     * SessionStorage Getter
     *
     * @return SessionStorage The value
     */
    public function getStorage()
    {
        return $this->storage;
    }


    /**
     * Checks if a session is available to be reactivated
     *
     * @return bool True if a cookie is available, otherwise false is returned
     */
    public function isAvailable()
    {
        return $this->client->hasCookie($this->cookie->getName());
    }


    /**
     * Tells us if a session has started
     *
     * @return bool True if session is started otherwise false is returned
     */
    public function isStarted()
    {
        return $this->storage->isLoaded();
    }


    /**
     * Gets the cookie to persist the session
     *
     * @return null|bool The cookie if it is available, otherwise null is returned
     */
    public function persist()
    {
        return $this->client->getCookie($this->cookie->getName());
    }


    /**
     * Regenerates and replaces the current session id
     *
     * Also regenerates the CSRF token value if one exists
     *
     * @return bool True is regeneration worked, otherwise false is returned
     */
    public function regenerateId()
    {
        /*
        TODO?: Implement seamless transfer of sessions without deleting the old session right away.
        Otherwise, browsers that are slow to update the cookie will log the user out.
        A good way to test is to renegerate on every page load and repeatedly refresh the page.
        If we are logged out at any point, there exists a possibility that any regenerate could log out a user.
        TEST THIS BEFORE MAKING THE CHANGE -- IT MIGHT NO LONGER BE AN ISSUE NOWADAYS
         */
        if ($result = $this->storage->regenerateId(true)) {
            $this->cookie->setValue($this->getId());

            if ($this->CSRFToken) {
                $this->CSRFToken->regenerate();
            }
        }

        return $result;
    }


    /**
     * Starts a new session, or resumes an existing one
     *
     * @return bool
     */
    public function start()
    {
        if (!$this->isStarted()) {
            return $this->storage->start();
        }

        return true;
    }
}

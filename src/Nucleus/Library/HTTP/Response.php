<?php
/**
 * Class Response
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\HTTP;

use Nucleus\Library\HTTP\Response\Cookies;
use Nucleus\Library\HTTP\Response\Headers;

/**
 * Class Response
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 */
class Response
{
    /**
     * The response cookie object
     * @var Cookies
     */
    public $cookies;
    /**
     * The response header object
     * @var Headers
     */
    public $headers;
    /**
     * The content of the response
     * @var string
     */
    private $content;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->content = '';
        $this->cookies = new Cookies();
        $this->headers = new Headers();
    }


    /**
     * Appends content for the response body
     *
     * @param string $content The content
     */
    public function appendContent($content)
    {
        $this->content .= $content;
    }


    /**
     * Gets the content ready to be sent (or already sent)
     *
     * @return string The content
     */
    public function getContent()
    {
        return $this->content;
    }


    /**
     * Sends the prepared headers, cookies, and content to the client
     */
    public function send()
    {
        $this->headers->send();
        $this->cookies->send();

        echo $this->content;
    }


    /**
     * Sets the content for the response body
     *
     * @param string $content The content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }
}

<?php
/**
 * Class Cookie
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\HTTP;

/**
 * Class Cookie
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 */
class Cookie
{
    /**
     * If set to true, PHP will attempt to send the httponly flag when setting the cookie
     * @var bool
     */
    private $HTTPOnly;
    /**
     * Cookie domain, for example 'www.domain.com'
     * To make cookies visible on all subdomains then the domain must be prefixed with a dot like '.domain.com'
     * @var string
     */
    private $domain;
    /**
     * Expire time of the session cookie, as a Unix timestamp
     * @var int
     */
    private $expire;
    /**
     * Cookie name
     * @var string
     */
    private $name;
    /**
     * Path on the domain where the cookie will work. Use a single slash ('/') for all paths on the domain
     * @var string
     */
    private $path;
    /**
     * If true, cookie will only be sent over secure connections
     * @var bool
     */
    private $secure;
    /**
     * Cookie value
     * @var mixed
     */
    private $value;


    /**
     * Constructor
     *
     * @param string $name [optional] The cookie name
     * @param mixed $value [optional] The cookie value
     */
    public function __construct($name = null, $value = null)
    {
        $this->name     = $name;
        $this->value    = $value;
        $this->HTTPOnly = false;
        $this->expire   = 0;
        $this->path     = '/';
        $this->secure   = false;
    }


    /**
     * Domain Getter
     *
     * @return string The value
     */
    public function getDomain()
    {
        return $this->domain;
    }


    /**
     * Expire Getter
     *
     * @return int The value
     */
    public function getExpire()
    {
        return $this->expire;
    }


    /**
     * Name Getter
     *
     * @return string The value
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Path Getter
     *
     * @return string The value
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * Value Getter
     *
     * @return mixed The value
     */
    public function getValue()
    {
        return $this->value;
    }


    /**
     * HTTPOnly Getter
     *
     * @return boolean The value
     */
    public function isHTTPOnly()
    {
        return $this->HTTPOnly;
    }


    /**
     * Secure Getter
     *
     * @return boolean The value
     */
    public function isSecure()
    {
        return $this->secure;
    }


    /**
     * Domain Setter
     *
     * @param string $domain The value
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }


    /**
     * Expire Setter
     *
     * @param int $expire The value
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;
    }


    /**
     * HTTPOnly Setter
     *
     * @param boolean $HTTPOnly The value
     */
    public function setHTTPOnly($HTTPOnly)
    {
        $this->HTTPOnly = $HTTPOnly;
    }


    /**
     * Name Setter
     *
     * @param string $name The value
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Path Setter
     *
     * @param string $path The value
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


    /**
     * Secure Setter
     *
     * @param boolean $secure The value
     */
    public function setSecure($secure)
    {
        $this->secure = $secure;
    }


    /**
     * Value Setter
     *
     * @param mixed $value The value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}

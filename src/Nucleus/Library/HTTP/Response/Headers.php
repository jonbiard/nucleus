<?php
/**
 * Class Headers
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\HTTP\Response;

/**
 * Class Headers
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 */
class Headers
{
    /**
     * HTTP protocol version
     * @var string
     */
    private $HTTPVersion;
    /**
     * Internal array of headers to be sent
     * @var array
     */
    private $headers;
    /**
     * Status code to be sent
     * @var int
     */
    private $statusCode;
    /**
     * Possible status codes to use
     * @var array
     */
    private $statusCodes = [
        '100' => 'Continue',
        '101' => 'Switching Protocols',
        '200' => 'OK',
        '201' => 'Created',
        '202' => 'Accepted',
        '203' => 'Non-Authoritative Information',
        '204' => 'No Content',
        '205' => 'Reset Content',
        '206' => 'Partial Content',
        '300' => 'Multiple Choices',
        '301' => 'Moved Permanently',
        '302' => 'Found',
        '303' => 'See Other',
        '304' => 'Not Modified',
        '305' => 'Use Proxy',
        '307' => 'Temporary Redirect',
        '400' => 'Bad Request',
        '401' => 'Unauthorized',
        '402' => 'Payment Required',
        '403' => 'Forbidden',
        '404' => 'Not Found',
        '405' => 'Method Not Allowed',
        '406' => 'Not Acceptable',
        '407' => 'Proxy Authentication Required',
        '408' => 'Request Timeout',
        '409' => 'Conflict',
        '410' => 'Gone',
        '411' => 'Length Required',
        '412' => 'Precondition Failed',
        '413' => 'Request Entity Too Large',
        '414' => 'Request-URI Too Long',
        '415' => 'Unsupported Media Type',
        '416' => 'Requested Range Not Satisfiable',
        '417' => 'Expectation Failed',
        '500' => 'Internal Server Error',
        '501' => 'Not Implemented',
        '502' => 'Bad Gateway',
        '503' => 'Service Unavailable',
        '504' => 'Gateway Timeout',
        '505' => 'HTTP Version Not Supported',
    ];
    /**
     * Status text to be sent
     * @var string
     */
    private $statusText;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->headers     = [];
        $this->statusCode  = 200;
        $this->statusText  = 'OK';
        $this->HTTPVersion = '1.1';

        foreach (headers_list() as $header) {
            list($label, $value) = explode(':', $header, 2);
            $this->headers[$label] = $value;
        }
    }


    /**
     * Gets a header that has been prepared for this response
     *
     * @param string $label The label
     *
     * @return null|string The value
     */
    public function get($label)
    {
        return isset($this->headers[$label]) ? $this->headers[$label] : null;
    }


    /**
     * Get all headers that have been prepared for this response
     *
     * @return array The headers
     */
    public function getAll()
    {
        return $this->headers;
    }


    /**
     * Get the HTTP protocol version
     */
    public function getHTTPVersion()
    {
        return $this->HTTPVersion;
    }


    /**
     * Get the status code of the response
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }


    /**
     * Get the status text of the response
     */
    public function getStatusText()
    {
        return $this->statusText;
    }


    /**
     * Removes a previously-set header
     *
     * @param string $label The label
     */
    public function remove($label)
    {
        unset($this->headers[$label]);
    }


    /**
     * Send all prepared headers
     */
    public function send()
    {
        header("HTTP/$this->HTTPVersion $this->statusCode $this->statusText");

        foreach ($this->headers as $label => $value) {
            header("$label: $value");
        }
    }


    /**
     * Sets a header
     *
     * @param string $label The label
     * @param string $value The value
     */
    public function set($label, $value)
    {
        $this->headers[$label] = $value;
    }


    /**
     * Shorthand for setting the Accept-Ranges header
     *
     * @param string $value The value
     */
    public function setAcceptRanges($value)
    {
        $this->set('Accept-Ranges', $value);
    }


    /**
     * Shorthand for setting the Access-Control-Allow-Origin header
     *
     * @param string $value The value
     */
    public function setAccessControlAllowOrigin($value)
    {
        $this->set('Access-Control-Allow-Origin', $value);
    }


    /**
     * Shorthand for setting the Age header
     *
     * @param string $seconds The value
     */
    public function setAge($seconds)
    {
        $this->set('Age', $seconds);
    }


    /**
     * Shorthand for setting multiple headers at once
     *
     * @param array $headers Associative array consisting of label=>value pairs
     */
    public function setAll(array $headers)
    {
        foreach ($headers as $label => $value) {
            $this->set($label, $value);
        }
    }


    /**
     * Shorthand for setting the Allow header
     *
     * @param string $methods The allowed methods. Eg: GET, POST
     */
    public function setAllow($methods)
    {
        $this->set('Allow', $methods);
    }


    /**
     * Shorthand for setting the Cache-Control header
     *
     * @param string $directives The directives. Eg: max-age=3600, must-revalidate
     */
    public function setCacheControl($directives)
    {
        $this->set('Cache-Control', $directives);
    }


    /**
     * Shorthand for setting the Connection header
     *
     * @param string $value The value
     */
    public function setConnection($value)
    {
        $this->set('Connection', $value);
    }


    /**
     * Shorthand for setting the Content-Disposition header
     *
     * @param string $value The value
     */
    public function setContentDisposition($value)
    {
        $this->set('Content-Disposition', $value);
    }


    /**
     * Shorthand for setting the Content-Encoding header
     *
     * @param string $value The value
     */
    public function setContentEncoding($value)
    {
        $this->set('Content-Encoding', $value);
    }


    /**
     * Shorthand for setting the Content-Language header
     *
     * @param string $value The value
     */
    public function setContentLanguage($value)
    {
        $this->set('Content-Language', $value);
    }


    /**
     * Shorthand for setting the Content-Length header
     *
     * @param string $value The value
     */
    public function setContentLength($value)
    {
        $this->set('Content-Length', $value);
    }


    /**
     * Shorthand for setting the Content-Location header
     *
     * @param string $value The value
     */
    public function setContentLocation($value)
    {
        $this->set('Content-Location', $value);
    }


    /**
     * Shorthand for setting the Content-MD5 header
     *
     * @param string $value The value
     */
    public function setContentMD5($value)
    {
        $this->set('Content-MD5', $value);
    }


    /**
     * Shorthand for setting the Content-Range header
     *
     * @param string $value The value
     */
    public function setContentRange($value)
    {
        $this->set('Content-Range', $value);
    }


    /**
     * Shorthand for setting the Content-Type header
     *
     * @param string $contentType The content type. Eg: text/html
     * @param null|string $charset The charset identifier code. Eg: utf-8
     */
    public function setContentType($contentType, $charset = null)
    {
        $charset = $charset !== null ? "; charset=$charset" : '';
        $this->set('Content-Type', $contentType . $charset);
    }


    /**
     * Shorthand for setting the Date header
     *
     * @param string $timestamp The timestamp
     */
    public function setDate($timestamp)
    {
        $this->set('Date', gmdate('D, j M Y H:i:s', $timestamp) . ' GMT');
    }


    /**
     * Shorthand for setting the ETag header
     *
     * @param string $value The value
     */
    public function setETag($value)
    {
        $this->set('ETag', $value);
    }


    /**
     * Shorthand for setting the Expires header
     *
     * @param string $timestamp The timestamp
     */
    public function setExpires($timestamp)
    {
        $this->set('Expires', gmdate('D, j M Y H:i:s', $timestamp) . ' GMT');
    }


    /**
     * Get the HTTP protocol version
     *
     * @param mixed $version The version (will be cast to a string)
     */
    public function setHTTPVersion($version)
    {
        $this->HTTPVersion = (string)$version;
    }


    /**
     * Shorthand for setting the Last-Modified header
     *
     * @param string|int $timestamp The timestamp
     */
    public function setLastModified($timestamp)
    {
        $this->set('Last-Modified', gmdate('D, j M Y H:i:s', (int)$timestamp) . ' GMT');
    }


    /**
     * Shorthand for setting the Link header
     *
     * @param string $value The value
     */
    public function setLink($value)
    {
        $this->set('Link', $value);
    }


    /**
     * Shorthand for setting a Location header
     *
     * @param string $url The value
     */
    public function setLocation($url)
    {
        $this->set('Location', $url);
    }


    /**
     * Shorthand for setting the P3P header
     *
     * @param string $value The value
     */
    public function setP3P($value)
    {
        $this->set('P3P', $value);
    }


    /**
     * Shorthand for setting the Pragma header
     *
     * @param string $value The value
     */
    public function setPragma($value)
    {
        $this->set('Pragma', $value);
    }


    /**
     * Shorthand for setting the Proxy-Authenticate header
     *
     * @param string $value The value
     */
    public function setProxyAuthenticate($value)
    {
        $this->set('Proxy-Authenticate', $value);
    }


    /**
     * Shorthand for setting the Refresh header
     *
     * @param string $value The value
     */
    public function setRefresh($value)
    {
        $this->set('Refresh', $value);
    }


    /**
     * Shorthand for setting the Retry-After header
     *
     * @param string $value The value
     */
    public function setRetryAfter($value)
    {
        $this->set('Retry-After', $value);
    }


    /**
     * Shorthand for setting the Server header
     *
     * @param string $value The value
     */
    public function setServer($value)
    {
        $this->set('Server', $value);
    }


    /**
     * Sets the status code of the response
     *
     * @param int $code The code
     */
    public function setStatusCode($code)
    {
        $this->statusCode = (int)$code;
        $this->statusText = $this->statusCodes[(string)$code];
    }


    /**
     * Sets the status text of the response
     *
     * @param string $text The text
     */
    public function setStatusText($text)
    {
        $this->statusText = trim(str_replace(["\r", "\n"], '', $text));
    }


    /**
     * Shorthand for setting the Strict-Transport-Security header
     *
     * @param string $value The value
     */
    public function setStrictTransportSecurity($value)
    {
        $this->set('Strict-Transport-Security', $value);
    }


    /**
     * Shorthand for setting the Trailer header
     *
     * @param string $value The value
     */
    public function setTrailer($value)
    {
        $this->set('Trailer', $value);
    }


    /**
     * Shorthand for setting the Transfer-Encoding header
     *
     * @param string $value The value
     */
    public function setTransferEncoding($value)
    {
        $this->set('Transfer-Encoding', $value);
    }


    /**
     * Shorthand for setting the Vary header
     *
     * @param string $value The value
     */
    public function setVary($value)
    {
        $this->set('Vary', $value);
    }


    /**
     * Shorthand for setting the Via header
     *
     * @param string $value The value
     */
    public function setVia($value)
    {
        $this->set('Via', $value);
    }


    /**
     * Shorthand for setting the WWW-Authenticate header
     *
     * @param string $value The value
     */
    public function setWWWAuthenticate($value)
    {
        $this->set('WWW-Authenticate', $value);
    }


    /**
     * Shorthand for setting the Warning header
     *
     * @param string $value The value
     */
    public function setWarning($value)
    {
        $this->set('Warning', $value);
    }
}

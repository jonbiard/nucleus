<?php
/**
 * Class Cookies
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\HTTP\Response;

use Nucleus\Library\HTTP\Cookie;

/**
 * Class Cookies
 *
 * @package Nucleus\Library
 * @subpackage HTTP
 */
class Cookies
{
    /**
     * Collection of cookies to be sent
     * @var Cookie[]
     */
    private $cookies;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cookies = [];
    }


    /**
     * Get a cookie that has been prepared for this response
     *
     * @param null|string $name The name
     *
     * @return null|Cookie The cookie
     */
    public function get($name)
    {
        return isset($this->cookies[$name]) ? $this->cookies[$name] : null;
    }


    /**
     * Get all cookies that have been prepared for this response
     *
     * @return Cookie[] The cookies
     */
    public function getAll()
    {
        return $this->cookies;
    }


    /**
     * Removes a previously-set cookie
     *
     * @param string $name The name
     */
    public function remove($name)
    {
        unset($this->cookies[$name]);
    }


    /**
     * Send all prepared cookies
     */
    public function send()
    {
        foreach ($this->cookies as $cookie) {
            $name  = $cookie->getName();
            $value = $cookie->getValue();

            if ($name === null || $value === null) {
                continue;
            }

            setcookie(
                $name,
                $value,
                $cookie->getExpire(),
                $cookie->getPath(),
                $cookie->getDomain(),
                $cookie->isSecure(),
                $cookie->isHTTPOnly()
            );
        }
    }


    /**
     * Shorthand for setting a Set-Cookie header
     *
     * @param Cookie $cookie The cookie
     */
    public function set(Cookie $cookie)
    {
        $name = $cookie->getName();

        if ($name === null) {
            return;
        }

        $this->cookies[$name] = $cookie;
    }
}

<?php
/**
 * Class File
 *
 * @package Nucleus\Library
 * @subpackage FileSystem
 *
 * @author Martin Biard <info@martinbiard.com>
 */
namespace Nucleus\Library\FileSystem;

use Nucleus\Library\Exception\IOException;

/**
 * Class File
 *
 * @package Nucleus\Library
 * @subpackage FileSystem
 */
class File
{
    /**
     * File handle
     * @var resource
     */
    protected $fh;
    /**
     * File path
     * @var string
     */
    protected $path;


    /**
     * Constructor
     *
     * @param string $path The file path
     * @param string $mode The mode (see PHP's fopen() function)
     *
     * @throws IOException When the file path cannot be opened
     */
    public function __construct($path, $mode)
    {
        $this->path = $path;

        if (($this->fh = @fopen($this->path, $mode)) === false) {
            throw new IOException("Failed to open file: $this->path");
        }
    }


    /**
     * Closes the file
     *
     * @throws IOException When the file path cannot be closed
     */
    public function close()
    {
        if (($this->fh = @fclose($this->fh)) === false) {
            throw new IOException("Failed to close file: $this->path");
        }
    }


    /**
     * Gets the file's pointer offset
     *
     * @return int The position of the file pointer
     *
     * @throws IOException When the pointer offset cannot be fetched
     */
    public function getOffset()
    {
        if (($pos = @ftell($this->fh)) === false) {
            throw new IOException("Failed to get pointer offset in file: $this->path");
        }

        return $pos;
    }


    /**
     * Checks if the file pointer is at the end of the file
     *
     * @return bool True if the file pointer is pointing at the end of the file, false otherwise
     */
    public function isAtEOF()
    {
        return @feof($this->fh);
    }


    /**
     * Locks the file
     *
     * @param bool $exclusive Whether this should be an exclusive lock or not
     * @param bool $wait Whether to wait until the file gets released if another process is holding a lock on it
     *
     * @throws IOException When a lock cannot be established on the file
     */
    public function lock($exclusive = true, $wait = true)
    {
        $opts = $exclusive ? LOCK_EX : LOCK_SH;

        if (!$wait) {
            $opts |= LOCK_NB;
        }

        if (@flock($this->fh, $opts) === false) {
            throw new IOException("Failed to lock file: $this->path");
        }
    }


    /**
     * Reads characters from the file
     *
     * @param int $length The amount of characters to read
     *
     * @return string|bool The characters read or false on failure
     */
    public function read($length)
    {
        return @fread($this->fh, $length);
    }


    /**
     * Reads one character from the file
     *
     * @return string|bool The character read or false on failure
     */
    public function readChar()
    {
        return @fgetc($this->fh);
    }


    /**
     * Reads a line from the file
     *
     * @param null $length The amount of characters to read if a newline or EOF is not reached
     *
     * @return string The characters read
     */
    public function readLine($length = null)
    {
        if ($length === null) {
            return @fgets($this->fh);
        }

        return @fgets($this->fh, $length);
    }


    /**
     * Truncates the file to a certain size
     *
     * @param int $size The size to truncate to
     *
     * @throws IOException When the file cannot be resized
     */
    public function resize($size)
    {
        if (@ftruncate($this->fh, $size) === false) {
            throw new IOException("Failed to resize file: $this->path");
        }
    }


    /**
     * Seeks to a specific offset in the file
     *
     * Valid $from values are: 'start', 'current', and 'end'
     *
     * @param int $offset The offset
     * @param string $from Where to start the seek from
     *
     * @throws IOException When seeking an offset fails
     */
    public function seek($offset, $from = 'start')
    {
        switch (strtolower($from)) {
            case ('current'):
                $from = SEEK_CUR;
                break;
            case ('end'):
                $from = SEEK_END;
                break;
            default:
                $from = SEEK_SET;
        }

        if (@fseek($this->fh, $offset, $from) === -1) {
            throw new IOException("Failed to seek to offset in file: $this->path");
        }
    }


    /**
     * Unlocks the file
     *
     * @throws IOException When unlocking the file fails
     */
    public function unlock()
    {
        if (@flock($this->fh, LOCK_UN) === false) {
            throw new IOException("Failed to unlock file: $this->path");
        }
    }


    /**
     * Writes to the file
     *
     * @param mixed $data The data to write
     * @param null|int $length The amount of characters or bytes to write
     *
     * @throws IOException When writing to the file fails
     */
    public function write($data, $length = null)
    {
        if ($length === null) {
            $result = @fwrite($this->fh, $data);
        } else {
            $result = @fwrite($this->fh, $data, $length);
        }

        if ($result === false) {
            throw new IOException("Failed to write to file: $this->path");
        }
    }
}

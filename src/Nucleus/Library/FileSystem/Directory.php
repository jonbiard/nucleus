<?php
/**
 * Class Directory
 *
 * @package Nucleus\Library
 * @subpackage FileSystem
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\FileSystem;

use Nucleus\Library\Exception\IOException;

/**
 * Class Directory
 *
 * @package Nucleus\Library
 * @subpackage FileSystem
 */
class Directory
{
    /**
     * Directory handle
     * @var resource
     */
    protected $dh;
    /**
     * Directory path
     * @var string
     */
    protected $path;


    /**
     * Constructor
     *
     * @param string $path The path to the directory
     *
     * @throws IOException When the directory path cannot be opened
     */
    public function __construct($path)
    {
        $this->path = $path;

        if (($this->dh = @opendir($this->path)) === false) {
            throw new IOException("Failed to open directory: $this->path");
        }
    }


    /**
     * Close the directory handle
     */
    public function close()
    {
        @closedir($this->dh);
    }


    /**
     * Get the path to the next file in the directory
     *
     * @return bool|string The path to the next file in the directory
     */
    public function read()
    {
        while (($item = @readdir($this->dh)) !== false) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            return "$this->path/$item";
        }

        return false;
    }


    /**
     * Rewind the directory handle to point at the beginning
     */
    public function rewind()
    {
        @rewinddir($this->dh);
    }
}

<?php
/**
 * Class ValidatorData
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator;

/**
 * Class ValidatorData
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class ValidatorData
{
    /**
     * Holds the AbstractRule objects to use for validation
     * @var \Nucleus\Library\Validator\Rule\AbstractRule
     */
    private $rule;
    /**
     * Holds the rules' error messages
     * @var string
     */
    private $errorMessage;
    /**
     * Holds the rules' options
     * @var int
     */
    private $options;
    /**
     * Holds the rules' arguments
     * @var array
     */
    private $args;


    /**
     * Args Setter
     *
     * @param array $args The value
     */
    public function setArgs($args)
    {
        $this->args = $args;
    }


    /**
     * Args Getter
     *
     * @return array The value
     */
    public function getArgs()
    {
        return $this->args;
    }


    /**
     * ErrorMessage Setter
     *
     * @param string $errorMessage The value
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }


    /**
     * ErrorMessage Getter
     *
     * @return string The value
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }


    /**
     * Options Setter
     *
     * @param int $options The value
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }


    /**
     * Options Getter
     *
     * @return int The value
     */
    public function getOptions()
    {
        return $this->options;
    }


    /**
     * Rule Setter
     *
     * @param \Nucleus\Library\Validator\Rule\AbstractRule $rule The value
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
    }


    /**
     * Rule Getter
     *
     * @return \Nucleus\Library\Validator\Rule\AbstractRule The value
     */
    public function getRule()
    {
        return $this->rule;
    }
}

<?php
/**
 * Class Validate
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator;

use Nucleus\Library\Validator\Rule\AgeBetweenRule;
use Nucleus\Library\Validator\Rule\AgeMaxRule;
use Nucleus\Library\Validator\Rule\AgeMinRule;
use Nucleus\Library\Validator\Rule\AlnumRule;
use Nucleus\Library\Validator\Rule\AlphaRule;
use Nucleus\Library\Validator\Rule\ArrayRule;
use Nucleus\Library\Validator\Rule\Base64Rule;
use Nucleus\Library\Validator\Rule\BetweenRule;
use Nucleus\Library\Validator\Rule\BlankRule;
use Nucleus\Library\Validator\Rule\BoolRule;
use Nucleus\Library\Validator\Rule\CreditCardRule;
use Nucleus\Library\Validator\Rule\DatetimeRule;
use Nucleus\Library\Validator\Rule\EmailRule;
use Nucleus\Library\Validator\Rule\EqualToRule;
use Nucleus\Library\Validator\Rule\FloatRule;
use Nucleus\Library\Validator\Rule\InKeysRule;
use Nucleus\Library\Validator\Rule\IntRule;
use Nucleus\Library\Validator\Rule\InValuesRule;
use Nucleus\Library\Validator\Rule\IPRule;
use Nucleus\Library\Validator\Rule\IPv4Rule;
use Nucleus\Library\Validator\Rule\IPv6Rule;
use Nucleus\Library\Validator\Rule\LocaleRule;
use Nucleus\Library\Validator\Rule\MaxRule;
use Nucleus\Library\Validator\Rule\MinRule;
use Nucleus\Library\Validator\Rule\RegexRule;
use Nucleus\Library\Validator\Rule\StrictEqualToRule;
use Nucleus\Library\Validator\Rule\StringRule;
use Nucleus\Library\Validator\Rule\StrlenBetweenRule;
use Nucleus\Library\Validator\Rule\StrlenMaxRule;
use Nucleus\Library\Validator\Rule\StrlenMinRule;
use Nucleus\Library\Validator\Rule\StrlenRule;
use Nucleus\Library\Validator\Rule\TimezoneRule;
use Nucleus\Library\Validator\Rule\TrimRule;
use Nucleus\Library\Validator\Rule\URLRule;
use Nucleus\Library\Validator\Rule\WordRule;

/**
 * Class Validate
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Validate
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Checks if the value is a valid date representing an age within a range
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isAgeBetween($value, $format, $min, $max)
    {
        $rule = new AgeBetweenRule();

        return $rule->is($value, [$format, $min, $max]);
    }


    /**
     * Checks if the value is a valid date representing an age of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isAgeMax($value, $format, $max)
    {
        $rule = new AgeMaxRule();

        return $rule->is($value, [$format, $max]);
    }


    /**
     * Checks if the value is a valid date representing an age of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isAgeMin($value, $format, $min)
    {
        $rule = new AgeMinRule();

        return $rule->is($value, [$format, $min]);
    }


    /**
     * Checks if the value is alphanumerical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isAlnum($value)
    {
        $rule = new AlnumRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is alphabetical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isAlpha($value)
    {
        $rule = new AlphaRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is an array
     *
     * @param array $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isArray($value)
    {
        $rule = new ArrayRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a base64 encoded string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isBase64($value)
    {
        $rule = new Base64Rule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isBetween($value, $min, $max)
    {
        $rule = new BetweenRule();

        return $rule->is($value, [$min, $max]);
    }


    /**
     * Checks if the value is a blank value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isBlank($value)
    {
        $rule = new BlankRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a boolean value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isBool($value)
    {
        $rule = new BoolRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a credit card number
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isCreditCard($value)
    {
        $rule = new CreditCardRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a datetime string
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     *
     * @return bool The result of the validation
     */
    public function isDatetime($value, $format)
    {
        $rule = new DatetimeRule();

        return $rule->is($value, [$format]);
    }


    /**
     * Checks if the value is an email address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isEmail($value)
    {
        $rule = new EmailRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is equal to another (value only)
     *
     * @param mixed $value The value to assert true
     * @param mixed $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function isEqualTo($value, $otherValue)
    {
        $rule = new EqualToRule();

        return $rule->is($value, [$otherValue]);
    }


    /**
     * Checks if the value is a float value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isFloat($value)
    {
        $rule = new FloatRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is an IP address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isIP($value)
    {
        $rule = new IPRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is an IPv4 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isIPv4($value)
    {
        $rule = new IPv4Rule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is an IPv6 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isIPv6($value)
    {
        $rule = new IPv6Rule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is in the keys of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function isInKeys($value, $array)
    {
        $rule = new InKeysRule();

        return $rule->is($value, [$array]);
    }


    /**
     * Checks if the value is in the values of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function isInValues($value, $array)
    {
        $rule = new InValuesRule();

        return $rule->is($value, [$array]);
    }


    /**
     * Checks if the value is an integer value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isInt($value)
    {
        $rule = new IntRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a locale code
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isLocale($value)
    {
        $rule = new LocaleRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a number of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isMax($value, $max)
    {
        $rule = new MaxRule();

        return $rule->is($value, [$max]);
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isMin($value, $min)
    {
        $rule = new MinRule();

        return $rule->is($value, [$min]);
    }


    /**
     * Checks if the value is a regex match
     *
     * @param mixed $value The value to assert true
     * @param string $pattern The regex pattern to test with
     *
     * @return bool The result of the validation
     */
    public function isRegex($value, $pattern)
    {
        $rule = new RegexRule();

        return $rule->is($value, [$pattern]);
    }


    /**
     * Checks if the value is strictly equal to another (value and data type)
     *
     * @param mixed $value The value to assert true
     * @param string $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function isStrictEqualTo($value, $otherValue)
    {
        $rule = new StrictEqualToRule();

        return $rule->is($value, [$otherValue]);
    }


    /**
     * Checks if the value is, or can be easily converted to, a string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isString($value)
    {
        $rule = new StringRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value has a specific string length
     *
     * @param mixed $value The value to assert true
     * @param int $length The length
     *
     * @return bool The result of the validation
     */
    public function isStrlen($value, $length)
    {
        $rule = new StrlenRule();

        return $rule->is($value, [$length]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isStrlenBetween($value, $min, $max)
    {
        $rule = new StrlenBetweenRule();

        return $rule->is($value, [$min, $max]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isStrlenMax($value, $max)
    {
        $rule = new StrlenMaxRule();

        return $rule->is($value, [$max]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function isStrlenMin($value, $min)
    {
        $rule = new StrlenMinRule();

        return $rule->is($value, [$min]);
    }


    /**
     * Checks if the value is a timezone
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isTimezone($value)
    {
        $rule = new TimezoneRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is trimmed
     *
     * @param mixed $value The value to assert true
     * @param mixed $chars The characters to trim off the beginning and end of the string
     *
     * @return bool The result of the validation
     */
    public function isTrim($value, $chars = null)
    {
        $rule = new TrimRule();

        $args = $chars === null ? [] : [$chars];

        return $rule->is($value, $args);
    }


    /**
     * Checks if the value is a URL
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isURL($value)
    {
        $rule = new URLRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is a word
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function isWord($value)
    {
        $rule = new WordRule();

        return $rule->is($value);
    }


    /**
     * Checks if the value is not a valid date representing an age within a range
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notAgeBetween($value, $format, $min, $max)
    {
        $rule = new AgeBetweenRule();

        return $rule->isNot($value, [$format, $min, $max]);
    }


    /**
     * Checks if the value is not a valid date representing an age of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notAgeMax($value, $format, $max)
    {
        $rule = new AgeMaxRule();

        return $rule->isNot($value, [$format, $max]);
    }


    /**
     * Checks if the value is not a valid date representing an age of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notAgeMin($value, $format, $min)
    {
        $rule = new AgeMinRule();

        return $rule->isNot($value, [$format, $min]);
    }


    /**
     * Checks if the value is not alphanumerical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notAlnum($value)
    {
        $rule = new AlnumRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not alphabetical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notAlpha($value)
    {
        $rule = new AlphaRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not an array
     *
     * @param array $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notArray($value)
    {
        $rule = new ArrayRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a base64 encoded string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notBase64($value)
    {
        $rule = new Base64Rule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a number within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notBetween($value, $min, $max)
    {
        $rule = new BetweenRule();

        return $rule->isNot($value, [$min, $max]);
    }


    /**
     * Checks if the value is not a blank value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notBlank($value)
    {
        $rule = new BlankRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a boolean value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notBool($value)
    {
        $rule = new BoolRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a credit card number
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notCreditCard($value)
    {
        $rule = new CreditCardRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a datetime string
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     *
     * @return bool The result of the validation
     */
    public function notDatetime($value, $format)
    {
        $rule = new DatetimeRule();

        return $rule->isNot($value, [$format]);
    }


    /**
     * Checks if the value is not an email address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notEmail($value)
    {
        $rule = new EmailRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not equal to another (value only)
     *
     * @param mixed $value The value to assert true
     * @param mixed $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function notEqualTo($value, $otherValue)
    {
        $rule = new EqualToRule();

        return $rule->isNot($value, [$otherValue]);
    }


    /**
     * Checks if the value is not a float value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notFloat($value)
    {
        $rule = new FloatRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not an IP address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notIP($value)
    {
        $rule = new IPRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not an IPv4 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notIPv4($value)
    {
        $rule = new IPv4Rule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not an IPv6 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notIPv6($value)
    {
        $rule = new IPv6Rule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not in the keys of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function notInKeys($value, $array)
    {
        $rule = new InKeysRule();

        return $rule->isNot($value, [$array]);
    }


    /**
     * Checks if the value is not in the values of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function notInValues($value, $array)
    {
        $rule = new InValuesRule();

        return $rule->isNot($value, [$array]);
    }


    /**
     * Checks if the value is not an integer value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notInt($value)
    {
        $rule = new IntRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a locale code
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notLocale($value)
    {
        $rule = new LocaleRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a number of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notMax($value, $max)
    {
        $rule = new MaxRule();

        return $rule->isNot($value, [$max]);
    }


    /**
     * Checks if the value is not a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notMin($value, $min)
    {
        $rule = new MinRule();

        return $rule->isNot($value, [$min]);
    }


    /**
     * Checks if the value is not a regex match
     *
     * @param mixed $value The value to assert true
     * @param string $pattern The regex pattern to test with
     *
     * @return bool The result of the validation
     */
    public function notRegex($value, $pattern)
    {
        $rule = new RegexRule();

        return $rule->isNot($value, [$pattern]);
    }


    /**
     * Checks if the value is not strictly equal to another (value and data type)
     *
     * @param mixed $value The value to assert true
     * @param string $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function notStrictEqualTo($value, $otherValue)
    {
        $rule = new StrictEqualToRule();

        return $rule->isNot($value, [$otherValue]);
    }


    /**
     * Checks if the value is not a string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notString($value)
    {
        $rule = new StringRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value does not have a specific string length
     *
     * @param mixed $value The value to assert true
     * @param int $length The length
     *
     * @return bool The result of the validation
     */
    public function notStrlen($value, $length)
    {
        $rule = new StrlenRule();

        return $rule->isNot($value, [$length]);
    }


    /**
     * Checks if the value does not have a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notStrlenBetween($value, $min, $max)
    {
        $rule = new StrlenBetweenRule();

        return $rule->isNot($value, [$min, $max]);
    }


    /**
     * Checks if the value does not have a string length of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notStrlenMax($value, $max)
    {
        $rule = new StrlenMaxRule();

        return $rule->isNot($value, [$max]);
    }


    /**
     * Checks if the value does not have a string length of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function notStrlenMin($value, $min)
    {
        $rule = new StrlenMinRule();

        return $rule->isNot($value, [$min]);
    }


    /**
     * Checks if the value is not a timezone
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notTimezone($value)
    {
        $rule = new TimezoneRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not trimmed
     *
     * @param mixed $value The value to assert true
     * @param mixed $chars The characters to trim off the beginning and end of the string
     *
     * @return bool The result of the validation
     */
    public function notTrim($value, $chars = null)
    {
        $rule = new TrimRule();

        $args = $chars === null ? [] : [$chars];

        return $rule->isNot($value, $args);
    }


    /**
     * Checks if the value is not a URL
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notURL($value)
    {
        $rule = new URLRule();

        return $rule->isNot($value);
    }


    /**
     * Checks if the value is not a word
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function notWord($value)
    {
        $rule = new WordRule();

        return $rule->isNot($value);
    }
}

<?php
/**
 * Class StrlenMaxRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class StrlenMaxRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class StrlenMaxRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $max = $args[0];

        return mb_strlen($val) <= $max;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $max = $args[0];

        if (mb_strlen($val) > $max) {
            $val = mb_substr($val, 0, $max);
        }

        return $val;
    }
}

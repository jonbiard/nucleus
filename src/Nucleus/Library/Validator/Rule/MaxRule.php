<?php
/**
 * Class MaxRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class MaxRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class MaxRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $max = $args[0];

        return is_scalar($val) && $val <= $max;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $max = $args[0];

        return $val <= $max ? $val : $max;
    }
}

<?php
/**
 * Class StringRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class StringRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class StringRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        // Any scalar value can be represented as a string
        return (bool)is_scalar($val);
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        return (string)$val;
    }
}

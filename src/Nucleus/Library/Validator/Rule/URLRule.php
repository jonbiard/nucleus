<?php
/**
 * Class URLRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class URLRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class URLRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        // first, make sure there are no invalid chars, list from ext/filter
        $other = "$-_.+" // safe
            . "!*'()," // extra
            . "{}|\\^~[]`" // national
            . "<>#%\"" // punctuation
            . ";/?:@&="; // reserved

        $valid = 'a-zA-Z0-9' . preg_quote($other, '/');
        $clean = preg_replace("/[^$valid]/", '', $val);

        if ($val != $clean) {
            return false;
        }

        $result = @parse_url($val);
        if (empty($result['scheme']) || trim($result['scheme']) == '' ||
            empty($result['host']) || trim($result['host']) == '') {
            return false;
        }

        return true;
    }


    /**
     * Cannot be filtered
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return null Always returns null
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

<?php
/**
 * Class TrimRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class TrimRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class TrimRule extends AbstractRule
{
    /**
     * Same trim chars as PHP's trim()
     * @var string
     */
    protected $chars = " \t\n\r\0\x0B";


    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $chars = isset($args[0]) ? $args[0] : $this->chars;

        return trim($val, $chars) === (string)$val;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $chars = isset($args[0]) ? $args[0] : $this->chars;

        return trim($val, $chars);
    }
}

<?php
/**
 * Class StrlenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class StrlenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class StrlenRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $len = $args[0];

        return mb_strlen($val) == $len;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $len = $args[0];
        $padType = isset($args[1]) ? $args[1] : STR_PAD_RIGHT;
        $padChar = isset($args[2]) ? $args[2] : ' ';

        if (mb_strlen($val) < $len) {
            $val = str_pad($val, $len, $padChar, $padType);
        }

        if (mb_strlen($val) > $len) {
            $val = mb_substr($val, 0, $len);
        }

        return $val;
    }
}

<?php
/**
 * Class StrlenMinRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class StrlenMinRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class StrlenMinRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $min = $args[0];

        return mb_strlen($val) >= $min;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $min     = $args[0];
        $padType = isset($args[1]) ? $args[1] : STR_PAD_RIGHT;
        $padChar = isset($args[2]) ? $args[2] : ' ';

        if (mb_strlen($val) < $min) {
            $val = str_pad($val, $min, $padChar, $padType);
        }

        return $val;
    }
}

<?php
/**
 * Class AbstractRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class AbstractRule
 *
 * See Validator class for a usage example
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
abstract class AbstractRule
{
    /**
     * "Is" validation
     *
     * Returns true if the value complies with the rule. False otherwise.
     *
     * @param mixed $val The value to validate
     * @param array $args The extra arguments required
     *
     * @return bool Returns true if the value complies with the rule. False otherwise.
     */
    public function is($val, $args = [])
    {
        return $this->validate($val, $args);
    }


    /**
     * "Is Not" validation
     *
     * Returns true if the value does not comply with the rule. False otherwise.
     *
     * @param mixed $val The value to validate
     * @param array $args The extra arguments required
     *
     * @return bool Returns true if the value does not comply with the rule. False otherwise.
     */
    public function isNot($val, $args = [])
    {
        return !$this->validate($val, $args);
    }


    /**
     * Sanitizes the value
     *
     * @param mixed $val The value to sanitize
     * @param array $args The extra arguments required
     *
     * @return mixed Returns the sanitized value
     */
    public function fix($val, $args = [])
    {
        return $this->filter($val, $args);
    }


    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    abstract public function validate($val, $args = []);


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    abstract public function filter($val, $args = []);
}

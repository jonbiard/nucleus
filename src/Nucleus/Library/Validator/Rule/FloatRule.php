<?php
/**
 * Class FloatRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class FloatRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class FloatRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (is_float($val)) {
            return true;
        }

        return is_numeric($val) && $val == (float)$val;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (is_float($val)) {
            return $val;
        }

        if (is_numeric($val)) {
            return (float)$val;
        }

        if (!is_string($val)) {
            return (float)0;
        }

        // it's a non-numeric string, attempt to extract a float from it.
        // remove all + signs; any - sign takes precedence because ...
        // 0 + -1 = -1
        // 0 - +1 = -1
        // ... at least it seems that way to me now.
        $val = str_replace('+', '', $val);

        // reduce multiple decimals and minuses
        $val = preg_replace('/[\.-]{2,}/', '.', $val);

        // remove all decimals without a digit or minus next to them
        $val = preg_replace('/([^0-9-]\.[^0-9])/', '', $val);

        // remove all chars except digit, decimal, and minus
        $val = preg_replace('/[^0-9\.-]/', '', $val);

        // remove all trailing decimals and minuses
        $val = rtrim($val, '.-');

        // remove all minuses not at the front
        $isNegative = ($val[0] == '-');
        $val = str_replace('-', '', $val);
        if ($isNegative) {
            $val = '-' . $val;
        }

        // remove all decimals but the first
        $pos = strpos($val, '.');
        $val = str_replace('.', '', $val);
        if ($pos !== false) {
            $val = substr($val, 0, $pos) . '.' . substr($val, $pos);
        }

        return (float)$val;
    }
}

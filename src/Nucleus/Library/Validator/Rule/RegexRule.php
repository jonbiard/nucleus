<?php
/**
 * Class RegexRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class RegexRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class RegexRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $pattern = $args[0];

        return (bool)preg_match($pattern, $val);
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $pattern = $args[0];
        $replace = isset($args[1]) ? $args[1] : '';

        return preg_replace($pattern, $replace, $val);
    }
}

<?php
/**
 * Class BoolRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class BoolRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class BoolRule extends AbstractRule
{
    /**
     * Aliases of "true"
     * @var array
     */
    private $true  = ['1', 'on', 'true', 't', 'yes', 'y'];
    /**
     * Aliases of "false"
     * @var array
     */
    private $false = ['0', 'off', 'false', 'f', 'no', 'n'];

    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if ($val === true || $val === false) {
            return true;
        }

        $lower = strtolower(trim($val));
        if (in_array($lower, $this->true, true)) {
            return true;
        } elseif (in_array($lower, $this->false, true)) {
            return true;
        }

        return false;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if ($val === true || $val === false) {
            return $val;
        }

        $lower = strtolower(trim($val));
        if (in_array($lower, $this->true, true)) {
            return true;
        } elseif (in_array($lower, $this->false, true)) {
            return false;
        }

        return null;
    }
}

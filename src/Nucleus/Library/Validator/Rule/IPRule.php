<?php
/**
 * Class IPRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class IPRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class IPRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        return (bool)filter_var($val, FILTER_VALIDATE_IP);
    }


    /**
     * Cannot be filtered
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return null Always returns null
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

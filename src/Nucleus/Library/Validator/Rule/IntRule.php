<?php
/**
 * Class IntRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class IntRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class IntRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (is_int($val)) {
            return true;
        }

        return is_numeric($val) && $val == (int)$val;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (is_numeric($val)) {
            // we double-cast here to honor scientific notation.
            // (int) 1E5 == 15, but (int) (float) 1E5 == 100000
            return (int)((float)$val);
        }

        if (!is_string($val)) {
            return (int)0;
        }

        // it's a non-numeric string, attempt to extract an integer from it.

        // remove all chars except digit and minus.
        // this removes all + signs; any - sign takes precedence because ...
        // 0 + -1 = -1
        // 0 - +1 = -1
        // ... at least it seems that way to me now.
        $val = preg_replace('/[^0-9-]/', '', $val);

        // remove all trailing minuses
        $val = rtrim($val, '-');

        // remove all minuses not at the front
        $isNegative = ($val[0] == '-');
        $val = str_replace('-', '', $val);
        if ($isNegative) {
            $val = '-' . $val;
        }

        return (int)$val;
    }
}

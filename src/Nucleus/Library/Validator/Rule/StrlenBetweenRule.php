<?php
/**
 * Class StrlenBetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class StrlenBetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class StrlenBetweenRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        if (!is_scalar($val)) {
            return false;
        }

        $min = $args[0];
        $max = $args[1];
        $len = mb_strlen($val);

        return ($len >= $min && $len <= $max);
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $min = $args[0];
        $max = $args[1];
        $padType = isset($args[2]) ? $args[2] : STR_PAD_RIGHT;
        $padChar = isset($args[3]) ? $args[3] : ' ';

        if (mb_strlen($val) < $min) {
            $val = str_pad($val, $min, $padChar, $padType);
        }

        if (mb_strlen($val) > $max) {
            $val = mb_substr($val, 0, $max);
        }

        return $val;
    }
}

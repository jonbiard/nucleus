<?php
/**
 * Class TimezoneRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

use DateTimeZone;
use Exception;

/**
 * Class TimezoneRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class TimezoneRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        try {
            new DateTimeZone($val);
        } catch (Exception $e) {
            return false;
        }

        return true;
    }


    /**
     * Cannot be filtered
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return null Always returns null
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

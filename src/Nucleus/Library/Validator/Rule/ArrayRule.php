<?php
/**
 * Class ArrayRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class ArrayRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class ArrayRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $elementRule = isset($args[0]) ? $args[0] : null;

        if ($elementRule instanceof AbstractRule) {
            if (!is_array($val)) {
                return false;
            }

            $newArgs = $args;
            array_shift($newArgs);

            foreach ($val as $v) {
                if (!$elementRule->validate($v, $newArgs)) {
                    return false;
                }
            }

            return true;
        }

        return is_array($val);
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        return (array)$val;
    }
}

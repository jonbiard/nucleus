<?php
/**
 * Class LocaleRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class LocaleRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class LocaleRule extends AbstractRule
{
    /**
     * Valid locale codes
     * @var array
     */
    private $localeCodes = [
        'af_ZA', 'am_ET', 'be_BY', 'bg_BG', 'ca_ES', 'cs_CZ', 'da_DK',
        'de_AT', 'de_CH', 'de_DE', 'el_GR', 'en_AU', 'en_CA', 'en_GB',
        'en_IE', 'en_NZ', 'en_US', 'es_ES', 'et_EE', 'eu_ES', 'fi_FI',
        'fr_BE', 'fr_CA', 'fr_CH', 'fr_FR', 'he_IL', 'hi_IN', 'hr_HR',
        'hu_HU', 'hy_AM', 'is_IS', 'it_CH', 'it_IT', 'ja_JP', 'kk_KZ',
        'ko_KR', 'lt_LT', 'nl_BE', 'nl_NL', 'no_NO', 'pl_PL', 'pt_BR',
        'pt_PT', 'ro_RO', 'ru_RU', 'sk_SK', 'sl_SI', 'sr_YU', 'sv_SE',
        'tr_TR', 'uk_UA', 'zh_CN', 'zh_HK', 'zh_TW',
    ];


    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        return in_array($val, $this->localeCodes);
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

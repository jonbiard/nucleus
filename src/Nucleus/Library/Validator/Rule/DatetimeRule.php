<?php
/**
 * Class DatetimeRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

use DateTime;

/**
 * Class DatetimeRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class DatetimeRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $format = isset($args[0]) ? $args[0] : 'Y-m-d H:i:s';

        if ($date = DateTime::createFromFormat($format, $val)) {
            $errors = $date->getLastErrors();

            return !($errors['error_count'] || $errors['warning_count']);
        }

        return false;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        $format = isset($args[0]) ? $args[0] : 'Y-m-d H:i:s';

        $date = DateTime::createFromFormat($format, $val);

        return $date ? $date->format($format) : null;
    }
}

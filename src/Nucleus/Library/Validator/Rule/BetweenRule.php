<?php
/**
 * Class BetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class BetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class BetweenRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $min = $args[0];
        $max = $args[1];

        return is_scalar($val) && $val >= $min && $val <= $max;
    }


    /**
     * Filters value
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return mixed The filtered value
     */
    public function filter($val, $args = [])
    {
        if (!is_scalar($val)) {
            return null;
        }

        $min = $args[0];
        $max = $args[1];

        if ($val < $min) {
            return $min;

        } elseif ($val > $max) {
            return $max;
        }

        return $val;
    }
}

<?php
/**
 * Class AgeBetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

use DateTime;

/**
 * Class AgeBetweenRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class AgeBetweenRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $format = isset($args[0]) ? $args[0] : 'Y-m-d H:i:s';
        $min = isset($args[1]) ? $args[1] : 0;
        $max = isset($args[2]) ? $args[2] : PHP_INT_MAX;

        if ($date = DateTime::createFromFormat($format, $val)) {
            list($year, $month, $day) = sscanf($date->format('Y-m-d'), '%d-%d-%d');

            $currentDay   = (int)date('j');
            $currentMonth = (int)date('n');
            $currentYear  = (int)date('Y');

            $age = $currentYear - $year;

            if ($currentMonth < $month || ($currentMonth == $month && $currentDay < $day)) {
                $age--;
            }

            return $age >= $min && $age <= $max;
        }

        return false;
    }


    /**
     * Cannot be filtered
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return null Always returns null
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

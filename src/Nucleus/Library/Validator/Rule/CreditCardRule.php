<?php
/**
 * Class CreditCardRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator\Rule;

/**
 * Class CreditCardRule
 *
 * @package Nucleus\Library
 * @subpackage Validator
 */
class CreditCardRule extends AbstractRule
{
    /**
     * Validates
     *
     * @param mixed $val The value to validate
     * @param array $args The arguments to use
     *
     * @return bool True on success or false on failure
     */
    public function validate($val, $args = [])
    {
        $value = str_replace([' ', '-', '.'], '', (string)$val);

        if (!ctype_digit($value)) {
            return false;
        }

        $sumTable = [
            [0,1,2,3,4,5,6,7,8,9],
            [0,2,4,6,8,1,3,5,7,9],
        ];

        $sum = 0;
        $flip = 0;

        for ($i = strlen($value) - 1; $i >= 0; $i--) {
            $sum += $sumTable[$flip++ & 0x1][$value[$i]];
        }

        return $sum % 10 === 0;
    }


    /**
     * Cannot be filtered
     *
     * @param mixed $val The value to filter
     * @param array $args The arguments to use
     *
     * @return null Always returns null
     */
    public function filter($val, $args = [])
    {
        return null;
    }
}

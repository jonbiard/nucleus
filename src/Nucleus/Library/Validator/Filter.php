<?php
/**
 * Class Filter
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator;

use Nucleus\Library\Validator\Rule\AgeBetweenRule;
use Nucleus\Library\Validator\Rule\AgeMaxRule;
use Nucleus\Library\Validator\Rule\AgeMinRule;
use Nucleus\Library\Validator\Rule\AlnumRule;
use Nucleus\Library\Validator\Rule\AlphaRule;
use Nucleus\Library\Validator\Rule\ArrayRule;
use Nucleus\Library\Validator\Rule\Base64Rule;
use Nucleus\Library\Validator\Rule\BetweenRule;
use Nucleus\Library\Validator\Rule\BlankRule;
use Nucleus\Library\Validator\Rule\BoolRule;
use Nucleus\Library\Validator\Rule\CreditCardRule;
use Nucleus\Library\Validator\Rule\DatetimeRule;
use Nucleus\Library\Validator\Rule\EmailRule;
use Nucleus\Library\Validator\Rule\EqualToRule;
use Nucleus\Library\Validator\Rule\FloatRule;
use Nucleus\Library\Validator\Rule\InKeysRule;
use Nucleus\Library\Validator\Rule\IntRule;
use Nucleus\Library\Validator\Rule\InValuesRule;
use Nucleus\Library\Validator\Rule\IPRule;
use Nucleus\Library\Validator\Rule\IPv4Rule;
use Nucleus\Library\Validator\Rule\IPv6Rule;
use Nucleus\Library\Validator\Rule\LocaleRule;
use Nucleus\Library\Validator\Rule\MaxRule;
use Nucleus\Library\Validator\Rule\MinRule;
use Nucleus\Library\Validator\Rule\RegexRule;
use Nucleus\Library\Validator\Rule\StrictEqualToRule;
use Nucleus\Library\Validator\Rule\StringRule;
use Nucleus\Library\Validator\Rule\StrlenBetweenRule;
use Nucleus\Library\Validator\Rule\StrlenMaxRule;
use Nucleus\Library\Validator\Rule\StrlenMinRule;
use Nucleus\Library\Validator\Rule\StrlenRule;
use Nucleus\Library\Validator\Rule\TimezoneRule;
use Nucleus\Library\Validator\Rule\TrimRule;
use Nucleus\Library\Validator\Rule\URLRule;
use Nucleus\Library\Validator\Rule\WordRule;

/**
 * Class Filter
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Filter
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Checks if the value is a valid date representing an age within a range
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixAgeBetween($value, $format, $min, $max)
    {
        $rule = new AgeBetweenRule();

        return $rule->fix($value, [$format, $min, $max]);
    }


    /**
     * Checks if the value is a valid date representing an age of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $max The maximum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixAgeMax($value, $format, $max)
    {
        $rule = new AgeMaxRule();

        return $rule->fix($value, [$format, $max]);
    }


    /**
     * Checks if the value is a valid date representing an age of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixAgeMin($value, $format, $min)
    {
        $rule = new AgeMinRule();

        return $rule->fix($value, [$format, $min]);
    }


    /**
     * Checks if the value is alphanumerical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixAlnum($value)
    {
        $rule = new AlnumRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is alphabetical
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixAlpha($value)
    {
        $rule = new AlphaRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is an array
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixArray($value)
    {
        $rule = new ArrayRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a base64 encoded string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixBase64($value)
    {
        $rule = new Base64Rule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixBetween($value, $min, $max)
    {
        $rule = new BetweenRule();

        return $rule->fix($value, [$min, $max]);
    }


    /**
     * Checks if the value is a blank value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixBlank($value)
    {
        $rule = new BlankRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a boolean value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixBool($value)
    {
        $rule = new BoolRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a credit card number
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixCreditCard($value)
    {
        $rule = new CreditCardRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a datetime string
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     *
     * @return bool The result of the validation
     */
    public function fixDatetime($value, $format)
    {
        $rule = new DatetimeRule();

        return $rule->fix($value, [$format]);
    }


    /**
     * Checks if the value is an email address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixEmail($value)
    {
        $rule = new EmailRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is equal to another (value only)
     *
     * @param mixed $value The value to assert true
     * @param mixed $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function fixEqualTo($value, $otherValue)
    {
        $rule = new EqualToRule();

        return $rule->fix($value, [$otherValue]);
    }


    /**
     * Checks if the value is a float value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixFloat($value)
    {
        $rule = new FloatRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is an IP address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixIP($value)
    {
        $rule = new IPRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is an IPv4 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixIPv4($value)
    {
        $rule = new IPv4Rule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is an IPv6 address
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixIPv6($value)
    {
        $rule = new IPv6Rule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is in the keys of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function fixInKeys($value, $array)
    {
        $rule = new InKeysRule();

        return $rule->fix($value, [$array]);
    }


    /**
     * Checks if the value is in the values of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @return bool The result of the validation
     */
    public function fixInValues($value, $array)
    {
        $rule = new InValuesRule();

        return $rule->fix($value, [$array]);
    }


    /**
     * Checks if the value is an integer value
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixInt($value)
    {
        $rule = new IntRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a locale code
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixLocale($value)
    {
        $rule = new LocaleRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a number of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixMax($value, $max)
    {
        $rule = new MaxRule();

        return $rule->fix($value, [$max]);
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @return bool The result of the validation
     */
    public function fixMin($value, $min)
    {
        $rule = new MinRule();

        return $rule->fix($value, [$min]);
    }


    /**
     * Checks if the value is a regex match
     *
     * @param mixed $value The value to assert true
     * @param string $pattern The regex pattern to search with
     * @param string $replace The string to replace with
     *
     * @return bool The result of the validation
     */
    public function fixRegex($value, $pattern, $replace)
    {
        $rule = new RegexRule();

        return $rule->fix($value, [$pattern, $replace]);
    }


    /**
     * Checks if the value is strictly equal to another (value and data type)
     *
     * @param mixed $value The value to assert true
     * @param string $otherValue The value to test against
     *
     * @return bool The result of the validation
     */
    public function fixStrictEqualTo($value, $otherValue)
    {
        $rule = new StrictEqualToRule();

        return $rule->fix($value, [$otherValue]);
    }


    /**
     * Checks if the value is, or can be easily converted to, a string
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixString($value)
    {
        $rule = new StringRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value has a specific string length
     *
     * @param mixed $value The value to assert true
     * @param int $length The length
     * @param int $padType The padding type (STR_PAD_RIGHT, STR_PAD_LEFT, or STR_PAD_BOTH)
     * @param string $padChar The padding character or characters to be used
     *
     * @return bool The result of the validation
     */
    public function fixStrlen($value, $length, $padType = STR_PAD_RIGHT, $padChar = ' ')
    {
        $rule = new StrlenRule();

        return $rule->fix($value, [$length, $padType, $padChar]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     * @param int $padType The padding type (STR_PAD_RIGHT, STR_PAD_LEFT, or STR_PAD_BOTH)
     * @param string $padChar The padding character or characters to be used
     *
     * @return bool The result of the validation
     */
    public function fixStrlenBetween($value, $min, $max, $padType = STR_PAD_RIGHT, $padChar = ' ')
    {
        $rule = new StrlenBetweenRule();

        return $rule->fix($value, [$min, $max, $padType, $padChar]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     * @param int $padType The padding type (STR_PAD_RIGHT, STR_PAD_LEFT, or STR_PAD_BOTH)
     * @param string $padChar The padding character or characters to be used
     *
     * @return bool The result of the validation
     */
    public function fixStrlenMax($value, $max, $padType = STR_PAD_RIGHT, $padChar = ' ')
    {
        $rule = new StrlenMaxRule();

        return $rule->fix($value, [$max, $padType, $padChar]);
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $padType The padding type (STR_PAD_RIGHT, STR_PAD_LEFT, or STR_PAD_BOTH)
     * @param string $padChar The padding character or characters to be used
     *
     * @return bool The result of the validation
     */
    public function fixStrlenMin($value, $min, $padType = STR_PAD_RIGHT, $padChar = ' ')
    {
        $rule = new StrlenMinRule();

        return $rule->fix($value, [$min, $padType, $padChar]);
    }


    /**
     * Checks if the value is a timezone
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixTimezone($value)
    {
        $rule = new TimezoneRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is trimmed
     *
     * @param mixed $value The value to assert true
     * @param mixed $chars The characters to trim off the beginning and end of the string
     *
     * @return bool The result of the validation
     */
    public function fixTrim($value, $chars = null)
    {
        $rule = new TrimRule();

        $args = $chars === null ? [] : [$chars];

        return $rule->fix($value, $args);
    }


    /**
     * Checks if the value is a URL
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixURL($value)
    {
        $rule = new URLRule();

        return $rule->fix($value);
    }


    /**
     * Checks if the value is a word
     *
     * @param mixed $value The value to assert true
     *
     * @return bool The result of the validation
     */
    public function fixWord($value)
    {
        $rule = new WordRule();

        return $rule->fix($value);
    }
}

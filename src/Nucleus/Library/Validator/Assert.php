<?php
/**
 * Class Assert
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Validator;

use Nucleus\Library\Exception\AssertionException;
use Nucleus\Library\Validator\Rule\AgeBetweenRule;
use Nucleus\Library\Validator\Rule\AgeMaxRule;
use Nucleus\Library\Validator\Rule\AgeMinRule;
use Nucleus\Library\Validator\Rule\AlnumRule;
use Nucleus\Library\Validator\Rule\AlphaRule;
use Nucleus\Library\Validator\Rule\ArrayRule;
use Nucleus\Library\Validator\Rule\Base64Rule;
use Nucleus\Library\Validator\Rule\BetweenRule;
use Nucleus\Library\Validator\Rule\BlankRule;
use Nucleus\Library\Validator\Rule\BoolRule;
use Nucleus\Library\Validator\Rule\CreditCardRule;
use Nucleus\Library\Validator\Rule\DatetimeRule;
use Nucleus\Library\Validator\Rule\EmailRule;
use Nucleus\Library\Validator\Rule\EqualToRule;
use Nucleus\Library\Validator\Rule\FloatRule;
use Nucleus\Library\Validator\Rule\InKeysRule;
use Nucleus\Library\Validator\Rule\IntRule;
use Nucleus\Library\Validator\Rule\InValuesRule;
use Nucleus\Library\Validator\Rule\IPRule;
use Nucleus\Library\Validator\Rule\IPv4Rule;
use Nucleus\Library\Validator\Rule\IPv6Rule;
use Nucleus\Library\Validator\Rule\LocaleRule;
use Nucleus\Library\Validator\Rule\MaxRule;
use Nucleus\Library\Validator\Rule\MinRule;
use Nucleus\Library\Validator\Rule\RegexRule;
use Nucleus\Library\Validator\Rule\StrictEqualToRule;
use Nucleus\Library\Validator\Rule\StringRule;
use Nucleus\Library\Validator\Rule\StrlenBetweenRule;
use Nucleus\Library\Validator\Rule\StrlenMaxRule;
use Nucleus\Library\Validator\Rule\StrlenMinRule;
use Nucleus\Library\Validator\Rule\StrlenRule;
use Nucleus\Library\Validator\Rule\TimezoneRule;
use Nucleus\Library\Validator\Rule\TrimRule;
use Nucleus\Library\Validator\Rule\URLRule;
use Nucleus\Library\Validator\Rule\WordRule;

/**
 * Class Assert
 *
 * @package Nucleus\Library
 * @subpackage Validator
 *
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class Assert
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Checks if the value is a valid date representing an age within a range
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     * @param int $max The maximum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isAgeBetween($value, $format, $min, $max)
    {
        $rule = new AgeBetweenRule();

        if (!$rule->is($value, [$format, $min, $max])) {
            throw new AssertionException("Assertion failure: Not a date representing an age between $min and $max");
        }
    }


    /**
     * Checks if the value is a valid date representing an age of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $max The maximum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isAgeMax($value, $format, $max)
    {
        $rule = new AgeMaxRule();

        if (!$rule->is($value, [$format, $max])) {
            throw new AssertionException("Assertion failure: Not a date representing an age of at most $max");
        }
    }


    /**
     * Checks if the value is a valid date representing an age of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isAgeMin($value, $format, $min)
    {
        $rule = new AgeMinRule();

        if (!$rule->is($value, [$format, $min])) {
            throw new AssertionException("Assertion failure: Not a date representing an age of at least $min");
        }
    }


    /**
     * Checks if the value is alphanumerical
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isAlnum($value)
    {
        $rule = new AlnumRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not alphanumerical");
        }
    }


    /**
     * Checks if the value is alphabetical
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isAlpha($value)
    {
        $rule = new AlphaRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not alphabetical");
        }
    }


    /**
     * Checks if the value is an array
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isArray($value)
    {
        $rule = new ArrayRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an array'");
        }
    }


    /**
     * Checks if the value is a base64 encoded string
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isBase64($value)
    {
        $rule = new Base64Rule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a base64 encoded string");
        }
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isBetween($value, $min, $max)
    {
        $rule = new BetweenRule();

        if (!$rule->is($value, [$min, $max])) {
            throw new AssertionException("Assertion failure: Not a number between $min and $max");
        }
    }


    /**
     * Checks if the value is a blank value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isBlank($value)
    {
        $rule = new BlankRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a blank value");
        }
    }


    /**
     * Checks if the value is a boolean value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isBool($value)
    {
        $rule = new BoolRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a boolean value");
        }
    }


    /**
     * Checks if the value is a credit card number
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isCreditCard($value)
    {
        $rule = new CreditCardRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a credit card number");
        }
    }


    /**
     * Checks if the value is a datetime string
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isDatetime($value, $format)
    {
        $rule = new DatetimeRule();

        if (!$rule->is($value, [$format])) {
            throw new AssertionException("Assertion failure: Not a datetime string");
        }
    }


    /**
     * Checks if the value is an email address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isEmail($value)
    {
        $rule = new EmailRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an email address");
        }
    }


    /**
     * Checks if the value is equal to another (value only)
     *
     * @param mixed $value The value to assert true
     * @param mixed $otherValue The value to test against
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isEqualTo($value, $otherValue)
    {
        $rule = new EqualToRule();

        if (!$rule->is($value, [$otherValue])) {
            throw new AssertionException("Assertion failure: Not equal");
        }
    }


    /**
     * Checks if the value is a float value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isFloat($value)
    {
        $rule = new FloatRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a float value");
        }
    }


    /**
     * Checks if the value is an IP address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isIP($value)
    {
        $rule = new IPRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an IP address");
        }
    }


    /**
     * Checks if the value is an IPv4 address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isIPv4($value)
    {
        $rule = new IPv4Rule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an IPv4 address");
        }
    }


    /**
     * Checks if the value is an IPv6 address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isIPv6($value)
    {
        $rule = new IPv6Rule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an IPv6 address");
        }
    }


    /**
     * Checks if the value is in the keys of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isInKeys($value, $array)
    {
        $rule = new InKeysRule();

        if (!$rule->is($value, [$array])) {
            throw new AssertionException("Assertion failure: Not in the keys of the array");
        }
    }


    /**
     * Checks if the value is in the values of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isInValues($value, $array)
    {
        $rule = new InValuesRule();

        if (!$rule->is($value, [$array])) {
            throw new AssertionException("Assertion failure: Not in the values of the array");
        }
    }


    /**
     * Checks if the value is an integer value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isInt($value)
    {
        $rule = new IntRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not an integer value");
        }
    }


    /**
     * Checks if the value is a locale code
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isLocale($value)
    {
        $rule = new LocaleRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a locale code");
        }
    }


    /**
     * Checks if the value is a number of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isMax($value, $max)
    {
        $rule = new MaxRule();

        if (!$rule->is($value, [$max])) {
            throw new AssertionException("Assertion failure: Not a number of at most $max'");
        }
    }


    /**
     * Checks if the value is a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isMin($value, $min)
    {
        $rule = new MinRule();

        if (!$rule->is($value, [$min])) {
            throw new AssertionException("Assertion failure: Not a number of at least $min");
        }
    }


    /**
     * Checks if the value is a regex match
     *
     * @param mixed $value The value to assert true
     * @param string $pattern The regex pattern to test with
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isRegex($value, $pattern)
    {
        $rule = new RegexRule();

        if (!$rule->is($value, [$pattern])) {
            throw new AssertionException("Assertion failure: Not a regex match");
        }
    }


    /**
     * Checks if the value is strictly equal to another (value and data type)
     *
     * @param mixed $value The value to assert true
     * @param string $otherValue The value to test against
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isStrictEqualTo($value, $otherValue)
    {
        $rule = new StrictEqualToRule();

        if (!$rule->is($value, [$otherValue])) {
            throw new AssertionException("Assertion failure: Not strictly equal");
        }
    }


    /**
     * Checks if the value is, or can be easily converted to, a string
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isString($value)
    {
        $rule = new StringRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a string");
        }
    }


    /**
     * Checks if the value has a specific string length
     *
     * @param mixed $value The value to assert true
     * @param int $length The length
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isStrlen($value, $length)
    {
        $rule = new StrlenRule();

        if (!$rule->is($value, [$length])) {
            throw new AssertionException("Assertion failure: Not a string with length of $length");
        }
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isStrlenBetween($value, $min, $max)
    {
        $rule = new StrlenBetweenRule();

        if (!$rule->is($value, [$min, $max])) {
            throw new AssertionException("Assertion failure: Not a string with length between $min and $max");
        }
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isStrlenMax($value, $max)
    {
        $rule = new StrlenMaxRule();

        if (!$rule->is($value, [$max])) {
            throw new AssertionException("Assertion failure: Not a string with length of at most $max");
        }
    }


    /**
     * Checks if the value has a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isStrlenMin($value, $min)
    {
        $rule = new StrlenMinRule();

        if (!$rule->is($value, [$min])) {
            throw new AssertionException("Assertion failure: Not a string with length of at least $min");
        }
    }


    /**
     * Checks if the value is a timezone
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isTimezone($value)
    {
        $rule = new TimezoneRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a timezone");
        }
    }


    /**
     * Checks if the value is trimmed
     *
     * @param mixed $value The value to assert true
     * @param mixed $chars The characters to trim off the beginning and end of the string
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isTrim($value, $chars = null)
    {
        $rule = new TrimRule();

        $args = $chars === null ? [] : [$chars];

        if (!$rule->is($value, $args)) {
            throw new AssertionException("Assertion failure: Not trimmed");
        }
    }


    /**
     * Checks if the value is a URL
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isURL($value)
    {
        $rule = new URLRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a URL");
        }
    }


    /**
     * Checks if the value is a word
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function isWord($value)
    {
        $rule = new WordRule();

        if (!$rule->is($value)) {
            throw new AssertionException("Assertion failure: Not a word");
        }
    }


    /**
     * Checks if the value is not a valid date representing an age within a range
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     * @param int $max The maximum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notAgeBetween($value, $format, $min, $max)
    {
        $rule = new AgeBetweenRule();

        if (!$rule->isNot($value, [$format, $min, $max])) {
            throw new AssertionException("Assertion failure: Is a date representing an age between $min and $max");
        }
    }


    /**
     * Checks if the value is not a valid date representing an age of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $max The maximum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notAgeMax($value, $format, $max)
    {
        $rule = new AgeMaxRule();

        if (!$rule->isNot($value, [$format, $max])) {
            throw new AssertionException("Assertion failure: Is a date representing an age of at most $max");
        }
    }


    /**
     * Checks if the value is not a valid date representing an age of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     * @param int $min The minimum age (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notAgeMin($value, $format, $min)
    {
        $rule = new AgeMinRule();

        if (!$rule->isNot($value, [$format, $min])) {
            throw new AssertionException("Assertion failure: Is a date representing an age of at least $min");
        }
    }


    /**
     * Checks if the value is not alphanumerical
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notAlnum($value)
    {
        $rule = new AlnumRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is alphanumerical");
        }
    }


    /**
     * Checks if the value is not alphabetical
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notAlpha($value)
    {
        $rule = new AlphaRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is alphabetical");
        }
    }


    /**
     * Checks if the value is not an array
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notArray($value)
    {
        $rule = new ArrayRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an array'");
        }
    }


    /**
     * Checks if the value is not a base64 encoded string
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notBase64($value)
    {
        $rule = new Base64Rule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a base64 encoded string");
        }
    }


    /**
     * Checks if the value is not a number within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notBetween($value, $min, $max)
    {
        $rule = new BetweenRule();

        if (!$rule->isNot($value, [$min, $max])) {
            throw new AssertionException("Assertion failure: Is a number between $min and $max");
        }
    }


    /**
     * Checks if the value is not a blank value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notBlank($value)
    {
        $rule = new BlankRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a blank value");
        }
    }


    /**
     * Checks if the value is not a boolean value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notBool($value)
    {
        $rule = new BoolRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a boolean value");
        }
    }


    /**
     * Checks if the value is not a credit card number
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notCreditCard($value)
    {
        $rule = new CreditCardRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a credit card number");
        }
    }


    /**
     * Checks if the value is not a datetime string
     *
     * @param mixed $value The value to assert true
     * @param string $format The format of the date (see PHP's DateTime::createFromFormat() function)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notDatetime($value, $format)
    {
        $rule = new DatetimeRule();

        if (!$rule->isNot($value, [$format])) {
            throw new AssertionException("Assertion failure: Is a datetime string");
        }
    }


    /**
     * Checks if the value is not an email address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notEmail($value)
    {
        $rule = new EmailRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an email address");
        }
    }


    /**
     * Checks if the value is not equal to another (value only)
     *
     * @param mixed $value The value to assert true
     * @param mixed $otherValue The value to test against
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notEqualTo($value, $otherValue)
    {
        $rule = new EqualToRule();

        if (!$rule->isNot($value, [$otherValue])) {
            throw new AssertionException("Assertion failure: Is equal");
        }
    }


    /**
     * Checks if the value is not a float value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notFloat($value)
    {
        $rule = new FloatRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a float value");
        }
    }


    /**
     * Checks if the value is not an IP address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notIP($value)
    {
        $rule = new IPRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an IP address");
        }
    }


    /**
     * Checks if the value is not an IPv4 address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notIPv4($value)
    {
        $rule = new IPv4Rule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an IPv4 address");
        }
    }


    /**
     * Checks if the value is not an IPv6 address
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notIPv6($value)
    {
        $rule = new IPv6Rule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an IPv6 address");
        }
    }


    /**
     * Checks if the value is not in the keys of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notInKeys($value, $array)
    {
        $rule = new InKeysRule();

        if (!$rule->isNot($value, [$array])) {
            throw new AssertionException("Assertion failure: Is in the keys of the array");
        }
    }


    /**
     * Checks if the value is not in the values of an array
     *
     * @param int|string $value The value to assert true
     * @param array $array The array to check in
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notInValues($value, $array)
    {
        $rule = new InValuesRule();

        if (!$rule->isNot($value, [$array])) {
            throw new AssertionException("Assertion failure: Is in the values of the array");
        }
    }


    /**
     * Checks if the value is not an integer value
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notInt($value)
    {
        $rule = new IntRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is an integer value");
        }
    }


    /**
     * Checks if the value is not a locale code
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notLocale($value)
    {
        $rule = new LocaleRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a locale code");
        }
    }


    /**
     * Checks if the value is not a number of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notMax($value, $max)
    {
        $rule = new MaxRule();

        if (!$rule->isNot($value, [$max])) {
            throw new AssertionException("Assertion failure: Is a number of at most $max'");
        }
    }


    /**
     * Checks if the value is not a number between
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notMin($value, $min)
    {
        $rule = new MinRule();

        if (!$rule->isNot($value, [$min])) {
            throw new AssertionException("Assertion failure: Is a number of at least $min");
        }
    }


    /**
     * Checks if the value is not a regex match
     *
     * @param mixed $value The value to assert true
     * @param string $pattern The regex pattern to test with
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notRegex($value, $pattern)
    {
        $rule = new RegexRule();

        if (!$rule->isNot($value, [$pattern])) {
            throw new AssertionException("Assertion failure: Is a regex match");
        }
    }


    /**
     * Checks if the value is not strictly equal to another (value and data type)
     *
     * @param mixed $value The value to assert true
     * @param string $otherValue The value to test against
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notStrictEqualTo($value, $otherValue)
    {
        $rule = new StrictEqualToRule();

        if (!$rule->isNot($value, [$otherValue])) {
            throw new AssertionException("Assertion failure: Is strictly equal");
        }
    }


    /**
     * Checks if the value is not a string
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notString($value)
    {
        $rule = new StringRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a string");
        }
    }


    /**
     * Checks if the value does not have a specific string length
     *
     * @param mixed $value The value to assert true
     * @param int $length The length
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notStrlen($value, $length)
    {
        $rule = new StrlenRule();

        if (!$rule->isNot($value, [$length])) {
            throw new AssertionException("Assertion failure: Is a string with length of $length");
        }
    }


    /**
     * Checks if the value does not have a string length within a range
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notStrlenBetween($value, $min, $max)
    {
        $rule = new StrlenBetweenRule();

        if (!$rule->isNot($value, [$min, $max])) {
            throw new AssertionException("Assertion failure: Is a string with length between $min and $max");
        }
    }


    /**
     * Checks if the value does not have a string length of at most maximum
     *
     * @param mixed $value The value to assert true
     * @param int $max The maximum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notStrlenMax($value, $max)
    {
        $rule = new StrlenMaxRule();

        if (!$rule->isNot($value, [$max])) {
            throw new AssertionException("Assertion failure: Is a string with length of at most $max");
        }
    }


    /**
     * Checks if the value does not have a string length of at least minimum
     *
     * @param mixed $value The value to assert true
     * @param int $min The minimum (inclusively)
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notStrlenMin($value, $min)
    {
        $rule = new StrlenMinRule();

        if (!$rule->isNot($value, [$min])) {
            throw new AssertionException("Assertion failure: Is a string with length of at least $min");
        }
    }


    /**
     * Checks if the value is not a timezone
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notTimezone($value)
    {
        $rule = new TimezoneRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a timezone");
        }
    }


    /**
     * Checks if the value is not trimmed
     *
     * @param mixed $value The value to assert true
     * @param mixed $chars The characters to trim off the beginning and end of the string
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notTrim($value, $chars = null)
    {
        $rule = new TrimRule();

        $args = $chars === null ? [] : [$chars];

        if (!$rule->isNot($value, $args)) {
            throw new AssertionException("Assertion failure: Is trimmed");
        }
    }


    /**
     * Checks if the value is not a URL
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notURL($value)
    {
        $rule = new URLRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a URL");
        }
    }


    /**
     * Checks if the value is not a word
     *
     * @param mixed $value The value to assert true
     *
     * @throws AssertionException When the assertion fails to evaluate to true
     */
    public function notWord($value)
    {
        $rule = new WordRule();

        if (!$rule->isNot($value)) {
            throw new AssertionException("Assertion failure: Is a word");
        }
    }
}

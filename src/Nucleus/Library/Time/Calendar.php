<?php
/**
 * Class Calendar
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

/**
 * Class Calendar
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class Calendar
{
    /**
     * The datetime object
     * @var DateTime
     */
    private $datetime;


    /**
     * Constructor
     *
     * @param DateTime $datetime The datetime object
     */
    public function __construct(DateTime $datetime)
    {
        $this->datetime = $datetime;
    }


    /**
     * Get an array of days representing the calendar
     *
     * @return array The array of days
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function get()
    {
        $arr       = [];
        $days      = $this->datetime->getDaysInMonth();
        $year      = $this->datetime->getYear();
        $month     = $this->datetime->getMonth();
        $prevMonth = $month > 1 ? $month - 1 : 12;
        $prevYear  = $prevMonth === 12 ? $year - 1 : $year;

        $prevDays = DateTime::createFromFormat('Y-m-d H:i:s', "$prevYear-$prevMonth-01 00:00:00")->getDaysInMonth();
        $startDay = DateTime::createFromFormat('Y-m-d H:i:s', "$year-$month-01 00:00:00");

        // Start at the right place in the previous month
        if ($startDay->getWeekday()) {
            $day = $prevDays - $startDay->getWeekday() + 1;
        } else {
            $day = $prevDays - 6;
        }

        // Fill the first row and rollover using last month's total days
        $inMonth = false;
        for ($j = 0; $j < 7; $j++) {
            if ($day > $prevDays) {
                $day     = 1;
                $inMonth = true;
            }

            $arr[0][$j] = new CalendarDay($day++, $inMonth);
        }

        // Fill the rest of the rows and rollover using this month's total days
        $day     = $day > 10 ? 1 : $day;
        $inMonth = true;
        for ($i = 1; $i < 6; $i++) {
            for ($j = 0; $j < 7; $j++) {
                if ($day > $days) {
                    $day     = 1;
                    $inMonth = false;
                }

                $arr[$i][$j] = new CalendarDay($day++, $inMonth);
            }
        }

        return $arr;
    }


    /**
     * Get the datetime object used for this calendar
     *
     * @return DateTime New datetime object
     */
    public function getDateTime()
    {
        return $this->datetime;
    }


    /**
     * Get the calendar for the next month
     *
     * @return Calendar New calendar object
     */
    public function next()
    {
        if ($this->datetime->getMonth() == 12) {
            $Y = $this->datetime->getYear() + 1;
            $m = '01';
        } else {
            $Y = $this->datetime->getYear();
            $m = $this->datetime->getMonth() + 1;
        }

        return new self(DateTime::createFromFormat('Y-m-d H:i:s', "$Y-$m-01 00:00:00"));
    }


    /**
     * Get the calendar for the previous month
     *
     * @return Calendar New calendar object
     */
    public function previous()
    {
        if ($this->datetime->getMonth() == 1) {
            $Y = $this->datetime->getYear() - 1;
            $m = '12';
        } else {
            $Y = $this->datetime->getYear();
            $m = $this->datetime->getMonth() - 1;
        }

        return new self(DateTime::createFromFormat('Y-m-d H:i:s', "$Y-$m-01 00:00:00"));
    }
}

<?php
/**
 * Class CalendarDay
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

/**
 * Class CalendarDay
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class CalendarDay
{
    /**
     * The day
     * @var int
     */
    public $day;
    /**
     * Whether the day is part of the current month
     * @var bool
     */
    public $inMonth;


    /**
     * Constructor
     *
     * @param int $day The day
     * @param bool $inMonth Whether the day is part of the current month
     */
    public function __construct($day, $inMonth = true)
    {
        $this->day     = $day;
        $this->inMonth = $inMonth;
    }
}

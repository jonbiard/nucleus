<?php
/**
 * Class DateTimeInterval
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

use DateInterval as NativeDateInterval;
use DateTime as NativeDateTime;

/**
 * Class DateTimeInterval
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class DateTimeInterval
{
    /**
     * The days
     * @var int
     */
    protected $days;
    /**
     * The hours
     * @var int
     */
    protected $hours;
    /**
     * The native interval
     * @var NativeDateInterval
     */
    protected $interval;
    /**
     * The minutes
     * @var int
     */
    protected $minutes;
    /**
     * The months
     * @var int
     */
    protected $months;
    /**
     * Whether this instance is negative or not
     * @var bool
     */
    protected $negative;
    /**
     * The seconds
     * @var
     */
    protected $seconds;
    /**
     * The years
     * @var int
     */
    protected $years;


    /**
     * Constructor
     *
     * @param int $seconds The seconds
     * @param int $minutes The minutes
     * @param int $hours The hours
     * @param int $days The days
     * @param int $months The months
     * @param int $years The years
     * @param bool $negative Whether this instance should be negative or not
     */
    public function __construct(
        $seconds,
        $minutes = 0,
        $hours = 0,
        $days = 0,
        $months = 0,
        $years = 0,
        $negative = false
    ) {
        $this->seconds  = $seconds;
        $this->minutes  = $minutes;
        $this->hours    = $hours;
        $this->days     = $days;
        $this->months   = $months;
        $this->years    = $years;
        $this->negative = $negative;

        $this->interval = new NativeDateInterval('P0D');

        $this->interval->s      = $this->seconds;
        $this->interval->i      = $this->minutes;
        $this->interval->h      = $this->hours;
        $this->interval->d      = $this->days;
        $this->interval->m      = $this->months;
        $this->interval->y      = $this->years;
        $this->interval->invert = (int)$this->negative;
        $this->interval->days   = false;
    }


    /**
     * Creates interval using the difference of two datetime objects
     *
     * @param DateTime $datetime1 The datetime object
     * @param DateTime $datetime2 The relative datetime object
     *
     * @return DateTimeInterval New datetime interval object
     */
    public static function createFromDateTime(DateTime $datetime1, DateTime $datetime2)
    {
        return $datetime1->difference($datetime2);
    }


    /**
     * Creates interval from PHP's spec functionality (see PHP's DateInterval::__construct())
     *
     * @param string $spec The spec string
     *
     * @return DateTimeInterval New datetime interval object
     */
    public static function createFromSpec($spec)
    {
        $i = new NativeDateInterval($spec);
        return new self($i->s, $i->i, $i->h, $i->d, $i->m, $i->y, (bool)$i->invert);
    }


    /**
     * Creates interval from PHP's date string functionality (see PHP's DateInterval::createFromDateString())
     *
     * @param string $string The date string
     *
     * @return DateTimeInterval New datetime interval object
     */
    public static function createFromString($string)
    {
        $i = NativeDateInterval::createFromDateString($string);
        return new self($i->s, $i->i, $i->h, $i->d, $i->m, $i->y, (bool)$i->invert);
    }


    /**
     * Creates interval using the difference of two timestamps
     *
     * @param int $timestamp1 The timestamp
     * @param int $timestamp2 The relative timestamp
     *
     * @return DateTimeInterval New datetime interval object
     */
    public static function createFromTimestamp($timestamp1, $timestamp2)
    {
        $datetime1 = new DateTime("@$timestamp1");
        $datetime2 = new DateTime("@$timestamp2");

        return $datetime1->difference($datetime2);
    }


    /**
     * Adds this instance to a native datetime object
     *
     * @param NativeDateTime $datetime The native datetime object
     *
     * @return NativeDateTime New native datetime object
     */
    public function addTo(NativeDateTime $datetime)
    {
        return $datetime->add($this->interval);
    }


    /**
     * Formats this interval
     *
     * @param string $format The format
     *
     * @return string The formatted string
     */
    public function format($format)
    {
        return $this->interval->format($format);
    }


    /**
     * Get days
     *
     * @return int The days
     */
    public function getDays()
    {
        return $this->days;
    }


    /**
     * Get hours
     *
     * @return int The hours
     */
    public function getHours()
    {
        return $this->hours;
    }


    /**
     * Get minutes
     *
     * @return int The minutes
     */
    public function getMinutes()
    {
        return $this->minutes;
    }


    /**
     * Get months
     *
     * @return int The months
     */
    public function getMonths()
    {
        return $this->months;
    }


    /**
     * Get seconds
     *
     * @return mixed The seconds
     */
    public function getSeconds()
    {
        return $this->seconds;
    }


    /**
     * Get years
     *
     * @return int The years
     */
    public function getYears()
    {
        return $this->years;
    }


    /**
     * Check if this instance is negative
     *
     * @return int True if this instance is negative
     */
    public function isNegative()
    {
        return $this->negative;
    }


    /**
     * Set days
     *
     * @param int $days The days
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setDays($days)
    {
        return new self(
            $this->seconds,
            $this->minutes,
            $this->hours,
            $days,
            $this->months,
            $this->years,
            $this->negative
        );
    }


    /**
     * Set hours
     *
     * @param int $hours The hours
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setHours($hours)
    {
        return new self(
            $this->seconds,
            $this->minutes,
            $hours,
            $this->days,
            $this->months,
            $this->years,
            $this->negative
        );
    }


    /**
     * Set minutes
     *
     * @param int $minutes The minutes
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setMinutes($minutes)
    {
        return new self(
            $this->seconds,
            $minutes,
            $this->hours,
            $this->days,
            $this->months,
            $this->years,
            $this->negative
        );
    }


    /**
     * Set months
     *
     * @param int $months The months
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setMonths($months)
    {
        return new self(
            $this->seconds,
            $this->minutes,
            $this->hours,
            $this->days,
            $months,
            $this->years,
            $this->negative
        );
    }


    /**
     * Set negative
     *
     * @param int $negative Set to true for a negative interval
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setNegative($negative)
    {
        return new self(
            $this->seconds,
            $this->minutes,
            $this->hours,
            $this->days,
            $this->months,
            $this->years,
            $negative
        );
    }


    /**
     * Set seconds
     *
     * @param int $seconds The seconds
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setSeconds($seconds)
    {
        return new self(
            $seconds,
            $this->minutes,
            $this->hours,
            $this->days,
            $this->months,
            $this->years,
            $this->negative
        );
    }


    /**
     * Set years
     *
     * @param int $years The years
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function setYears($years)
    {
        return new self(
            $this->seconds,
            $this->minutes,
            $this->hours,
            $this->days,
            $this->months,
            $years,
            $this->negative
        );
    }


    /**
     * Subtracts this instance from a native datetime object
     *
     * @param NativeDateTime $datetime The native datetime object
     *
     * @return NativeDateTime New native datetime object
     */
    public function subtractFrom(NativeDateTime $datetime)
    {
        return $datetime->sub($this->interval);
    }
}

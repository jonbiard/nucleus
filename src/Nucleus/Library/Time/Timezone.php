<?php
/**
 * Class Timezone
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

use DateTimeZone;
use Exception;
use Nucleus\Library\Exception\RuntimeException;

/**
 * Class Timezone
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class Timezone
{
    /**
     * The timezone identifiers list singleton
     * @var array
     */
    private static $timezoneList;
    /**
     * The datetimezone object
     * @var DateTimeZone
     */
    protected $timezone;


    /**
     * Constructor
     *
     * @param string $timezone The timezone identifier
     *
     * @throws RuntimeException When the timezone object cannot be created
     */
    public function __construct($timezone = 'UTC')
    {
        if (is_array($timezone)) {
            $timezone = implode('/', $timezone);
        }

        try {
            $this->timezone = new DateTimeZone($timezone);
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }


    /**
     * Get all timezones available on the system
     *
     * @param bool $asStrings Whether to have each element as a string or array of exploded keys
     *
     * @return array The list of identifiers available on the system
     */
    public static function getAll($asStrings = false)
    {
        if (self::$timezoneList) {
            return self::$timezoneList;
        }

        self::$timezoneList = [];

        $tzs = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        foreach ($tzs as $tz) {
            if ($tz == 'UTC') {
                continue;
            }

            if ($asStrings) {
                self::$timezoneList[] = $tz;
                continue;
            }

            $tz = explode('/', $tz);

            if (isset($tz[2])) {
                self::$timezoneList[$tz[0]][$tz[1]][] = $tz[2];
                continue;
            }

            if (isset($tz[1])) {
                self::$timezoneList[$tz[0]][] = $tz[1];
                continue;
            }

            self::$timezoneList[] = $tz[0];
        }

        return self::$timezoneList;
    }


    /**
     * Magic method (to string)
     *
     * @return string The timezone identifier
     */
    public function __toString()
    {
        return $this->timezone->getName();
    }


    /**
     * Get the abbreviation
     *
     * @return mixed The abbreviation such as EST, EDT, PST, etc...
     */
    public function getAbbreviation()
    {
        $time = time();
        return $this->timezone->getTransitions($time, $time)[0]['abbr'];
    }


    /**
     * Get the comments
     *
     * @return mixed The comments
     */
    public function getComments()
    {
        return $this->timezone->getLocation()['comments'];
    }


    /**
     * Get the country code
     *
     * @return mixed The country code
     */
    public function getCountryCode()
    {
        return $this->timezone->getLocation()['country_code'];
    }


    /**
     * Get the latitude
     *
     * @return mixed The latitude
     */
    public function getLatitude()
    {
        return $this->timezone->getLocation()['latitude'];
    }


    /**
     * Get the longitude
     *
     * @return mixed The longitude
     */
    public function getLongitude()
    {
        return $this->timezone->getLocation()['longitude'];
    }


    /**
     * Get the name
     *
     * @return string The name
     */
    public function getName()
    {
        return $this->timezone->getName();
    }


    /**
     * Get the offset
     *
     * @param null $when The timestamp at which to check the offset
     *
     * @return mixed The offset at the specified timestamp
     */
    public function getOffset($when = null)
    {
        if ($when === null) {
            $when = time();
        }

        return $this->timezone->getTransitions($when, $when)[0]['offset'];
    }


    /**
     * Checks whether the timezone uses daylight savings
     *
     * @return bool True if the timezone uses daylight savings
     */
    public function hasDST()
    {
        $time = time();

        $transitions = $this->timezone->getTransitions($time - 31557600, $time);

        foreach ($transitions as $transition) {
            if ($transition['isdst']) {
                return true;
            }
        }

        return false;
    }
}

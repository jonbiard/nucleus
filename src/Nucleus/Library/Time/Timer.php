<?php
/**
 * Class Timer
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

/**
 * Class Timer
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class Timer
{
    /**
     * The marks in time
     * @var array
     */
    protected $marks;
    /**
     * The float precision
     * @var int
     */
    protected $precision;


    /**
     * Constructor
     *
     * @param int $precision The float precision
     */
    public function __construct($precision = 8)
    {
        $this->marks     = [];
        $this->precision = $precision;
    }


    /**
     * Get time span
     *
     * @param string|int $from The start mark
     * @param string|int $to The end mark
     *
     * @return float The difference between the marks
     */
    public function get($from, $to)
    {
        return round($this->marks[$to] - $this->marks[$from], $this->precision);
    }


    /**
     * Mark a point in time
     *
     * @param string|int $name The name
     */
    public function mark($name)
    {
        $this->marks[$name] = microtime(true);
    }


    /**
     * Reset marks
     */
    public function reset()
    {
        $this->marks = [];
    }
}

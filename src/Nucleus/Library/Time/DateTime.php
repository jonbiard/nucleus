<?php
/**
 * Class DateTime
 *
 * @package Nucleus\Library
 * @subpackage Time
 *
 * @author Martin Biard <info@martinbiard.com>
 * @author Jonathan Biard <info@jonathanbiard.com>
 */
namespace Nucleus\Library\Time;

use DateTime as NativeDateTime;
use DateTimeZone as NativeDateTimeZone;
use Nucleus\Library\Exception\RuntimeException;

/**
 * Class DateTime
 *
 * @package Nucleus\Library
 * @subpackage Time
 */
class DateTime
{
    /**
     * The native datetime object
     * @var NativeDateTime
     */
    protected $datetime;
    /**
     * The timezone object
     * @var Timezone
     */
    protected $timezone;


    /**
     * Constructor
     *
     * @param string $time The time builder string (see PHP's DateTime::__construct())
     * @param Timezone $timezone The timezone object
     */
    public function __construct($time = 'now', Timezone $timezone = null)
    {
        if ($timezone === null) {
            $timezone = new Timezone('UTC');
        }

        $this->timezone = $timezone;
        $this->datetime = new NativeDateTime($time);
        $this->datetime->setTimezone(new NativeDateTimeZone($timezone->getName()));
    }


    /**
     * Create datetime object from format
     *
     * @param string $format The format to use
     * @param string $time The time string to use
     * @param Timezone $timezone The timezone object
     * @param bool $strict Whether to deny dates that are not real dates or not
     *
     * @return DateTime New datetime object
     *
     * @throws RuntimeException When a datetime object cannot be created
     */
    public static function createFromFormat($format, $time, Timezone $timezone = null, $strict = true)
    {
        if (!($datetime = NativeDateTime::createFromFormat($format, $time))) {
            throw new RuntimeException(
                "DateTime error: Cannot create DateTime object from specified format"
            );
        }

        if ($strict) {
            $errors = $datetime->getLastErrors();

            if ($errors['error_count'] || $errors['warning_count']) {
                throw new RuntimeException(
                    "DateTime error: Cannot create DateTime object strictly from specified format"
                );
            }
        }

        $timestamp = $datetime->getTimestamp();

        return new self("@$timestamp", $timezone);
    }


    /**
     * Create date time object from local datetime values
     *
     * @param int $hour The hour
     * @param int $minute The minute
     * @param int $second The second
     * @param int $month The month
     * @param int $day The day
     * @param int $year The year
     * @param Timezone $timezone The timezone object
     *
     * @return DateTime New datetime object
     *
     * @throws RuntimeException When a datetime object cannot be created
     */
    public static function createFromLocalValues(
        $hour,
        $minute,
        $second,
        $month,
        $day,
        $year,
        Timezone $timezone = null
    ) {
        if (!($timestamp = mktime($hour, $minute, $second, $month, $day, $year))) {
            throw new RuntimeException(
                "DateTime error: Cannot create DateTime object from specified local values"
            );
        }

        return new self("@$timestamp", $timezone);
    }


    /**
     * Create date time object from timestamp
     *
     * @param int $timestamp The timestamp
     * @param Timezone $timezone The timezone object
     *
     * @return DateTime New datetime object
     */
    public static function createFromTimestamp($timestamp, Timezone $timezone = null)
    {
        return new self("@$timestamp", $timezone);
    }


    /**
     * Create date time object from UTC datetime values
     *
     * @param int $hour The hour
     * @param int $minute The minute
     * @param int $second The second
     * @param int $month The month
     * @param int $day The day
     * @param int $year The year
     * @param Timezone $timezone The timezone object
     *
     * @return DateTime New datetime object
     *
     * @throws RuntimeException When a datetime object cannot be created
     */
    public static function createFromValues($hour, $minute, $second, $month, $day, $year, Timezone $timezone = null)
    {
        if (!($timestamp = gmmktime($hour, $minute, $second, $month, $day, $year))) {
            throw new RuntimeException(
                "DateTime error: Cannot create DateTime object from specified values"
            );
        }

        return new self("@$timestamp", $timezone);
    }


    /**
     * Magic method (to string)
     *
     * @return string The timestamp as a string
     */
    public function __toString()
    {
        return (string)$this->datetime->getTimestamp();
    }


    /**
     * Adds an interval to the internal datetime
     *
     * @param DateTimeInterval $interval The interval object
     *
     * @return DateTime New datetime object
     */
    public function add(DateTimeInterval $interval)
    {
        $timestamp = $this->datetime->getTimestamp();

        $newDateTime = new NativeDateTime(
            "@$timestamp",
            new NativeDateTimeZone($this->timezone->getName())
        );

        $interval->addTo($newDateTime);

        $timestamp = $newDateTime->getTimestamp();

        return new self("@$timestamp", $this->timezone);
    }


    /**
     * Adds seconds to the internal timestamp
     *
     * @param int $seconds The amount of seconds
     *
     * @return DateTime New datetime object
     */
    public function addSeconds($seconds)
    {
        $timestamp = $this->datetime->getTimestamp() + $seconds;

        return new self("@$timestamp", $this->timezone);
    }


    /**
     * Calculates the difference between this datetime and another
     *
     * @param DateTime $datetime The datetime object to get difference from
     *
     * @return DateTimeInterval New datetime interval object
     */
    public function difference(DateTime $datetime)
    {
        $i = $this->datetime->diff($datetime->datetime);
        return new DateTimeInterval($i->s, $i->i, $i->h, $i->d, $i->m, $i->y, (bool)$i->invert);
    }


    /**
     * Formats a string using this datetime's internal values
     *
     * @param string $format The format
     *
     * @return string The formatted string
     */
    public function format($format)
    {
        return $this->datetime->format($format);
    }


    /**
     * Gets the day of the month (1-31)
     *
     * @return int The day of the month
     */
    public function getDay()
    {
        return (int)$this->format('j');
    }


    /**
     * Gets the day of the year (0-365)
     *
     * @return int The day of the year
     */
    public function getDayOfYear()
    {
        return (int)$this->format('z');
    }


    /**
     * Gets the days in the current month (28-31)
     *
     * @return int The days in the current month
     */
    public function getDaysInMonth()
    {
        return (int)$this->format('t');
    }


    /**
     * Gets the hour (0-23)
     *
     * @return int The hour
     */
    public function getHour()
    {
        return (int)$this->format('G');
    }


    /**
     * Gets the micro second (0-999999)
     *
     * @return int The micro second
     */
    public function getMicroSecond()
    {
        return (int)$this->format('u');
    }


    /**
     * Gets the minute (0-59)
     *
     * @return int The minute
     */
    public function getMinute()
    {
        return (int)$this->format('i');
    }


    /**
     * Gets the month (1-12)
     *
     * @return int The month
     */
    public function getMonth()
    {
        return (int)$this->format('n');
    }


    /**
     * Gets the second (0-59)
     *
     * @return int The second
     */
    public function getSecond()
    {
        return (int)$this->format('s');
    }


    /**
     * Gets the timestamp
     *
     * @return int The timestamp
     */
    public function getTimestamp()
    {
        return $this->datetime->getTimestamp();
    }


    /**
     * Gets the timezone object
     *
     * @return Timezone The timezone object associated with this instance
     */
    public function getTimezone()
    {
        return $this->timezone;
    }


    /**
     * Gets the weekday (0-6, sunday to saturday)
     *
     * @return int The weekday
     */
    public function getWeekday()
    {
        return (int)$this->format('w');
    }


    /**
     * Gets the full year (YYYY)
     *
     * @return int The full year
     */
    public function getYear()
    {
        return (int)$this->format('Y');
    }


    /**
     * Checks if this instance represents an AM time
     *
     * @return bool True if the time is AM
     */
    public function isAM()
    {
        return $this->format('a') === 'am';
    }


    /**
     * Checks if this instance is currently in daylight savings time
     *
     * @return bool True if the time is currently DST time
     */
    public function isDST()
    {
        return (bool)$this->format('I');
    }


    /**
     * Checks if this instance represents a leap year
     *
     * @return bool True if the current year is a leap year
     */
    public function isLeapYear()
    {
        return (bool)$this->format('L');
    }


    /**
     * Checks if this instance represents a PM time
     *
     * @return bool True if the time is PM
     */
    public function isPM()
    {
        return $this->format('a') === 'pm';
    }


    /**
     * Sets a new timezone
     *
     * @param Timezone $timezone The timezone object
     *
     * @return DateTime New datetime object
     */
    public function setTimezone(Timezone $timezone)
    {
        $timestamp = $this->datetime->getTimestamp();
        return new self("@$timestamp", $timezone);
    }


    /**
     * Subtracts an interval from the internal datetime
     *
     * @param DateTimeInterval $interval The interval object
     *
     * @return DateTime New datetime object
     */
    public function subtract(DateTimeInterval $interval)
    {
        $timestamp = $this->datetime->getTimestamp();

        $newDateTime = new NativeDateTime(
            "@$timestamp",
            new NativeDateTimeZone($this->timezone->getName())
        );

        $interval->subtractFrom($newDateTime);

        $timestamp = $newDateTime->getTimestamp();

        return new self("@$timestamp", $this->timezone);
    }


    /**
     * Subtracts seconds from the internal timestamp
     *
     * @param int $seconds The amount of seconds
     *
     * @return DateTime New datetime object
     */
    public function subtractSeconds($seconds)
    {
        $timestamp = $this->datetime->getTimestamp() - $seconds;

        return new self("@$timestamp", $this->timezone);
    }
}

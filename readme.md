# Welcome to the Nucleus Project!

#### Browse the [API Index](https://bitbucket.org/jonbiard/nucleus/wiki/ApiIndex)

------------

### Features and Description

Nucleus is an object-oriented library of utilities that attempts to wrap many of PHP's native functions into objects.
Our aim is to make API's that are re-usable, expandable, robust, versatile, and more consistent with its naming and parameter orders.

$(document).ready(function ()
{
    if (window.top != window.self) {
        return;
    }

    var $window = $(window),
        $contents = $('#contents'),
        $sidebar = $('#sidebar'),
        $resizable = $('#resizable'),
        $dbHeader = $('#db-header'),
        $dbFooter = $('#db-footer');

    // to make the page work without JS we need to set a margin-left;
    // this distorts the splitter plugin and thus we set margin
    // to 0 when JS is enabled
    $contents.css('margin', '0');

    $window
        .on('resize', function(){
            $contents.add($sidebar).height($window.height() - 66);

            var heightForContent = $window.height() - $dbHeader.outerHeight(true) - $dbFooter.outerHeight(true);

            $contents.add($sidebar).height(heightForContent);
        })
        .trigger('resize');

    $resizable
        .splitter({
            sizeLeft: 300
        });
});
<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\RectangularPrism;
use PHPUnit_Framework_TestCase;

class RectangularPrismTest extends PHPUnit_Framework_TestCase
{
    private $depth = 5;
    private $diagonal = 7.0710678118655;
    private $floatMaxDiff = 0.000001;
    private $height = 4;
    private $length = 3;
    /**
     * For auto-complete
     * @var RectangularPrism
     */
    private $rectangularPrism;
    private $surfaceArea = 94;
    private $volume = 60;


    public function assertPropertiesSuccess(RectangularPrism $rectangularPrism)
    {
        $actualLengthValue      = $rectangularPrism->getLength();
        $actualHeightValue      = $rectangularPrism->getHeight();
        $actualDepthValue       = $rectangularPrism->getDepth();
        $actualDiagonalValue    = $rectangularPrism->getDiagonal();
        $actualSurfaceAreaValue = $rectangularPrism->getSurfaceArea();
        $actualVolumeValue      = $rectangularPrism->getVolume();

        $this->assertTrue(abs($this->length - $actualLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->height - $actualHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->depth - $actualDepthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->diagonal - $actualDiagonalValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->rectangularPrism = new RectangularPrism($this->length, $this->height, $this->depth);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism);
    }


    public function testExceptionForDepthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new RectangularPrism($this->length, $this->height, ['NonScalar']);
    }


    public function testExceptionForDepthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new RectangularPrism($this->length, $this->height, 0);
    }


    public function testExceptionForHeightWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new RectangularPrism($this->length, ['NonScalar']);
    }


    public function testExceptionForHeightWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new RectangularPrism($this->length, 0);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new RectangularPrism(['NonScalar']);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new RectangularPrism(0);
    }


    public function testSetDepthSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setDepth($this->depth));
    }


    public function testSetDiagonalSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setDiagonal($this->diagonal));
    }


    public function testSetHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setHeight($this->height));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setLength($this->length));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangularPrism->setVolume($this->volume));
    }
}

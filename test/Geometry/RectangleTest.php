<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Rectangle;
use PHPUnit_Framework_TestCase;

class RectangleTest extends PHPUnit_Framework_TestCase
{
    private $area = 8;
    private $floatMaxDiff = 0.000001;
    private $height = 4;
    private $length = 2;
    private $perimeter = 12;
    /**
     * For auto-complete
     * @var Rectangle
     */
    private $rectangle;


    public function assertPropertiesSuccess(Rectangle $rectangle)
    {
        $actualLengthValue    = $rectangle->getLength();
        $actualHeightValue    = $rectangle->getHeight();
        $actualAreaValue      = $rectangle->getArea();
        $actualPerimeterValue = $rectangle->getPerimeter();

        $this->assertTrue(abs($this->length - $actualLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->height - $actualHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->perimeter - $actualPerimeterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->rectangle = new Rectangle($this->length, $this->height);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangle);
    }


    public function testExceptionForHeightWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Rectangle($this->height, ['NonScalar']);
    }


    public function testExceptionForHeightWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Rectangle($this->height, 0);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Rectangle(['NonScalar'], $this->length);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Rectangle(0, $this->length);
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangle->setArea($this->area));
    }


    public function testSetHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangle->setHeight($this->height));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangle->setLength($this->length));
    }


    public function testSetPerimeterSuccess()
    {
        $this->assertPropertiesSuccess($this->rectangle->setPerimeter($this->perimeter));
    }
}

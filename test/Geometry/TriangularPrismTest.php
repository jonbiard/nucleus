<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\TriangularPrism;
use PHPUnit_Framework_TestCase;

class TriangularPrismTest extends PHPUnit_Framework_TestCase
{
    private $angleA = 41.726765054188;
    private $angleB = 93.273234945812;
    private $angleC = 45;
    private $depth = 10;
    private $floatMaxDiff = 0.000001;
    private $sideA = 4;
    private $sideB = 6;
    private $sideC = 4.2495734495412;
    private $surfaceArea = 159.46629724389;
    /**
     * For auto-complete
     * @var TriangularPrism
     */
    private $triangularPrism;
    private $volume = 84.852813742386;


    public function assertPropertiesSuccess(TriangularPrism $triangularPrism)
    {
        $actualSideALengthValue = $triangularPrism->getSideALength();
        $actualSideBLengthValue = $triangularPrism->getSideBLength();
        $actualSideCLengthValue = $triangularPrism->getSideCLength();
        $actualAngleAValue      = $triangularPrism->getAngleA();
        $actualAngleBValue      = $triangularPrism->getAngleB();
        $actualAngleCValue      = $triangularPrism->getAngleC();
        $actualDepthValue       = $triangularPrism->getDepth();
        $actualSurfaceAreaValue = $triangularPrism->getSurfaceArea();
        $actualVolumeValue      = $triangularPrism->getVolume();

        $this->assertTrue(abs($this->sideA - $actualSideALengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideB - $actualSideBLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideC - $actualSideCLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleA - $actualAngleAValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleB - $actualAngleBValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleC - $actualAngleCValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->depth - $actualDepthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function assertRotatedClockwiseSuccess(TriangularPrism $triangularPrism)
    {
        $actualSideALengthValue = $triangularPrism->getSideBLength();
        $actualSideBLengthValue = $triangularPrism->getSideCLength();
        $actualSideCLengthValue = $triangularPrism->getSideALength();
        $actualAngleAValue      = $triangularPrism->getAngleB();
        $actualAngleBValue      = $triangularPrism->getAngleC();
        $actualAngleCValue      = $triangularPrism->getAngleA();
        $actualDepthValue       = $triangularPrism->getDepth();
        $actualSurfaceAreaValue = $triangularPrism->getSurfaceArea();
        $actualVolumeValue      = $triangularPrism->getVolume();

        $this->assertTrue(abs($this->sideA - $actualSideALengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideB - $actualSideBLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideC - $actualSideCLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleA - $actualAngleAValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleB - $actualAngleBValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleC - $actualAngleCValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->depth - $actualDepthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->triangularPrism = new TriangularPrism($this->sideA, $this->angleC, null, $this->angleB, $this->depth);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism);
    }


    public function testExceptionForAngleBWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new TriangularPrism($this->sideA, $this->angleC, null, ['NonScalar']);
    }


    public function testExceptionForAngleBWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new TriangularPrism($this->sideA, $this->angleC, null, 0);
    }


    public function testExceptionForAngleCWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new TriangularPrism($this->sideA, ['NonScalar']);
    }


    public function testExceptionForAngleCWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new TriangularPrism($this->sideA, 0);
    }


    public function testExceptionForDepthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new TriangularPrism($this->sideA, $this->angleC, null, $this->angleB, ['NonScalar']);
    }


    public function testExceptionForDepthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new TriangularPrism($this->sideA, $this->angleC, null, $this->angleB, 0);
    }


    public function testExceptionForSideAWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new TriangularPrism(['NonScalar']);
    }


    public function testExceptionForSideAWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new TriangularPrism(0);
    }


    public function testExceptionForSideBWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new TriangularPrism($this->sideA, $this->angleC, ['NonScalar']);
    }


    public function testExceptionForSideBWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new TriangularPrism($this->sideA, $this->angleC, 0);
    }


    public function testRotationSuccess()
    {
        $this->triangularPrism->rotateClockwise();
        $this->assertRotatedClockwiseSuccess($this->triangularPrism);
        $this->triangularPrism->rotateCounterClockwise();
        $this->assertPropertiesSuccess($this->triangularPrism);
    }


    public function testSetAngleASuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setAngleA($this->angleA));
    }


    public function testSetAngleBSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setAngleB($this->angleB));
    }


    public function testSetAngleCSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setAngleC($this->angleC));
    }


    public function testSetDepthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setDepth($this->depth));
    }


    public function testSetSideALengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setALength($this->sideA));
    }


    public function testSetSideBLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setBLength($this->sideB));
    }


    public function testSetSideCLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setCLength($this->sideC));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->triangularPrism->setVolume($this->volume));
    }
}

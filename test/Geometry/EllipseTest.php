<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Ellipse;
use PHPUnit_Framework_TestCase;

class EllipseTest extends PHPUnit_Framework_TestCase
{
    private $area = 37.699111843078;
    private $circumference = 22.103492160709;
    private $eccentricity = 0.66143782776615;
    /**
     * For auto-complete
     * @var Ellipse
     */
    private $ellipse;
    private $floatMaxDiff = 0.000001;
    private $fociDistance = 5.2915026221292;
    private $semiAxisA = 3;
    private $semiAxisB = 4;


    public function assertPropertiesSuccess(Ellipse $ellipse)
    {
        $actualEccentricityValue    = $ellipse->getEccentricity();
        $actualFociDistanceValue    = $ellipse->getFociDistance();
        $actualSemiAxisALengthValue = $ellipse->getSemiAxisALength();
        $actualSemiAxisBLengthValue = $ellipse->getSemiAxisBLength();
        $actualCircumferenceValue   = $ellipse->getCircumference();
        $actualPerimeterValue       = $ellipse->getPerimeter();
        $actualAreaValue            = $ellipse->getArea();

        $this->assertTrue(abs($this->eccentricity - $actualEccentricityValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->fociDistance - $actualFociDistanceValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->semiAxisA - $actualSemiAxisALengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->semiAxisB - $actualSemiAxisBLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->circumference - $actualCircumferenceValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->circumference - $actualPerimeterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->ellipse = new Ellipse($this->semiAxisA, $this->semiAxisB);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse);
    }


    public function testExceptionForSemiAxisAWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Ellipse(['NonScalar']);
    }


    public function testExceptionForSemiAxisAWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Ellipse(0);
    }


    public function testExceptionForSemiAxisBWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Ellipse($this->semiAxisA, ['NonScalar']);
    }


    public function testExceptionForSemiAxisBWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Ellipse($this->semiAxisA, 0);
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setArea($this->area));
    }


    public function testSetAxisALengthSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setAxisALength($this->semiAxisA * 2));
    }


    public function testSetAxisBLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setAxisBLength($this->semiAxisB * 2));
    }


    public function testSetCircumferenceSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setCircumference($this->circumference));
    }


    public function testSetFociDistanceSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setFociDistance($this->fociDistance));
    }


    public function testSetPerimeterSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setPerimeter($this->circumference));
    }


    public function testSetSemiAxisALengthSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setSemiAxisALength($this->semiAxisA));
    }


    public function testSetSemiAxisBLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->ellipse->setSemiAxisBLength($this->semiAxisB));
    }
}

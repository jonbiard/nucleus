<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Cylinder;
use PHPUnit_Framework_TestCase;

class CylinderTest extends PHPUnit_Framework_TestCase
{
    /**
     * For auto-complete
     * @var Cylinder
     */
    private $cylinder;
    private $floatMaxDiff = 0.000001;
    private $height = 6;
    private $lateralArea = 113.09733552923;
    private $radius = 3;
    private $surfaceArea = 169.64600329385;
    private $volume = 169.64600329385;


    public function assertPropertiesSuccess(Cylinder $cylinder)
    {
        $actualRadiusValue      = $cylinder->getRadius();
        $actualHeightValue      = $cylinder->getHeight();
        $actualLateralAreaValue = $cylinder->getLateralArea();
        $actualSurfaceAreaValue = $cylinder->getSurfaceArea();
        $actualVolumeValue      = $cylinder->getVolume();

        $this->assertTrue(abs($this->radius - $actualRadiusValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->height - $actualHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->lateralArea - $actualLateralAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->cylinder = new Cylinder($this->radius, $this->height);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder);
    }


    public function testExceptionForHeightWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Cylinder($this->radius, ['NonScalar']);
    }


    public function testExceptionForHeightWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Cylinder($this->radius, 0);
    }


    public function testExceptionForRadiusWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Cylinder(['NonScalar']);
    }


    public function testExceptionForRadiusWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Cylinder(0);
    }


    public function testSetHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder->setHeight($this->height));
    }


    public function testSetLateralAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder->setLateralArea($this->lateralArea));
    }


    public function testSetRadiusSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder->setRadius($this->radius));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->cylinder->setVolume($this->volume));
    }
}

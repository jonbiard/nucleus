<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Circle;
use PHPUnit_Framework_TestCase;

class CircleTest extends PHPUnit_Framework_TestCase
{
    private $area = 3.1415926535898;
    /**
     * For auto-complete
     * @var Circle
     */
    private $circle;
    private $circumference = 6.2831853071796;
    private $diameter = 2;
    private $floatMaxDiff = 0.000001;
    private $radius = 1;


    public function assertPropertiesSuccess(Circle $circle)
    {
        $actualRadiusValue        = $circle->getRadius();
        $actualDiameterValue      = $circle->getDiameter();
        $actualAreaValue          = $circle->getArea();
        $actualCircumferenceValue = $circle->getCircumference();
        $actualPerimeterValue     = $circle->getPerimeter();

        $this->assertTrue(abs($this->radius - $actualRadiusValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->diameter - $actualDiameterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->circumference - $actualCircumferenceValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->circumference - $actualPerimeterValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->circle = new Circle($this->radius);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->circle);
    }


    public function testExceptionForRadiusWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Circle(['NonScalar']);
    }


    public function testExceptionForRadiusWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Circle(0);
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->circle->setArea($this->area));
    }


    public function testSetCircumferenceSuccess()
    {
        $this->assertPropertiesSuccess($this->circle->setCircumference($this->circumference));
    }


    public function testSetDiameterSuccess()
    {
        $this->assertPropertiesSuccess($this->circle->setDiameter($this->diameter));
    }


    public function testSetRadiusSuccess()
    {
        $this->assertPropertiesSuccess($this->circle->setRadius($this->radius));
    }
}

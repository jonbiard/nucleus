<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Spheroid;
use PHPUnit_Framework_TestCase;

class SpheroidTest extends PHPUnit_Framework_TestCase
{
    private $eccentricity = 53.130102354156;
    private $floatMaxDiff = 0.000001;
    private $semiAxisA = 3;
    private $semiAxisC = 5;
    /**
     * For auto-complete
     * @var Spheroid
     */
    private $spheroid;
    private $surfaceArea = 165.79306193647;
    private $volume = 188.49555921539;


    public function assertPropertiesSuccess(Spheroid $spheroid)
    {
        $actualSemiAxisAValue    = $spheroid->getSemiAxisALength();
        $actualSemiAxisCValue    = $spheroid->getSemiAxisCLength();
        $actualEccentricityValue = $spheroid->getEccentricity();
        $actualSurfaceAreaValue  = $spheroid->getSurfaceArea();
        $actualVolumeValue       = $spheroid->getVolume();

        $this->assertTrue(abs($this->semiAxisA - $actualSemiAxisAValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->semiAxisC - $actualSemiAxisCValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->eccentricity - $actualEccentricityValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->spheroid = new Spheroid($this->semiAxisA, $this->semiAxisC);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid);
    }


    public function testExceptionForSemiAxisAWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Spheroid(['NonScalar']);
    }


    public function testExceptionForSemiAxisAWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Spheroid(0);
    }


    public function testExceptionForSemiAxisCWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Spheroid($this->semiAxisA, ['NonScalar']);
    }


    public function testExceptionForSemiAxisCWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Spheroid($this->semiAxisA, 0);
    }


    public function testSetAxisASuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setAxisALength($this->semiAxisA * 2));
    }


    public function testSetAxisCSuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setAxisCLength($this->semiAxisC * 2));
    }


    public function testSetSemiAxisASuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setSemiAxisALength($this->semiAxisA));
    }


    public function testSetSemiAxisCSuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setSemiAxisCLength($this->semiAxisC));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->spheroid->setVolume($this->volume));
    }
}

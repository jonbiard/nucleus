<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Cube;
use PHPUnit_Framework_TestCase;

class CubeTest extends PHPUnit_Framework_TestCase
{
    /**
     * For auto-complete
     * @var Cube
     */
    private $cube;
    private $faceDiagonal = 4.2426406871193;
    private $floatMaxDiff = 0.000001;
    private $length = 3;
    private $solidDiagonal = 5.1961524227066;
    private $surfaceArea = 54;
    private $volume = 27;


    public function assertPropertiesSuccess(Cube $cube)
    {
        $actualLengthValue        = $cube->getLength();
        $actualFaceDiagonalValue  = $cube->getFaceDiagonal();
        $actualSolidDiagonalValue = $cube->getSolidDiagonal();
        $actualSurfaceAreaValue   = $cube->getSurfaceArea();
        $actualVolumeValue        = $cube->getVolume();

        $this->assertTrue(abs($this->length - $actualLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->faceDiagonal - $actualFaceDiagonalValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->solidDiagonal - $actualSolidDiagonalValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->cube = new Cube($this->length);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->cube);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Cube(['NonScalar']);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Cube(0);
    }


    public function testSetFaceDiagonalSuccess()
    {
        $this->assertPropertiesSuccess($this->cube->setFaceDiagonal($this->faceDiagonal));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->cube->setLength($this->length));
    }


    public function testSetSolidDiagonalSuccess()
    {
        $this->assertPropertiesSuccess($this->cube->setSolidDiagonal($this->solidDiagonal));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->cube->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->cube->setVolume($this->volume));
    }
}

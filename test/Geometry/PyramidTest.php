<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Pyramid;
use PHPUnit_Framework_TestCase;

class PyramidTest extends PHPUnit_Framework_TestCase
{
    private $edgeHeight = 5.3602286152336;
    private $floatMaxDiff = 0.000001;
    private $height = 5;
    private $lateralArea = 32.021146592096;
    private $numSides = 12;
    /**
     * For auto-complete
     * @var Pyramid
     */
    private $pyramid;
    private $sideLength = 1;
    private $slantHeight = 5.3368577653493;
    private $surfaceArea = 43.217299014802;
    private $volume = 18.660254037844;


    public function assertPropertiesSuccess(Pyramid $pyramid)
    {
        $actualSideLengthValue  = $pyramid->getSideLength();
        $actualHeightValue      = $pyramid->getHeight();
        $actualNumSidesValue    = $pyramid->getNumberOfSides();
        $actualEdgeHeightValue  = $pyramid->getEdgeHeight();
        $actualSlantHeightValue = $pyramid->getSlantHeight();
        $actualLateralAreaValue = $pyramid->getLateralArea();
        $actualSurfaceAreaValue = $pyramid->getSurfaceArea();
        $actualVolumeValue      = $pyramid->getVolume();

        $this->assertTrue(abs($this->sideLength - $actualSideLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->height - $actualHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->numSides - $actualNumSidesValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->edgeHeight - $actualEdgeHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->slantHeight - $actualSlantHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->lateralArea - $actualLateralAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->pyramid = new Pyramid($this->sideLength, $this->height, $this->numSides);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid);
    }


    public function testExceptionForHeightWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Pyramid($this->sideLength, ['NonScalar'], $this->numSides);
    }


    public function testExceptionForHeightWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Pyramid($this->sideLength, 0, $this->numSides);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Pyramid(['NonScalar'], $this->height, $this->numSides);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Pyramid(0, $this->height, $this->numSides);
    }


    public function testExceptionForNumberOfSidesWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Pyramid($this->sideLength, $this->height, ['NonScalar']);
    }


    public function testExceptionForNumberOfSidesWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Pyramid($this->sideLength, $this->height, 2);
    }


    public function testSetEdgeHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setEdgeHeight($this->edgeHeight));
    }


    public function testSetHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setHeight($this->height));
    }


    public function testSetLateralAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setLateralArea($this->lateralArea));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setLength($this->sideLength));
    }


    public function testSetNumberOfSidesSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setNumberOfSides($this->numSides));
    }


    public function testSetSlantHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setSlantHeight($this->slantHeight));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->pyramid->setVolume($this->volume));
    }
}

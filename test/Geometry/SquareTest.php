<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Square;
use PHPUnit_Framework_TestCase;

class SquareTest extends PHPUnit_Framework_TestCase
{
    private $area = 9;
    private $floatMaxDiff = 0.000001;
    private $length = 3;
    private $perimeter = 12;
    /**
     * For auto-complete
     * @var Square
     */
    private $square;


    public function assertPropertiesSuccess(Square $square)
    {
        $actualLengthValue    = $square->getLength();
        $actualAreaValue      = $square->getArea();
        $actualPerimeterValue = $square->getPerimeter();

        $this->assertTrue(abs($this->length - $actualLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->perimeter - $actualPerimeterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->square = new Square($this->length);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->square);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Square(['NonScalar']);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Square(0);
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->square->setArea($this->area));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->square->setLength($this->length));
    }


    public function testSetPerimeterSuccess()
    {
        $this->assertPropertiesSuccess($this->square->setPerimeter($this->perimeter));
    }
}

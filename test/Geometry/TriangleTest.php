<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Triangle;
use PHPUnit_Framework_TestCase;

class TriangleTest extends PHPUnit_Framework_TestCase
{
    private $angleA = 41.726765054188;
    private $angleB = 93.273234945812;
    private $angleC = 45;
    private $area = 8.4852813742386;
    private $floatMaxDiff = 0.000001;
    private $perimeter = 14.249573449541;
    private $sideA = 4;
    private $sideB = 6;
    private $sideC = 4.2495734495412;
    /**
     * For auto-complete
     * @var Triangle
     */
    private $triangle;


    public function assertPropertiesSuccess(Triangle $triangle)
    {
        $actualSideALengthValue = $triangle->getSideALength();
        $actualSideBLengthValue = $triangle->getSideBLength();
        $actualSideCLengthValue = $triangle->getSideCLength();
        $actualAngleAValue      = $triangle->getAngleA();
        $actualAngleBValue      = $triangle->getAngleB();
        $actualAngleCValue      = $triangle->getAngleC();
        $actualPerimeterValue   = $triangle->getPerimeter();
        $actualAreaValue        = $triangle->getArea();

        $this->assertTrue(abs($this->sideA - $actualSideALengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideB - $actualSideBLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideC - $actualSideCLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleA - $actualAngleAValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleB - $actualAngleBValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleC - $actualAngleCValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->perimeter - $actualPerimeterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
    }


    public function assertRotatedClockwiseSuccess(Triangle $triangle)
    {
        $actualSideALengthValue = $triangle->getSideBLength();
        $actualSideBLengthValue = $triangle->getSideCLength();
        $actualSideCLengthValue = $triangle->getSideALength();
        $actualAngleAValue      = $triangle->getAngleB();
        $actualAngleBValue      = $triangle->getAngleC();
        $actualAngleCValue      = $triangle->getAngleA();
        $actualPerimeterValue   = $triangle->getPerimeter();
        $actualAreaValue        = $triangle->getArea();

        $this->assertTrue(abs($this->sideA - $actualSideALengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideB - $actualSideBLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->sideC - $actualSideCLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleA - $actualAngleAValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleB - $actualAngleBValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->angleC - $actualAngleCValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->perimeter - $actualPerimeterValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->triangle = new Triangle($this->sideA, $this->angleC, $this->sideB);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle);
    }


    public function testExceptionForAngleBWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Triangle($this->sideA, $this->angleC, null, ['NonScalar']);
    }


    public function testExceptionForAngleBWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Triangle($this->sideA, $this->angleC, null, 0);
    }


    public function testExceptionForAngleCWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Triangle($this->sideA, ['NonScalar']);
    }


    public function testExceptionForAngleCWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Triangle($this->sideA, 0);
    }


    public function testExceptionForSideAWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Triangle(['NonScalar']);
    }


    public function testExceptionForSideAWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Triangle(0);
    }


    public function testExceptionForSideBWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Triangle($this->sideA, $this->angleC, ['NonScalar']);
    }


    public function testExceptionForSideBWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Triangle($this->sideA, $this->angleC, 0);
    }


    public function testRotationSuccess()
    {
        $this->triangle->rotateClockwise();
        $this->assertRotatedClockwiseSuccess($this->triangle);
        $this->triangle->rotateCounterClockwise();
        $this->assertPropertiesSuccess($this->triangle);
    }


    public function testSetAngleASuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setAngleA($this->angleA));
    }


    public function testSetAngleBSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setAngleB($this->angleB));
    }


    public function testSetAngleCSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setAngleC($this->angleC));
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setArea($this->area));
    }


    public function testSetPerimeterSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setPerimeter($this->perimeter));
    }


    public function testSetSideALengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setALength($this->sideA));
    }


    public function testSetSideBLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setBLength($this->sideB));
    }


    public function testSetSideCLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->triangle->setCLength($this->sideC));
    }
}

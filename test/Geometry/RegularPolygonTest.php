<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\RegularPolygon;
use PHPUnit_Framework_TestCase;

class RegularPolygonTest extends PHPUnit_Framework_TestCase
{
    private $apothem = 2.0645728807068;
    private $area = 15.484296605301;
    private $circumradius = 2.5519524250561;
    private $exteriorAngle = 108;
    private $floatMaxDiff = 0.000001;
    private $interiorAngle = 72;
    private $numSides = 5;
    private $perimeter = 15;
    /**
     * For auto-complete
     * @var RegularPolygon
     */
    private $regularPolygon;
    private $sideLength = 3;


    public function assertPropertiesSuccess(RegularPolygon $regularPolygon)
    {
        $actualSideLengthValue    = $regularPolygon->getLength();
        $actualNumSidesValue      = $regularPolygon->getNumberOfSides();
        $actualApothemValue       = $regularPolygon->getApothem();
        $actualCircumradiusValue  = $regularPolygon->getCircumradius();
        $actualExteriorAngleValue = $regularPolygon->getExteriorAngle();
        $actualInteriorAngleValue = $regularPolygon->getInteriorAngle();
        $actualAreaValue          = $regularPolygon->getArea();
        $actualPerimeterValue     = $regularPolygon->getPerimeter();

        $this->assertTrue(abs($this->sideLength - $actualSideLengthValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->numSides - $actualNumSidesValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->apothem - $actualApothemValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->circumradius - $actualCircumradiusValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->exteriorAngle - $actualExteriorAngleValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->interiorAngle - $actualInteriorAngleValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->area - $actualAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->perimeter - $actualPerimeterValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->regularPolygon = new RegularPolygon($this->sideLength, $this->numSides);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon);
    }


    public function testExceptionForLengthWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new RegularPolygon(['NonScalar'], $this->numSides);
    }


    public function testExceptionForLengthWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new RegularPolygon(0, $this->numSides);
    }


    public function testExceptionForNumberOfSidesWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new RegularPolygon($this->sideLength, ['NonScalar']);
    }


    public function testExceptionForNumberOfSidesWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new RegularPolygon($this->sideLength, 2);
    }


    public function testSetApothemSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setApothem($this->apothem));
    }


    public function testSetAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setArea($this->area));
    }


    public function testSetCircumradiusSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setCircumradius($this->circumradius));
    }


    public function testSetLengthSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setLength($this->sideLength));
    }


    public function testSetNumberOfSidesSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setNumberOfSides($this->numSides));
    }


    public function testSetPerimeterSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setPerimeter($this->perimeter));
    }


    public function testSetRadiusSuccess()
    {
        $this->assertPropertiesSuccess($this->regularPolygon->setRadius($this->circumradius));
    }
}

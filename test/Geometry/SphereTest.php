<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Sphere;
use PHPUnit_Framework_TestCase;

class SphereTest extends PHPUnit_Framework_TestCase
{
    private $floatMaxDiff = 0.000001;
    private $radius = 5;
    /**
     * For auto-complete
     * @var Sphere
     */
    private $sphere;
    private $surfaceArea = 314.15926535898;
    private $volume = 523.5987755983;


    public function assertPropertiesSuccess(Sphere $sphere)
    {
        $actualRadiusValue      = $sphere->getRadius();
        $actualSurfaceAreaValue = $sphere->getSurfaceArea();
        $actualVolumeValue      = $sphere->getVolume();

        $this->assertTrue(abs($this->radius - $actualRadiusValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->sphere = new Sphere($this->radius);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->sphere);
    }


    public function testExceptionForRadiusWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Sphere(['NonScalar']);
    }


    public function testExceptionForRadiusWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Sphere(0);
    }


    public function testSetRadiusSuccess()
    {
        $this->assertPropertiesSuccess($this->sphere->setRadius($this->radius));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->sphere->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->sphere->setVolume($this->volume));
    }
}

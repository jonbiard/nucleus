<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Geometry\Cone;
use PHPUnit_Framework_TestCase;

class ConeTest extends PHPUnit_Framework_TestCase
{
    /**
     * For auto-complete
     * @var Cone
     */
    private $cone;
    private $floatMaxDiff = 0.000001;
    private $height = 6;
    private $lateralArea = 39.738353063184;
    private $radius = 2;
    private $slantHeight = 6.3245553203368;
    private $surfaceArea = 52.304723677544;
    private $volume = 25.132741228718;


    public function assertPropertiesSuccess(Cone $cone)
    {
        $actualRadiusValue      = $cone->getRadius();
        $actualHeightValue      = $cone->getHeight();
        $actualSlantHeightValue = $cone->getSlantHeight();
        $actualLateralAreaValue = $cone->getLateralArea();
        $actualSurfaceAreaValue = $cone->getSurfaceArea();
        $actualVolumeValue      = $cone->getVolume();

        $this->assertTrue(abs($this->radius - $actualRadiusValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->height - $actualHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->slantHeight - $actualSlantHeightValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->lateralArea - $actualLateralAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->surfaceArea - $actualSurfaceAreaValue) <= $this->floatMaxDiff);
        $this->assertTrue(abs($this->volume - $actualVolumeValue) <= $this->floatMaxDiff);
    }


    public function setUp()
    {
        $this->cone = new Cone($this->radius, $this->height);
    }


    public function testConstructSuccess()
    {
        $this->assertPropertiesSuccess($this->cone);
    }


    public function testExceptionForHeightWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Cone($this->radius, ['NonScalar']);
    }


    public function testExceptionForHeightWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Cone($this->radius, 0);
    }


    public function testExceptionForRadiusWhenValueIsNonScalar()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        /** @noinspection PhpParamsInspection */
        new Cone(['NonScalar']);
    }


    public function testExceptionForRadiusWhenValueIsTooLow()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\LogicException');
        new Cone(0);
    }


    public function testSetHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setHeight($this->height));
    }


    public function testSetLateralAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setLateralArea($this->lateralArea));
    }


    public function testSetRadiusSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setRadius($this->radius));
    }


    public function testSetSlantHeightSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setSlantHeight($this->slantHeight));
    }


    public function testSetSurfaceAreaSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setSurfaceArea($this->surfaceArea));
    }


    public function testSetVolumeSuccess()
    {
        $this->assertPropertiesSuccess($this->cone->setVolume($this->volume));
    }
}

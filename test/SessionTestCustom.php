<?php
/**
 * Testing area
 *
 * @package FitnessDistrict
 * @author Jonathan Biard <info@jonathanbiard.com>
 */

use Nucleus\Library\Env\Client;
use Nucleus\Library\HTTP\Cookie;
use Nucleus\Library\HTTP\Response;
use Nucleus\Library\Session\Session;
use Nucleus\Library\Session\SessionStorage\NativeSessionStorage;

require '../vendor/autoload.php';

// Set some required variables
$cookie   = new Cookie();
$client   = new Client($_SERVER, $_COOKIE);
$storage  = new NativeSessionStorage();
$response = new Response();

$cookie->setName('info');
$cookie->setDomain('.nucleus');
$cookie->setExpire(time() + 60 * 60 * 24 * 30);
$cookie->setPath('/');

$storage->setSavePath(__DIR__);

// Test stuff
$session = new Session($cookie, $client, $response, $storage);

$segment = $session->getSegment('Nucleus\Library\Test');

// Set and get examples on the first segment
$segment->userId = 1000000; // Lazy loads session as the first segment gets accessed
$segment->count += 1;

$s = '<pre>';

$s .= "\n<b>=== Segment ===</b>\n\n\n";

$s .= "Segment Count:   {$segment->count}\n";
$s .= "                 <span style=\"color: #00a\">+1 every request</span>\n\n";
$s .= "Segment User ID: {$segment->userId}\n";
$s .= "                 <span style=\"color: #00a\">== across requests</span>\n\n";

$s .= "\n<b>=== Session ===</b>\n\n\n";

$s .= "Name:            {$session->getName()}\n";
$s .= "                 <span style=\"color: #00a\">== 'info'</span>\n\n";
$s .= "ID:              {$session->getId()}\n";
$s .= "                 <span style=\"color: #00a\">== previous request's RegenSession->ID</span>\n\n";
$s .= "CSRFToken:       {$session->CSRFToken->get()}\n";
$s .= "                 <span style=\"color: #00a\">== previous request's RegenSession->NewCSRFToken</span>\n\n";
$session->CSRFToken->regenerate();
$s .= "New CSRF Token:  {$session->CSRFToken->get()}\n";
$s .= "                 <span style=\"color: #00a\">!= Session->CSRFToken</span>\n\n";

$s .= "\n<b>=== RegenSession ===</b>\n\n\n";

$session->regenerateId();

$s .= "Name:            {$session->getName()}\n";
$s .= "                 <span style=\"color: #00a\">== 'info'</span>\n\n";
$s .= "ID:              {$session->getId()}\n";
$s .= "                 <span style=\"color: #00a\">!= Session->ID</span>\n\n";
$s .= "CSRFToken:       {$session->CSRFToken->get()}\n";
$s .= "                 <span style=\"color: #00a\">!= anything (brand new)</span>\n\n";
$session->CSRFToken->regenerate();
$s .= "NewCSRFToken:    {$session->CSRFToken->get()}\n";
$s .= "                 <span style=\"color: #00a\">!= RegenSession->CSRFToken</span>\n\n";

$s .= "\n\n<span style=\"color: #a00\">"
    . "Note: This test includes <b><i>Response</i></b>, <b><i>ClientEnv-&gt;has/getCookie()</i></b>, "
    . "<b><i>Cookie</i></b>, and all <b><i>Session</i></b>-related classes"
    . "</span>";

$s .= '</pre>';

$response->headers->setContentType('text/html', 'utf-8');
$response->setContent($s);
$response->send();

exit;

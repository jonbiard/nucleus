<?php
namespace Nucleus\Library\Test\Time;

use Nucleus\Library\Time\DateTime;
use Nucleus\Library\Time\DateTimeInterval;
use PHPUnit_Framework_TestCase;

class DateTimeIntervalTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }


    public function testGetters()
    {
        $interval = new DateTimeInterval(6, 5, 4, 3, 2, 1, false);

        $this->assertEquals(6, $interval->getSeconds());
        $this->assertEquals(5, $interval->getMinutes());
        $this->assertEquals(4, $interval->getHours());
        $this->assertEquals(3, $interval->getDays());
        $this->assertEquals(2, $interval->getMonths());
        $this->assertEquals(1, $interval->getYears());
        $this->assertFalse($interval->isNegative());
    }


    public function testInstantiations()
    {
        $datetime1 = DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00');
        $datetime2 = DateTime::createFromFormat('Y-m-d H:i:s', '2001-03-04 04:05:06');

        $this->validateInstance(new DateTimeInterval(6, 5, 4, 3, 2, 1, true), true);
        $this->validateInstance(DateTimeInterval::createFromSpec('P1Y2M3DT4H5M6S'), false);
        $this->validateInstance(
            DateTimeInterval::createFromString('1 year + 2 months + 3 days + 4 hours + 5 minutes + 6 seconds'),
            false
        );
        $this->validateInstance(DateTimeInterval::createFromDateTime($datetime1, $datetime2), false);
        $this->validateInstance(DateTimeInterval::createFromDateTime($datetime2, $datetime1), true);
        $this->validateInstance(DateTimeInterval::createFromTimestamp((string)$datetime1, (string)$datetime2), false);
        $this->validateInstance(DateTimeInterval::createFromTimestamp((string)$datetime2, (string)$datetime1), true);
    }


    public function testSetters()
    {
        $interval = new DateTimeInterval(6, 5, 4, 3, 2, 1, false);

        $this->validateInstance($interval->setSeconds(6), false);
        $this->validateInstance($interval->setMinutes(5), false);
        $this->validateInstance($interval->setHours(4), false);
        $this->validateInstance($interval->setDays(3), false);
        $this->validateInstance($interval->setMonths(2), false);
        $this->validateInstance($interval->setYears(1), false);
        $this->validateInstance($interval->setNegative(true), true);
    }


    private function validateInstance(DateTimeInterval $interval, $neg)
    {
        $neg = $neg ? '-' : '+';
        $this->assertEquals("$neg 1 2 3 4 5 6", $interval->format('%R %y %m %d %h %i %s'));
    }
}

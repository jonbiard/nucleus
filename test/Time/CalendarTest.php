<?php
namespace Nucleus\Library\Test\Time;

use Nucleus\Library\Time\Calendar;
use Nucleus\Library\Time\CalendarDay;
use Nucleus\Library\Time\DateTime;
use Nucleus\Library\Time\DateTimeInterval;
use Nucleus\Library\Time\Timezone;
use PHPUnit_Framework_TestCase;

class CalendarTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }


    public function testCalendarWithNext()
    {
        $calendar = new Calendar(
            DateTime::createFromFormat('Y-m', "2013-01", new Timezone('America/Montreal'))
        );

        $actual = [];

        for ($i = 0; $i < 12; $i++) {
            $actual[$calendar->getDateTime()->format('Y-m')] = $calendar->get();
            $calendar = $calendar->next();
        }

        $this->compare($actual);
    }


    public function testCalendarWithPrevious()
    {
        $calendar = new Calendar(
            DateTime::createFromFormat('Y-m', "2013-12", new Timezone('America/Montreal'))
        );

        $actual = [];

        for ($i = 12; $i > 0; $i--) {
            $actual[$calendar->getDateTime()->format('Y-m')] = $calendar->get();
            $calendar = $calendar->previous();
        }

        $this->compare($actual);
    }


    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    private function compare($actual)
    {
        $expected = [
            '2013-01' => [
                0 => [
                    0 => new CalendarDay(30, false),
                    1 => new CalendarDay(31, false),
                    2 => new CalendarDay(1, true),
                    3 => new CalendarDay(2, true),
                    4 => new CalendarDay(3, true),
                    5 => new CalendarDay(4, true),
                    6 => new CalendarDay(5, true),
                ],
                1 => [
                    0 => new CalendarDay(6, true),
                    1 => new CalendarDay(7, true),
                    2 => new CalendarDay(8, true),
                    3 => new CalendarDay(9, true),
                    4 => new CalendarDay(10, true),
                    5 => new CalendarDay(11, true),
                    6 => new CalendarDay(12, true),
                ],
                2 => [
                    0 => new CalendarDay(13, true),
                    1 => new CalendarDay(14, true),
                    2 => new CalendarDay(15, true),
                    3 => new CalendarDay(16, true),
                    4 => new CalendarDay(17, true),
                    5 => new CalendarDay(18, true),
                    6 => new CalendarDay(19, true),
                ],
                3 => [
                    0 => new CalendarDay(20, true),
                    1 => new CalendarDay(21, true),
                    2 => new CalendarDay(22, true),
                    3 => new CalendarDay(23, true),
                    4 => new CalendarDay(24, true),
                    5 => new CalendarDay(25, true),
                    6 => new CalendarDay(26, true),
                ],
                4 => [
                    0 => new CalendarDay(27, true),
                    1 => new CalendarDay(28, true),
                    2 => new CalendarDay(29, true),
                    3 => new CalendarDay(30, true),
                    4 => new CalendarDay(31, true),
                    5 => new CalendarDay(1, false),
                    6 => new CalendarDay(2, false),
                ],
                5 => [
                    0 => new CalendarDay(3, false),
                    1 => new CalendarDay(4, false),
                    2 => new CalendarDay(5, false),
                    3 => new CalendarDay(6, false),
                    4 => new CalendarDay(7, false),
                    5 => new CalendarDay(8, false),
                    6 => new CalendarDay(9, false),
                ],
            ],
            '2013-02' => [
                0 => [
                    0 => new CalendarDay(27, false),
                    1 => new CalendarDay(28, false),
                    2 => new CalendarDay(29, false),
                    3 => new CalendarDay(30, false),
                    4 => new CalendarDay(31, false),
                    5 => new CalendarDay(1, true),
                    6 => new CalendarDay(2, true),
                ],
                1 => [
                    0 => new CalendarDay(3, true),
                    1 => new CalendarDay(4, true),
                    2 => new CalendarDay(5, true),
                    3 => new CalendarDay(6, true),
                    4 => new CalendarDay(7, true),
                    5 => new CalendarDay(8, true),
                    6 => new CalendarDay(9, true),
                ],
                2 => [
                    0 => new CalendarDay(10, true),
                    1 => new CalendarDay(11, true),
                    2 => new CalendarDay(12, true),
                    3 => new CalendarDay(13, true),
                    4 => new CalendarDay(14, true),
                    5 => new CalendarDay(15, true),
                    6 => new CalendarDay(16, true),
                ],
                3 => [
                    0 => new CalendarDay(17, true),
                    1 => new CalendarDay(18, true),
                    2 => new CalendarDay(19, true),
                    3 => new CalendarDay(20, true),
                    4 => new CalendarDay(21, true),
                    5 => new CalendarDay(22, true),
                    6 => new CalendarDay(23, true),
                ],
                4 => [
                    0 => new CalendarDay(24, true),
                    1 => new CalendarDay(25, true),
                    2 => new CalendarDay(26, true),
                    3 => new CalendarDay(27, true),
                    4 => new CalendarDay(28, true),
                    5 => new CalendarDay(1, false),
                    6 => new CalendarDay(2, false),
                ],
                5 => [
                    0 => new CalendarDay(3, false),
                    1 => new CalendarDay(4, false),
                    2 => new CalendarDay(5, false),
                    3 => new CalendarDay(6, false),
                    4 => new CalendarDay(7, false),
                    5 => new CalendarDay(8, false),
                    6 => new CalendarDay(9, false),
                ],
            ],
            '2013-03' => [
                0 => [
                    0 => new CalendarDay(24, false),
                    1 => new CalendarDay(25, false),
                    2 => new CalendarDay(26, false),
                    3 => new CalendarDay(27, false),
                    4 => new CalendarDay(28, false),
                    5 => new CalendarDay(1, true),
                    6 => new CalendarDay(2, true),
                ],
                1 => [
                    0 => new CalendarDay(3, true),
                    1 => new CalendarDay(4, true),
                    2 => new CalendarDay(5, true),
                    3 => new CalendarDay(6, true),
                    4 => new CalendarDay(7, true),
                    5 => new CalendarDay(8, true),
                    6 => new CalendarDay(9, true),
                ],
                2 => [
                    0 => new CalendarDay(10, true),
                    1 => new CalendarDay(11, true),
                    2 => new CalendarDay(12, true),
                    3 => new CalendarDay(13, true),
                    4 => new CalendarDay(14, true),
                    5 => new CalendarDay(15, true),
                    6 => new CalendarDay(16, true),
                ],
                3 => [
                    0 => new CalendarDay(17, true),
                    1 => new CalendarDay(18, true),
                    2 => new CalendarDay(19, true),
                    3 => new CalendarDay(20, true),
                    4 => new CalendarDay(21, true),
                    5 => new CalendarDay(22, true),
                    6 => new CalendarDay(23, true),
                ],
                4 => [
                    0 => new CalendarDay(24, true),
                    1 => new CalendarDay(25, true),
                    2 => new CalendarDay(26, true),
                    3 => new CalendarDay(27, true),
                    4 => new CalendarDay(28, true),
                    5 => new CalendarDay(29, true),
                    6 => new CalendarDay(30, true),
                ],
                5 => [
                    0 => new CalendarDay(31, true),
                    1 => new CalendarDay(1, false),
                    2 => new CalendarDay(2, false),
                    3 => new CalendarDay(3, false),
                    4 => new CalendarDay(4, false),
                    5 => new CalendarDay(5, false),
                    6 => new CalendarDay(6, false),
                ],
            ],
            '2013-04' => [
                0 => [
                    0 => new CalendarDay(31, false),
                    1 => new CalendarDay(1, true),
                    2 => new CalendarDay(2, true),
                    3 => new CalendarDay(3, true),
                    4 => new CalendarDay(4, true),
                    5 => new CalendarDay(5, true),
                    6 => new CalendarDay(6, true),
                ],
                1 => [
                    0 => new CalendarDay(7, true),
                    1 => new CalendarDay(8, true),
                    2 => new CalendarDay(9, true),
                    3 => new CalendarDay(10, true),
                    4 => new CalendarDay(11, true),
                    5 => new CalendarDay(12, true),
                    6 => new CalendarDay(13, true),
                ],
                2 => [
                    0 => new CalendarDay(14, true),
                    1 => new CalendarDay(15, true),
                    2 => new CalendarDay(16, true),
                    3 => new CalendarDay(17, true),
                    4 => new CalendarDay(18, true),
                    5 => new CalendarDay(19, true),
                    6 => new CalendarDay(20, true),
                ],
                3 => [
                    0 => new CalendarDay(21, true),
                    1 => new CalendarDay(22, true),
                    2 => new CalendarDay(23, true),
                    3 => new CalendarDay(24, true),
                    4 => new CalendarDay(25, true),
                    5 => new CalendarDay(26, true),
                    6 => new CalendarDay(27, true),
                ],
                4 => [
                    0 => new CalendarDay(28, true),
                    1 => new CalendarDay(29, true),
                    2 => new CalendarDay(30, true),
                    3 => new CalendarDay(1, false),
                    4 => new CalendarDay(2, false),
                    5 => new CalendarDay(3, false),
                    6 => new CalendarDay(4, false),
                ],
                5 => [
                    0 => new CalendarDay(5, false),
                    1 => new CalendarDay(6, false),
                    2 => new CalendarDay(7, false),
                    3 => new CalendarDay(8, false),
                    4 => new CalendarDay(9, false),
                    5 => new CalendarDay(10, false),
                    6 => new CalendarDay(11, false),
                ],
            ],
            '2013-05' => [
                0 => [
                    0 => new CalendarDay(28, false),
                    1 => new CalendarDay(29, false),
                    2 => new CalendarDay(30, false),
                    3 => new CalendarDay(1, true),
                    4 => new CalendarDay(2, true),
                    5 => new CalendarDay(3, true),
                    6 => new CalendarDay(4, true),
                ],
                1 => [
                    0 => new CalendarDay(5, true),
                    1 => new CalendarDay(6, true),
                    2 => new CalendarDay(7, true),
                    3 => new CalendarDay(8, true),
                    4 => new CalendarDay(9, true),
                    5 => new CalendarDay(10, true),
                    6 => new CalendarDay(11, true),
                ],
                2 => [
                    0 => new CalendarDay(12, true),
                    1 => new CalendarDay(13, true),
                    2 => new CalendarDay(14, true),
                    3 => new CalendarDay(15, true),
                    4 => new CalendarDay(16, true),
                    5 => new CalendarDay(17, true),
                    6 => new CalendarDay(18, true),
                ],
                3 => [
                    0 => new CalendarDay(19, true),
                    1 => new CalendarDay(20, true),
                    2 => new CalendarDay(21, true),
                    3 => new CalendarDay(22, true),
                    4 => new CalendarDay(23, true),
                    5 => new CalendarDay(24, true),
                    6 => new CalendarDay(25, true),
                ],
                4 => [
                    0 => new CalendarDay(26, true),
                    1 => new CalendarDay(27, true),
                    2 => new CalendarDay(28, true),
                    3 => new CalendarDay(29, true),
                    4 => new CalendarDay(30, true),
                    5 => new CalendarDay(31, true),
                    6 => new CalendarDay(1, false),
                ],
                5 => [
                    0 => new CalendarDay(2, false),
                    1 => new CalendarDay(3, false),
                    2 => new CalendarDay(4, false),
                    3 => new CalendarDay(5, false),
                    4 => new CalendarDay(6, false),
                    5 => new CalendarDay(7, false),
                    6 => new CalendarDay(8, false),
                ],
            ],
            '2013-06' => [
                0 => [
                    0 => new CalendarDay(26, false),
                    1 => new CalendarDay(27, false),
                    2 => new CalendarDay(28, false),
                    3 => new CalendarDay(29, false),
                    4 => new CalendarDay(30, false),
                    5 => new CalendarDay(31, false),
                    6 => new CalendarDay(1, true),
                ],
                1 => [
                    0 => new CalendarDay(2, true),
                    1 => new CalendarDay(3, true),
                    2 => new CalendarDay(4, true),
                    3 => new CalendarDay(5, true),
                    4 => new CalendarDay(6, true),
                    5 => new CalendarDay(7, true),
                    6 => new CalendarDay(8, true),
                ],
                2 => [
                    0 => new CalendarDay(9, true),
                    1 => new CalendarDay(10, true),
                    2 => new CalendarDay(11, true),
                    3 => new CalendarDay(12, true),
                    4 => new CalendarDay(13, true),
                    5 => new CalendarDay(14, true),
                    6 => new CalendarDay(15, true),
                ],
                3 => [
                    0 => new CalendarDay(16, true),
                    1 => new CalendarDay(17, true),
                    2 => new CalendarDay(18, true),
                    3 => new CalendarDay(19, true),
                    4 => new CalendarDay(20, true),
                    5 => new CalendarDay(21, true),
                    6 => new CalendarDay(22, true),
                ],
                4 => [
                    0 => new CalendarDay(23, true),
                    1 => new CalendarDay(24, true),
                    2 => new CalendarDay(25, true),
                    3 => new CalendarDay(26, true),
                    4 => new CalendarDay(27, true),
                    5 => new CalendarDay(28, true),
                    6 => new CalendarDay(29, true),
                ],
                5 => [
                    0 => new CalendarDay(30, true),
                    1 => new CalendarDay(1, false),
                    2 => new CalendarDay(2, false),
                    3 => new CalendarDay(3, false),
                    4 => new CalendarDay(4, false),
                    5 => new CalendarDay(5, false),
                    6 => new CalendarDay(6, false),
                ],
            ],
            '2013-07' => [
                0 => [
                    0 => new CalendarDay(30, false),
                    1 => new CalendarDay(1, true),
                    2 => new CalendarDay(2, true),
                    3 => new CalendarDay(3, true),
                    4 => new CalendarDay(4, true),
                    5 => new CalendarDay(5, true),
                    6 => new CalendarDay(6, true),
                ],
                1 => [
                    0 => new CalendarDay(7, true),
                    1 => new CalendarDay(8, true),
                    2 => new CalendarDay(9, true),
                    3 => new CalendarDay(10, true),
                    4 => new CalendarDay(11, true),
                    5 => new CalendarDay(12, true),
                    6 => new CalendarDay(13, true),
                ],
                2 => [
                    0 => new CalendarDay(14, true),
                    1 => new CalendarDay(15, true),
                    2 => new CalendarDay(16, true),
                    3 => new CalendarDay(17, true),
                    4 => new CalendarDay(18, true),
                    5 => new CalendarDay(19, true),
                    6 => new CalendarDay(20, true),
                ],
                3 => [
                    0 => new CalendarDay(21, true),
                    1 => new CalendarDay(22, true),
                    2 => new CalendarDay(23, true),
                    3 => new CalendarDay(24, true),
                    4 => new CalendarDay(25, true),
                    5 => new CalendarDay(26, true),
                    6 => new CalendarDay(27, true),
                ],
                4 => [
                    0 => new CalendarDay(28, true),
                    1 => new CalendarDay(29, true),
                    2 => new CalendarDay(30, true),
                    3 => new CalendarDay(31, true),
                    4 => new CalendarDay(1, false),
                    5 => new CalendarDay(2, false),
                    6 => new CalendarDay(3, false),
                ],
                5 => [
                    0 => new CalendarDay(4, false),
                    1 => new CalendarDay(5, false),
                    2 => new CalendarDay(6, false),
                    3 => new CalendarDay(7, false),
                    4 => new CalendarDay(8, false),
                    5 => new CalendarDay(9, false),
                    6 => new CalendarDay(10, false),
                ],
            ],
            '2013-08' => [
                0 => [
                    0 => new CalendarDay(28, false),
                    1 => new CalendarDay(29, false),
                    2 => new CalendarDay(30, false),
                    3 => new CalendarDay(31, false),
                    4 => new CalendarDay(1, true),
                    5 => new CalendarDay(2, true),
                    6 => new CalendarDay(3, true),
                ],
                1 => [
                    0 => new CalendarDay(4, true),
                    1 => new CalendarDay(5, true),
                    2 => new CalendarDay(6, true),
                    3 => new CalendarDay(7, true),
                    4 => new CalendarDay(8, true),
                    5 => new CalendarDay(9, true),
                    6 => new CalendarDay(10, true),
                ],
                2 => [
                    0 => new CalendarDay(11, true),
                    1 => new CalendarDay(12, true),
                    2 => new CalendarDay(13, true),
                    3 => new CalendarDay(14, true),
                    4 => new CalendarDay(15, true),
                    5 => new CalendarDay(16, true),
                    6 => new CalendarDay(17, true),
                ],
                3 => [
                    0 => new CalendarDay(18, true),
                    1 => new CalendarDay(19, true),
                    2 => new CalendarDay(20, true),
                    3 => new CalendarDay(21, true),
                    4 => new CalendarDay(22, true),
                    5 => new CalendarDay(23, true),
                    6 => new CalendarDay(24, true),
                ],
                4 => [
                    0 => new CalendarDay(25, true),
                    1 => new CalendarDay(26, true),
                    2 => new CalendarDay(27, true),
                    3 => new CalendarDay(28, true),
                    4 => new CalendarDay(29, true),
                    5 => new CalendarDay(30, true),
                    6 => new CalendarDay(31, true),
                ],
                5 => [
                    0 => new CalendarDay(1, false),
                    1 => new CalendarDay(2, false),
                    2 => new CalendarDay(3, false),
                    3 => new CalendarDay(4, false),
                    4 => new CalendarDay(5, false),
                    5 => new CalendarDay(6, false),
                    6 => new CalendarDay(7, false),
                ],
            ],
            '2013-09' => [
                0 => [
                    0 => new CalendarDay(25, false),
                    1 => new CalendarDay(26, false),
                    2 => new CalendarDay(27, false),
                    3 => new CalendarDay(28, false),
                    4 => new CalendarDay(29, false),
                    5 => new CalendarDay(30, false),
                    6 => new CalendarDay(31, false),
                ],
                1 => [
                    0 => new CalendarDay(1, true),
                    1 => new CalendarDay(2, true),
                    2 => new CalendarDay(3, true),
                    3 => new CalendarDay(4, true),
                    4 => new CalendarDay(5, true),
                    5 => new CalendarDay(6, true),
                    6 => new CalendarDay(7, true),
                ],
                2 => [
                    0 => new CalendarDay(8, true),
                    1 => new CalendarDay(9, true),
                    2 => new CalendarDay(10, true),
                    3 => new CalendarDay(11, true),
                    4 => new CalendarDay(12, true),
                    5 => new CalendarDay(13, true),
                    6 => new CalendarDay(14, true),
                ],
                3 => [
                    0 => new CalendarDay(15, true),
                    1 => new CalendarDay(16, true),
                    2 => new CalendarDay(17, true),
                    3 => new CalendarDay(18, true),
                    4 => new CalendarDay(19, true),
                    5 => new CalendarDay(20, true),
                    6 => new CalendarDay(21, true),
                ],
                4 => [
                    0 => new CalendarDay(22, true),
                    1 => new CalendarDay(23, true),
                    2 => new CalendarDay(24, true),
                    3 => new CalendarDay(25, true),
                    4 => new CalendarDay(26, true),
                    5 => new CalendarDay(27, true),
                    6 => new CalendarDay(28, true),
                ],
                5 => [
                    0 => new CalendarDay(29, true),
                    1 => new CalendarDay(30, true),
                    2 => new CalendarDay(1, false),
                    3 => new CalendarDay(2, false),
                    4 => new CalendarDay(3, false),
                    5 => new CalendarDay(4, false),
                    6 => new CalendarDay(5, false),
                ],
            ],
            '2013-10' => [
                0 => [
                    0 => new CalendarDay(29, false),
                    1 => new CalendarDay(30, false),
                    2 => new CalendarDay(1, true),
                    3 => new CalendarDay(2, true),
                    4 => new CalendarDay(3, true),
                    5 => new CalendarDay(4, true),
                    6 => new CalendarDay(5, true),
                ],
                1 => [
                    0 => new CalendarDay(6, true),
                    1 => new CalendarDay(7, true),
                    2 => new CalendarDay(8, true),
                    3 => new CalendarDay(9, true),
                    4 => new CalendarDay(10, true),
                    5 => new CalendarDay(11, true),
                    6 => new CalendarDay(12, true),
                ],
                2 => [
                    0 => new CalendarDay(13, true),
                    1 => new CalendarDay(14, true),
                    2 => new CalendarDay(15, true),
                    3 => new CalendarDay(16, true),
                    4 => new CalendarDay(17, true),
                    5 => new CalendarDay(18, true),
                    6 => new CalendarDay(19, true),
                ],
                3 => [
                    0 => new CalendarDay(20, true),
                    1 => new CalendarDay(21, true),
                    2 => new CalendarDay(22, true),
                    3 => new CalendarDay(23, true),
                    4 => new CalendarDay(24, true),
                    5 => new CalendarDay(25, true),
                    6 => new CalendarDay(26, true),
                ],
                4 => [
                    0 => new CalendarDay(27, true),
                    1 => new CalendarDay(28, true),
                    2 => new CalendarDay(29, true),
                    3 => new CalendarDay(30, true),
                    4 => new CalendarDay(31, true),
                    5 => new CalendarDay(1, false),
                    6 => new CalendarDay(2, false),
                ],
                5 => [
                    0 => new CalendarDay(3, false),
                    1 => new CalendarDay(4, false),
                    2 => new CalendarDay(5, false),
                    3 => new CalendarDay(6, false),
                    4 => new CalendarDay(7, false),
                    5 => new CalendarDay(8, false),
                    6 => new CalendarDay(9, false),
                ],
            ],
            '2013-11' => [
                0 => [
                    0 => new CalendarDay(27, false),
                    1 => new CalendarDay(28, false),
                    2 => new CalendarDay(29, false),
                    3 => new CalendarDay(30, false),
                    4 => new CalendarDay(31, false),
                    5 => new CalendarDay(1, true),
                    6 => new CalendarDay(2, true),
                ],
                1 => [
                    0 => new CalendarDay(3, true),
                    1 => new CalendarDay(4, true),
                    2 => new CalendarDay(5, true),
                    3 => new CalendarDay(6, true),
                    4 => new CalendarDay(7, true),
                    5 => new CalendarDay(8, true),
                    6 => new CalendarDay(9, true),
                ],
                2 => [
                    0 => new CalendarDay(10, true),
                    1 => new CalendarDay(11, true),
                    2 => new CalendarDay(12, true),
                    3 => new CalendarDay(13, true),
                    4 => new CalendarDay(14, true),
                    5 => new CalendarDay(15, true),
                    6 => new CalendarDay(16, true),
                ],
                3 => [
                    0 => new CalendarDay(17, true),
                    1 => new CalendarDay(18, true),
                    2 => new CalendarDay(19, true),
                    3 => new CalendarDay(20, true),
                    4 => new CalendarDay(21, true),
                    5 => new CalendarDay(22, true),
                    6 => new CalendarDay(23, true),
                ],
                4 => [
                    0 => new CalendarDay(24, true),
                    1 => new CalendarDay(25, true),
                    2 => new CalendarDay(26, true),
                    3 => new CalendarDay(27, true),
                    4 => new CalendarDay(28, true),
                    5 => new CalendarDay(29, true),
                    6 => new CalendarDay(30, true),
                ],
                5 => [
                    0 => new CalendarDay(1, false),
                    1 => new CalendarDay(2, false),
                    2 => new CalendarDay(3, false),
                    3 => new CalendarDay(4, false),
                    4 => new CalendarDay(5, false),
                    5 => new CalendarDay(6, false),
                    6 => new CalendarDay(7, false),
                ],
            ],
            '2013-12' => [
                0 => [
                    0 => new CalendarDay(24, false),
                    1 => new CalendarDay(25, false),
                    2 => new CalendarDay(26, false),
                    3 => new CalendarDay(27, false),
                    4 => new CalendarDay(28, false),
                    5 => new CalendarDay(29, false),
                    6 => new CalendarDay(30, false),
                ],
                1 => [
                    0 => new CalendarDay(1, true),
                    1 => new CalendarDay(2, true),
                    2 => new CalendarDay(3, true),
                    3 => new CalendarDay(4, true),
                    4 => new CalendarDay(5, true),
                    5 => new CalendarDay(6, true),
                    6 => new CalendarDay(7, true),
                ],
                2 => [
                    0 => new CalendarDay(8, true),
                    1 => new CalendarDay(9, true),
                    2 => new CalendarDay(10, true),
                    3 => new CalendarDay(11, true),
                    4 => new CalendarDay(12, true),
                    5 => new CalendarDay(13, true),
                    6 => new CalendarDay(14, true),
                ],
                3 => [
                    0 => new CalendarDay(15, true),
                    1 => new CalendarDay(16, true),
                    2 => new CalendarDay(17, true),
                    3 => new CalendarDay(18, true),
                    4 => new CalendarDay(19, true),
                    5 => new CalendarDay(20, true),
                    6 => new CalendarDay(21, true),
                ],
                4 => [
                    0 => new CalendarDay(22, true),
                    1 => new CalendarDay(23, true),
                    2 => new CalendarDay(24, true),
                    3 => new CalendarDay(25, true),
                    4 => new CalendarDay(26, true),
                    5 => new CalendarDay(27, true),
                    6 => new CalendarDay(28, true),
                ],
                5 => [
                    0 => new CalendarDay(29, true),
                    1 => new CalendarDay(30, true),
                    2 => new CalendarDay(31, true),
                    3 => new CalendarDay(1, false),
                    4 => new CalendarDay(2, false),
                    5 => new CalendarDay(3, false),
                    6 => new CalendarDay(4, false),
                ],
            ],
        ];

        foreach ($expected as $month => $calendar) {
            foreach ($calendar as $rowKey => $row) {
                foreach ($row as $dayKey => $day) {
                    $this->assertEquals($day->day, $actual[$month][$rowKey][$dayKey]->day);
                    $this->assertEquals($day->inMonth, $actual[$month][$rowKey][$dayKey]->inMonth);
                }
            }
        }
    }
}

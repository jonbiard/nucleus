<?php
namespace Nucleus\Library\Test\Time;

use Nucleus\Library\Time\DateTime;
use Nucleus\Library\Time\DateTimeInterval;
use Nucleus\Library\Time\Timezone;
use PHPUnit_Framework_TestCase;

class DateTimeTest extends PHPUnit_Framework_TestCase
{
    private $tz;


    public function setUp()
    {
        $this->tz = new Timezone('America/Montreal');
    }


    public function testAddSubtractIntervals()
    {
        $datetime   = DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00', $this->tz);
        $interval   = DateTimeInterval::createFromSpec('P1Y1M1DT1H1M3661S');
        $added      = $datetime->add($interval);
        $subtracted = $added->subtract($interval);

        $this->validateAddSubtract($added, $subtracted);
    }


    public function testAddSubtractSeconds()
    {
        $datetime   = DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00', $this->tz);
        $added      = $datetime->addSeconds(34394521);
        $subtracted = $added->subtractSeconds(34394521);

        $this->validateAddSubtract($added, $subtracted);
    }


    public function testGetters()
    {
        $datetime = DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00', $this->tz);

        $this->assertEquals(0, $datetime->getMicroSecond());
        $this->assertEquals(0, $datetime->getSecond());
        $this->assertEquals(0, $datetime->getMinute());
        $this->assertEquals(0, $datetime->getHour());
        $this->assertEquals(1, $datetime->getDay());
        $this->assertEquals(1, $datetime->getMonth());
        $this->assertEquals(2000, $datetime->getYear());

        $this->assertEquals(0, $datetime->getDayOfYear());
        $this->assertEquals(6, $datetime->getWeekDay());
        $this->assertEquals(31, $datetime->getDaysInMonth());
        $this->assertEquals(946702800, $datetime->getTimestamp());
        $this->assertEquals('946702800', $datetime);
        $this->assertTrue($datetime->isAM());
        $this->assertFalse($datetime->isPM());
        $this->assertFalse($datetime->isDST());
        $this->assertTrue($datetime->isLeapYear());

        $datetime = DateTime::createFromFormat('Y-m-d H:i:s', '2001-07-01 15:00:00', $this->tz);

        $this->assertFalse($datetime->isAM());
        $this->assertTrue($datetime->isPM());
        $this->assertTrue($datetime->isDST());
        $this->assertFalse($datetime->isLeapYear());
    }


    public function testInstantiations()
    {
        $this->validateInstance(new DateTime('first day of January 2000', $this->tz));
        $this->validateInstance(DateTime::createFromFormat('Y-m-d H:i:s', '2000-01-01 00:00:00', $this->tz));
        $this->validateInstance(DateTime::createFromValues(5, 0, 0, 1, 1, 2000, $this->tz));
        $this->validateInstance(DateTime::createFromTimestamp(946702800, $this->tz));

        $tmp = date_default_timezone_get();
        date_default_timezone_set($this->tz);
        $this->validateInstance(DateTime::createFromLocalValues(0, 0, 0, 1, 1, 2000, $this->tz));
        date_default_timezone_set($tmp);
    }


    private function validateAddSubtract(DateTime $added, DateTime $subtracted)
    {
        $this->assertEquals('2001-02-02 02:02:01', $added->format('Y-m-d H:i:s'));
        $this->assertEquals('2000-01-01 00:00:00', $subtracted->format('Y-m-d H:i:s'));
    }


    private function validateInstance(DateTime $datetime)
    {
        $this->assertEquals('2000-01-01 00:00:00', $datetime->format('Y-m-d H:i:s'));
        $this->assertEquals($this->tz, $datetime->getTimezone());

        $utc = new Timezone('UTC');

        $datetime = $datetime->setTimezone($utc);
        $this->assertEquals('2000-01-01 05:00:00', $datetime->format('Y-m-d H:i:s'));
        $this->assertEquals('UTC', $datetime->getTimezone());
    }
}

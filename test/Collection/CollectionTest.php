<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Collection\Collection;
use PHPUnit_Framework_TestCase;

class CollectionTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }


    public function testAppendSuccess()
    {
        $c = new Collection([1, 'b' => 1.5, 7 => 'str']);

        $this->assertSame([1, 'b' => 1.5, 7 => 'str', 1], $c->append(1)->toArray());
        $this->assertSame([1, 'b' => 1.5, 7 => 'str', 1 => 1], $c->append(1, 1)->toArray());
        $this->assertSame([1, 'b' => 1.5, 7 => 'str', 'z' => 1], $c->append('z', 1)->toArray());
    }


    public function testAtSuccess()
    {
        $rawArray = [1, 'b' => 1.5, 7 => 'str'];
        $c        = new Collection($rawArray);

        $this->assertSame($rawArray, $c->at(0, 1, -1)->toArray());
    }


    public function testChunkSuccess()
    {
        $c = new Collection([1, 'b' => 1.5, 7 => 'str']);

        $this->assertSame([0 => [1, 'b' => 1.5], 1 => [7 => 'str']], $c->chunk(2, true)->toArray());
        $this->assertSame([0 => [1, 1.5], 1 => ['str']], $c->chunk(2, false)->toArray());
    }


    public function testConstructStaticSuccess()
    {
        $this->assertSame(
            [
                1 => 4,
                2 => 5,
                3 => 6
            ],
            Collection::combine([1, 2, 3], [4, 5, 6])->toArray()
        );
        $this->assertSame(
            [
                'a' => 'val',
                0   => 'val',
                5   => 'val',
                'b' => 'val',
            ],
            Collection::fillKeys(['a', 0, 5, 'b'], 'val')->toArray()
        );
        $this->assertSame(
            [
                10 => 'val',
                11 => 'val',
                12 => 'val',
            ],
            Collection::fillValues('val', 3, 10)->toArray()
        );
        $this->assertSame(
            [
                -10 => 'val',
                0   => 'val',
                1   => 'val',
            ],
            Collection::fillValues('val', 3, -10)->toArray()
        );
    }


    public function testConstructSuccess()
    {
        $rawArray = [
            1,
            1.5,
            'string',
            [1, 1.5, 'string', new Collection([1, 2, 3])],
            new Collection([1, 2, 3]),
            'a' => 1,
            'b' => 1.5,
            'c' => 'string',
            'd' => [1, 1.5, 'string', new Collection([1, 2, 3])],
            'e' => new Collection([1, 2, 3]),
            5   => 1,
            6   => 1.5,
            7   => 'string',
            8   => [1, 1.5, 'string', new Collection([1, 2, 3])],
            9   => new Collection([1, 2, 3]),
        ];

        $c = new Collection($rawArray);

        $this->assertSame($rawArray, $c->toArray());
    }


    public function testCountSuccess()
    {
        $c        = new Collection([1, 'hello', 1, 'world', 'hello']);
        $cb = function ($k, $v) {
            if ($k === 0 || $v === 'hello') {
                return true;
            }

            return false;
        };

        $this->assertSame([1 => 2, 'hello' => 2, 'world' => 1], $c->count()->toArray());
        $this->assertSame(3, $c->count($cb));
    }


    public function testDifferenceByAssocSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "yellow", "red"]);

        $this->assertSame(
            ["b" => "brown", "c" => "blue", 0 => "red"],
            $c1->differenceByAssoc($c2)->toArray()
        );
    }


    public function testDifferenceByKeysSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "yellow", "red"]);

        $this->assertSame(
            ["b" => "brown", "c" => "blue"],
            $c1->differenceByKeys($c2)->toArray()
        );
    }


    public function testDifferenceByValuesSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "yellow", "redder"]);

        $this->assertSame(
            ["b" => "brown", "c" => "blue", 0 => "red"],
            $c1->differenceByValues($c2)->toArray()
        );
    }


    public function testDuplicatesSuccess()
    {
        $c = new Collection(["a" => "green", "blue", "blue", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame(
            [1 => "blue", "c" => "blue", 2 => "green"],
            $c->duplicates()->toArray()
        );
    }


    public function testFirstSuccess()
    {
        $c = new Collection(["a" => "green", "blue", "blue", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame(
            ["a" => "green", "blue", "blue"],
            $c->first(3)->toArray()
        );
    }


    public function testFlipSuccess()
    {
        $c = new Collection(["a" => "green", "blue", "blue", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame(
            ["green" => 2, "blue" => "c", "brown" => "b"],
            $c->flip()->toArray()
        );
    }


    public function testFormatSuccess()
    {
        $c = new Collection(["a" => "a", 1 => 1, "discard" => "discard"]);

        $this->assertSame(
            ["a" => "a", 1 => 1, "added" => 0],
            $c->format(["a" => 0, 1 => 0, "added" => 0])->toArray()
        );
    }


    public function testGetSuccess()
    {
        $c = new Collection(["a" => "a", 1 => 1]);

        $this->assertSame('a', $c->get('a'));
        $this->assertSame(1, $c->get(1));
    }


    public function testHasAllSuccess()
    {
        $c             = new Collection(["a" => "a", 1 => 1]);
        $true  = function ($k, $v) {
            if ($k === 'a' || $v === 1) {
                return true;
            }
            return false;
        };
        $false = function ($k, $v) {
            if ($k === null || $v === null) {
                return true;
            }
            return false;
        };

        $this->assertTrue($c->hasAll($true));
        $this->assertFalse($c->hasAll($false));
    }


    public function testHasAnySuccess()
    {
        $c             = new Collection(["a" => "a", 1 => 1]);
        $true  = function ($k, $v) {
            if ($k === 'a' || $v === null) {
                return true;
            }
            return false;
        };
        $false = function ($k, $v) {
            if ($k === null || $v === null) {
                return true;
            }
            return false;
        };

        $this->assertTrue($c->hasAny($true));
        $this->assertFalse($c->hasAny($false));
    }


    public function testHasKeysSuccess()
    {
        $c = new Collection(["a" => "a", 1 => 1]);

        $this->assertTrue($c->hasKey('a'));
        $this->assertFalse($c->hasKey(2));
        $this->assertTrue($c->hasKeys('a', 1));
        $this->assertFalse($c->hasKeys('a', 1, 2));
    }


    public function testHasValuesSuccess()
    {
        $c = new Collection(["a" => "a", 1 => 1]);

        $this->assertTrue($c->hasValue('a'));
        $this->assertFalse($c->hasValue(2));
        $this->assertTrue($c->hasValues('a', 1));
        $this->assertFalse($c->hasValues('a', 1, 2));
    }


    public function testInsertSuccess()
    {
        $c = new Collection(["a" => "a", 1 => 1]);

        $this->assertSame(["a" => "a", 0 => "b", 1 => 1], $c->insertAfter('a', 'b')->toArray());
        $this->assertSame(["a" => "a", 1 => 1, 2 => "b"], $c->insertAfter(1, 'b')->toArray());
        $this->assertSame(["a" => "a", "b" => "b", 1 => 1], $c->insertAfter('a', 'b', 'b')->toArray());
        $this->assertSame(["a" => "a", "b" => null, 1 => 1], $c->insertAfter('a', 'b', null)->toArray());

        $this->assertSame(["a" => "a", 0 => "b", 1 => 1], $c->insertAt(1, 'b')->toArray());
        $this->assertSame(["a" => "a", 1 => 1, 2 => "b"], $c->insertAt(2, 'b')->toArray());
        $this->assertSame(["a" => "a", "b" => "b", 1 => 1], $c->insertAt(1, 'b', 'b')->toArray());
        $this->assertSame(["a" => "a", "b" => null, 1 => 1], $c->insertAt(1, 'b', null)->toArray());

        $this->assertSame(["a" => "a", 0 => "b", 1 => 1], $c->insertBefore(1, 'b')->toArray());
        $this->assertSame([0 => "b", "a" => "a", 1 => 1], $c->insertBefore('a', 'b')->toArray());
        $this->assertSame(["b" => "b", "a" => "a", 1 => 1], $c->insertBefore('a', 'b', 'b')->toArray());
        $this->assertSame(["b" => null, "a" => "a", 1 => 1], $c->insertBefore('a', 'b', null)->toArray());
    }


    public function testIntersectByAssocSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "red", "yellow"]);

        $this->assertSame(
            ["a" => "green", 0 => "red"],
            $c1->intersectByAssoc($c2)->toArray()
        );
    }


    public function testIntersectByKeysSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "b" => "red", "brown"]);

        $this->assertSame(
            ["a" => "green", "b" => "brown", 0 => "red"],
            $c1->intersectByKeys($c2)->toArray()
        );
    }


    public function testIntersectByValuesSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection(["a" => "green", "red", "brown"]);

        $this->assertSame(
            ["a" => "green", "b" => "brown", 0 => "red"],
            $c1->intersectByValues($c2)->toArray()
        );
    }


    public function testIsEmptySuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection([]);
        $c3 = new Collection();

        $this->assertFalse($c1->isEmpty());
        $this->assertTrue($c2->isEmpty());
        $this->assertTrue($c3->isEmpty());
    }


    public function testIteratorSuccess()
    {
        $c = new Collection(["a" => "b", 1 => 2]);

        foreach ($c as $k => $v) {
            if ($k === 'a') {
                $this->assertSame('a', $k);
                $this->assertSame('b', $v);
            } else {
                $this->assertSame(1, $k);
                $this->assertSame(2, $v);
            }
        }
    }


    public function testJoinSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "red"]);
        $c2 = new Collection([1, 2, 3]);

        $this->assertSame('green,brown,blue,red', $c1->join(','));
        $this->assertSame('1;2;3', $c2->join(';'));
    }


    public function testKeysSuccess()
    {
        $c = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame('a', $c->key());
        $this->assertSame(['a', 'b', 'c', 0], $c->keys()->toArray());
        $this->assertSame(['a', 0], $c->keysOf('green')->toArray());
    }


    public function testLastSuccess()
    {
        $c = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame(["c" => "blue", "green"], $c->last(2)->toArray());
    }


    public function testLengthSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "green"]);
        $c2 = new Collection(["a" => "green"]);
        $c3 = new Collection();

        $this->assertSame(4, $c1->length());
        $this->assertSame(1, $c2->length());
        $this->assertSame(0, $c3->length());
    }


    public function testMapSuccess()
    {
        $c = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "green"]);
        $toUpper = function ($k) {
            if (is_int($k)) {
                return $k;
            }
            return strtoupper($k);
        };

        $this->assertSame(["a" => "GREEN", "b" => "BROWN", "c" => "BLUE", "GREEN"], $c->map($toUpper)->toArray());
        $this->assertSame(["A" => "green", "B" => "brown", "C" => "blue", "green"], $c->mapKeys($toUpper)->toArray());
    }


    public function testMergeSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "green"]);
        $c2 = new Collection(["a" => "blue", 0 => "blue", 1 => "orange"]);

        $this->assertSame(
            ["a" => "blue", "b" => "brown", 0 => "green", 1 => "blue", 2 => "orange"],
            $c1->merge($c2)->toArray()
        );

        $c1 = new Collection(["a" => [1, 2, 'c' => [1, 2]], "b" => "brown"]);
        $c2 = new Collection(["a" => ['a', 'b', 'c' => [3, 4]], 0 => "blue"]);

        $this->assertSame(
            ["a" => [1, 2, 'c' => [1, 2, 3, 4], 'a', 'b'], "b" => "brown", 0 => "blue"],
            $c1->mergeRecursive($c2)->toArray()
        );
    }


    public function testNotSuccess()
    {
        $c = new Collection(["a" => "green", "b" => "brown", "green"]);
        $isNumeric = function ($k) {
            if (is_numeric($k)) {
                return true;
            }
            return false;
        };

        $this->assertSame(["a" => "green", "b" => "brown"], $c->not($isNumeric)->toArray());
        $this->assertSame(["b" => "brown"], $c->notAt(0, 2)->toArray());
        $this->assertSame(["b" => "brown", "green"], $c->notFirst(1)->toArray());
        $this->assertSame(["green"], $c->notFirst(2)->toArray());
        $this->assertSame(["a" => "green", "b" => "brown"], $c->notLast(1)->toArray());
        $this->assertSame(["a" => "green"], $c->notLast(2)->toArray());
    }


    public function testPadSuccess()
    {
        $c = new Collection(["a" => "a", "b" => "b", "c"]);

        $this->assertSame(["a" => "a", "b" => "b", "c"], $c->padLeft(2, 'value')->toArray());
        $this->assertSame(['value', 'value', "a" => "a", "b" => "b", "c"], $c->padLeft(5, 'value')->toArray());
        $this->assertSame(["a" => "a", "b" => "b", "c"], $c->padRight(2, 'value')->toArray());
        $this->assertSame(["a" => "a", "b" => "b", "c", 'value', 'value'], $c->padRight(5, 'value')->toArray());
    }


    public function testPluckSuccess()
    {
        $c = new Collection(['a' => ['a' => 1, 'b' => 2], ['a' => 3, 'c' => 4]]);

        $this->assertSame(['a' => 1, 3], $c->pluck('a')->toArray());
        $this->assertSame(['a' => 2, null], $c->pluck('b')->toArray());
        $this->assertSame(['a' => null, 4], $c->pluck('c')->toArray());
        $this->assertSame(['a' => null, null], $c->pluck('d')->toArray());
    }


    public function testPrependSuccess()
    {
        $c = new Collection(['a' => 'a', 'b' => 'b', 'c']);

        $this->assertSame(['val', 'a' => 'a', 'b' => 'b', 'c'], $c->prepend('val')->toArray());
        $this->assertSame(['key' => 'val', 'a' => 'a', 'b' => 'b', 'c'], $c->prepend('key', 'val')->toArray());
        $this->assertSame(['val', 'a' => 'a', 'b' => 'b'], $c->prepend(0, 'val')->toArray());
        $this->assertSame([1 => 'val', 'a' => 'a', 'b' => 'b', 0 => 'c'], $c->prepend(1, 'val')->toArray());
    }


    public function testRandomSuccess()
    {
        $c = new Collection(['a' => 1, 2, 3, 'b' => 4, 5, 6, 7, 'c' => 8, 9]);
        $c->randomSeed(1234567890);

        $this->assertSame([0 => 2, 'b' => 4, 4 => 7], $c->random(3)->toArray());
    }


    public function testReduceSuccess()
    {
        $c = new Collection(['a' => 1, 2, 3, 'b' => 4, 5, 6, 7, 'c' => 8, 9]);
        $add = function ($a, $b) {
            return $a + $b;
        };
        $mul = function ($a, $b) {
            return $a * $b;
        };

        $this->assertSame(45, $c->reduce($add));
        $this->assertSame(55, $c->reduce($add, 10));

        $this->assertSame(0, $c->reduce($mul));
        $this->assertSame(362880, $c->reduce($mul, 1));
        $this->assertSame(3628800, $c->reduce($mul, 10));
    }


    public function testReindexSuccess()
    {
        $c = new Collection(['a' => 1, 15 => 2, 3, 'b' => 4, 7 => 5, 6, 7, 'c' => 8, 5 => 9]);

        $this->assertSame(['a' => 1, 2, 3, 'b' => 4, 5, 6, 7, 'c' => 8, 9], $c->reindex()->toArray());
    }


    public function testRenameKeySuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c']);

        $this->assertSame(['d' => 1, 'b' => 2, 'c'], $c->renameKey('a', 'd')->toArray());
    }


    public function testRepeatSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c']);

        $this->assertSame([1, 2, 'c', 1, 2, 'c'], $c->repeat(2)->toArray());
        $this->assertSame([1, 2, 'c'], $c->repeat(1)->toArray());
    }


    public function testReplaceSuccess()
    {
        $c1 = new Collection(["a" => "green", "b" => "brown", "green"]);
        $c2 = new Collection(["a" => "blue", 0 => "blue", 1 => "orange"]);

        $this->assertSame(
            ["a" => "blue", "b" => "brown", 0 => "blue", 1 => "orange"],
            $c1->replace($c2)->toArray()
        );

        $c1 = new Collection(["a" => [1, 2, 'c' => [1, 2]], "b" => "brown"]);
        $c2 = new Collection(["a" => ['a', 'b', 'c' => [3, 4]], 0 => "blue"]);

        $this->assertSame(
            ["a" => ['a', 'b', 'c' => [3, 4]], "b" => "brown", 0 => "blue"],
            $c1->replaceRecursive($c2)->toArray()
        );
    }


    public function testReverseSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c']);

        $this->assertSame(['c', 'b' => 2, 'a' => 1], $c->reverse()->toArray());
    }


    public function testRotateSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c']);

        $this->assertSame(['b' => 2, 'c', 'a' => 1], $c->rotate(2)->toArray());
        $this->assertSame(['c', 'a' => 1, 'b' => 2], $c->rotate(1)->toArray());
        $this->assertSame(['a' => 1, 'b' => 2, 'c'], $c->rotate(3)->toArray());
        $this->assertSame(['a' => 1, 'b' => 2, 'c'], $c->rotate(0)->toArray());
    }


    public function testSelectSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c']);
        $isNumeric = function ($k) {
            if (is_numeric($k)) {
                return true;
            }
            return false;
        };
        $isString = function ($k) {
            if (is_string($k)) {
                return true;
            }
            return false;
        };
        /** @noinspection PhpUnusedParameterInspection */
        $isNumericV = function ($k, $v) {
            if (is_numeric($v)) {
                return true;
            }
            return false;
        };
        /** @noinspection PhpUnusedParameterInspection */
        $isStringV = function ($k, $v) {
            if (is_string($v)) {
                return true;
            }
            return false;
        };

        $this->assertSame(['c'], $c->select($isNumeric)->toArray());
        $this->assertSame(['a' => 1, 'b' => 2], $c->select($isString)->toArray());
        $this->assertSame(['a' => 1], $c->select($isString, 1)->toArray());
        $this->assertSame(['a' => 1, 'b' => 2], $c->select($isNumericV)->toArray());
        $this->assertSame(['a' => 1], $c->select($isNumericV, 1)->toArray());
        $this->assertSame(['c'], $c->select($isStringV)->toArray());
    }


    public function testShuffleSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c', 3, 4]);

        $c->randomSeed(1234567890);
        $this->assertSame([2 => 4, 'b' => 2, 0 => 'c', 'a' => 1, 1 => 3], $c->shuffle()->toArray());

        $c->randomSeed(123456789);
        $this->assertSame(['b' => 2, 1 => 3, 'a' => 1, 0 => 'c', 2 => 4], $c->shuffle()->toArray());
    }


    public function testSliceSuccess()
    {
        $c = new Collection(['a' => 1, 'b' => 2, 'c', 3, 4]);

        $this->assertSame(['b' => 2, 'c', 3, 4], $c->slice()->toArray());
        $this->assertSame(['b' => 2, 'c', 3, 4], $c->slice(1)->toArray());
        $this->assertSame(['b' => 2, 'c', 3], $c->slice(1, 3)->toArray());
        $this->assertSame(['c', 3], $c->slice(2, 2)->toArray());
        $this->assertSame([1 => 3, 2 => 4], $c->slice(3, 6)->toArray());
    }


    public function testSortSuccess()
    {
        $c = new Collection(['a' => 1, 1 => 'c', 2 => 3, 'b' => 2, 0 => 4]);

        $this->assertSame([0 => 4, 1 => 'c', 2 => 3, 'a' => 1, 'b' => 2], $c->sortByKeys()->toArray());
        $this->assertSame(['b' => 2, 'a' => 1, 2 => 3, 1 => 'c', 0 => 4], $c->sortByKeys(true)->toArray());
        $this->assertSame([1 => 'c', 'a' => 1, 'b' => 2, 2 => 3, 0 => 4], $c->sortByValues()->toArray());
        $this->assertSame([0 => 4, 2 => 3, 'b' => 2, 'a' => 1, 1 => 'c'], $c->sortByValues(true)->toArray());
    }


    public function testSpliceSuccess()
    {
        $c1 = new Collection(['a' => 1, 1 => 'c', 2 => 3, 'b' => 2, 0 => 4]);
        $c2 = new Collection(['d' => 5, 4 => 'f', 6 => 8]);
        $c3 = new Collection([1 => 'c', 2 => 3]);

        $this->assertSame(
            ['a' => 1, 1 => 'c', 'd' => 5, 4 => 'f', 6 => 8, 0 => 4],
            $c1->splice(2, 2, $c2)->toArray()
        );

        $this->assertSame(
            [1 => 'c', 2 => 3, 'b' => 2, 0 => 4],
            $c1->splice(0, 2, $c3)->toArray()
        );
    }


    public function testUniquesSuccess()
    {
        $c = new Collection(['a' => 1, 1 => 'c', 2 => 1, 'b' => 2, 0 => 'c']);

        $this->assertSame(['a' => 1, 1 => 'c', 'b' => 2], $c->uniques()->toArray());
    }


    public function testValuesSuccess()
    {
        $c = new Collection(["a" => "green", "b" => "brown", "c" => "blue", "green"]);

        $this->assertSame('green', $c->value());
        $this->assertSame(['green', 'brown', 'blue', 'green'], $c->values()->toArray());
    }
}

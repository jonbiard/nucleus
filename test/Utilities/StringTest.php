<?php
namespace Nucleus\Library\Test\Geometry;

use Nucleus\Library\Collection\Collection;
use Nucleus\Library\Utilities\StringUtilities;
use PHPUnit_Framework_TestCase;

class StringTest extends PHPUnit_Framework_TestCase
{
    /** @var StringUtilities */
    private $su;


    public function setUp()
    {
        $this->su = new StringUtilities('utf-8');
    }


    public function testBestMatchSuccess()
    {
        $str1  = 'carrrot';
        $str2  = 'çàrrròt';
        $str3  = 'àbc';
        $words = ['apple', 'pineapple', 'banana', 'orange', 'radish', 'carrot', 'pea', 'bean', 'potato'];

        $this->assertSame('carrot', $this->su->bestMatch($str1, $words, 0.25));
        $this->assertSame('carrot', $this->su->bestMatch($str2, $words, 0.75));
        $this->assertSame('pea', $this->su->bestMatch($str3, $words, 1));
        $this->assertFalse($this->su->bestMatch($str2, $words, 0.5));
    }


    public function testByteSizeSuccess()
    {
        $this->assertSame(1, $this->su->byteSize('a'));
        $this->assertSame(2, $this->su->byteSize('à'));
        $this->assertSame(2, $this->su->byteSize('¢'));
        $this->assertSame(3, $this->su->byteSize('€'));
        $this->assertSame(4, $this->su->byteSize('𤭢'));
    }


    public function testCharAtSuccess()
    {
        $str = 'aà¢€𤭢';

        $this->assertSame('a', $this->su->charAt($str, 0));
        $this->assertSame('à', $this->su->charAt($str, 1));
        $this->assertSame('¢', $this->su->charAt($str, 2));
        $this->assertSame('€', $this->su->charAt($str, 3));
        $this->assertSame('𤭢', $this->su->charAt($str, 4));
    }


    public function testChunkSuccess()
    {
        $str = 'aà¢€𤭢';

        $this->assertSame(['a', 'à', '¢', '€', '𤭢'], $this->su->chunk($str, 1));
        $this->assertSame(['aà', '¢€', '𤭢'], $this->su->chunk($str, 2));
        $this->assertSame(['aà¢', '€𤭢'], $this->su->chunk($str, 3));
    }


    public function testCollateSuccess()
    {
        $str1 = 'aà¢€𤭢';
        $str2 = 'AÀ¢€𤭢';
        $str3 = '𤭢€¢ÀA';

        $this->assertSame(0, $this->su->collate($str1, $str1, 'en_US', false));
        $this->assertSame(1, $this->su->collate($str1, $str2, 'en_US', false));
        $this->assertSame(-1, $this->su->collate($str1, $str3, 'en_US', false));
        $this->assertSame(0, $this->su->collate($str1, $str1, 'en_US', true));
        $this->assertSame(0, $this->su->collate($str1, $str2, 'en_US', true));
        $this->assertSame(-1, $this->su->collate($str1, $str3, 'en_US', true));
    }


    public function testCompareSuccess()
    {
        $str1 = 'aà¢€𤭢';
        $str2 = 'AÀ¢€𤭢';
        $str3 = '𤭢€¢ÀA';

        $this->assertSame(0, $this->su->compare($str1, $str1, false));
        $this->assertSame(1, $this->su->compare($str1, $str2, false));
        $this->assertSame(-1, $this->su->compare($str1, $str3, false));
        $this->assertSame(0, $this->su->compare($str1, $str1, true));
        $this->assertSame(0, $this->su->compare($str1, $str2, true));
        $this->assertSame(-1, $this->su->compare($str1, $str3, true));
    }


    public function testCompressSuccess()
    {
        $str1 = 'a    à  ¢  € 𤭢';
        $str2 = 'A----À--¢--€-𤭢';

        $this->assertSame('a à ¢ € 𤭢', $this->su->compress($str1, ' '));
        $this->assertSame('A-À-¢-€-𤭢', $this->su->compress($str2, '-'));
    }


    public function testContainsSuccess()
    {
        $str1 = 'aà¢€𤭢';
        $str2 = 'À¢';

        $this->assertSame(false, $this->su->contains($str1, $str2, false));
        $this->assertSame(true, $this->su->contains($str1, $str2, true));
        $this->assertSame(false, $this->su->contains($str1, 'z', true));
    }


    public function testConvertCharsetSuccess()
    {
        $str1 = 'aà¢€𤭢';

        $this->assertSame('a????', $this->su->convertCharset($str1, 'ASCII', '?'));
        $this->assertSame('azzzz', $this->su->convertCharset($str1, 'ASCII', 'z'));
        $this->assertSame('azzzz', $this->su->convertCharset($str1, 'ASCII', 'zzz'));
        $this->assertSame('a', $this->su->convertCharset($str1, 'ASCII', 'à'));
    }


    public function testCountSuccess()
    {
        $str1 = 'aà¢€À¢€À¢€';

        $this->assertSame(1, $this->su->count($str1, 'aà¢€', true));
        $this->assertSame(3, $this->su->count($str1, 'à¢€', true));
        $this->assertSame(1, $this->su->count($str1, 'à¢€', false));
    }


    public function testCountWordsSuccess()
    {
        $str1 = 'Thîs ìs à têst-strîng';

        $this->assertSame(5, $this->su->countWords($str1));
        $this->assertSame(4, $this->su->countWords($str1, '-'));
    }


    public function testDensityOfSuccess()
    {
        $str1 = 'Têst str';
        $str2 = 'TÊST';

        $this->assertSame(0.5, $this->su->densityOf($str1, $str2, true));
        $this->assertSame(0.0, $this->su->densityOf($str1, $str2, false));
    }


    public function testDetectCharsetSuccess()
    {
        $str1 = 'Têst str';
        $str2 = 'Test str';
        $str3 = 'Tést str';

        $this->assertSame('UTF-8', $this->su->detectCharset($str1));
        $this->assertSame('ASCII', $this->su->detectCharset($str2));
        $this->assertSame('ISO-8859-1', $this->su->detectCharset($str3, 'ASCII,ISO-8859-1,UTF-8'));
        $this->assertSame('UTF-8', $this->su->detectCharset($str3, 'ASCII,UTF-8,ISO-8859-1'));
    }


    public function testEncloseSuccess()
    {
        $str1 = 'strîng';

        $this->assertSame('TêststrîngTêst', $this->su->enclose($str1, 'Têst'));
        $this->assertSame('"strîng"', $this->su->enclose($str1, '"'));
    }


    public function testEndsWithSuccess()
    {
        $str1 = 'strîng';

        $this->assertSame(true, $this->su->endsWith($str1, 'îng', false));
        $this->assertSame(true, $this->su->endsWith($str1, 'ÎNG', true));
        $this->assertSame(false, $this->su->endsWith($str1, 'str', true));
    }


    public function testEqualsSuccess()
    {
        $str1 = 'strîng';
        $str2 = 'string';
        $str3 = 'STRÎNG';

        $this->assertSame(false, $this->su->equals($str1, $str2, false));
        $this->assertSame(false, $this->su->equals($str1, $str2, true));
        $this->assertSame(false, $this->su->equals($str1, $str3, false));
        $this->assertSame(true, $this->su->equals($str1, $str3, true));
    }


    public function testFitsCharsetSuccess()
    {
        $str1 = 'Têst str';

        $this->assertSame(true, $this->su->fitsCharset($str1, 'UTF-8'));
        $this->assertSame(false, $this->su->fitsCharset($str1, 'ASCII'));
        $this->assertSame(true, $this->su->fitsCharset($str1, 'ISO-8859-1'));
    }


    public function testFormatSuccess()
    {
        $str1 = 'Têst %d %s';

        $this->assertSame('Têst 1 strîng', $this->su->format($str1, 1, 'strîng'));
    }


    public function testHashSuccess()
    {
        $str1 = 'Têst strîng';

        $this->assertSame('3c2372eb843d81808ad7313138a25869', $this->su->hash($str1, 'md5'));
        $this->assertSame('3bf62ecde0dd229203ee82d9115f60cdfa045898', $this->su->hash($str1, 'sha1'));
    }


    public function testIndexOfSuccess()
    {
        $str1 = 'Têst strîng';

        $this->assertSame(2, $this->su->indexOf($str1, 'st'));
        $this->assertSame(1, $this->su->indexOf($str1, 'êst'));
        $this->assertSame(false, $this->su->indexOf($str1, 'à'));

        $this->assertSame(5, $this->su->indexOfLast($str1, 'st'));
        $this->assertSame(1, $this->su->indexOfLast($str1, 'êst'));
        $this->assertSame(false, $this->su->indexOfLast($str1, 'à'));

        $this->assertSame([2, 5], $this->su->indicesOf($str1, 'st'));
        $this->assertSame([1], $this->su->indicesOf($str1, 'êst'));
        $this->assertSame(false, $this->su->indicesOf($str1, 'à'));
    }


    public function testLengthSuccess()
    {
        $str1 = 'aà¢€𤭢';
        $str2 = 'Têst strîng';

        $this->assertSame(5, $this->su->length($str1));
        $this->assertSame(11, $this->su->length($str2));
    }


    public function testLevenshteinSuccess()
    {
        $str1 = 'Têst str';
        $str2 = 'Test';

        $this->assertSame(5, $this->su->levenshtein($str1, $str2, $pct));
        $this->assertSame(0.625, $pct);
    }


    public function testPadSuccess()
    {
        $str1 = 'Têst strîng';
        $str2 = 'à';

        $this->assertSame('Têst strîng', $this->su->pad($str1, 1, $str2));
        $this->assertSame('ààTêst strîngàà', $this->su->pad($str1, 15, $str2));
        $this->assertSame('ààTêst strîngààà', $this->su->pad($str1, 16, $str2));

        $this->assertSame('Têst strîng', $this->su->padLeft($str1, 1, $str2));
        $this->assertSame('ààTêst strîng', $this->su->padLeft($str1, 13, $str2));

        $this->assertSame('Têst strîng', $this->su->padRight($str1, 1, $str2));
        $this->assertSame('Têst strîngàà', $this->su->padRight($str1, 13, $str2));
    }


    public function testPassword()
    {
        $this->su->randomSeed(1234567890);
        $hash    = $this->su->passwordCreateHash('123password123');
        $isValid = $this->su->passwordHashIsValid('123password123', $hash);

        $this->assertSame('4aa21d714156d34799a623c4bffe12f799f62b7464a128227919d212e39bcf67', $hash);
        $this->assertSame(true, $isValid);

        $this->su->randomSeed(123456789);
        $hash    = $this->su->passwordCreateHash('123password123', 'md5');
        $isValid = $this->su->passwordHashIsValid('123password123', $hash, 'md5');

        $this->assertSame('2a9eaca96c37378e7708faef889b498b', $hash);
        $this->assertSame(true, $isValid);
    }


    public function testRandomSuccess()
    {
        $this->su->randomSeed(1234567890);
        $this->assertSame('rrcêêtscàî', $this->su->random(10, 'Têst chàrlîst'));
        $this->su->randomSeed(12345678);
        $this->assertSame('lîc rs ctr', $this->su->random(10, 'Têst chàrlîst'));
    }


    public function testRepeatSuccess()
    {
        $str1 = 'Têst strîng';

        $this->assertSame('Têst strîng', $this->su->repeat($str1, 0));
        $this->assertSame('Têst strîngTêst strîng', $this->su->repeat($str1, 1));
        $this->assertSame('Têst strîngTêst strîngTêst strîng', $this->su->repeat($str1, 2));
    }


    public function testReplaceSuccess()
    {
        $str1 = 'Lórem ipsum ipsum dolor sít æmeť, ád dígňissim appellantur æmeť meí';

        $this->assertSame(
            'Lórem ipsum ipsum dolor sít æmeť, ád æmeť appellantur æmeť meí',
            $this->su->replace($str1, 'dígňissim', 'æmeť', false, null)
        );

        $this->assertSame(
            'Lórem ìpsùm ipsum dolor sít æmeť, ád dígňissim appellantur æmeť meí',
            $this->su->replace($str1, 'ipsum', 'ìpsùm', false, 1)
        );

        $this->assertSame(
            'Lórem ipsum ipsum dolor sít æmeť, ád dígňissim appellantur æmeť meí',
            $this->su->replace($str1, 'ipsum', 'ìpsùm', false, 0)
        );

        $this->assertSame(
            'Lórem ìpsùm ìpsùm dolor sít æmeť, ád dígňissim appellantur æmeť meí',
            $this->su->replace($str1, 'ipsum', 'ìpsùm', false, 3)
        );

        $this->assertSame(
            'Lórem ipsum ipsum dolor sít æmeť, ád DÍGŇIS appellantur æmeť meí',
            $this->su->replace($str1, 'DÍGŇISSIM', 'DÍGŇIS', true, 2)
        );
    }


    public function testReverseSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame('ťemæ tís muspi meróL', $this->su->reverse($str1));
    }


    public function testRot13Success()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame('Yóerz vcfhz fíg æzrť', $this->su->rot13($str1));
    }


    public function testSanitizeSuccess()
    {
        $str1 = 'Lórem ipsum~sít æmeť';

        $this->assertSame('Lrem ipsumst me', $this->su->sanitize($str1, 'a-zA-Z0-9 '));
        $this->assertSame('Lrem ipsum~st me', $this->su->sanitize($str1, 'a-zA-Z0-9~ '));
    }


    public function testScanSuccess()
    {
        $str1 = 'Lórem 2 sít 1:æmeť';

        $this->assertSame(['Lórem', '2', 1, 'æmeť'], $this->su->scan($str1, '%s %s sít %d:%s'));
    }


    public function testShuffleSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->su->randomSeed(1234567890);
        $this->assertSame('  ťspmms uóeeLimæírt', $this->su->shuffle($str1));

        $this->su->randomSeed(123456789);
        $this->assertSame('íemt eťósmupL i mrsæ', $this->su->shuffle($str1));
    }


    public function testSliceSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame('Lórem ip', $this->su->slice($str1, 0, 8));
        $this->assertSame('sum sít æmeť', $this->su->slice($str1, 8));

        $this->assertSame('sít æmeť', $this->su->sliceAfter($str1, 'ipsum '));
        $this->assertSame('', $this->su->sliceAfter($str1, 'æmeť'));

        $this->assertSame('Lórem', $this->su->sliceBefore($str1, ' ipsum'));
        $this->assertSame('', $this->su->sliceBefore($str1, 'Lórem'));

        $this->assertSame('ipsum sít æmeť', $this->su->sliceFrom($str1, 'ipsum '));
        $this->assertSame('æmeť', $this->su->sliceFrom($str1, 'æmeť'));
        $this->assertSame('rem ipsum sít æmeť', $this->su->sliceFrom($str1, 'rem'));
    }


    public function testSpliceSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame('Lórem ìpsùm sít æmeť', $this->su->splice($str1, 6, 5, 'ìpsùm'));
        $this->assertSame('Lórem ìpsùm', $this->su->splice($str1, 6, null, 'ìpsùm'));
    }


    public function testSplitSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame(['Lórem', 'ipsum', 'sít', 'æmeť'], $this->su->split($str1, ' ', null));
        $this->assertSame(['Lórem', 'ipsum', 'sít æmeť'], $this->su->split($str1, ' ', 3));
        $this->assertSame(['Lórem', 'ipsum sít æmeť'], $this->su->split($str1, ' ', 2));
        $this->assertSame(['Lórem ipsum sít æmeť'], $this->su->split($str1, ' ', 1));
        $this->assertSame(['Lórem ipsum sít æmeť'], $this->su->split($str1, ' ', 0));
    }


    public function testStartsWithSuccess()
    {
        $str1 = 'Lórem ipsum sít æmeť';

        $this->assertSame(true, $this->su->startsWith($str1, 'Lór', false));
        $this->assertSame(true, $this->su->startsWith($str1, 'LÓR', true));
        $this->assertSame(false, $this->su->startsWith($str1, 'ipsum', true));
    }


    public function testStripInvisibleSuccess()
    {
        $str1 = "Ló\tre\nm ip\r\nsum sít æm\x0Beť"; //\t\n\r\0\x0B
        $str2 = 'Lórem ipsum sít æmeť';

        $this->assertSame('Lórem ipsum sít æmeť', $this->su->stripInvisible($str2));
        $this->assertSame('Ló re m ip  sum sít æm eť', $this->su->stripInvisible($str1));
    }


    public function testToASCIISuccess()
    {
        $str1 = 'LóREm ÌpSÙm';

        $this->assertSame('LoREm IpSUm', $this->su->toASCII($str1));
    }


    public function testToArraySuccess()
    {
        $str1 = 'Lórem ipsum';

        $this->assertSame(['L', 'ó', 'r', 'e', 'm', ' ', 'i', 'p', 's', 'u', 'm'], $this->su->toArray($str1));
    }


    public function testToCaseSuccess()
    {
        $str1 = 'LóREm ÌpSÙm';

        $this->assertSame('lórem ìpsùm', $this->su->toLowerCase($str1));
        $this->assertSame('LÓREM ÌPSÙM', $this->su->toUpperCase($str1));
        $this->assertSame('Lórem Ìpsùm', $this->su->toTitleCase($str1));
        $this->assertSame('lorem-ipsum', $this->su->toSlug($str1, '-'));
        $this->assertSame('lorem_ipsum', $this->su->toSlug("!$/ $str1 %?&", '_'));
    }


    public function testTransformsSuccess()
    {
        $str1 = 'Têst strîng';

        $this->assertSame('TSTSTRNK', $this->su->metaphone($str1));
        $this->assertSame('TSTST', $this->su->metaphone($str1, 5));
        $this->assertSame('T232', $this->su->soundex($str1));
    }


    public function testTrimSuccess()
    {
        $str1 = " - \n\tLóREm\tÌpSÙm\t\n ~ ";

        $this->assertSame("LóREm\tÌpSÙm", $this->su->trim($str1, " -~\n\t"));
        $this->assertSame("LóREm\tÌpSÙm\t\n ~ ", $this->su->trimLeft($str1, " -~\n\t"));
        $this->assertSame(" - \n\tLóREm\tÌpSÙm", $this->su->trimRight($str1, " -~\n\t"));
    }


    public function testTruncateSuccess()
    {
        $str1 = 'LóREm ÌpSÙm';

        $this->assertSame("LóREm", $this->su->truncate($str1, 5));
        $this->assertSame("LóREm ...", $this->su->truncate($str1, 9, '...'));
    }


    public function testWordsSuccess()
    {
        $str1 = 'Lorém ìpsum-dòlor sít amet, ludus quodsi àn~ius';

        $this->assertSame(
            ['Lorém', 'ìpsum-dòlor', 'sít', 'amet', 'ludus', 'quodsi', 'àn~ius'],
            $this->su->words($str1, '-~')
        );
    }


    public function testWrapSuccess()
    {
        $str1 = 'Lòrem ipsúm dòlor sít amèt, duó díco omnìum persecutí no, víx justó audíam at, '
            . 'accùsam òmittantur còmprèhensam éam ei. Id facéte pòssim usú, per èt vulputate referrentur. '
            . 'Errem pùtént tractatos ea est. Sìt dìcat tollit at, ut wisi verí ludús eám, an ponderùm laboràmus '
            . 'demócritum mea. Eú iús latínè vùlputatè.'
            . 'Èi àdipíscing elaboraret pri, quí debét òcurreret né, eàm ea dìcó ùtamur. Eì alterùm ocurreret '
            . 'his, sit at case vìvéndo assùevérìt, eu sìt tántas dicèret senserít. An dòmíng élaboraret percipítur '
            . 'vel. Vix dìctas ìuvàret ex, purtò nátum alíènum qúi ùt, ei vivendum éfficiantùr concludàtúrque nec. '
            . 'Eum alìí dòlor reformidáns nè. Vis éx éxpètèndis dìssentìunt. Duo mélius erròribus constitúam ut, cu '
            . 'mazím populo eum.'
            . 'Vìm laudem assentior ìd, vitae fuísset perpetua cù eos. An úsu áutem dícàt medíòcrem. Éi facìlis '
            . 'facìlisi ìnteresset pro. Te verì malis prompta eam, ut élit omniúm méi. Àccusatà scriptòrem '
            . 'vituperàtoribus pri ex. Ea dòlorem molestiae qui, vix nó tempór déserunt. Offícìis democritum '
            . 'vituperatorìbus eà nám. Méa in níbh habeo tacimates, ín vis mundi facilìs opórtère. Cù jústo dicant '
            . 'èst, at cetero audìam nónùmes úsu. Vìs éx wísi ìmpetús inermis, ad víris moléstiaè dùo. Mel cù '
            . 'vulputaté reformidans signìferúmqùe, quì éx decore assueverit, ius vidít sémper impèdit tè. Ea jústo '
            . 'graecì lobòrtis èám, íllud aèterno pro àt, eu eos quod maiorúm vìvendúm. Èúm cu vèrèar débitis '
            . 'facilisì. Ullum salútatus prò eu, admódum adipiscing dèterruisset ín mea.';

        $this->assertSame(
            "Lòrem ipsúm dòlor sít amèt, duó díco omnìum persecutí no, víx justó audíam\n"
            . "at, accùsam òmittantur còmprèhensam éam ei. Id facéte pòssim usú, per èt\n"
            . "vulputate referrentur. Errem pùtént tractatos ea est. Sìt dìcat tollit at,\n"
            . "ut wisi verí ludús eám, an ponderùm laboràmus demócritum mea. Eú iús latínè\n"
            . "vùlputatè.Èi àdipíscing elaboraret pri, quí debét òcurreret né, eàm ea dìcó\n"
            . "ùtamur. Eì alterùm ocurreret his, sit at case vìvéndo assùevérìt, eu sìt\n"
            . "tántas dicèret senserít. An dòmíng élaboraret percipítur vel. Vix dìctas\n"
            . "ìuvàret ex, purtò nátum alíènum qúi ùt, ei vivendum éfficiantùr\n"
            . "concludàtúrque nec. Eum alìí dòlor reformidáns nè. Vis éx éxpètèndis\n"
            . "dìssentìunt. Duo mélius erròribus constitúam ut, cu mazím populo eum.Vìm\n"
            . "laudem assentior ìd, vitae fuísset perpetua cù eos. An úsu áutem dícàt\n"
            . "medíòcrem. Éi facìlis facìlisi ìnteresset pro. Te verì malis prompta eam,\n"
            . "ut élit omniúm méi. Àccusatà scriptòrem vituperàtoribus pri ex. Ea dòlorem\n"
            . "molestiae qui, vix nó tempór déserunt. Offícìis democritum vituperatorìbus\n"
            . "eà nám. Méa in níbh habeo tacimates, ín vis mundi facilìs opórtère. Cù\n"
            . "jústo dicant èst, at cetero audìam nónùmes úsu. Vìs éx wísi ìmpetús\n"
            . "inermis, ad víris moléstiaè dùo. Mel cù vulputaté reformidans\n"
            . "signìferúmqùe, quì éx decore assueverit, ius vidít sémper impèdit tè. Ea\n"
            . "jústo graecì lobòrtis èám, íllud aèterno pro àt, eu eos quod maiorúm\n"
            . "vìvendúm. Èúm cu vèrèar débitis facilisì. Ullum salútatus prò eu, admódum\n"
            . "adipiscing dèterruisset ín mea.",
            $this->su->wrap($str1, 75, "\n", false)
        );

        $this->assertSame(
            "Lòrem ipsúm dòlor sít amèt, duó díco\n"
            . "omnìum persecutí no, víx justó audíam\n"
            . "at, accùsam òmittantur còmprèhensam éam\n"
            . "ei. Id facéte pòssim usú, per èt\n"
            . "vulputate referrentur. Errem pùtént\n"
            . "tractatos ea est. Sìt dìcat tollit at,\n"
            . "ut wisi verí ludús eám, an ponderùm\n"
            . "laboràmus demócritum mea. Eú iús latínè\n"
            . "vùlputatè.Èi àdipíscing elaboraret pri,\n"
            . "quí debét òcurreret né, eàm ea dìcó\n"
            . "ùtamur. Eì alterùm ocurreret his, sit at\n"
            . "case vìvéndo assùevérìt, eu sìt tántas\n"
            . "dicèret senserít. An dòmíng élaboraret\n"
            . "percipítur vel. Vix dìctas ìuvàret ex,\n"
            . "purtò nátum alíènum qúi ùt, ei vivendum\n"
            . "éfficiantùr concludàtúrque nec. Eum alìí\n"
            . "dòlor reformidáns nè. Vis éx éxpètèndis\n"
            . "dìssentìunt. Duo mélius erròribus\n"
            . "constitúam ut, cu mazím populo eum.Vìm\n"
            . "laudem assentior ìd, vitae fuísset\n"
            . "perpetua cù eos. An úsu áutem dícàt\n"
            . "medíòcrem. Éi facìlis facìlisi\n"
            . "ìnteresset pro. Te verì malis prompta\n"
            . "eam, ut élit omniúm méi. Àccusatà\n"
            . "scriptòrem vituperàtoribus pri ex. Ea\n"
            . "dòlorem molestiae qui, vix nó tempór\n"
            . "déserunt. Offícìis democritum\n"
            . "vituperatorìbus eà nám. Méa in níbh\n"
            . "habeo tacimates, ín vis mundi facilìs\n"
            . "opórtère. Cù jústo dicant èst, at cetero\n"
            . "audìam nónùmes úsu. Vìs éx wísi ìmpetús\n"
            . "inermis, ad víris moléstiaè dùo. Mel cù\n"
            . "vulputaté reformidans signìferúmqùe, quì\n"
            . "éx decore assueverit, ius vidít sémper\n"
            . "impèdit tè. Ea jústo graecì lobòrtis\n"
            . "èám, íllud aèterno pro àt, eu eos quod\n"
            . "maiorúm vìvendúm. Èúm cu vèrèar débitis\n"
            . "facilisì. Ullum salútatus prò eu,\n"
            . "admódum adipiscing dèterruisset ín mea.",
            $this->su->wrap($str1, 40, "\n", false)
        );
    }
}

<?php
namespace Nucleus\Library\Test\Imagick;

use Nucleus\Library\ImageEditor\Imagick;
use PHPUnit_Framework_TestCase;

class ImagickTest extends PHPUnit_Framework_TestCase
{
    private $deleteTestImages = true;
    private $filePaths = [];


    public function assertResizeSuccess($filePath, $options)
    {
        $resultFileName = pathinfo($filePath)['filename'] . '_to_' . implode('_', $options);
        $resultFilePath = __DIR__ . "/TestImages/result/$resultFileName.jpg";

        $image = new Imagick($filePath);

        $image->autoResizeImage(
            $options['maxWidth'],
            $options['maxHeight'],
            $options['minWidth'],
            $options['minHeight'],
            $image::X_ALIGN_CENTER,
            $image::Y_ALIGN_MIDDLE,
            '#000000'
        );

        $image->save($resultFilePath);

        $newImage = new Imagick($resultFilePath);

        $newWidth  = $newImage->getImageWidth();
        $newHeight = $newImage->getImageHeight();

        $this->assertTrue(
            $newWidth >= $options['minWidth'] && $newWidth <= $options['maxWidth'],
            "$resultFileName: Width $newWidth is not between $options[minWidth] and $options[maxWidth]"
        );
        $this->assertTrue(
            $newHeight >= $options['minHeight'] && $newHeight <= $options['maxHeight'],
            "$resultFileName: Height $newHeight is not between $options[minHeight] and $options[maxHeight]"
        );

        if ($this->deleteTestImages) {
            @unlink($resultFilePath);
        }
    }


    public function setUp()
    {
        $files = scandir(__DIR__ . '/TestImages/original');

        foreach ($files as $file) {
            if ($file !== '.' && $file !== '..') {
                $this->filePaths[] = __DIR__ . "/TestImages/original/$file";
            }
        }
    }


    public function testFilePathsOneByOneForFixedSize()
    {
        $options = [
            'minWidth'  => 128,
            'maxWidth'  => 128,
            'minHeight' => 128,
            'maxHeight' => 128,
        ];

        foreach ($this->filePaths as $filePath) {
            $this->assertResizeSuccess($filePath, $options);
        }
    }


    public function testFilePathsOneByOneForFluidWidth()
    {
        $options = [
            'minWidth'  => 128,
            'maxWidth'  => 256,
            'minHeight' => 128,
            'maxHeight' => 128,
        ];

        foreach ($this->filePaths as $filePath) {
            $this->assertResizeSuccess($filePath, $options);
        }
    }


    public function testFilePathsOneByOneForFluidHeight()
    {
        $options = [
            'minWidth'  => 128,
            'maxWidth'  => 128,
            'minHeight' => 128,
            'maxHeight' => 256,
        ];

        foreach ($this->filePaths as $filePath) {
            $this->assertResizeSuccess($filePath, $options);
        }
    }


    public function testFilePathsOneByOneForFluidBoth()
    {
        $options = [
            'minWidth'  => 128,
            'maxWidth'  => 256,
            'minHeight' => 128,
            'maxHeight' => 256,
        ];

        foreach ($this->filePaths as $filePath) {
            $this->assertResizeSuccess($filePath, $options);
        }
    }
}

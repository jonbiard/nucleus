<?php
namespace Nucleus\Library\Test\Validator;

use Nucleus\Library\Validator\Filter;
use PHPUnit_Framework_TestCase;

class FilterTest extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }


    public function testAll()
    {
        $filter = new Filter();

        $this->assertEquals(null, $filter->fixAgeBetween('01-01-1970', 'd-m-Y', 1, 99));
        $this->assertEquals(null, $filter->fixAgeMax('01-01-1970', 'd-m-Y', 99));
        $this->assertEquals(null, $filter->fixAgeMin('01-01-1970', 'd-m-Y', 1));
        $this->assertEquals('abc123', $filter->fixAlnum('abc-!"/123'));
        $this->assertEquals('abcdef', $filter->fixAlpha('abc123-.!"/$def'));
        $this->assertEquals([1], $filter->fixArray(1));
        $this->assertEquals(null, $filter->fixBase64(base64_encode('test')));
        $this->assertEquals(3, $filter->fixBetween(2, 3, 4));
        $this->assertEquals(3, $filter->fixBetween(5, 1, 3));
        $this->assertEquals(null, $filter->fixBlank(''));
        $this->assertEquals(null, $filter->fixBlank('     '));
        $this->assertEquals(null, $filter->fixBlank(null));
        $this->assertEquals(true, $filter->fixBool(true));
        $this->assertEquals(true, $filter->fixBool('1'));
        $this->assertEquals(true, $filter->fixBool('on'));
        $this->assertEquals(true, $filter->fixBool('true'));
        $this->assertEquals(true, $filter->fixBool('t'));
        $this->assertEquals(true, $filter->fixBool('yes'));
        $this->assertEquals(true, $filter->fixBool('y'));
        $this->assertEquals(false, $filter->fixBool(false));
        $this->assertEquals(false, $filter->fixBool('0'));
        $this->assertEquals(false, $filter->fixBool('off'));
        $this->assertEquals(false, $filter->fixBool('false'));
        $this->assertEquals(false, $filter->fixBool('f'));
        $this->assertEquals(false, $filter->fixBool('no'));
        $this->assertEquals(false, $filter->fixBool('n'));
        $this->assertEquals(null, $filter->fixCreditCard('5514689104747025'));
        $this->assertEquals('03-03-1970', $filter->fixDatetime('31-02-1970', 'd-m-Y'));
        $this->assertEquals(null, $filter->fixDatetime('abcdef', 'd-m-Y'));
        $this->assertEquals(null, $filter->fixEmail('name@domain.com'));
        $this->assertEquals(null, $filter->fixEmail('name@domain.co.uk'));
        $this->assertEquals(1, $filter->fixEqualTo(1, 1));
        $this->assertEquals(1.0, $filter->fixEqualTo(1, 1.0));
        $this->assertEquals('1', $filter->fixEqualTo(1, '1'));
        $this->assertEquals(1.0, $filter->fixFloat(1.0));
        $this->assertEquals(M_PI, $filter->fixFloat(M_PI));
        $this->assertEquals(3.14159265359, $filter->fixFloat('3.14159265359'));
        $this->assertEquals(null, $filter->fixInKeys('a', ['a' => 1, 'b' => 2, 'c' => 3]));
        $this->assertEquals(1, $filter->fixInt(1));
        $this->assertEquals(null, $filter->fixInValues(2, ['a' => 1, 'b' => 2, 'c' => 3]));
        $this->assertEquals(null, $filter->fixIP('184.165.132.11'));
        $this->assertEquals(null, $filter->fixIP('2001:0db8:85a3:0042:1000:8a2e:0370:7334'));
        $this->assertEquals(null, $filter->fixIPv4('184.165.132.11'));
        $this->assertEquals(null, $filter->fixIPv6('2001:0db8:85a3:0042:1000:8a2e:0370:7334'));
        $this->assertEquals(null, $filter->fixLocale('en_US'));
        $this->assertEquals(null, $filter->fixLocale('fr_CA'));
        $this->assertEquals(1, $filter->fixMax(2, 1));
        $this->assertEquals(5, $filter->fixMin(2, 5));
        $this->assertEquals('abc---def', $filter->fixRegex('abc123def', '~[0-9]~', '-'));
        $this->assertEquals(1, $filter->fixStrictEqualTo(1, 1));
        $this->assertEquals('1', $filter->fixString(1));
        $this->assertEquals('12345     ', $filter->fixStrlenBetween('12345', 10, 15));
        $this->assertEquals('123', $filter->fixStrlenBetween('12345', 1, 3));
        $this->assertEquals('  123', $filter->fixStrlenBetween('123', 5, 6, STR_PAD_LEFT));
        $this->assertEquals('123', $filter->fixStrlenMax('12345', 3));
        $this->assertEquals('12345  ', $filter->fixStrlenMin('12345', 7));
        $this->assertEquals('  12345', $filter->fixStrlenMin('12345', 7, STR_PAD_LEFT));
        $this->assertEquals('123', $filter->fixStrlen('12345', 3));
        $this->assertEquals('12345 ', $filter->fixStrlen('12345', 6));
        $this->assertEquals(null, $filter->fixTimezone('America/Argentina/Buenos_Aires'));
        $this->assertEquals('test', $filter->fixTrim(' test '));
        $this->assertEquals('te   st', $filter->fixTrim(' te   st '));
        $this->assertEquals('te st', $filter->fixTrim('- te st -', ' -'));
        $this->assertEquals(null, $filter->fixURL('http://www.php.net/manual/en/timezones.america.php'));
        $this->assertEquals('test12', $filter->fixWord('test 1 2'));
    }
}

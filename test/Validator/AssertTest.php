<?php
namespace Nucleus\Library\Test\Validator;

use Nucleus\Library\Validator\Assert;
use PHPUnit_Framework_TestCase;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class AssertTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Assert
     */
    private $assert;


    public function setUp()
    {
        $this->assert = new Assert();
    }


    public function testIsAgeBetweenBadDateFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAgeBetween('31-02-1970', 'd-m-Y', 1, 10);
    }


    public function testIsAgeBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAgeBetween('01-01-1970', 'd-m-Y', 1, 10);
    }


    public function testIsAgeMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAgeMax('01-01-1970', 'd-m-Y', 1);
    }


    public function testIsAgeMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAgeMin('01-01-1970', 'd-m-Y', 99);
    }


    public function testIsAlnumFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAlnum('a.b.c-1/2/3');
    }


    public function testIsAlphaFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isAlpha('abc.def.123');
    }


    public function testIsArrayFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isArray(123);
    }


    public function testIsBase64Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isBase64('123');
    }


    public function testIsBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isBetween(2, 5, 10);
    }


    public function testIsBlankFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isBlank('notblank');
    }


    public function testIsBoolFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isBool('notbool');
    }


    public function testIsCreditCardFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isCreditCard('notcreditcard');
    }


    public function testIsDatetimeFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isDatetime('31-02-1970', 'd-m-Y');
    }


    public function testIsEmailFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isEmail('not.an.email.com');
    }


    public function testIsEqualToFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isEqualTo(1, 2);
    }


    public function testIsFloatFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isFloat([1, 2, 3]);
    }


    public function testIsIPFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isIP('notanip');
    }


    public function testIsIPv4Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isIPv4('notanip');
    }


    public function testIsIPv6Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isIPv6('notanip');
    }


    public function testIsInKeysFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isInKeys('d', ['a' => 1, 'b' => 2, 'c' => 3]);
    }


    public function testIsInValuesFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isInValues(4, ['a' => 1, 'b' => 2, 'c' => 3]);
    }


    public function testIsIntFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isInt('notanint');
    }


    public function testIsLocaleFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isLocale('notalocale');
    }


    public function testIsMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isMax(5, 1);
    }


    public function testIsMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isMin(5, 10);
    }


    public function testIsRegexFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isRegex('abc123def', '~^[a-zA-Z]*[0-9]*$~');
    }


    public function testIsStrictEqualToFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isStrictEqualTo(1, '1');
    }


    public function testIsStringFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isString(['not', 'a', 'string']);
    }


    public function testIsStrlenBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isStrlenBetween('123', 5, 10);
    }


    public function testIsStrlenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isStrlen('123', 5);
    }


    public function testIsStrlenMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isStrlenMax('123', 1);
    }


    public function testIsStrlenMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isStrlenMin('123', 5);
    }


    public function testIsTimezoneFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isTimezone('notatimezone');
    }


    public function testIsTrimFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isTrim(' nottrimmed ');
    }


    public function testIsURLFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isURL('notaurl');
    }


    public function testIsWordFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->isWord('not a word');
    }


    public function testNotAgeBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notAgeBetween('01-01-1970', 'd-m-Y', 1, 99);
    }


    public function testNotAgeMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notAgeMax('01-01-1970', 'd-m-Y', 99);
    }


    public function testNotAgeMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notAgeMin('01-01-1970', 'd-m-Y', 1);
    }


    public function testNotAlnumFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notAlnum('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
    }


    public function testNotAlphaFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notAlpha('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
    }


    public function testNotArrayFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notArray([1, 2, 3]);
    }


    public function testNotBase64Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notBase64(base64_encode('test'));
    }


    public function testNotBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notBetween(2, 2, 2);
    }


    public function testNotBlankFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notBlank('     ');
    }


    public function testNotBoolFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notBool(true);
    }


    public function testNotCreditCardFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notCreditCard('5514689104747025');
    }


    public function testNotDatetimeFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notDatetime('01-01-1970', 'd-m-Y');
    }


    public function testNotEmailFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notEmail('name@domain.com');
    }


    public function testNotEqualToFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notEqualTo(1, 1);
    }


    public function testNotFloat()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notFloat(1.0);
    }


    public function testNotIP()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notIP('184.165.132.11');
    }


    public function testNotIPv4Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notIPv4('184.165.132.11');
    }


    public function testNotIPv6Failure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notIPv6('2001:0db8:85a3:0042:1000:8a2e:0370:7334');
    }


    public function testNotInKeys()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notInKeys('a', ['a' => 1, 'b' => 2, 'c' => 3]);
    }


    public function testNotInValues()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notInValues(2, ['a' => 1, 'b' => 2, 'c' => 3]);
    }


    public function testNotInt()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notInt(1);
    }


    public function testNotLocaleFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notLocale('en_US');
    }


    public function testNotMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notMax(2, 2);
    }


    public function testNotMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notMin(2, 2);
    }


    public function testNotRegexFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notRegex('abc123def', '~^[^0-9]*123(.*)$~');
    }


    public function testNotStrictEqualToFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notStrictEqualTo(1, 1);
    }


    public function testNotStringFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notString('test');
    }


    public function testNotStrlenBetweenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notStrlenBetween('12345', 5, 5);
    }


    public function testNotStrlenFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notStrlen('12345', 5);
    }


    public function testNotStrlenMaxFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notStrlenMax('12345', 5);
    }


    public function testNotStrlenMinFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notStrlenMin('12345', 5);
    }


    public function testNotTimezoneFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notTimezone('America/Argentina/Buenos_Aires');
    }


    public function testNotTrimFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notTrim('te   st');
    }


    public function testNotURLFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notURL('http://ca1.php.net/manual-lookup.php?pattern=test&lang=en&scope=quickref');
    }


    public function testNotWordFailure()
    {
        $this->setExpectedException('\Nucleus\Library\Exception\AssertionException');
        $this->assert->notWord('test');
    }


    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function testSuccessAll()
    {
        $this->assert->isAgeBetween('01-01-1970', 'd-m-Y', 1, 99);
        $this->assert->isAgeMax('01-01-1970', 'd-m-Y', 99);
        $this->assert->isAgeMin('01-01-1970', 'd-m-Y', 1);
        $this->assert->isAlnum('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        $this->assert->isAlpha('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $this->assert->isArray([1, 2, 3]);
        $this->assert->isBase64(base64_encode('test'));
        $this->assert->isBetween(2, 1, 3);
        $this->assert->isBetween(2, 2, 2);
        $this->assert->isBlank('');
        $this->assert->isBlank('     ');
        $this->assert->isBlank(null);
        $this->assert->isBool(true);
        $this->assert->isBool('1');
        $this->assert->isBool('on');
        $this->assert->isBool('true');
        $this->assert->isBool('t');
        $this->assert->isBool('yes');
        $this->assert->isBool('y');
        $this->assert->isBool(false);
        $this->assert->isBool('0');
        $this->assert->isBool('off');
        $this->assert->isBool('false');
        $this->assert->isBool('f');
        $this->assert->isBool('no');
        $this->assert->isBool('n');
        $this->assert->isCreditCard('5514689104747025');
        $this->assert->isDatetime('01-01-1970', 'd-m-Y');
        $this->assert->isEmail('name@domain.com');
        $this->assert->isEmail('name@domain.co.uk');
        $this->assert->isEqualTo(1, 1);
        $this->assert->isEqualTo(1, 1.0);
        $this->assert->isEqualTo(1, '1');
        $this->assert->isFloat(1.0);
        $this->assert->isFloat(M_PI);
        $this->assert->isFloat('3.14159265359');
        $this->assert->isInKeys('a', ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assert->isInt(1);
        $this->assert->isInValues(2, ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assert->isIP('184.165.132.11');
        $this->assert->isIP('2001:0db8:85a3:0042:1000:8a2e:0370:7334');
        $this->assert->isIPv4('184.165.132.11');
        $this->assert->isIPv6('2001:0db8:85a3:0042:1000:8a2e:0370:7334');
        $this->assert->isLocale('en_US');
        $this->assert->isLocale('fr_CA');
        $this->assert->isMax(2, 3);
        $this->assert->isMax(2, 2);
        $this->assert->isMin(2, 1);
        $this->assert->isMin(2, 2);
        $this->assert->isRegex('abc123def', '~^[^0-9]*123(.*)$~');
        $this->assert->isStrictEqualTo(1, 1);
        $this->assert->isString('test');
        $this->assert->isStrlenBetween('12345', 4, 6);
        $this->assert->isStrlenBetween('12345', 5, 5);
        $this->assert->isStrlenMax('12345', 6);
        $this->assert->isStrlenMax('12345', 5);
        $this->assert->isStrlenMin('12345', 4);
        $this->assert->isStrlenMin('12345', 5);
        $this->assert->isStrlen('12345', 5);
        $this->assert->isTimezone('America/New_York');
        $this->assert->isTimezone('America/Argentina/Buenos_Aires');
        $this->assert->isTrim('test');
        $this->assert->isTrim('te   st');
        $this->assert->isURL('http://www.php.net/manual/en/timezones.america.php');
        $this->assert->isURL('http://ca1.php.net/manual-lookup.php?pattern=test&lang=en&scope=quickref');
        $this->assert->isWord('test');

        $this->assert->notAgeBetween('31-02-1970', 'd-m-Y', 1, 10);
        $this->assert->notAgeBetween('01-01-1970', 'd-m-Y', 1, 10);
        $this->assert->notAgeMax('01-01-1970', 'd-m-Y', 1);
        $this->assert->notAgeMin('01-01-1970', 'd-m-Y', 99);
        $this->assert->notAlnum('a.b.c-1/2/3');
        $this->assert->notAlpha('abc.def.123');
        $this->assert->notArray(123);
        $this->assert->notBase64('123');
        $this->assert->notBetween(2, 5, 10);
        $this->assert->notBlank('notblank');
        $this->assert->notBool('notbool');
        $this->assert->notCreditCard('notcreditcard');
        $this->assert->notDatetime('31-02-1970', 'd-m-Y');
        $this->assert->notEmail('not.an.email.com');
        $this->assert->notEqualTo(1, 2);
        $this->assert->notFloat([1, 2, 3]);
        $this->assert->notIP('notanip');
        $this->assert->notIPv4('notanip');
        $this->assert->notIPv6('notanip');
        $this->assert->notInKeys('d', ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assert->notInValues(4, ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assert->notInt('notanint');
        $this->assert->notLocale('notalocale');
        $this->assert->notMax(5, 1);
        $this->assert->notMin(5, 10);
        $this->assert->notRegex('abc123def', '~^[a-zA-Z]*[0-9]*$~');
        $this->assert->notStrictEqualTo(1, '1');
        $this->assert->notString(['not', 'a', 'string']);
        $this->assert->notStrlenBetween('123', 5, 10);
        $this->assert->notStrlen('123', 5);
        $this->assert->notStrlenMax('123', 1);
        $this->assert->notStrlenMin('123', 5);
        $this->assert->notTimezone('notatimezone');
        $this->assert->notTrim(' nottrimmed ');
        $this->assert->notURL('notaurl');
        $this->assert->notWord('not a word');
    }
}
